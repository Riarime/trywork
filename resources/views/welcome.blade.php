@extends('layouts.master')

@section('title', 'Home')

@section('css')

@endsection

@section('js')

@endsection

@section('navbar')

@endsection

@section('account')
    <li>
        <a href="{{ route('login') }}">
                @if(Auth::user())
                    <img src="{{ Avatar::create(\App\User::getName())->toBase64() }}" class="user-img" alt="" />
                    <span class="hidden-md hidden-sm hidden-xs">Welcome, {{ ucwords(strtolower(\App\User::getName())) }}</span>
                @else
                    <img src="{{ asset('assets/frontend/img/user/user-1.jpg') }}" class="user-img" alt="" />
                    <span class="hidden-md hidden-sm hidden-xs">Login</span>
                @endif
        </a>
    </li>
    <li>
        <a href="{{ route('faq') }}">
            <span class="hidden-md hidden-sm hidden-xs">FAQ</span>
        </a>
    </li>
@endsection

@section('content')
    <!-- BEGIN #slider -->
    <div id="slider" class="section-container p-0 bg-black-darker">
        <!-- BEGIN carousel -->
        <div id="main-carousel" class="carousel slide" data-ride="carousel">
            <!-- BEGIN carousel-inner -->
            <div class="carousel-inner">
                <!-- BEGIN item -->
                <div class="item active">
                    <img src="{{ asset('assets/frontend/img/slider/slider-1-cover.jpg') }}" class="bg-cover-img" alt="" />
                    <div class="container">
                        <img src="../assets/img/slider/slider-1-product.png" class="product-img right bottom fadeInRight animated" alt="" />
                    </div>
                    <div class="carousel-caption carousel-caption-left">
                        <div class="container">
                            <h3 class="title m-b-5 fadeInLeftBig animated"><b>Try</b>Work</h3>
                            <p class="m-b-15 fadeInLeftBig animated">Student Training. Gain Experience. Work Ready.</p>
                            {{--<div class="price m-b-30 fadeInLeftBig animated"><small>from</small> <span>$649.00</span></div>--}}
                            {{--<a href="#" class="btn btn-outline btn-lg fadeInLeftBig animated">Join Now</a>--}}
                        </div>
                    </div>
                </div>
                <!-- END item -->
            </div>
            <!-- END carousel-inner -->
        </div>
        <!-- END carousel -->
    </div>
    <!-- END #slider -->

    <!-- BEGIN #promotions -->
    <div id="promotions" class="section-container bg-white">
        <!-- BEGIN container -->
        <div class="container">
            <!-- BEGIN section-title -->
            <h4 class="section-title clearfix">
                <a href="{{route('Registration')}}" class="pull-right">REGISTRATION</a>
                Features
                <small>of TryWork Intern Recommendation System and Management Services</small>
            </h4>
            <!-- END section-title -->
            <!-- BEGIN #policy -->
            <div id="policy" class="section-container p-t-30 p-b-30">
                <!-- BEGIN container -->
                <div class="container">
                    <!-- BEGIN row -->
                    <div class="row">
                        <!-- BEGIN col-4 -->
                        <div class="col-md-4 col-sm-4">
                            <!-- BEGIN policy -->
                            <div class="policy">
                                <div class="policy-icon"><i class="fa fa-graduation-cap"></i></div>
                                <div class="policy-info">
                                    <h4>Intelligently recommends interns</h4>
                                    <p>Feature where a host training establishment can find potential interns.</p>
                                </div>
                            </div>
                            <!-- END policy -->
                        </div>
                        <!-- END col-4 -->
                        <!-- BEGIN col-4 -->
                        <div class="col-md-4 col-sm-4">
                            <!-- BEGIN policy -->
                            <div class="policy">
                                <div class="policy-icon"><i class="fa fa-building-o"></i></div>
                                <div class="policy-info">
                                    <h4>Suggests Training Establishments</h4>
                                    <p>feature where an intern can find the Host Training Establishments which will address their needs in Internship Training.</p>
                                </div>
                            </div>
                            <!-- END policy -->
                        </div>
                        <!-- END col-4 -->
                        <!-- BEGIN col-4 -->
                        <div class="col-md-4 col-sm-4">
                            <!-- BEGIN policy -->
                            <div class="policy">
                                <div class="policy-icon"><i class="fa fa-users"></i></div>
                                <div class="policy-info">
                                    <h4>TryWork tracks interns and HTEs</h4>
                                    <p>Feature where an analytics is displayed for the coordinators to keep in track with the intenrs and HTEs statuses.</p>
                                </div>
                            </div>
                            <!-- END policy -->
                        </div>
                        <!-- END col-4 -->

                    </div>
                    <!-- END row -->




                </div>
                <!-- END container -->
            <!-- END #policy -->
        {{--<!-- BEGIN section-title -->--}}
        {{--<h4 class="section-title clearfix">--}}
        {{--<a href="#" class="pull-right">SHOW ALL</a>--}}
        {{--Announcements--}}
        {{--<small>for the month of August</small>--}}
        {{--</h4>--}}
        {{--<!-- END section-title -->--}}
        {{--<!-- BEGIN row -->--}}
        {{--<div class="row row-space-10">--}}
        {{--<!-- BEGIN col-6 -->--}}
        {{--<div class="col-md-6">--}}
        {{--<!-- BEGIN promotion -->--}}
        {{--<div class="promotion promotion-lg bg-black-darker">--}}
        {{--<div class="promotion-image text-right promotion-image-overflow-bottom">--}}
        {{--<img src="{{ asset('assets/frontend/img/product/product-iphone-se.png') }}" alt="" />--}}
        {{--</div>--}}
        {{--<div class="promotion-caption promotion-caption-inverse">--}}
        {{--<h4 class="promotion-title">i-Interno Development</h4>--}}
        {{--<div class="promotion-price"><small></small></div>--}}
        {{--<p class="promotion-desc">A new way to match interns with companies.<br />Made with love and Laravel.</p>--}}
        {{--<a href="#" class="promotion-btn">View More</a>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<!-- END promotion -->--}}
        {{--</div>--}}
        {{--<!-- END col-6 -->--}}
        {{--<!-- BEGIN col-3 -->--}}
        {{--<div class="col-md-3 col-sm-6">--}}
        {{--<!-- BEGIN promotion -->--}}
        {{--<div class="promotion bg-blue">--}}
        {{--<div class="promotion-image promotion-image-overflow-bottom promotion-image-overflow-top">--}}
        {{--<img src="{{ asset('assets/frontend/img/product/product-apple-watch-sm.png') }}" alt="" />--}}
        {{--</div>--}}
        {{--<div class="promotion-caption promotion-caption-inverse text-right">--}}
        {{--<h4 class="promotion-title">Version 0.1</h4>--}}
        {{--<div class="promotion-price"><small>from</small> $299.00</div>--}}
        {{--<p class="promotion-desc">You. At a glance.</p>--}}
        {{--<a href="#" class="promotion-btn">View More</a>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<!-- END promotion -->--}}
        {{--<!-- BEGIN promotion -->--}}
        {{--<div class="promotion bg-silver">--}}
        {{--<div class="promotion-image text-center promotion-image-overflow-bottom">--}}
        {{--<img src="{{ asset('assets/frontend/img/product/product-mac-mini.png') }}" alt="" />--}}
        {{--</div>--}}
        {{--<div class="promotion-caption text-center">--}}
        {{--<h4 class="promotion-title">Version 0.2</h4>--}}
        {{--<div class="promotion-price"><small>from</small> $199.00</div>--}}
        {{--<p class="promotion-desc">It’s mini in a massive way.</p>--}}
        {{--<a href="#" class="promotion-btn">View More</a>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<!-- END promotion -->--}}
        {{--</div>--}}
        {{--<!-- END col-3 -->--}}
        {{--<!-- BEGIN col-3 -->--}}
        {{--<div class="col-md-3 col-sm-6">--}}
        {{--<!-- BEGIN promotion -->--}}
        {{--<div class="promotion bg-silver">--}}
        {{--<div class="promotion-image promotion-image-overflow-right promotion-image-overflow-bottom text-right">--}}
        {{--<img src="{{ asset('assets/frontend/img/product/product-mac-accessories.png') }}" alt="" />--}}
        {{--</div>--}}
        {{--<div class="promotion-caption text-center">--}}
        {{--<h4 class="promotion-title">Version 0.3</h4>--}}
        {{--<div class="promotion-price"><small>from</small> $99.00</div>--}}
        {{--<p class="promotion-desc">Redesigned. Rechargeable. Remarkable.</p>--}}
        {{--<a href="#" class="promotion-btn">View More</a>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<!-- END promotion -->--}}
        {{--<!-- BEGIN promotion -->--}}
        {{--<div class="promotion bg-black">--}}
        {{--<div class="promotion-image text-right">--}}
        {{--<img src="{{ asset('assets/frontend/img/product/product-mac-pro.png') }}" alt="" />--}}
        {{--</div>--}}
        {{--<div class="promotion-caption promotion-caption-inverse">--}}
        {{--<h4 class="promotion-title">Version 0.4</h4>--}}
        {{--<div class="promotion-price"><small>from</small> $1,299.00</div>--}}
        {{--<p class="promotion-desc">Built for creativity on an epic scale.</p>--}}
        {{--<a href="#" class="promotion-btn">View More</a>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<!-- END promotion -->--}}
        {{--</div>--}}
        <!-- END col-3 -->
        </div>
        <!-- END row -->
    </div>
    <!-- END container -->
    </div>
    <!-- END #promotions -->
@endsection