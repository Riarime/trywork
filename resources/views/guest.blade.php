<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <title>Home | TryWork</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="Trywork: Internship Recommender Serivces and Management System is a website made for Interns, Host Training Establishments, OJT Directors, Heads, and Coordinators. This benefits them by making their job easily, by making the system to help them with suggesting ideal interns/companies." name="description" />
    <meta content="SRG 7th Generation" name="author" />

    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <link href="{{ asset('assets/frontend/plugins/bootstrap3/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/frontend/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/frontend/plugins/animate/animate.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/frontend/css/forum/style.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/frontend/css/forum/style-responsive.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/frontend/css/forum/theme/default.css') }}" id="theme" rel="stylesheet" />
    <!-- ================== END BASE CSS STYLE ================== -->

    <!-- ================== BEGIN BASE JS ================== -->
    <script src="{{ asset('assets/frontend/plugins/pace/pace.min.js') }}"></script>
    <!-- ================== END BASE JS ================== -->
</head>
<body>
<!-- begin #header -->
<div id="header" class="header navbar navbar-default navbar-fixed-top">
    <!-- begin container -->
    <div class="container">
        <!-- begin navbar-header -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#header-navbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="header-logo" style="margin-top: 8%">
                <a href="{{ route('home') }}">
                    <img src="{{ asset('assets/images/logo-light.png') }}" style="height: 35px"/>
                    <span style="margin-top: 10%">
                    <span style="font-size: 20px;">
                        Try<span style="color: #000">Work</span> <small style="font-size:9px;color:#000">Home</small>
                    </span>
                </span>
                </a>
            </div>
        </div>
        <!-- end navbar-header -->
        <!-- begin #header-navbar -->
        <div class="collapse navbar-collapse" id="header-navbar">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="{{ route('faq') }}">
                        <span class="hidden-md hidden-sm hidden-xs">FAQ</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('login') }}">Log In</a>
                </li>
            </ul>
        </div>
        <!-- end #header-navbar -->
    </div>
    <!-- end container -->
</div>
<!-- end #header -->

<!-- begin search-banner -->
<div class="search-banner has-bg">
    <!-- begin bg-cover -->
    <div class="bg-cover particleground" style="background: #118c8a">
        <img src="{{ asset('assets/frontend/img/cover/cover-14.jpg') }}" alt="" />
    </div>
    <!-- end bg-cover -->
    <!-- begin container -->
    <div class="container">
        <h1>Try the Recommender System</h1>
        <div class="input-group m-b-20">
            <input id="skills" type="text" class="form-control input-lg" placeholder="Enter Skills" />
            <span class="input-group-btn">
                <button class="btn btn-lg disabled" style="cursor: default"><i class="fa fa-list-alt"></i></button>
            </span>
        </div>
        <div class="input-group m-b-20">
            <input type="text" class="form-control input-lg" placeholder="Address" />
            <span class="input-group-btn">
                <button class="btn btn-lg disabled" style="cursor: default"><i class="fa fa-home"></i></button>
            </span>
        </div>
        <a href="javascript:;" class="btn btn-secondary btn-block text-white" style="background: #118c8a" onclick="getSuggestedHTE($('#skills').val())" id="openBtn"><b>Process Suggestions</b></a>
        <br/>
        <p style="align-content: center">TryWork: Internship Recommender Services and Management System</p>
    </div>
    <!-- end container -->
</div>
<!-- end search-banner -->

<!-- begin content -->
<div class="content">
    <!-- begin container -->
    <div class="container">

        <div id="myModal" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h3>Recommended Job Trainings</h3>
                    </div>
                    <div class="modal-body">
                        <ul class="forum-list" id="hteList">
                            <li>
                                <p class="desc">
                                <h5 style="text-align: center">Please input details first to see the results here.</h5>
                                </p>
                            </li>
                        </ul>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="promotions" class="section-container bg-white">
            <!-- BEGIN container -->
            <div class="container">
                <!-- BEGIN section-title -->
                <h4 class="section-title clearfix" style="text-align: center">
                    Features
                    <small>of TryWork Intern Recommendation System and Management Services</small>
                </h4>
                <!-- END section-title -->
                <!-- BEGIN #policy -->
                <div id="policy" class="section-container p-t-30 p-b-30">
                    <!-- BEGIN container -->
                    <div class="container">
                        <!-- BEGIN row -->
                        <div class="row">
                            <!-- BEGIN col-4 -->
                            <div class="col-md-4 col-sm-4">
                                <!-- BEGIN policy -->
                                <div class="policy" style="text-align: center">
                                    <div class="policy-icon"><i class="fa fa-graduation-cap fa-5x"></i></div>
                                    <div class="policy-info">
                                        <h4>Intelligently recommends interns</h4>
                                        <p>Feature where a host training establishment can find potential interns.</p>
                                    </div>
                                </div>
                                <!-- END policy -->
                            </div>
                            <!-- END col-4 -->
                            <!-- BEGIN col-4 -->
                            <div class="col-md-4 col-sm-4">
                                <!-- BEGIN policy -->
                                <div class="policy" style="text-align: center">
                                    <div class="policy-icon"><i class="fa fa-building-o fa-5x"></i></div>
                                    <div class="policy-info">
                                        <h4>Suggests Training Establishments</h4>
                                        <p>Feature where an intern can find the Host Training Establishments which will address their needs in Internship Training.</p>
                                    </div>
                                </div>
                                <!-- END policy -->
                            </div>
                            <!-- END col-4 -->
                            <!-- BEGIN col-4 -->
                            <div class="col-md-4 col-sm-4">
                                <!-- BEGIN policy -->
                                <div class="policy" style="text-align: center">
                                    <div class="policy-icon"><i class="fa fa-users fa-5x"></i></div>
                                    <div class="policy-info">
                                        <h4>TryWork tracks interns and HTEs</h4>
                                        <p>Feature where an analytics is displayed for the coordinators to keep in track with the intenrs and HTEs statuses.</p>
                                    </div>
                                </div>
                                <!-- END policy -->
                            </div>
                            <!-- END col-4 -->

                        </div>
                        <!-- END row -->




                    </div>
                    <!-- END container -->
                    <!-- END #policy -->
                {{--<!-- BEGIN section-title -->--}}
                {{--<h4 class="section-title clearfix">--}}
                {{--<a href="#" class="pull-right">SHOW ALL</a>--}}
                {{--Announcements--}}
                {{--<small>for the month of August</small>--}}
                {{--</h4>--}}
                {{--<!-- END section-title -->--}}
                {{--<!-- BEGIN row -->--}}
                {{--<div class="row row-space-10">--}}
                {{--<!-- BEGIN col-6 -->--}}
                {{--<div class="col-md-6">--}}
                {{--<!-- BEGIN promotion -->--}}
                {{--<div class="promotion promotion-lg bg-black-darker">--}}
                {{--<div class="promotion-image text-right promotion-image-overflow-bottom">--}}
                {{--<img src="{{ asset('assets/frontend/img/product/product-iphone-se.png') }}" alt="" />--}}
                {{--</div>--}}
                {{--<div class="promotion-caption promotion-caption-inverse">--}}
                {{--<h4 class="promotion-title">i-Interno Development</h4>--}}
                {{--<div class="promotion-price"><small></small></div>--}}
                {{--<p class="promotion-desc">A new way to match interns with companies.<br />Made with love and Laravel.</p>--}}
                {{--<a href="#" class="promotion-btn">View More</a>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<!-- END promotion -->--}}
                {{--</div>--}}
                {{--<!-- END col-6 -->--}}
                {{--<!-- BEGIN col-3 -->--}}
                {{--<div class="col-md-3 col-sm-6">--}}
                {{--<!-- BEGIN promotion -->--}}
                {{--<div class="promotion bg-blue">--}}
                {{--<div class="promotion-image promotion-image-overflow-bottom promotion-image-overflow-top">--}}
                {{--<img src="{{ asset('assets/frontend/img/product/product-apple-watch-sm.png') }}" alt="" />--}}
                {{--</div>--}}
                {{--<div class="promotion-caption promotion-caption-inverse text-right">--}}
                {{--<h4 class="promotion-title">Version 0.1</h4>--}}
                {{--<div class="promotion-price"><small>from</small> $299.00</div>--}}
                {{--<p class="promotion-desc">You. At a glance.</p>--}}
                {{--<a href="#" class="promotion-btn">View More</a>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<!-- END promotion -->--}}
                {{--<!-- BEGIN promotion -->--}}
                {{--<div class="promotion bg-silver">--}}
                {{--<div class="promotion-image text-center promotion-image-overflow-bottom">--}}
                {{--<img src="{{ asset('assets/frontend/img/product/product-mac-mini.png') }}" alt="" />--}}
                {{--</div>--}}
                {{--<div class="promotion-caption text-center">--}}
                {{--<h4 class="promotion-title">Version 0.2</h4>--}}
                {{--<div class="promotion-price"><small>from</small> $199.00</div>--}}
                {{--<p class="promotion-desc">It’s mini in a massive way.</p>--}}
                {{--<a href="#" class="promotion-btn">View More</a>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<!-- END promotion -->--}}
                {{--</div>--}}
                {{--<!-- END col-3 -->--}}
                {{--<!-- BEGIN col-3 -->--}}
                {{--<div class="col-md-3 col-sm-6">--}}
                {{--<!-- BEGIN promotion -->--}}
                {{--<div class="promotion bg-silver">--}}
                {{--<div class="promotion-image promotion-image-overflow-right promotion-image-overflow-bottom text-right">--}}
                {{--<img src="{{ asset('assets/frontend/img/product/product-mac-accessories.png') }}" alt="" />--}}
                {{--</div>--}}
                {{--<div class="promotion-caption text-center">--}}
                {{--<h4 class="promotion-title">Version 0.3</h4>--}}
                {{--<div class="promotion-price"><small>from</small> $99.00</div>--}}
                {{--<p class="promotion-desc">Redesigned. Rechargeable. Remarkable.</p>--}}
                {{--<a href="#" class="promotion-btn">View More</a>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<!-- END promotion -->--}}
                {{--<!-- BEGIN promotion -->--}}
                {{--<div class="promotion bg-black">--}}
                {{--<div class="promotion-image text-right">--}}
                {{--<img src="{{ asset('assets/frontend/img/product/product-mac-pro.png') }}" alt="" />--}}
                {{--</div>--}}
                {{--<div class="promotion-caption promotion-caption-inverse">--}}
                {{--<h4 class="promotion-title">Version 0.4</h4>--}}
                {{--<div class="promotion-price"><small>from</small> $1,299.00</div>--}}
                {{--<p class="promotion-desc">Built for creativity on an epic scale.</p>--}}
                {{--<a href="#" class="promotion-btn">View More</a>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<!-- END promotion -->--}}
                {{--</div>--}}
                <!-- END col-3 -->
                </div>
                <!-- END row -->
            </div>
            <!-- END container -->
        </div>


        <!-- begin panel-forum -->
        <div class="panel panel-forum">
            <!-- begin panel-heading -->
            <div class="panel-heading">
                <h4 class="panel-title">Lists of Host Training Establishments available</h4>
            </div>
            <!-- end panel-heading -->
            <!-- begin forum-list -->
            <ul class="forum-list">
                @foreach($htes as $hte)
                    <li>
                        <div class="media" style="background: #fff">
                            <i class="fa fa-building" style="background: #fff"></i>
                            </div>
                        <div class="info-container">
                            <div class="info">
                                <h4 class="title"><a href="#">
                                        {{ $hte->hte_name }}
                                        </a></h4>
                                <p class="desc">
                                    {{ $hte->address }}
                                    </p>
                                </div>
                            <div class="total-count">
                                <span class="total-comment" id="distance"><small>Enter address first to see the distance</small></span>
                                </div>
                            <div class="latest-post">
                                <h6 class="title">
                                    <small>Latest Job Training: </small>
                                    </h6>
                                <h4 class="title"><a href="#" title="{{ $jobTrainings->where('hte_id', $hte->hte_id)->last()->description or 'N/A' }}">
                                        {{ $jobTrainings->where('hte_id', $hte->hte_id)->last()->job_training_name or 'N/A' }}
                                        </a></h4>
                                <p class="time">
                                    <small>
                                        <time class="timeago" datetime="{{ ($jobTrainings->where('hte_id', $hte->hte_id)->last() == null) ? null : (new DateTime($jobTrainings->where('hte_id', $hte->hte_id)->last()->created_at))->format('c') }}">{{ ($jobTrainings->where('hte_id', $hte->hte_id)->last() == null) ? null : (new DateTime($jobTrainings->where('hte_id', $hte->hte_id)->last()->created_at))->format('F d, Y, h:ia') }}</time>
                                    </small>
                                    </p>
                                </div>
                            </div>
                    </li>
                @endforeach
            </ul>
            <!-- end forum-list -->
        </div>
        <!-- end panel-forum -->
    </div>
    <!-- end container -->
</div>
<!-- end content -->

<!-- begin #footer -->
<div id="footer" class="footer">
    <!-- begin container -->
    <div class="container">
        <!-- begin row -->
        <div class="row">
            <!-- begin col-4 -->
            <div class="col-md-8">
                <!-- begin section-container -->
                <div class="section-container">
                    <h4>About Trywork: Internship Recommender Services and Management System</h4>
                    <p>
                        Trywork: Internship Recommender Serivces and Management System is a website made for Interns, Host Training Establishments, OJT Directors, Heads, and Coordinators. This benefits them by making their job easily, by making the system to help them with suggesting ideal interns/companies.
                    </p>
                </div>
                <!-- end section-container -->
            </div>
            <!-- end col-4 -->
            <!-- begin col-4 -->
            {{--<div class="col-md-4">--}}
                {{--<!-- begin section-container -->--}}
                {{--<div class="section-container">--}}
                    {{--<h4>New Users</h4>--}}
                    {{--<ul class="new-user">--}}
                        {{--<li><a href="#"><img src="{{ asset('assets/frontend/img/user/user-1.jpg') }}" alt="" /></a></li>--}}
                        {{--<li><a href="#"><img src="{{ asset('assets/frontend/img/user/user-2.jpg') }}" alt="" /></a></li>--}}
                        {{--<li><a href="#"><img src="{{ asset('assets/frontend/img/user/user-3.jpg') }}" alt="" /></a></li>--}}
                        {{--<li><a href="#"><img src="{{ asset('assets/frontend/img/user/user-4.jpg') }}" alt="" /></a></li>--}}
                        {{--<li><a href="#"><img src="{{ asset('assets/frontend/img/user/user-5.jpg') }}" alt="" /></a></li>--}}
                        {{--<li><a href="#"><img src="{{ asset('assets/frontend/img/user/user-6.jpg') }}" alt="" /></a></li>--}}
                        {{--<li><a href="#"><img src="{{ asset('assets/frontend/img/user/user-7.jpg') }}" alt="" /></a></li>--}}
                        {{--<li><a href="#"><img src="{{ asset('assets/frontend/img/user/user-8.jpg') }}" alt="" /></a></li>--}}
                        {{--<li><a href="#"><img src="{{ asset('assets/frontend/img/user/user-9.jpg') }}" alt="" /></a></li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
                {{--<!-- end section-container -->--}}
            {{--</div>--}}
            <!-- end col-4 -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
</div>
<!-- end #footer -->
<!-- begin #footer-copyright -->
<div id="footer-copyright" class="footer-copyright">
    <div class="container">
        &copy;{{ date('Y', \Carbon\Carbon::now()->timestamp) }} SRG 7th Generation, All Right Reserved
        <a href="http://srg.pupqc.net">About SRG</a>
        <a href="#">Portfolio</a>
    </div>
</div>
<!-- end #footer-copyright -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="{{ asset('assets/frontend/plugins/jquery/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('assets/frontend/plugins/bootstrap3/js/bootstrap.min.js') }}"></script>
<!--[if lt IE 9]>
<script src="{{ asset('assets/frontend/crossbrowserjs/html5shiv.js') }}"></script>
<script src="{{ asset('assets/frontend/crossbrowserjs/respond.min.js') }}"></script>
<script src="{{ asset('assets/frontend/crossbrowserjs/excanvas.min.js') }}"></script>
<![endif]-->
<script src="{{ asset('assets/frontend/plugins/js-cookie/js.cookie.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/fusejs/dist/fuse.min.js') }}"></script>
<script src="{{ asset('assets/timeago/jquery.timeago.js') }}"></script>
<script src="{{ asset('assets/particleground/jquery.particleground.min.js') }}"></script>
<script src="{{ asset('assets/vaguejs/Vague.js') }}"></script>
<script src="{{ asset('assets/frontend/js/forum/apps.min.js') }}"></script>
<!-- ================== END BASE JS ================== -->
<script>
    $("time.timeago").timeago();
</script>
<script>
    $("div.particleground").children().remove();
    $("div.particleground").particleground({
        parallax: true,
        parallaxMultiplier: 10,
        dotColor: '#3abfbd',
        lineColor: '#3abfbd',
        density: 2500,
    });
</script>
<script>
    var vague = $(".bg-cover").Vague({intensity:});
    vague.blur();
</script>
<script>
    $(document).ready(function() {
        App.init();
    });

    $('#openBtn').click(function(){
        $('#myModal').modal({show:true});
    });

    function getSuggestedHTE(skills){
        var fuse = new Fuse(jobTrainings, options);
        var result = fuse.search(skills);
        console.log(result);

        $("ul[id='hteList']").children().remove();
        if(result.length == 0){
            var append = '<li>' +
                '<p class="desc">' +
                '<h5 style="text-align: center">No job trainings available for you this moment, please try again later.</h5>' +
                '</p>' +
                '</li>';

            $("ul[id='hteList']").append(append);
        }
        else{
            for(count = 0; count < result.length; count++){
                var append = '<li>' +
                    '<div class="media" style="background: #fff">' +
                    '<i class="fa fa-building" style="background: #fff"></i>' +
                    '</div>' +
                    '<div class="info-container">' +
                    '<div class="info">' +
                    '<h4 class="title"><a href="category_list.html">' +
                    result[count]['item']['job_training_name'] +
                    '</a></h4>' +
                    '<p class="desc">' +
                    result[count]['item']['job_training_description'] +
                    '</p>' +
                    '</div>' +
                    '<div class="total-count">' +
                    '<span class="total-comment" title="Enter Distance First to see Distance">N/A</span>' +
                    '</div>' +
                    '<div class="latest-post">' +
                    '<h6 class="title">' +
                    '<small>Job Training from: </small>' +
                    '</h6>' +
                    '<h4 class="title"><a href="#">' +
                    result[count]['item']['hte_name'] +
                    '</a></h4>' +
                    '<p class="time">' +
                    '<small>' +
                    result[count]['item']['hte_address'] +
                    '</small>' +
                    '</p>' +
                    '</div>' +
                    '</div>' +
                    '</li>';
                if(result[count]['score'] < 0.75){
                    $("ul[id='hteList']").append(append);
                }
            }
        }
    };

    var jobTrainings = [
        @foreach($jobTrainings as $jobTraining)
        {
            'hte_name': '{{ $htes->where('hte_id', $jobTraining->hte_id)->first()->hte_name }}',
            'hte_address': '{{ $htes->where('hte_id', $jobTraining->hte_id)->first()->address }}',
            'job_training_id': {{ $jobTraining->job_training_id }},
            'job_training_name': '{{ $jobTraining->job_training_name }}',
            'job_training_description': '{{ $jobTraining->description }}',
            'created_at': '{{ $jobTraining->created_at }}'
        },
        @endforeach
    ];

    var options = {
        includeScore: true,
        shouldSort: true,
        tokenize: true,
        matchAllTokens: true,
        findAllMatches: true,
        keys: ['job_training_name', 'job_training_description'],
        threshold: 0.4,
        caseSensitive: false
    };w
</script>
</body>
</html>
