<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <title>Color Admin | One Page Parallax Front End Theme</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    {{--<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />--}}
    <link href="{{ asset('assets/frontend/plugins/bootstrap3/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/frontend/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/frontend/plugins/animate/animate.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/frontend/css/one-page-parallax/style.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/frontend/css/one-page-parallax/style-responsive.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/frontend/css/one-page-parallax/theme/default.css') }}" id="theme" rel="stylesheet" />
    <!-- ================== END BASE CSS STYLE ================== -->

    <!-- ================== BEGIN BASE JS ================== -->
    <script src="{{ asset('assets/frontends/plugins/pace/pace.min.js') }}"></script>
    <!-- ================== END BASE JS ================== -->
</head>
<body data-spy="scroll" data-target="#header-navbar" data-offset="51">
<!-- begin #page-container -->
<div id="page-container" class="fade">
    <!-- BEGIN #top-nav -->
    <div id="top-nav" class="top-nav">
        <!-- BEGIN container -->
        <div class="container">
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="#">Link #1</a></li>
                    <li><a href="#">Link #2</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">Link #3</a></li>
                    <li><a href="#">Link #4</a></li>
                    <li><a href="#">Link #5</a></li>
                    <li><a href="#"><i class="fa fa-facebook f-s-14"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter f-s-14"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram f-s-14"></i></a></li>
                    <li><a href="#"><i class="fa fa-dribbble f-s-14"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus f-s-14"></i></a></li>
                </ul>
            </div>
        </div>
        <!-- END container -->
    </div>
    <!-- END #top-nav -->

    <!-- BEGIN #header -->
    <div id="header" class="header" data-fixed-top="true">
        <!-- BEGIN container -->
        <div class="container">
            <!-- BEGIN header-container -->
            <div class="header-container">
                <!-- BEGIN navbar-header -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="header-logo">
                        <a href="{{ route('home') }}">
                            <span class="brand"></span>
                            <span>i</span>-Interno
                            <small>Recommendation System</small>
                        </a>
                    </div>
                </div>
                <!-- END navbar-header -->
                <!-- BEGIN header-nav -->
                <div class="header-nav">
                    <div class=" collapse navbar-collapse" id="navbar-collapse">
                        <ul class="nav">
                            @section('navbar')
                            @show
                        </ul>
                    </div>
                </div>
                <!-- END header-nav -->
                <!-- BEGIN header-nav -->
                <div class="header-nav">
                    <ul class="nav pull-right">
                        <li>
                            @section('account')
                            @show
                        </li>
                    </ul>
                </div>
                <!-- END header-nav -->
            </div>
            <!-- END header-container -->
        </div>
        <!-- END container -->
    </div>
    <!-- END #header -->

    <!-- begin #team -->
    <div id="team" class="content" data-scrollview="true">
        <!-- begin container -->
        <div class="container">
            <h2 class="content-title">Our Team</h2>
            <p class="content-desc">
                Phasellus suscipit nisi hendrerit metus pharetra dignissim. Nullam nunc ante, viverra quis<br />
                ex non, porttitor iaculis nisi.
            </p>
            <!-- begin row -->
            <div class="row">
                <!-- begin col-4 -->
                <div class="col-md-4 col-sm-4">
                    <!-- begin team -->
                    <div class="team">
                        <div class="image" data-animation="true" data-animation-type="flipInX">
                            <img src="../assets/img/user/user-1.jpg" alt="Ryan Teller" />
                        </div>
                        <div class="info">
                            <h3 class="name">Ryan Teller</h3>
                            <div class="title text-theme">FOUNDER</div>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</p>
                            <div class="social">
                                <a href="#"><i class="fa fa-facebook fa-lg fa-fw"></i></a>
                                <a href="#"><i class="fa fa-twitter fa-lg fa-fw"></i></a>
                                <a href="#"><i class="fa fa-google-plus fa-lg fa-fw"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- end team -->
                </div>
                <!-- end col-4 -->
                <!-- begin col-4 -->
                <div class="col-md-4 col-sm-4">
                    <!-- begin team -->
                    <div class="team">
                        <div class="image" data-animation="true" data-animation-type="flipInX">
                            <img src="../assets/img/user/user-2.jpg" alt="Jonny Cash" />
                        </div>
                        <div class="info">
                            <h3 class="name">Johnny Cash</h3>
                            <div class="title text-theme">WEB DEVELOPER</div>
                            <p>Donec quam felis, ultricies nec, pellentesque eu sem. Nulla consequat massa quis enim.</p>
                            <div class="social">
                                <a href="#"><i class="fa fa-facebook fa-lg fa-fw"></i></a>
                                <a href="#"><i class="fa fa-twitter fa-lg fa-fw"></i></a>
                                <a href="#"><i class="fa fa-google-plus fa-lg fa-fw"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- end team -->
                </div>
                <!-- end col-4 -->
                <!-- begin col-4 -->
                <div class="col-md-4 col-sm-4">
                    <!-- begin team -->
                    <div class="team">
                        <div class="image" data-animation="true" data-animation-type="flipInX">
                            <img src="../assets/img/user/user-3.jpg" alt="Mia Donovan" />
                        </div>
                        <div class="info">
                            <h3 class="name">Mia Donovan</h3>
                            <div class="title text-theme">WEB DESIGNER</div>
                            <p>Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. </p>
                            <div class="social">
                                <a href="#"><i class="fa fa-facebook fa-lg fa-fw"></i></a>
                                <a href="#"><i class="fa fa-twitter fa-lg fa-fw"></i></a>
                                <a href="#"><i class="fa fa-google-plus fa-lg fa-fw"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- end team -->
                </div>
                <!-- end col-4 -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end #team -->

    <!-- begin #footer -->
    <div id="footer" class="footer">
        <div class="container">
            <div class="footer-brand">
                <div class="footer-brand-logo"></div>
                Color Admin
            </div>
            <p>
                &copy; Copyright Color Admin 2017 <br />
                An admin & front end theme with serious impact. Created by <a href="#">SeanTheme</a>
            </p>
            <p class="social-list">
                <a href="#"><i class="fa fa-facebook fa-fw"></i></a>
                <a href="#"><i class="fa fa-instagram fa-fw"></i></a>
                <a href="#"><i class="fa fa-twitter fa-fw"></i></a>
                <a href="#"><i class="fa fa-google-plus fa-fw"></i></a>
                <a href="#"><i class="fa fa-dribbble fa-fw"></i></a>
            </p>
        </div>
    </div>
    <!-- end #footer -->

    <!-- begin theme-panel -->
    <div class="theme-panel">
        <a href="javascript:;" data-click="theme-panel-expand" class="theme-collapse-btn"><i class="fa fa-cog"></i></a>
        <div class="theme-panel-content">
            <ul class="theme-list clearfix">
                <li><a href="javascript:;" class="bg-purple" data-theme="purple" data-theme-file="../assets/css/one-page-parallax/theme/purple.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Purple">&nbsp;</a></li>
                <li><a href="javascript:;" class="bg-blue" data-theme="blue" data-theme-file="../assets/css/one-page-parallax/theme/blue.css" data-theme-file="" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Blue">&nbsp;</a></li>
                <li class="active"><a href="javascript:;" class="bg-green" data-theme-file="../assets/css/one-page-parallax/theme/default.css" data-theme-file="" data-theme="default" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Default">&nbsp;</a></li>
                <li><a href="javascript:;" class="bg-orange" data-theme="orange" data-theme-file="../assets/css/one-page-parallax/theme/orange.css" data-theme-file="" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Orange">&nbsp;</a></li>
                <li><a href="javascript:;" class="bg-red" data-theme="red" data-theme-file="../assets/css/one-page-parallax/theme/red.css" data-theme-file="" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Red">&nbsp;</a></li>
            </ul>
        </div>
    </div>
    <!-- end theme-panel -->
</div>
<!-- end #page-container -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="../assets/frontend/plugins/jquery/jquery-3.2.1.min.js"></script>
<script src="../assets/frontend/plugins/bootstrap3/js/bootstrap.min.js"></script>
<!--[if lt IE 9]>
<script src="{{ asset('assets/frontend/crossbrowserjs/html5shiv.js') }}"></script>
<script src="{{ asset('assets/frontend/crossbrowserjs/respond.min.js') }}"></script>
<script src="{{ asset('assets/frontend/crossbrowserjs/excanvas.min.js') }}"></script>
<![endif]-->
<script src="{{ asset('assets/frontend/plugins/js-cookie/js.cookie.js') }}"></script>
<script src="{{ asset('assets/frontend/plugins/scrollMonitor/scrollMonitor.js') }}"></script>
<script src="{{ asset('assets/frontend/js/one-page-parallax/apps.min.js') }}"></script>
<!-- ================== END BASE JS ================== -->

<script>
    $(document).ready(function() {
        App.init();
    });
</script>
</body>
</html>
