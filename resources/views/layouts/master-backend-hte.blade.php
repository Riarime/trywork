<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <title>@yield('title') | TryWork</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/bootstrap/4.0.0/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/font-awesome/5.0/css/fontawesome-all.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/animate/animate.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/gritter/css/jquery.gritter.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/css/default/style.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/css/default/style-responsive.min') }}.css" rel="stylesheet" />
    <link href="{{ asset('assets/backend/css/default/theme/default.css') }}" rel="stylesheet" id="theme" />
    <!-- ================== END BASE CSS STYLE ================== -->

    <!-- ================== BEGIN PAGE LEVEL CSS STYLE ================== -->
@section('css')
@show
<!-- ================== END PAGE LEVEL CSS STYLE ================== -->

    <!-- ================== BEGIN BASE JS ================== -->
    <script src="{{ asset('assets/plugins/pace/pace.min.js') }}"></script>
    @section('base-js')
    @show
    <!-- ================== END BASE JS ================== -->
</head>
<body>
<!-- begin #page-loader -->
<div id="page-loader" class="fade show"><span class="spinner"></span></div>
<!-- end #page-loader -->

<!-- begin #page-container -->
<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
    <!-- begin #header -->
    <div id="header" class="header navbar-default">
        <!-- begin navbar-header -->
        <div class="navbar-header">
            <a href="index.html" class="navbar-brand"><span class="navbar-logo"></span> <b>Try</b>Work</a>
            <button type="button" class="navbar-toggle" data-click="sidebar-toggled">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <!-- end navbar-header -->
    </div>
    <!-- end #header -->

    <!-- begin #sidebar -->
    <div id="sidebar" class="sidebar sidebar-transparent">
        <!-- begin sidebar scrollbar -->
        <div data-scrollbar="true" data-height="100%">
            <!-- begin sidebar user -->
            <ul class="nav">
                <li class="nav-profile">
                    <a href="javascript:;" data-toggle="nav-profile">
                        <div class="cover with-shadow"></div>
                        <div class="image">
                            <img src="{{ Avatar::create(\App\User::getHTEName())->toBase64() }}" alt="" />
                        </div>
                        <div class="info">
                            <b class="caret pull-right"></b>
                            {{ \App\User::getHTEName() }}
                            <small></small>
                        </div>
                    </a>
                </li>
                <li>
                    <ul class="nav nav-profile">
                        <li><a href="#"><i class="fa fa-user-circle"></i> Profile</a></li>
                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();"><i class="fa fa-sign-out-alt"></i> Log Out</a></li>
                        <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </ul>
                </li>
            </ul>
            <!-- end sidebar user -->
            <!-- begin sidebar nav -->
            <ul class="nav">
                <li class="nav-header">Navigation</li>
                <li class="has-sub">
                    <a href="{{ route('hte.dashboard') }}">
                        <i class="fa fa-home"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="has-sub">
                    <a href="{{ route('hte.intern-finder') }}">
                        <i class="fa fa-search"></i>
                        <span>Intern Finder</span>
                    </a>
                </li>
                <li class="has-sub">
                    <a href="{{ route('hte.job-trainings-management') }}">
                        <i class="fa fa-search"></i>
                        <span>Job Trainings Manager</span>
                    </a>
                </li>
                <!-- begin sidebar minify button -->
                <li><a href="javascript;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
                <!-- end sidebar minify button -->
            </ul>
            <!-- end sidebar nav -->
        </div>
        <!-- end sidebar scrollbar -->
    </div>
    <div class="sidebar-bg"></div>
    <!-- end #sidebar -->

    <!-- begin #content -->
    <div id="content" class="content">
        @section('content')
        @show
    </div>
    <!-- end #content -->


    <!-- begin scroll to top btn -->
    <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
    <!-- end scroll to top btn -->
</div>
<!-- end page container -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="{{ asset('assets/backend/plugins/jquery/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/bootstrap/4.0.0/js/bootstrap.bundle.min.js') }}"></script>
<!--[if lt IE 9]>
<script src="{{ asset('assets/backend/crossbrowserjs/html5shiv.js') }}"></script>
<script src="{{ asset('assets/backend/crossbrowserjs/respond.min.js') }}"></script>
<script src="{{ asset('assets/backend/crossbrowserjs/excanvas.min.js') }}"></script>
<![endif]-->
<script src="{{ asset('assets/backend/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/js-cookie/js.cookie.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/gritter/js/jquery.gritter.js') }}"></script>
<script src="{{ asset('assets/backend/js/theme/default.min.js') }}"></script>
<script src="{{ asset('assets/particleground/jquery.particleground.min.js') }}"></script>
<script src="{{ asset('assets/vaguejs/Vague.js') }}"></script>
<script src="{{ asset('assets/backend/js/apps.min.js') }}"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
@section('js')
@show
@section('custom-js')
@show
<!-- ================== END PAGE LEVEL JS ================== -->


<script>
    $("div.sidebar-bg").children().remove();
    $("div.sidebar-bg").particleground({
        parallax: true,
        parallaxMultiplier: 10,
        dotColor: '#3abfbd',
        lineColor: '#3abfbd',
        density: 2500,
    });

    var vague = $("div.sidebar-bg").Vague({intensity:1.5});
    vague.blur();
</script>
<script>
    $(document).ready(function() {
        App.init();
                @section('document.ready')
                @show
        var notif = function() {
                $(window).on('load', function() {
                    setTimeout(function() {
                        $.gritter.add({
                            title: 'Welcome back, Admin!',
                            text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus lacus ut lectus rutrum placerat.',
                            image: '../assets/img/user/user-2.jpg',
                            sticky: true,
                            time: '',
                            class_name: 'my-sticky-class'
                        });
                    }, 1000);
                });
            };
        notif.init();
    });
</script>

</body>
</html>
