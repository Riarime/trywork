<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title') | TryWork</title>
	<link href="{{asset('assets/images/logo-dark.png')}}" rel="icon">

	<!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link rel="stylesheet" href="{{asset('assets/backend/plugins/jquery-ui/jquery-ui.min.css') }}">
	<link href="{{asset('assets/backend/plugins/bootstrap/4.0.0/css/bootstrap.min.css') }}" rel="stylesheet" />
	<link href="{{asset('assets/backend/plugins/font-awesome/5.0/css/fontawesome-all.min.css')}}" rel="stylesheet" />
	<link href="{{asset('assets/backend/plugins/animate/animate.min.css') }}" rel="stylesheet" />
	<link href="{{asset('assets/backend/css/default/style.min.css') }}" rel="stylesheet" />
	<link href="{{asset('assets/backend/css/default/style-responsive.min.css') }}" rel="stylesheet" />
	<!-- ================== END BASE CSS STYLE ================== -->
    
    <!-- ================== BEGIN PAGE LEVEL CSS STYLE ================== -->
    @section('css')
    @show
    <!-- ================== END PAGE LEVEL CSS STYLE ================== -->

    <!-- ================== BEGIN BASE JS ================== -->
	<script src="{{asset('assets/backend/plugins/pace/pace.min.js') }}"></script>
    @section('base-js')
    @show
	<!-- ================== END BASE JS ================== -->
</head>
<body>


	<!-- begin #page-loader -->
	<div id="page-loader" class="fade show"><span class="spinner"></span></div>
	<!-- end #page-loader -->
	
	<!-- begin #page-container -->
	<div id="page-container" class="page-container fade page-sidebar-fixed page-header-fixed">
        <!-- begin #header -->
        
		<div id="header" class="header navbar-default">
			<!-- begin navbar-header -->
			<div class="navbar-header">
				<a href="" class="navbar-brand">
					<img src="{{asset('assets/images/logo-light.png')}}" alt="" width="50" height="50" style="margin-top: -10px;"> <b>T</b><small><b>RY</b></small><b>W</b><small><b>ORK</b></small>
				</a>
				<button type="button" class="navbar-toggle" data-click="sidebar-toggled">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<!-- end navbar-header -->

			<!-- begin header-nav -->
			<ul class="navbar-nav navbar-right">
				<li class="dropdown navbar-user">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
						<img src="{{ Avatar::create($name["first_name"].' '.$name["last_name"])->toBase64() }}" alt="" />
						<span class="d-none d-md-inline">
							@if($name["middle_name"] == '')
								{{ $name["first_name"].' '.$name["last_name"] }}
							@else
								{{ $name["first_name"].' '.substr($name["middle_name"],0,1).'. '.$name["last_name"] }}
							@endif
						</span> <b class="caret"></b>
					</a>
					<div class="dropdown-menu dropdown-menu-right">
						<a href="javascript:;" class="dropdown-item">View Profile</a>
						<div class="dropdown-divider"></div>
						<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();" class="dropdown-item">Log Out</a>
						<form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
							{{ csrf_field() }}
						</form>
					</div>
				</li>
			</ul>
			<!-- end header navigation right -->
		</div>
		<!-- end #header -->

        <!-- begin #sidebar -->
		<div id="sidebar" class="sidebar">
			<!-- begin sidebar scrollbar -->
			<div data-scrollbar="true" data-height="100%">
				<!-- begin sidebar user -->
				<ul class="nav">
					<li class="nav-profile">
						<a href="javascript:;">
							<div class="with-shadow"></div>
							<div class="image">
								<img src="{{ Avatar::create($name["first_name"].' '.$name["last_name"])->toBase64() }}" alt="" />
							</div>
							<div class="info">
								@if($name["middle_name"] == '')
									{{ $name["first_name"].' '.$name["last_name"] }}
								@else
									{{ $name["first_name"].' '.substr($name["middle_name"],0,1).'. '.$name["last_name"] }}
								@endif
								<small>{{ $user_role }}</small>
							</div>
						</a>
					</li>
				</ul>
				<!-- end sidebar user -->
				<!-- begin sidebar nav -->
				<ul class="nav">
                    <li class="nav-header">Navigation</li>
                    @if($view == 'dashboard')
                        <li class="active">
                            <a href="javascript:;">
                                <i class="fa fa-th-large"></i>
                                <span>Dashboard</span>
                            </a>
                        </li>
                    @else
                        <li>
                            @if($user == 'coordinator')
                                <a href="{{ route('coordinator.dashboard') }}">
							@elseif($user == 'head')
								<a href="{{ route('head.dashboard') }}">
                            @elseif($user == 'director')
                                <a href="{{ route('director.dashboard') }}">
                            @endif
                                <i class="fa fa-th-large"></i>
                                <span>Dashboard</span>
                            </a>
                        </li>
                    @endif
					@if ($user == 'head' || $user == 'director')
						@if ($view == 'user management')
							<li class="active">
								<a href="javascript:;">
									<i class="fas fa-users"></i>
									<span>User Management</span>
								</a>
							</li>
						@else
							<li>
								@if($user == 'head')
									<a href="">
								@elseif($user == 'director')
									<a href="">
								@endif
									<i class="fas fa-users"></i>
									<span>User Management</span>
								</a>
							</li>
						@endif
					@endif
					@if ($user == 'coordinator')
						@if($view == 'config')
                            <li class="active">
                                <a href="javascript:;">
                                    <i class="fas fa-cog fa-fv"></i>
                                    <span>Configurations</span>
                                </a>
                            </li>
							{{--<li class="has-sub active">
								<a href="javascript:;">
									<i class="fas fa-cog fa-fw"></i>
									<span>Configuration</span>
								</a>
								<ul class="sub-menu">
									@if($sub == 'ay')
										<li class="active"><a href="javascript:;">Academic Year</a></li>
									@else
										<li><a href="{{route('config.acadyr')}}">Academic Year</a></li>
									@endif
									@if($sub == 'sem')
										<li class="active"><a href="javascript:;">Semester</a></li>
									@else
										<li><a href="{{route('config.sem')}}">Semester</a></li>
									@endif
									@if($sub == 'colleges')
										<li class="active"><a href="javascript:;">Colleges</a></li>
									@else
										<li><a href="{{route('config.college')}}">Colleges</a></li>
									@endif
									@if($sub == 'course')
										<li class="active"><a href="javascript:;">College Courses</a></li>
									@else
										<li><a href="{{route('config.course')}}">College Courses</a></li>
									@endif
									@if($sub == 'yrsec')
										<li class="active"><a href="javascript:;">Year & Section</a></li>
									@else
										<li><a href="{{route('config.yearsec')}}">Year & Section</a></li>
									@endif
								</ul>
							</li>--}}
						@else
                            <li>
                                <a href="{{ route('config') }}">
                                    <i class="fas fa-cog fa-fv"></i>
                                    <span>Configurations</span>
                                </a>
                            </li>
							{{--<li class="has-sub">
								<a href="javascript:;">
									<i class="fas fa-cog fa-fw"></i>
									<span>Configuration</span>
								</a>
								<ul class="sub-menu">
									<li><a href="{{route('config.acadyr')}}">Academic Year</a></li>
									<li><a href="{{route('config.sem')}}">Semester</a></li>
									<li><a href="{{route('config.college')}}">Colleges</a></li>
									<li><a href="{{route('config.course')}}">College Courses</a></li>
									<li><a href="{{route('config.yearsec')}}">Year & Section</a></li>
								</ul>
							</li>--}}
						@endif
						@if ($view == 'hte')
                            <li class="active">
                                <a href="javascript:;">
                                    <i class="fa fa-building"></i>
                                    <span>Host Training Establishment</span>
                                </a>
                            </li>
                        @else
							<li>
                                <a href="{{ route('services') }}">
                                    <i class="fa fa-building"></i>
                                    <span>Host Training Establishment</span>
                                </a>
                            </li>
						@endif
                        @if($view == 'intern')
                            {{--<li class="has-sub active">
                                <a href="javascript:;">
                                    <i class="fa fa-hdd"></i>
                                    <span>Interns</span>
                                </a>
                                <ul class="sub-menu">
                                    @if($sub == "assess")
                                        <li class="active"><a href="javascript:;">Registration</a></li>
                                    @else
                                        <li><a href="{{ route('intern.assessment') }}">Registration</a></li>
                                    @endif
                                    @if($sub == "list")
                                        <li class="active"><a href="javascript:;">Record List</a></li>
                                    @else
                                        <li><a href="{{ route('intern.list') }}">Record List</a></li>
                                    @endif
                                    @if($sub == "grade")
                                        <li class="active"><a href="javascript:;">Grade Assessment</a></li>
                                    @else
                                        <li><a href="{{ route('intern.grade') }}">Grade Assessment</a></li>
                                    @endif
                                </ul>
                            </li>--}}
							<li class="active">
								<a>
									<i class="fa fa-hdd"></i>
									<span>Interns</span>
								</a>
							</li>
                        @else
							<li>
								<a href="{{ route('intern') }}">
									<i class="fa fa-hdd"></i>
									<span>Interns</span>
								</a>
							</li>
                            {{--<li class="has-sub">
                                <a href="javascript:;">
                                    <i class="fa fa-hdd"></i>
                                    <span>Interns</span>
                                </a>
                                <ul class="sub-menu">
                                    <li><a href="{{ route('intern.assessment') }}">Registration</a></li>
                                    <li><a href="{{ route('intern.list') }}">Record List</a></li>
                                    <li><a href="{{ route('intern.grade') }}">Grade Assessment</a></li>
                                </ul>
                            </li>--}}
                        @endif
                        <li>
                            <a href="javascript:;">
                                <i class="fa fa-print"></i>
                                <span>Reports</span>
                            </a>
                        </li>
                    @endif
				</ul>
				<!-- end sidebar nav -->
			</div>
			<!-- end sidebar scrollbar -->
		</div>
		<div class="sidebar-bg"></div>
		<!-- end #sidebar -->

        <!-- begin #content -->
		<div id="content" class="content">
            @section('content')
            @show
        </div>
		<!-- end #content -->
		
		<!-- end #content -->
		<!-- begin scroll to top btn -->
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
		<!-- end scroll to top btn -->
	</div>
	<!-- end page container -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="{{asset('assets/backend/plugins/jquery/jquery-3.2.1.min.js') }}"></script>
	<script src="{{asset('assets/backend/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
	<script src="{{asset('assets/backend/plugins/bootstrap/4.0.0/js/bootstrap.bundle.min.js') }}"></script>
	<!--[if lt IE 9]>
		<script src="../assets/crossbrowserjs/html5shiv.js"></script>
		<script src="../assets/crossbrowserjs/respond.min.js"></script>
		<script src="../assets/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
	<script src="{{asset('assets/backend/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
	<script src="{{asset('assets/backend/plugins/js-cookie/js.cookie.js') }}"></script>
	<script src="{{asset('assets/backend/js/theme/default.min.js') }}"></script>
	<script src="{{ asset('assets/particleground/jquery.particleground.min.js') }}"></script>
	<script src="{{ asset('assets/vaguejs/Vague.js') }}"></script>
	<script src="{{asset('assets/backend/js/apps.min.js')}}"></script>
	<!-- ================== END BASE JS ================== -->
	
    <!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script>
        $("div.sidebar-bg").children().remove();
        $("div.sidebar-bg").particleground({
            parallax: true,
            parallaxMultiplier: 10,
            dotColor: '#3abfbd',
            lineColor: '#3abfbd',
            density: 2500,
        });

            var vague = $("div.sidebar-bg").Vague({intensity:1.5});
            vague.blur();
	</script>
	<script>
		App.setPageOption({
			pageSidebarTransparent: true,
			clearOptionOnLeave: true
		});
		App.restartGlobalFunction();
	</script>
    @section('js')
    @show
    <!-- ================== END PAGE LEVEL JS ================== -->
    
    @section('custom-js')
    @show

</body>
</html>