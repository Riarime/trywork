<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <title>Color Admin | Dashboard</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    {{--<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />--}}
    <link href="{{ asset('assets/backend/plugins/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/bootstrap/4.0.0/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/font-awesome/5.0/css/fontawesome-all.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/animate/animate.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/css/default/style.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/css/default/style-responsive.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/css/default/theme/default.css') }}" rel="stylesheet" id="theme" />
    <!-- ================== END BASE CSS STYLE ================== -->

    <!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
    <link href="{{ asset('assets/backend/plugins/jquery-jvectormap/jquery-jvectormap.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/gritter/css/jquery.gritter.css') }}" rel="stylesheet" />
    <!-- ================== END PAGE LEVEL STYLE ================== -->

    <!-- ================== BEGIN BASE JS ================== -->
    <script src="{{ asset('assets/backend/plugins/pace/pace.min.js') }}"></script>
    <!-- ================== END BASE JS ================== -->
</head>
<body>

<!-- begin #page-container -->
<div id="page-container" class="page-container fade page-header-fixed">

    @section('content')
    @show

    <!-- begin scroll to top btn -->
    <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
    <!-- end scroll to top btn -->
</div>
<!-- end page container -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="../assets/backend/plugins/jquery/jquery-3.2.1.min.js"></script>
<script src="../assets/backend/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="../assets/backend/plugins/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
<!--[if lt IE 9]>
<script src="../assets/backend/crossbrowserjs/html5shiv.js"></script>
<script src="../assets/backend/crossbrowserjs/respond.min.js"></script>
<script src="../assets/backend/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="../assets/backend/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="../assets/backend/plugins/js-cookie/js.cookie.js"></script>
<script src="../assets/backend/js/theme/default.min.js"></script>
<script src="../assets/backend/js/apps.min.js"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="../assets/backend/plugins/gritter/js/jquery.gritter.js"></script>
<script src="../assets/backend/plugins/flot/jquery.flot.min.js"></script>
<script src="../assets/backend/plugins/flot/jquery.flot.time.min.js"></script>
<script src="../assets/backend/plugins/flot/jquery.flot.resize.min.js"></script>
<script src="../assets/backend/plugins/flot/jquery.flot.pie.min.js"></script>
<script src="../assets/backend/plugins/sparkline/jquery.sparkline.js"></script>
<script src="../assets/backend/plugins/jquery-jvectormap/jquery-jvectormap.min.js"></script>
<script src="../assets/backend/plugins/jquery-jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="../assets/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="../assets/backend/js/demo/dashboard.min.js"></script>
<script src="{{ asset('assets/plugins/gritter/js/jquery.gritter.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-sweetalert/sweetalert.min.js') }}"></script>
<!-- ================== END PAGE LEVEL JS ================== -->

<script>
    $(document).ready(function() {
        App.init();
        Dashboard.init();
    });
</script>

@section('custom-js')
@show

</body>
</html>
