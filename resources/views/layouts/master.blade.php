<!DOCTYPE html>
<!--[if IE 8]> <html lang="{{ app()->getLocale() }}" class="ie8"> <![endif]-->

<!--[if !IE]><!-->
<html lang="{{ app()->getLocale() }}">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <title>@yield('title') | TryWork</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <link href="{{ asset('assets/frontend/plugins/bootstrap3/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/frontend/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/frontend/plugins/animate/animate.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/frontend/css/e-commerce/style.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/frontend/css/e-commerce/style-responsive.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/frontend/css/e-commerce/theme/default.css') }}" id="theme" rel="stylesheet" />
    <!-- ================== END BASE CSS STYLE ================== -->

    <!-- ================== BEGIN EXTRA CSS STYLE ================== -->
@section('css')
@show
<!-- ================== END EXTRA CSS STYLE ================== -->

    <!-- ================== BEGIN BASE JS ================== -->
    <script src="{{ asset('assets/frontend/plugins/pace/pace.min.js') }}"></script>
    <!-- ================== END BASE JS ================== -->
</head>
<body>
<!-- BEGIN #page-container -->
<div id="page-container" class="fade">
{{--<!-- BEGIN #top-nav -->--}}
{{--<div id="top-nav" class="top-nav">--}}
{{--<!-- BEGIN container -->--}}
{{--<div class="container">--}}
{{--<div class="collapse navbar-collapse">--}}
{{--<ul class="nav navbar-nav">--}}
{{--<li><a href="#">Link #1</a></li>--}}
{{--<li><a href="#">Link #2</a></li>--}}
{{--</ul>--}}
{{--<ul class="nav navbar-nav navbar-right">--}}
{{--<li><a href="#">Link #3</a></li>--}}
{{--<li><a href="#">Link #4</a></li>--}}
{{--<li><a href="#">Link #5</a></li>--}}
{{--<li><a href="#"><i class="fa fa-facebook f-s-14"></i></a></li>--}}
{{--<li><a href="#"><i class="fa fa-twitter f-s-14"></i></a></li>--}}
{{--<li><a href="#"><i class="fa fa-instagram f-s-14"></i></a></li>--}}
{{--<li><a href="#"><i class="fa fa-dribbble f-s-14"></i></a></li>--}}
{{--<li><a href="#"><i class="fa fa-google-plus f-s-14"></i></a></li>--}}
{{--</ul>--}}
{{--</div>--}}
{{--</div>--}}
{{--<!-- END container -->--}}
{{--</div>--}}
{{--<!-- END #top-nav -->--}}

<!-- BEGIN #header -->
    <div id="header" class="header" data-fixed-top="true">
        <!-- BEGIN container -->
        <div class="container">
            <!-- BEGIN header-container -->
            <div class="header-container">
                <!-- BEGIN navbar-header -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="header-logo">
                        <a href="{{ route('home') }}">
                            <img src="{{ asset('assets/images/logo-light.png') }}" style="height: 30px"/>
                            <span>Try</span>Work
                            <small style="font-size: 9px">Student Training. Gain Experience. Work Ready.</small>
                        </a>
                    </div>
                </div>
                <!-- END navbar-header -->
                <!-- BEGIN header-nav -->
                <div class="header-nav">
                    <div class=" collapse navbar-collapse" id="navbar-collapse">
                        <ul class="nav">
                            @section('navbar')
                            @show
                        </ul>
                    </div>
                </div>
                <!-- END header-nav -->
                <!-- BEGIN header-nav -->
                <div class="header-nav">
                    <ul class="nav pull-right">
                            @section('account')
                            @show
                    </ul>
                </div>
                <!-- END header-nav -->
            </div>
            <!-- END header-container -->
        </div>
        <!-- END container -->
    </div>
    <!-- END #header -->

@section('content')
@show

<!-- BEGIN #footer -->
    <div id="footer" class="footer">
        <!-- BEGIN container -->
        <div class="container">
            <!-- BEGIN row -->
            <div class="row">
                <!-- BEGIN col-3 -->
                <div class="col-md-9">
                    <h4 class="footer-header">ABOUT US</h4>
                    <p>
                        Trywork: Internship Recommender Serivces System is a website made for Interns, Host Training Establishments, OJT Directors, Heads, and Coordinators. This benefits them by making their job easily, by making the system to help them with suggesting ideal interns/companies.
                    </p>
                    <p>
                        This site is under maintenance by SRG 7th Generation.
                    </p>
                </div>
                <!-- END col-3 -->
                <!-- BEGIN col-3 -->
                <div class="col-md-3">
                    <h4 class="footer-header">OUR CONTACT</h4>
                    <address>
                        <strong>TryWork</strong><br />
                        #123 Address<br />
                        City, Province, Zip Code <br /><br />
                        <abbr title="Phone">Phone:</abbr> (123) 456-7890<br />
                        <abbr title="Fax">Fax:</abbr> (123) 456-7891<br />
                        <abbr title="Email">Email:</abbr> <a href="mailto:admin@trywork.cf">admin@trywork.com</a><br />
                        <abbr title="Skype">Skype:</abbr> <a href="skype:trywork">trywork</a>
                    </address>
                </div>
                <!-- END col-3 -->
            </div>
            <!-- END row -->
        </div>
        <!-- END container -->
    </div>
    <!-- END #footer -->

    <!-- BEGIN #footer-copyright -->
    <div id="footer-copyright" class="footer-copyright">
        <!-- BEGIN container -->
        <div class="container">
            <div class="copyright">
                Copyright &copy; 2018 SRG 7th Generation. All rights reserved.
            </div>
        </div>
        <!-- END container -->
    </div>
    <!-- END #footer-copyright -->
</div>
<!-- END #page-container -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="{{ asset('assets/frontend/plugins/jquery/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('assets/frontend/plugins/bootstrap3/js/bootstrap.min.js') }}"></script>
<!--[if lt IE 9]>
<script src="{{ asset('assets/frontend/crossbrowserjs/html5shiv.js') }}"></script>
<script src="{{ asset('assets/frontend/crossbrowserjs/respond.min.js') }}"></script>
<script src="{{ asset('assets/frontend/crossbrowserjs/excanvas.min.js') }}"></script>
<![endif]-->
<script src="{{ asset('assets/frontend/plugins/js-cookie/js.cookie.js') }}"></script>
<script src="{{ asset('assets/frontend/js/e-commerce/apps.min.js') }}"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN EXTRA JS STYLE  ================== -->
@section('js')
@show
<!-- ================== END EXTRA JS STYLE ================== -->

<script>
    $(document).ready(function() {
        App.init();
    });
</script>

@section('custom-js')
@show
</body>
</html>
