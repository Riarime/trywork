@extends('layouts.master-backend')

@section('title', 'HTE Finder')

@section('css')
    <link href="{{ asset('assets/backend/plugins/jquery-jvectormap/jquery-jvectormap.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/bootstrap-calendar/css/bootstrap_calendar.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/gritter/css/jquery.gritter.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/nvd3/build/nv.d3.css') }}" rel="stylesheet" />
@endsection

@section('js')
    <script type="text/javascript" src="https://js.api.here.com/v3/3.0/mapsjs-core.js"></script>
    <script type="text/javascript" src="https://js.api.here.com/v3/3.0/mapsjs-service.js"></script>
    <script type="text/javascript" src="https://js.api.here.com/v3/3.0/mapsjs-ui.js"></script>
    <script type="text/javascript" src="https://js.api.here.com/v3/3.0/mapsjs-mapevents.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.2/d3.min.js"></script>
    <script src="{{ asset('assets/backend/plugins/nvd3/build/nv.d3.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-jvectormap/jquery-jvectormap.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-jvectormap/jquery-jvectormap-world-merc-en.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/bootstrap-calendar/js/bootstrap_calendar.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/gritter/js/jquery.gritter.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/bootstrap-sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/backend/js/demo/dashboard-v2.min.js') }}"></script>
    <script src="{{ asset('assets/backend/js/demo/timeline.demo.min.js') }}"></script>
    <script>
        var handleSweetNotification = function() {
            $('[data-click="swal-submit"]').click(function (e) {
                e.preventDefault();
                swal({
                    title: 'Are you sure?',
                    text: '',
                    icon: 'info',
                    buttons: {
                        cancel: {
                            text: 'Cancel',
                            value: null,
                            visible: true,
                            className: 'btn btn-default',
                            closeModal: true,
                        },
                        confirm: {
                            text: 'Submit',
                            value: true,
                            visible: true,
                            className: 'btn btn-lime text-black',
                            closeModal: true
                        }
                    }
                });
            });
        }
        var svgMarkup = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="16 16 32 32" xml:space="preserve"><path fill="${FILL}" class="path1" d="M8 2.1c1.1 0 2.2 0.5 3 1.3 0.8 0.9 1.3 1.9 1.3 3.1s-0.5 2.5-1.3 3.3l-3 3.1-3-3.1c-0.8-0.8-1.3-2-1.3-3.3 0-1.2 0.4-2.2 1.3-3.1 0.8-0.8 1.9-1.3 3-1.3z"></path><path fill="${STROKE}" class="path2" d="M8 15.8l-4.4-4.6c-1.2-1.2-1.9-2.9-1.9-4.7 0-1.7 0.6-3.2 1.8-4.5 1.3-1.2 2.8-1.8 4.5-1.8s3.2 0.7 4.4 1.9c1.2 1.2 1.8 2.8 1.8 4.5s-0.7 3.5-1.8 4.7l-4.4 4.5zM4 10.7l4 4.1 3.9-4.1c1-1.1 1.6-2.6 1.6-4.2 0-1.5-0.6-2.9-1.6-4s-2.4-1.7-3.9-1.7-2.9 0.6-4 1.7c-1 1.1-1.6 2.5-1.6 4 0 1.6 0.6 3.2 1.6 4.2v0z"></path><path fill="#fff" class="path3" d="M8 16l-4.5-4.7c-1.2-1.2-1.9-3-1.9-4.8 0-1.7 0.6-3.3 1.9-4.6 1.2-1.2 2.8-1.9 4.5-1.9s3.3 0.7 4.5 1.9c1.2 1.3 1.9 2.9 1.9 4.6 0 1.8-0.7 3.6-1.9 4.8l-4.5 4.7zM8 0.3c-1.6 0-3.2 0.7-4.3 1.9-1.2 1.2-1.8 2.7-1.8 4.3 0 1.7 0.7 3.4 1.8 4.5l4.3 4.5 4.3-4.5c1.1-1.2 1.8-2.9 1.8-4.5s-0.6-3.1-1.8-4.4c-1.2-1.1-2.7-1.8-4.3-1.8zM8 15.1l-4.1-4.2c-1-1.2-1.7-2.8-1.7-4.4s0.6-3 1.7-4.1c1.1-1.1 2.6-1.7 4.1-1.7s3 0.6 4.1 1.7c1.1 1.1 1.7 2.6 1.7 4.1 0 1.6-0.6 3.2-1.7 4.3l-4.1 4.3zM4.2 10.6l3.8 4 3.8-4c1-1 1.6-2.6 1.6-4.1s-0.6-2.8-1.6-3.9c-1-1-2.4-1.6-3.8-1.6s-2.8 0.6-3.8 1.6c-1 1.1-1.6 2.4-1.6 3.9 0 1.6 0.6 3.1 1.6 4.1v0z"></path></svg>';

        function addMarkerToGroup(group, coordinate, icon, html) {
            var marker = new H.map.Marker(coordinate, icon);
            // add custom data to the marker
            marker.setData(html);
            group.addObject(marker);
        }
        function addMarkersToMap(map) {
            var group = new H.map.Group();

            map.addObject(group);

            // add 'tap' event listener, that opens info bubble, to the group
            group.addEventListener('tap', function (evt) {
                // event target is the marker itself, group is a parent event target
                // for all objects that it contains
                var bubble =  new H.ui.InfoBubble(evt.target.getPosition(), {
                    // read custom data
                    content: evt.target.getData()
                });
                // show info bubble
                ui.addBubble(bubble);
            }, false);
            var internIcon = new H.map.Icon(
                svgMarkup.replace('${FILL}', 'orange').replace('${STROKE}', 'black')),
                internMarker = new H.map.Marker({lat:{{ $coordsIntern['lat'] }}, lng:{{ $coordsIntern['long'] }}}, {icon: internIcon});
//            internMarker.setData('<div><a href=\'http://www.mcfc.co.uk\' >Manchester City</a>' +
//                '</div><div >City of Manchester Stadium<br>Capacity: 48,000</div>');
            group.addObject(internMarker);
            @foreach($htes as $hte)
                    @if('ERROR' !== $coordsHTE[$hte->hte_id])
                    var hteMarker = new H.map.Marker({
                    lat:{{ $coordsHTE[$hte->hte_id]['lat'] }},
                    lng:{{ $coordsHTE[$hte->hte_id]['long'] }}});
                    hteMarker.setData('<div>{{ $hte->hte_name }}' +
                        '<br/>{{ $hte->hte_address }}</div>');
            group.addObject(hteMarker);
            @endif
            @endforeach
        }
        var platform = new H.service.Platform({
            app_id: 'xpiObBngnxcok8LPnJHB',
            app_code: 'xr-tZl551VoWIrRWLP8TOg',
            useCIT: true,
            useHTTPS: true
        });
        var defaultLayers = platform.createDefaultLayers();

        var map = new H.Map(document.getElementById('map'),
            defaultLayers.normal.map,{
                center: {lat:{{ $coordsIntern['lat'] }}, lng:{{ $coordsIntern['long'] }}},
                zoom: 12
        });

        var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));

        var ui = H.ui.UI.createDefault(map, defaultLayers);
        addMarkersToMap(map);

        $('head').append('<link rel="stylesheet" href="https://js.api.here.com/v3/3.0/mapsjs-ui.css" type="text/css" />');
    </script>
@endsection

@section('document.ready')
    handleSweetNotification();
    handleVisitorsAreaChart();
    handleVisitorsDonutChart();
    handleVisitorsVectorMap();
    handleScheduleCalendar();

    {{--setTimeout(function() {--}}
    {{--$.gritter.add({--}}
    {{--title: 'Welcome back, Intern!',--}}
    {{--text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus lacus ut lectus rutrum placerat.',--}}
    {{--image: '{{ asset('assets/backend/img/user/user-12.jpg') }}',--}}
    {{--sticky: true,--}}
    {{--time: '',--}}
    {{--class_name: 'my-sticky-class'--}}
    {{--});--}}
    {{--}, 1000);--}}
@endsection

@section('content')
    <div class="panel panel-inverse" data-sortable-id="ui-widget-1">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="#" class="btn btn-xs btn-icon btn-circle btn-default"
                   data-click="panel-expand">
                    <i class="fa fa-expand"></i>
                </a>
            </div>
            <h4 class="panel-title" style="font-size: 18px">HTE Finder Map</h4>
        </div>
        <div class="panel-body bg-silver bg-gradient-silver" align="center">
            <div id="map" style="width: 600px; height: 400px; background: grey"></div>
        </div>
    </div>


    <div class="panel panel-inverse" data-sortable-id="ui-widget-1">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="#" class="btn btn-xs btn-icon btn-circle btn-default"
                   data-click="panel-expand">
                    <i class="fa fa-expand"></i>
                </a>
            </div>
            <h4 class="panel-title" style="font-size: 18px">HTE Finder<br/><small>Internship Training Recommender Services</small></h1>
        </div>
        <div class="panel-body bg-silver bg-gradient-silver">
            {{--<div data-scrollbar="true" data-height="450px">--}}
                <div>
                <div class="row">
                    <!-- begin col-12 -->
                    <div class="col-md-12">
                        <form class="row" id="frm-logout" action="{{ route('intern.hte-finder') }}" method="GET">
                            <div class="col-md-6">
                                <div class="m-b-10 f-s-10 m-t-5"><b class="text-inverse" style="font-size: 12px">List of Suggested Host Training Establishments for you (around <i><u><a href="#modal-geo-changer" class="link" data-toggle="modal">{{ \App\Region::where('region_id', $region_id)->first()->name }}</a></u></i>):</b></div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="query">
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-primary dropdown-toggle no-caret">
                                                Search
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- begin result-container -->
                        <div class="result-container">
                            <!-- begin result-list -->
                            <ul class="result-list">
                                @foreach($htes as $hte)
                                    @if(isset($final_recommendation) && $region_id == $hte->region_id && $ojts->whereIn('description', $final_recommendation)->where('hte_id', $hte->hte_id))
                                        <li>
                                            <div class="result-price" style="background-color: #e4e4e4">
                                                {{--@if(collect($final_recommendation)->whereIn(0, $ojts->pluck('description')->toArray()))--}}
                                                    {{--{{ collect($final_recommendation)->whereIn(0, $ojts->pluck('description')->toArray())->count() }}<br/><small style="color: #101010;"> number of job trainings available</small>--}}
                                                {{--@else--}}
                                                    {{--0<br/><small style="color: #101010;"> number of job trainings available</small>--}}
                                                {{--@endif--}}
{{--                                                {{ (\App\Region::getCoordinates($hte->address.' '.\App\Region::where('region_id', $hte->region_id)->first()->name) == 'ERROR') ? 'ERROR' : floatval(\App\Region::getDistance(\App\Region::getCoordinates(\App\UnivIntern::where('info_id', \App\UserRole::where('user_id', \Illuminate\Support\Facades\Auth::user()->id)->first()->info_id)->first()->intern_address)['lat'], \App\Region::getCoordinates(\App\UnivIntern::where('info_id', \App\UserRole::where('user_id', \Illuminate\Support\Facades\Auth::user()->id)->first()->info_id)->first()->intern_address)['long'], \App\Region::getCoordinates($hte->address.' '.\App\Region::where('region_id', $hte->region_id)->first()->name)['lat'], \App\Region::getCoordinates($hte->address.' '.\App\Region::where('region_id', $hte->region_id)->first()->name)['long'])['response']['matrixEntry'][0]['summary']['distance'] / 1000) }}km--}}
                                                {{ ($hte->distance !== floatval(999999)) ? $hte->distance.'km' : 'N/A' }}<br/><small style="color: #101010;">Distance between you and the HTE</small>
                                                <a href="#hte{{ $hte->hte_id }}" class="btn btn-yellow btn-block" data-toggle="modal">View Details</a>
                                            </div>
                                            <a href="#" class="result-image " style="background-image: url({{ Avatar::create($hte->hte_name)->toBase64() }})"></a>
                                            <div class="result-info">
                                                <h4 class="title"><a href="{{ route('view-hte.show', $hte->hte_id) }}">{{ $hte->hte_name }}</a></h4>
                                                <p class="location">&nbsp;<br/><b>Available Job Training:</b></p>
                                                <p class="desc">
                                                    There are <b>
                                                        {{--{{ collect($final_recommendation)->whereIn(0, $ojts->pluck('description')->toArray())->count() }}--}}
                                                        {{ $ojts->where('hte', $hte->hte_id)->whereIn('description', $descriptions)->count() }}
                                                    </b> available job trainings in this company ({{ $ojts->where('hte', $hte->hte_id)->count() }} total).
                                                </p>
                                                <span><small><b>Address:</b> {{ $hte->address }}, {{ \App\Region::where('region_id', $hte->region_id)->first()->name }}</small></span>
                                            </div>
                                        </li>
                                    <br/>
                                        <div class="modal fade modal-message" id="hte{{ $hte->hte_id }}">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">Suggested Job Trainings for you, from {{ $hte->hte_name }}</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    </div>
                                                   <div class="modal-body">
                                                        <table id="data-table-default" class="table table-striped table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th class="text-nowrap">Job Training Name</th>
                                                                <th class="text-nowrap">Description</th>
                                                                <th class="text-nowrap">Actions</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @if(isset($final_recommendation))
                                                                @foreach($ojts->where('hte', $hte->hte_id) as $ojt)
                                                                    @foreach($final_recommendation as $item)
                                                                        @if($item[0] == $ojt->description && \App\HTEJobTrainings::where('hte_id', $hte->hte_id)->get()->where('job_training_id', $ojt->job_id)->first())
                                                                            <tr>
                                                                                <td>
                                                                                    {{ $ojt->ojt_name }}
                                                                                </td>
                                                                                <td>
                                                                                    {{ $ojt->description }}
                                                                                </td>
                                                                                <td>
                                                                                    @if(\App\InternApplication::where('intern_id', \App\UnivIntern::where('info_id', \App\UserRole::where('user_id', \Auth::user()->id)->first()->info_id)->first()->intern_id)->where('job_training_id', \App\HTEJobTrainings::where('training_code', $ojt->code)->first()->job_training_id)->first())
                                                                                    <form id="frm-logout" action="{{ route('application-controller.store') }}" method="POST">
                                                                                        {{ csrf_field() }}
                                                                                        <input type="hidden" name="code" value="{{ $ojt->code }}"/>
                                                                                        <button class="btn btn-block btn-secondary" type="submit" disabled="disabled">Application Submitted</button>
                                                                                    </form>
                                                                                        @else
                                                                                        <form id="frm-logout" action="{{ route('application-controller.store') }}" method="POST">
                                                                                            {{ csrf_field() }}
                                                                                            <input type="hidden" name="code" value="{{ $ojt->code }}"/>
                                                                                            <button class="btn btn-block btn-lime" type="submit">Submit Application</button>
                                                                                        </form>
                                                                                        @endif
                                                                                </td>
                                                                            </tr>
                                                                        @endif
                                                                    @endforeach
                                                                @endforeach
                                                            @else
                                                                No available Job Trainings for you right now. Please try again later
                                                            @endif
                                                            </tbody>
                                                        </table>
                                                       <p><b>All Job Trainings for {{ $hte->hte_name }}</b></p>
                                                       <table id="data-table-default" class="table table-striped table-bordered">
                                                           <thead>
                                                           <tr>
                                                               <th class="text-nowrap">Job Training Name</th>
                                                               <th class="text-nowrap">Description</th>
                                                               <th class="text-nowrap">Actions</th>
                                                           </tr>
                                                           </thead>
                                                           <tbody>
                                                               @foreach($ojts->where('hte', $hte->hte_id)->whereNotIn('description', array_column($final_recommendation, 0)) as $ojt)
                                                                           <tr>
                                                                               <td>
                                                                                   {{ $ojt->ojt_name }}
                                                                               </td>
                                                                               <td>
                                                                                   {{ $ojt->description }}
                                                                               </td>
                                                                               <td>
                                                                                   @if(\App\InternApplication::where('intern_id', \App\UnivIntern::where('info_id', \App\UserRole::where('user_id', \Auth::user()->id)->first()->info_id)->first()->intern_id)->where('job_training_id', \App\HTEJobTrainings::where('training_code', $ojt->code)->first()->job_training_id)->first())
                                                                                       <form id="frm-logout" action="{{ route('application-controller.store') }}" method="POST">
                                                                                           {{ csrf_field() }}
                                                                                           <input type="hidden" name="code" value="{{ $ojt->code }}"/>
                                                                                           <button class="btn btn-block btn-secondary" type="submit" disabled="disabled">Application Submitted</button>
                                                                                       </form>
                                                                                   @else
                                                                                       <form id="frm-logout" action="{{ route('application-controller.store') }}" method="POST">
                                                                                           {{ csrf_field() }}
                                                                                           <input type="hidden" name="code" value="{{ $ojt->code }}"/>
                                                                                           <button class="btn btn-block btn-lime" type="submit">Submit Application</button>
                                                                                       </form>
                                                                                   @endif
                                                                               </td>
                                                                           </tr>
                                                                   @endforeach
                                                           </tbody>
                                                       </table>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </ul>
                            <!-- end result-list -->
                        </div>
                        <!-- end result-container -->
                    </div>
                    <!-- end col-12 -->
                </div>
            </div>
        </div>
    </div>
    </div>

    <div class="modal fade" id="modal-geo-changer">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">&nbsp</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    @foreach(\App\Region::all() as $region)
                        <div class="row">
                            <a class="link block" href="{{ route('intern.hte-finder', $region->region_id) }}">{{ $region->name }}</a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection