@extends('layouts.master-backend')

@section('title', $hte->hte_name)

@section('css')
    <link href="{{ asset('assets/backendplugins/superbox/css/superbox.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/lity/dist/lity.min.css') }}" rel="stylesheet" />
@endsection

@section('js')
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
    <script src="{{ asset('assets/backend/plugins/superbox/js/jquery.superbox.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/lity/dist/lity.min.js') }}"></script>
    <script src="{{ asset('assets/backend/js/demo/profile.demo.min.js') }}"></script>
@endsection

@section('document.ready')

@endsection

@section('content')
    <div class="profile">
        <div class="profile-header">
            <!-- BEGIN profile-header-cover -->
            <div class="profile-header-cover"></div>
            <!-- END profile-header-cover -->
            <!-- BEGIN profile-header-content -->
            <div class="profile-header-content">
                <!-- BEGIN profile-header-img -->
                <div class="profile-header-img">
                    <img src="{{ Avatar::create($hte->hte_name)->toBase64() }}" alt="">
                </div>
                <!-- END profile-header-img -->
                <!-- BEGIN profile-header-info -->
                <div class="profile-header-info">
                    <h4 class="m-t-10 m-b-5">{{ $hte->hte_name }}</h4>
                    <p class="m-b-10">{{ $hte->address1.', '.$hte->address2.', '.\App\Region::where('region_id', $hte->region_id)->first()->name }}</p>
                    <br/>
                    <span class="label label-primary" style="font-size: 12px;"><i class="fa fa-check-circle"></i> Verified Partner</span>
                </div>
                <!-- END profile-header-info -->
            </div>
            <!-- END profile-header-content -->
            <!-- BEGIN profile-header-tab -->
            <ul class="profile-header-tab nav nav-tabs">
                <li class="nav-item"><a href="#profile-job-trainings" class="nav-link active" data-toggle="tab">JOB TRAININGS</a></li>
            </ul>
            <!-- END profile-header-tab -->
        </div>
    </div>
    <!-- end profile -->
    <!-- begin profile-content -->
    <div>
        <div class="tab-content">
            <div class="tab-pane fade show active" id="profile-job-trainings">
                <div data-scrollbar="true" data-height="310px">
                    <ul class="media-list media-list-with-divider media-messaging">
                        @foreach($job_trainings as $job_training)
                            <li class="media media-sm">
                                <a href="#" class="pull-left">
                                    <img src="{{ Avatar::create(\App\JobTraining::where('job_training_id', $job_training->job_training_id)->first()->job_training_name)->toBase64() }}" alt="" class="media-object rounded-corner">
                                </a>
                                <div class="media-body">
                                    <a href="#"><h5 class="media-heading">{{ \App\JobTraining::where('job_training_id', $job_training->job_training_id)->first()->job_training_name }}</h5></a>
                                    <p>{{ \App\JobTraining::where('job_training_id', $job_training->job_training_id)->first()->description }}</p>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <!-- end profile-content -->
@endsection