@extends('layouts.master-backend-empty')

@section('title', 'Profile Wizard')

@section('css')
    <link href="{{ asset('assets/backend/plugins/jquery-smart-wizard/src/css/smart_wizard.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" />
@endsection

@section('js')
    <script src="{{ asset('assets/backend/plugins/jquery-smart-wizard/src/js/jquery.smartWizard.js') }}"></script>
    <script src="{{ asset('assets/backend/js/demo/form-wizards.demo.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/select2/dist/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/backend/js/demo/form-plugins.demo.min.js') }}"></script>
@endsection

@section('document.ready')
    FormWizard.init();
    handleSelect2();
@endsection

@section('content')
    <!-- begin page-header -->
    <h1 class="page-header">Setup your Profile <small>to proceed to i-Interno</small></h1>
    <!-- end page-header -->
    <!-- begin wizard-form -->
    <form action="{{ route('profile-wizard.store') }}" method="POST" class="form-control-with-bg">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="POST"/>
        <!-- begin wizard -->
        <div id="wizard">
            <!-- begin wizard-step -->
            <ul>
                <li class="col-md-3 col-sm-4 col-6">
                    <a href="#step-1">
                        <span class="number">1</span>
                        <span class="info text-ellipsis">
									Personal Info
									<small class="text-ellipsis">Name, Address, DOB, and Gender</small>
								</span>
                    </a>
                </li>
                <li class="col-md-3 col-sm-4 col-6">
                    <a href="#step-2">
                        <span class="number">2</span>
                        <span class="info text-ellipsis">
									State University/College Info
									<small class="text-ellipsis">Name, Address and Contact Number</small>
								</span>
                    </a>
                </li>
                <li class="col-md-3 col-sm-4 col-6">
                    <a href="#step-3">
                        <span class="number">3</span>
                        <span class="info text-ellipsis">
									Skills
									<small class="text-ellipsis">List your skills</small>
								</span>
                    </a>
                </li>
                <li class="col-md-3 col-sm-4 col-6">
                    <a href="#step-4">
                        <span class="number">4</span>
                        <span class="info text-ellipsis">
									Completed
									<small class="text-ellipsis">Complete Registration</small>
								</span>
                    </a>
                </li>
            </ul>
            <!-- end wizard-step -->
            <!-- begin wizard-content -->
            <div>
                <!-- begin step-1 -->
                <div id="step-1">
                    <!-- begin fieldset -->
                    <fieldset>
                        <!-- begin row -->
                        <div class="row">
                            <!-- begin col-8 -->
                            <div class="col-md-8 offset-md-2">
                                <legend class="no-border f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse">Personal info about yourself</legend>
                                <!-- begin form-group row -->
                                <div class="form-group row m-b-10">
                                    <label class="col-md-3 text-md-right col-form-label">First Name</label>
                                    <div class="col-md-6">
                                        <input type="text" name="firstname" placeholder="John" class="form-control" />
                                    </div>
                                </div>
                                <!-- end form-group row -->
                                <!-- begin form-group row -->
                                <div class="form-group row m-b-10">
                                    <label class="col-md-3 text-md-right col-form-label">Middle Name</label>
                                    <div class="col-md-6">
                                        <input type="text" name="middlename" placeholder="F." class="form-control" />
                                    </div>
                                </div>
                                <!-- end form-group row -->
                                <!-- begin form-group row -->
                                <div class="form-group row m-b-10">
                                    <label class="col-md-3 text-md-right col-form-label">Last Name</label>
                                    <div class="col-md-6">
                                        <input type="text" name="lastname" placeholder="Smith" class="form-control" />
                                    </div>
                                </div>
                                <!-- end form-group row -->
                                <!-- begin form-group row -->
                                <div class="form-group row m-b-10">
                                    <label class="col-md-3 text-md-right col-form-label">Date of Birth</label>
                                    <div class="col-md-6">
                                        <div class="row row-space-6">
                                            <div class="col-4">
                                                <select class="form-control" name="year">
                                                    <option selected disabled>Year</option>
                                                    @for($i=2018; $i>1950; $i--)
                                                        <option>{{ $i }}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                            <div class="col-4">
                                                <select class="form-control" name="month">
                                                    <option selected disabled>Month</option>
                                                    @for($i=1; $i<=12; $i++)
                                                        <option>{{ $i }}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                            <div class="col-4">
                                                <select class="form-control" name="day">
                                                    <option selected disabled>Day</option>
                                                    @for($i=1; $i<=31; $i++)
                                                        <option>{{ $i }}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end form-group row -->
                                <!-- begin form-group row -->
                                <div class="form-group row m-b-10">
                                    <label class="col-md-3 text-md-right col-form-label">Address</label>
                                    <div class="col-md-9">
                                        <input type="text" name="address1" placeholder="Address 1" class="form-control m-b-10" />
                                        <input type="text" name="address2" placeholder="Address 2" class="form-control" />
                                    </div>
                                </div>
                                <!-- end form-group row -->
                                <!-- begin form-group row -->
                                <div class="form-group row m-b-10">
                                    <label class="col-md-3 text-md-right col-form-label">Gender</label>
                                    <div class="col-md-6">
                                        <div class="form-group" style="margin-top: 2%">
                                            <input type="radio" id="male" name="gender" value="male" />
                                            <label for="male">Male</label>
                                            <input type="radio" id="female" name="gender" value="female" />
                                            <label for="female">Female</label>
                                        </div>
                                    </div>
                                </div>
                                <!-- end form-group row -->
                            </div>
                            <!-- end col-8 -->
                        </div>
                        <!-- end row -->
                    </fieldset>
                    <!-- end fieldset -->
                </div>
                <!-- end step-1 -->
                <!-- begin step-2 -->
                <div id="step-2">
                    <!-- begin fieldset -->
                    <fieldset>
                        <!-- begin row -->
                        <div class="row">
                            <!-- begin col-8 -->
                            <div class="col-md-8 offset-md-2">
                                <legend class="no-border f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse">State University/College Information</legend>
                                <!-- begin form-group row -->
                                <div class="form-group row m-b-10">
                                    <label class="col-md-3 text-md-right col-form-label">State University/College Name</label>
                                    <div class="col-md-6">
                                        <select class="default-select2 form-control">
                                            <option selected disabled>Select State University/College</option>
                                            {{--@foreach($sucs as $suc)--}}
                                            {{--<option value="{{ $suc->id }}">{{ $suc->name }}</option>--}}
                                            {{--@endforeach--}}
                                        </select>
                                    </div>
                                </div>
                                <!-- end form-group row -->
                                <!-- begin form-group row -->
                                <div class="form-group row m-b-10">
                                    <label class="col-md-3 text-md-right col-form-label">Course/Program Enrolled/Major</label>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select class="default-select2 form-control" name="sucmajor">
                                                <option selected disabled>Course/Program/Major</option>
                                                {{--@foreach($courses as $course)--}}
                                                {{--<option value="{{ $course->id }}">{{ $programs->where('id', $course->course_program_id)->first()->name }} </option>--}}
                                                {{--@endforeach--}}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <!-- end form-group row -->
                            </div>
                            <!-- end col-8 -->
                        </div>
                        <!-- end row -->
                    </fieldset>
                    <!-- end fieldset -->
                </div>
                <!-- end step-2 -->
                <!-- begin step-3 -->
                <div id="step-3">
                    <!-- begin fieldset -->
                    <fieldset>
                        <!-- begin row -->
                        <div class="row">
                            <!-- begin col-8 -->
                            <div class="col-md-8 offset-md-2">
                                <legend class="no-border f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse">Choose all the applicable skills you have.</legend>
                                <!-- begin form-group row -->
                                <div class="form-group row m-b-10">
                                    <label class="col-md-3 text-md-right col-form-label">Skill List</label>
                                    <div class="col-md-6">
                                        <select class="multiple-select2 form-control" multiple="multiple" name="skills[]">
                                            {{--@foreach($skills as $skill)--}}
                                            {{--<option value="{{ $skill->id }}">{{ $skill->name }} </option>--}}
                                            {{--@endforeach--}}
                                        </select>
                                    </div>
                                </div>
                                <!-- end form-group row -->
                            </div>
                            <!-- end col-8 -->
                        </div>
                        <!-- end row -->
                    </fieldset>
                    <!-- end fieldset -->
                </div>
                <!-- end step-3 -->
                <!-- begin step-4 -->
                <div id="step-4">
                    <div class="jumbotron m-b-0 text-center">
                        <h2 class="text-inverse">Confirm Profile</h2>
                        <p class="m-b-30 f-s-16">Please double check all of your details before you proceed. <br /><small>Note: You can change later the details in the 'Edit Profile' tab in your account</small></p>
                        <div class="form-group">
                            <input type="checkbox" id="confirm" name="confirm" required />
                            <label for="confirm">I hereby confirm the details I entered and I want to proceed now.</label>
                        </div>
                        <p><button type="submit" class="btn btn-primary btn-lg">Submit Details</button></p>
                    </div>
                </div>
                <!-- end step-4 -->
            </div>
            <!-- end wizard-content -->
        </div>
        <!-- end wizard -->
    </form>
    <!-- end wizard-form -->
@endsection