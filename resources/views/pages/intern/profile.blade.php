@extends('layouts.master-backend')

@section('title', 'Your Profile')

@section('css')
    <link href="{{ asset('assets/backendplugins/superbox/css/superbox.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/lity/dist/lity.min.css') }}" rel="stylesheet" />
@endsection

@section('js')
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
    <script src="{{ asset('assets/backend/plugins/superbox/js/jquery.superbox.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/lity/dist/lity.min.js') }}"></script>
    <script src="{{ asset('assets/backend/js/demo/profile.demo.min.js') }}"></script>
@endsection

@section('document.ready')

@endsection

@section('content')
    <div class="profile">
        <div class="profile-header">
            <!-- BEGIN profile-header-cover -->
            <div class="profile-header-cover"></div>
            <!-- END profile-header-cover -->
            <!-- BEGIN profile-header-content -->
            <div class="profile-header-content">
                <!-- BEGIN profile-header-img -->
                <div class="profile-header-img">
                    <img src="{{ Avatar::create(\App\User::getName())->toBase64() }}" alt="">
                </div>
                <!-- END profile-header-img -->
                <!-- BEGIN profile-header-info -->
                <div class="profile-header-info">
                    <h4 class="m-t-10 m-b-5">{{ ucwords(strtolower(\App\User::getName())) }}</h4>
                    <p class="m-b-10">{{ \App\User::getProgram() }}</p>
                    <p class="m-b-10 label label-primary">{{ \App\User::getSUC() }}</p> <p class="m-b-10 label label-primary">{{ \App\User::getSUCAbbrev() }}</p>
                    <br/>
                    <br/>
                </div>
                <!-- END profile-header-info -->
            </div>
            <!-- END profile-header-content -->
            <!-- BEGIN profile-header-tab -->
            <ul class="profile-header-tab nav nav-tabs">
                <li class="nav-item"><a href="#profile-about" class="nav-link active" data-toggle="tab">ABOUT</a></li>
            </ul>
            <!-- END profile-header-tab -->
        </div>
    </div>
    <!-- end profile -->
    <!-- begin profile-content -->
    <div>
        <div class="tab-content">
            <div class="tab-pane fade show active" id="profile-about">
                <div class="table-responsive">
                    <table class="table table-profile">
                        <thead>
                        <tr>
                            <th></th>
                            <th>
                                <h4>{{ ucwords(strtolower(\App\User::getName())) }} <small class="text-muted">Taking <i style="color:#000">{{ \App\User::getProgram() }}</i> at <i style="color:#000">{{ \App\User::getSUC() }}</i></small></h4>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="field">Contact Number</td>
                            <td><i class="fa fa-mobile fa-lg m-r-5"></i> {{ \App\UserInfo::where('info_id', \App\UserRole::where('user_id', \Auth::user()->id)->first()->info_id)->first()->cel_no }}</td>
                        </tr>
                        <tr>
                            <td class="field">Region</td>
                            <td>
                                {{ \App\Region::where('region_id', \App\UnivIntern::where('info_id', \App\UserRole::where('user_id', \Auth::user()->id)->first()->info_id)->first()->region_id)->first()->code }} - {{ \App\Region::where('region_id', \App\UnivIntern::where('info_id', \App\UserRole::where('user_id', \Auth::user()->id)->first()->info_id)->first()->region_id)->first()->name }}
                            </td>
                        </tr>
                        <tr>
                            <td class="field">Address</td>
                            <td>{{ \App\UnivIntern::where('info_id', \App\UserRole::where('user_id', \Auth::user()->id)->first()->info_id)->first()->intern_address }}</td>
                        </tr>
                        <tr class="divider">
                            <td colspan="2"></td>
                        </tr>
                        <tr class="highlight">
                            <td class="field">Skills</td>
                            <td>
                                <ul>
                                    @foreach(\App\User::getStudentSkills() as $skill)
                                        <li>{{ $skill }}</li>
                                    @endforeach
                                </ul>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- end profile-content -->
@endsection