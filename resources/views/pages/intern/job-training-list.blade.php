@extends('layouts.master-backend')

@section('title', 'Complete Job Training List')

@section('css')
    <link href="{{ asset('assets/backend/plugins/jquery-jvectormap/jquery-jvectormap.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/bootstrap-calendar/css/bootstrap_calendar.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/gritter/css/jquery.gritter.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/nvd3/build/nv.d3.css') }}" rel="stylesheet" />
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.2/d3.min.js"></script>
    <script src="{{ asset('assets/backend/plugins/nvd3/build/nv.d3.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-jvectormap/jquery-jvectormap.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-jvectormap/jquery-jvectormap-world-merc-en.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/bootstrap-calendar/js/bootstrap_calendar.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/gritter/js/jquery.gritter.js') }}"></script>
    <script src="{{ asset('assets/backend/js/demo/dashboard-v2.min.js') }}"></script>
@endsection

@section('document.ready')
    handleVisitorsAreaChart();
    handleVisitorsDonutChart();
    handleVisitorsVectorMap();
    handleScheduleCalendar();

    {{--setTimeout(function() {--}}
    {{--$.gritter.add({--}}
    {{--title: 'Welcome back, Intern!',--}}
    {{--text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus lacus ut lectus rutrum placerat.',--}}
    {{--image: '{{ asset('assets/backend/img/user/user-12.jpg') }}',--}}
    {{--sticky: true,--}}
    {{--time: '',--}}
    {{--class_name: 'my-sticky-class'--}}
    {{--});--}}
    {{--}, 1000);--}}
@endsection

@section('content')
    <div class="row">
        <!-- begin col-6 -->
        <div class="col-md-12">
            <div>
                <h4>All Job Training Listings</h4>
            </div>
            <div class="row">
                @foreach($ojts as $ojt)
                    <div class="col-md-4">
                        <!-- begin card -->
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">{{ $ojt->ojtname }}<br/><small>{{ $ojt->company_name }}</small></h4>
                                <p class="card-text">{{ $ojt->description }}</p>
                                <button class="btn btn-primary">Details</button>
                                <p class="card-text" style="text-align: right"><small class="text-muted">Start Date: {{ $ojt->date }}</small></p>
                            </div>
                        </div>
                        <!-- end card -->
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection