@extends('layouts.master-backend')

@section('title', 'HTE Finder')

@section('css')
    <link href="{{ asset('assets/backend/plugins/jquery-jvectormap/jquery-jvectormap.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/bootstrap-calendar/css/bootstrap_calendar.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/gritter/css/jquery.gritter.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/nvd3/build/nv.d3.css') }}" rel="stylesheet" />
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.2/d3.min.js"></script>
    <script src="{{ asset('assets/backend/plugins/nvd3/build/nv.d3.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-jvectormap/jquery-jvectormap.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-jvectormap/jquery-jvectormap-world-merc-en.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/bootstrap-calendar/js/bootstrap_calendar.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/gritter/js/jquery.gritter.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/bootstrap-sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/backend/js/demo/dashboard-v2.min.js') }}"></script>
    <script src="{{ asset('assets/backend/js/demo/timeline.demo.min.js') }}"></script>
    <script>
        var handleSweetNotification = function() {
            $('[data-click="swal-submit"]').click(function (e) {
                e.preventDefault();
                swal({
                    title: 'Are you sure?',
                    text: '',
                    icon: 'info',
                    buttons: {
                        cancel: {
                            text: 'Cancel',
                            value: null,
                            visible: true,
                            className: 'btn btn-default',
                            closeModal: true,
                        },
                        confirm: {
                            text: 'Submit',
                            value: true,
                            visible: true,
                            className: 'btn btn-lime text-black',
                            closeModal: true
                        }
                    }
                });
            });
        }
    </script>
@endsection

@section('document.ready')
    handleSweetNotification();
    handleVisitorsAreaChart();
    handleVisitorsDonutChart();
    handleVisitorsVectorMap();
    handleScheduleCalendar();

    {{--setTimeout(function() {--}}
    {{--$.gritter.add({--}}
    {{--title: 'Welcome back, Intern!',--}}
    {{--text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus lacus ut lectus rutrum placerat.',--}}
    {{--image: '{{ asset('assets/backend/img/user/user-12.jpg') }}',--}}
    {{--sticky: true,--}}
    {{--time: '',--}}
    {{--class_name: 'my-sticky-class'--}}
    {{--});--}}
    {{--}, 1000);--}}
@endsection

@section('content')
    <div class="panel panel-inverse" data-sortable-id="ui-widget-1">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="#" class="btn btn-xs btn-icon btn-circle btn-default"
                   data-click="panel-expand">
                    <i class="fa fa-expand"></i>
                </a>
            </div>
            <h4 class="panel-title" style="font-size: 18px">HTE Finder<br/><small>Search Results</small></h1>
        </div>
        <div class="panel-body bg-silver bg-gradient-silver">
            {{--<div data-scrollbar="true" data-height="450px">--}}
                <div>
                <div class="row">
                    <!-- begin col-12 -->
                    <div class="col-md-12">
                        <form class="row" id="frm-logout" action="{{ route('intern.hte-finder') }}" method="GET">
                            <div class="col-md-6">
                                <p>Job Training results for <b><i>{{ $queryoriginal }}</i>, </b><a href="{{ route('intern.hte-finder') }}" class="link text-black">Go Back to HTE Finder</a>.</p>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="  form-control" name="query">
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-primary dropdown-toggle no-caret">
                                                Search
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- begin result-container -->
                        <div class="result-container">
                            <!-- begin result-list -->
                            <ul class="result-list">
                                @if(null !== $results->first())
                                @foreach($htes as $hte)
                                        @foreach($results->where('hte_id', $hte->hte_id) as $result)
                                            <li>
                                                <div class="result-price" style="background-color: #e4e4e4">
                                                    {{--@if(null !== $ojts->where('hte', $hte->id)->count())--}}
                                                    {{--{{ $ojts->where('hte', $hte->hte_id)->count() }}<br/><small style="color: #101010;"> number of job trainings available</small>--}}
                                                    {{--@else--}}
                                                    5<br/><small style="color: #101010;"> slots available</small>
                                                    {{--@endif--}}
                                                    @if(\App\InternApplication::where('intern_id', \App\UnivIntern::where('info_id', \App\UserRole::where('user_id', \Auth::user()->id)->first()->info_id)->first()->intern_id)->where('job_training_id', \App\HTEJobTrainings::where('training_code', $result->code)->first()->job_training_id)->first())
                                                        <form id="frm-logout" action="{{ route('application-controller.store') }}" method="POST">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="code" value="{{ $result->code }}"/>
                                                            <button class="btn btn-block btn-secondary" type="submit" disabled="disabled">Application Submitted</button>
                                                        </form>
                                                    @else
                                                        <form id="frm-logout" action="{{ route('application-controller.store') }}" method="POST">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="code" value="{{ $result->code }}"/>
                                                            <button class="btn btn-block btn-lime" type="submit">Submit Application</button>
                                                        </form>
                                                    @endif
                                                </div>
                                                <a href="#" class="result-image " style="background-image: url({{ Avatar::create($hte->hte_name)->toBase64() }})"></a>
                                                <div class="result-info">
                                                    <h4 class="title">{{ $result->ojt_name }}</h4>
                                                    <p class="location"><a href="{{ route('view-hte.show', $hte->hte_id) }}">{{ $hte->hte_name }}</a>&nbsp;<br/><br/><b>Job Description:</b></p>
                                                    <p class="desc">
                                                        {{ $result->description }}
                                                    </p>
                                                    <span><small><b>Address:</b> {{ $hte->address }}, {{ \App\Region::where('region_id', $hte->region_id)->first()->name }}</small></span>
                                                </div>
                                            </li>
                                            <br/>
                                        @endforeach
                                @endforeach
                                @else
                                    <li style="text-align: center">
                                        <br/>
                                        <br/>
                                        <br/>
                                        <h2 class="text-muted">NO RESULTS FOUND</h2>
                                        <br/>
                                        <br/>
                                        <br/>
                                    </li>
                                @endif
                            </ul>
                            <!-- end result-list -->
                        </div>
                        <!-- end result-container -->
                    </div>
                    <!-- end col-12 -->
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection