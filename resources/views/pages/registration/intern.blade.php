@extends('layouts.master-backend-empty')

@section('title', 'Profile Wizard')

@section('css')
    <link href="{{ asset('assets/backend/plugins/jquery-smart-wizard/src/css/smart_wizard.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" />
@endsection

@section('js')
    <script src="{{ asset('assets/backend/plugins/jquery-smart-wizard/src/js/jquery.smartWizard.js') }}"></script>
    <script src="{{ asset('assets/backend/js/demo/form-wizards.demo.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/select2/dist/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/backend/js/demo/form-plugins.demo.min.js') }}"></script>
@endsection

@section('document.ready')
    FormWizard.init();
    handleSelect2();
@endsection

@section('content')
    <!-- begin page-header -->
    <h1 class="page-header">Complete your Profile <small>to proceed to Try-work</small></h1>
    <!-- end page-header -->
    <!-- begin wizard-form -->
    <form action="{{ route('internReg') }}" method="POST" class="form-control-with-bg">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="POST"/>
        <input type="hidden" name="intern_id" value="{{$resp[0]->intern_id}}"/>
        <input type="hidden" name="info_id" value="{{$resp[0]->info_id}}"/>
        <input class="hidden" style="display: none" name="referral_code" value="{{$resp[0]->referral_code}}"/>
        <input class="hidden" style="display: none" name="email" value="{{$resp[0]->contact_email}}"/>
        <!-- begin wizard -->
        <div id="wizard">
            <!-- begin wizard-step -->
            <ul>
                <li class="col-md-3 col-sm-4 col-6">
                    <a href="#step-1">
                        <span class="number">1</span>
                        <span class="info text-ellipsis">
									Personal Info
									<small class="text-ellipsis">Name, Address, DOB, and Gender</small>
								</span>
                    </a>
                </li>
                <li class="col-md-3 col-sm-4 col-6">
                    <a href="#step-2">
                        <span class="number">2</span>
                        <span class="info text-ellipsis">
									State University/College Info
									<small class="text-ellipsis">Name, Address and Contact Number</small>
								</span>
                    </a>
                </li>
                <li class="col-md-3 col-sm-4 col-6">
                    <a href="#step-3">
                        <span class="number">3</span>
                        <span class="info text-ellipsis">
									Skills
									<small class="text-ellipsis">List your skills</small>
								</span>
                    </a>
                </li>
                <li class="col-md-3 col-sm-4 col-6">
                    <a href="#step-4">
                        <span class="number">4</span>
                        <span class="info text-ellipsis">
									Completed
									<small class="text-ellipsis">Complete Registration</small>
								</span>
                    </a>
                </li>
            </ul>
            <!-- end wizard-step -->
            <!-- begin wizard-content -->
            <div>
                <!-- begin step-1 -->
                <div id="step-1">
                    <!-- begin fieldset -->
                    <fieldset>
                        <!-- begin row -->
                        <div class="row">
                            <!-- begin col-8 -->
                            <div class="col-md-8 offset-md-2">
                                <legend class="no-border f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse">Personal info about yourself</legend>
                                <!-- begin form-group row -->
                                <div class="form-group row m-b-10">
                                    <label class="col-md-3 text-md-right col-form-label">Student Number:</label>
                                    <div class="col-md-6">
                                        <label class="col-form-label">{{$resp[0]->stud_no}}</label>
                                    </div>
                                </div>
                                <div class="form-group row m-b-10">
                                    <label class="col-md-3 text-md-right col-form-label">Full Name:</label>
                                    <div class="col-md-6">
                                        <label class="col-form-label">{{$resp[0]->Stud_Name}}</label>
                                    </div>
                                </div>
                                <!-- end form-group row -->
                                <!-- begin form-group row -->

                                <div class="form-group row m-b-10">
                                    <label class="col-md-3 text-md-right col-form-label">Email Address:</label>
                                    <div class="col-md-6">
                                        <label class="col-form-label">{{$resp[0]->contact_email}}</label>
                                    </div>
                                </div>
                                <div class="form-group row m-b-10">
                                    <label class="col-md-3 text-md-right col-form-label">Gender:</label>
                                    <div class="col-md-6">
                                        <div class="form-group" style="margin-top: 2%">
                                            <input type="radio" id="male" name="gender" value="M" />
                                            <label for="male">Male</label>
                                            <input type="radio" id="female" name="gender" value="F" />
                                            <label for="female">Female</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row m-b-10">
                                    <label class="col-md-3 text-md-right col-form-label">Telephone No.:</label>
                                    <div class="col-md-6">
                                        <input type="text" name="telno" placeholder="Telephone No." class="form-control" value="{{$resp[0]->tel_no}}" />
                                    </div>
                                </div>
                                <div class="form-group row m-b-10">
                                    <label class="col-md-3 text-md-right col-form-label">Contact No.:</label>
                                    <div class="col-md-6">
                                        <input type="text" name="contactno" placeholder="Contact No." class="form-control"  value="{{$resp[0]->cel_no}}" />
                                    </div>
                                </div>
                                <!-- end form-group row -->
                                <!-- begin form-group row -->
                                <div class="form-group row m-b-10">
                                    <label class="col-md-3 text-md-right col-form-label">Date of Birth:</label>
                                    <div class="col-md-6">
                                        <div class="row row-space-6">
                                                <input type="date" name="bday" placeholder="Birth Date" class="form-control"  value="{{$resp[0]->bdate}}" >
                                        </div>
                                    </div>
                                </div>
                                <!-- end form-group row -->
                                <!-- begin form-group row -->
                                <div class="form-group row m-b-10">
                                    <label class="col-md-3 text-md-right col-form-label">Address:</label>
                                    <div class="col-md-6">
                                        <input type="text" name="address" placeholder="Address" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group row m-b-10">
                                    <label class="col-md-3 text-md-right col-form-label">Region:</label>
                                    <div class="col-md-6">
                                        <select class="multiple-select2 form-control"  name="region">
                                            @foreach(DB::select('select * from r_regions') as $reg)
                                                <option value="{{ $reg->region_id }}">{{ $reg->name }} </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!-- end col-8 -->
                        </div>
                        <!-- end row -->
                    </fieldset>
                    <!-- end fieldset -->
                </div>
                <!-- end step-1 -->
                <!-- begin step-2 -->
                <div id="step-2">
                    <!-- begin fieldset -->
                    <fieldset>
                        <!-- begin row -->
                        <div class="row">
                            <!-- begin col-8 -->
                            <div class="col-md-8 offset-md-2">
                                <legend class="no-border f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse">State University/College/Education Information</legend>
                                <!-- begin form-group row -->
                                <div class="form-group row m-b-12">
                                    {{--<label class="col-md-12 text-md-right col-form-label">State University/College Name</label>--}}
                                    <div class="col-md-12">
                                        <label class="col-form-label">{{'(Academic Year)    '.$resp[0]->academic_yr.'   (Branch Name)
                                        '.$resp[0]->branch_name.'   (College Name)
                                        '.$resp[0]->college_name.'  (Course Name)
                                        '.$resp[0]->course_name}}</label>
                                    </div>
                                </div>
                                <!-- end form-group row -->
                                <!-- begin form-group row -->

                            </div>
                            <!-- end col-8 -->
                        </div>
                        <!-- end row -->
                    </fieldset>
                    <!-- end fieldset -->
                </div>
                <!-- end step-2 -->
                <!-- begin step-3 -->
                <div id="step-3">
                    <!-- begin fieldset -->
                    <fieldset>
                        <!-- begin row -->
                        <div class="row">
                            <!-- begin col-8 -->
                            <div class="col-md-8 offset-md-2">
                                <legend class="no-border f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse">Choose all the applicable skills you have.</legend>
                                <!-- begin form-group row -->
                                <div class="form-group row m-b-10">
                                    <label class="col-md-3 text-md-right col-form-label">Skill List</label>
                                    <div class="col-md-6">
                                        <select class="multiple-select2 form-control" multiple="multiple" name="skills[]">
                                            @foreach(DB::select('SELECT * FROM `r_intern_skills`') as $skill)
                                            <option value="{{ $skill->skill_id }}">{{ $skill->name }} </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <!-- end form-group row -->
                            </div>
                            <!-- end col-8 -->
                        </div>
                        <!-- end row -->
                    </fieldset>
                    <!-- end fieldset -->
                </div>
                <!-- end step-3 -->
                <!-- begin step-4 -->
                <div id="step-4">
                    <div class="jumbotron m-b-0 text-center">
                        <h2 class="text-inverse">Confirm Profile</h2>

                        <div class="row">

                            <div class="col-md-8 offset-md-2">
                                <div class="form-group row m-b-10">
                                    <label class="col-md-3 text-md-right col-form-label">Email:</label>
                                    <div class="col-md-6">
                                        <label class="col-form-label">{{$resp[0]->contact_email}}</label>
                                    </div>
                                </div>
                                <!-- end form-group row -->
                            </div>
                            <!-- begin col-8 -->
                            <div class="col-md-8 offset-md-2">
                                 <div class="form-group row m-b-10">
                                    <label class="col-md-3 text-md-right col-form-label">Password:</label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="password" name="password" required/>
                                    </div>
                                </div>
                                <!-- end form-group row -->
                            </div>
                            <!-- end col-8 -->
                        </div>
                        <!-- end row -->

                        <div class="form-group">
                            <input type="checkbox" id="confirm" name="confirm" required />
                            <label for="confirm">I hereby confirm the details I entered and I want to proceed now.</label>
                        </div>

                        <p><button type="submit" class="btn btn-primary btn-lg">Submit Details</button></p>
                    </div>
                </div>
                <!-- end step-4 -->
            </div>
            <!-- end wizard-content -->
        </div>
        <!-- end wizard -->
    </form>
    <!-- end wizard-form -->
@endsection