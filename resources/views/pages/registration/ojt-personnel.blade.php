<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Color Admin | Wizards</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link href="{{ asset('assets/backend/plugins/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/bootstrap/4.0.0/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/font-awesome/5.0/css/fontawesome-all.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/animate/animate.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/css/default/style.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/css/default/style-custom.min.css') }}" rel="stylesheet" />
    <link href="{{asset('assets/backend/css/default/style-responsive.min.css')}}" rel="stylesheet" />
    <link href="{{ asset('assets/sweetalert/sweetalert2.css') }}" rel="stylesheet" />
    <!-- ================== END BASE CSS STYLE ================== -->

    <!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
    <link href="{{ asset('assets/backend/plugins/jquery-smart-wizard/src/css/smart_wizard.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/parsley/src/parsley.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/sweetalert/sweetalert2.js') }}" rel="stylesheet" />
    <!-- ================== END PAGE LEVEL STYLE ================== -->

    <!-- ================== BEGIN BASE JS ================== -->
    <script src="{{ asset('assets/backend/plugins/pace/pace.js') }}"></script>
    <!-- ================== END BASE JS ================== -->
</head>
<body>
<!-- begin #page-loader -->
<div id="page-loader" class="fade show"><span class="spinner"></span></div>
<!-- end #page-loader -->

<div class="row m-t-40 m-b-30">
</div>
<div class="row m-t-40 m-l-10 m-r-10">
    <div class="col-lg-12">
        <div class="card">
            <!-- begin wizard-form -->
            {{--<form class="form-control-with-bg">--}}
                <!-- begin wizard -->
                <div id="wizard">
                    <!-- begin wizard-step -->
                    <ul>
                        <li class="col-md-2 col-sm-4 col-6">
                            <a href="#step-1">
                                <span class="number">1</span>
                                <span class="info text-ellipsis">
									Personal Information
									<small class="text-ellipsis">Profile registration</small>
								</span>
                            </a>
                        </li>
                        <li class="col-md-3 col-sm-4 col-6">
                            <a href="#step-2">
                                <span class="number">2</span>
                                <span class="info text-ellipsis">
									Educational Background
									<small class="text-ellipsis">Educational Background is required</small>
								</span>
                            </a>
                        </li>
                        <li class="col-md-3 col-sm-4 col-6">
                            <a href="#step-3">
                                <span class="number">3</span>
                                <span class="info text-ellipsis">
									Attended Seminars & Trainings
									<small class="text-ellipsis">Seminars and trainings for the last 2 years</small>
								</span>
                            </a>
                        </li>
                        <li class="col-md-2 col-sm-4 col-6">
                            <a href="#step-4">
                                <span class="number">4</span>
                                <span class="info text-ellipsis">
									Login Account
									<small class="text-ellipsis">Setup your email and password</small>
								</span>
                            </a>
                        </li>
                        <li class="col-md-2 col-sm-4 col-6">
                            <a href="#step-5">
                                <span class="number">5</span>
                                <span class="info text-ellipsis">
									Completed
									<small class="text-ellipsis">Submit registration form</small>
								</span>
                            </a>
                        </li>
                    </ul>
                    <!-- end wizard-step -->
                    <!-- begin wizard-content -->
                    <div>
                        <!-- begin step-1 -->
                        <div id="step-1">
                            <!-- begin fieldset -->
                            <fieldset>
                                <!-- begin row -->
                                <div class="row">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-5">
                                        <div class="row form-group m-b-10">
                                            <label class="col-form-label col-md-3 text-md-right">First Name<span class="text-danger"> *</span></label>
                                            <div class="col-md-8">
                                                <input id="txt_fname" type="text" placeholder="First Name" data-parsley-group="step-1" data-parsley-required="true" class="form-control">
                                            </div>
                                        </div>
                                        <div class="row form-group m-b-10">
                                            <label class="col-form-label col-md-3 text-md-right">Middle Name<span class="text-danger"> *</span></label>
                                            <div class="col-md-8">
                                                <input id="txt_mname" type="text" placeholder="Middle Name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="row form-group m-b-10">
                                            <label class="col-form-label col-md-3 text-md-right">Last Name<span class="text-danger"> *</span></label>
                                            <div class="col-md-8">
                                                <input id="txt_lname" type="text" placeholder="Last Name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="row form-group m-b-10">
                                            <label class="col-form-label col-md-3 text-md-right">Address<span class="text-danger"> *</span></label>
                                            <div class="col-md-8">
                                                <input id="txt_address" type="text" placeholder="Street , Barangay, City/Province" class="form-control">
                                            </div>
                                        </div>
                                        <div class="row form-group m-b-10">
                                            <label class="col-form-label col-md-3 text-md-right">Region<span class="text-danger"> *</span></label>
                                            <div id="opt_region" class="col-md-8">
                                                <select id="opt_region" class="form-control">
                                                    <option>-- Select Region --</option>
                                                    @if(count($regions) > 0)
                                                        @foreach($regions as $region)
                                                            <option value="{{ $region["region_id"] }}">{{ $region["name"] }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row form-group m-b-10">
                                            <label class="col-form-label col-md-3 text-md-right">Gender<span class="text-danger"> *</span></label>
                                            <div class="col-md-4">
                                                <div class="form-check form-check-inline">
                                                    <input type="radio" name="radio_gender" id="genderMale" class="form-check-input" value="M">
                                                    <label class="form-check-label" for="genderMale">Male</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input type="radio" name="radio_gender" id="genderFemale" class="form-check-input" value="F">
                                                    <label class="form-check-label" for="genderFemale">Female</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="row form-group m-b-10">
                                            <label class="col-form-label col-md-3 text-md-right">Date of Birth<span class="text-danger"> *</span></label>
                                            <div class="col-md-8">
                                                <input id="txt_bdate" type="date" class="form-control">
                                            </div>
                                        </div>
                                        <div class="row form-group m-b-10">
                                            <label class="col-form-label col-md-3 text-md-right">Tel. Number<span class="text-danger"> *</span></label>
                                            <div class="col-md-8">
                                                <input id="txt_telno" type="text" placeholder="Telephone Number" class="form-control">
                                            </div>
                                        </div>
                                        <div class="row form-group m-b-10">
                                            <label class="col-form-label col-md-3 text-md-right">Cel. Number<span class="text-danger"> *</span></label>
                                            <div class="col-md-8">
                                                <input id="txt_celno" type="text" placeholder="Cellphone Number" class="form-control">
                                            </div>
                                        </div>
                                        <div class="row form-group m-b-10">
                                            <label class="col-form-label col-md-3 text-md-right">State University<span class="text-danger"> *</span></label>
                                            <div class="col-md-8">
                                                <select id="txt_su" class="default-select2 form-control">
                                                    <option>--Select State University--</option>
                                                    @if(count($univs) > 0)
                                                        @foreach($univs as $su)
                                                            <option value="{{ $su["su_code"] }}">{{ $su["su_name"] }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row form-group m-b-10">
                                            <label class="col-form-label col-md-3 text-md-right">Employment Status<span class="text-danger"> *</span></label>
                                            <div class="col-md-8">
                                                <select id="opt_empstat" class="form-control">
                                                    <option>-- Employment Status --</option>
                                                    <option value="Contractual">Contractual</option>
                                                    <option value="Temporary">Temporary</option>
                                                    <option value="Permanent">Permanent</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end row -->
                            </fieldset>
                            <!-- end fieldset -->
                        </div>
                        <!-- end step-1 -->
                        <!-- begin step-2 -->
                        <div id="step-2">
                            <!-- begin fieldset -->
                            <fieldset>
                                <!-- begin row -->
                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-5">
                                        <div class="row form-group m-b-10">
                                            <label class="col-form-label col-md-4 text-md-right">Degree<span class="text-danger"> *</span></label>
                                            <div class="col-md-8">
                                                <input id="txt_degree" type="text" placeholder="Degree" class="form-control">
                                            </div>
                                        </div>
                                        <div class="row form-group m-b-10">
                                            <label class="col-form-label col-md-4 text-md-right">School Graduated<span class="text-danger"> *</span></label>
                                            <div class="col-md-8">
                                                <input id="txt_gradSchool" type="text" placeholder="School Graduated" class="form-control">
                                            </div>
                                        </div>
                                        <div class="row form-group m-b-10">
                                            <label class="col-form-label col-md-4 text-md-right">Year Graduated<span class="text-danger"> *</span></label>
                                            <div class="col-md-8">
                                                <input id="txt_gradYear" type="text" placeholder="Year Graduated" class="form-control">
                                            </div>
                                        </div>
                                        <div class="row form-group m-b-10">
                                            <label class="col-form-label col-md-4 text-md-right">Awards<span class="text-danger"> *</span></label>
                                            <div class="col-md-8">
                                                <input id="txt_awards" type="text" placeholder="Awards" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end row -->
                            </fieldset>
                            <!-- end fieldset -->
                        </div>
                        <!-- end step-2 -->
                        <!-- begin step-3 -->
                        <div id="step-3">
                            <!-- begin fieldset -->
                            <fieldset>
                                <!-- begin row -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row pull-right">
                                                    <button id="btn_sAddRow" title="add input" class="btn btn-success btn-sm"><i class="fas fa-plus fa-lg"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <table id="tbl_seminar" class="table">
                                                <thead>
                                                <tr>
                                                    <th width="21%">Seminar Title<span class="text-danger"> *</span></th>
                                                    <th width="20%">Sponsoring Agency<span class="text-danger"> *</span></th>
                                                    <th width="15%">Level of the Seminar<span class="text-danger"> *</span></th>
                                                    <th width="13%">Date Held<span class="text-danger"> *</span></th>
                                                    <th width="25%">Venue<span class="text-danger"> *</span></th>
                                                    <th width="3%"></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr id="tbl_row">
                                                    <td><input id="txt_sTitle" type="text" placeholder="Seminar/Training Title" class="form-control"></td>
                                                    <td><input id="txt_sSponsor" type="text" placeholder="Sponsoring Agency" class="form-control"></td>
                                                    <td><input id="txt_sLevel" type="text" placeholder="Level of the Seminar" class="form-control"></td>
                                                    <td><input id="txt_sDate" type="date" class="form-control"></td>
                                                    <td><input id="txt_sVenue" type="text" placeholder="Venue" class="form-control"></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- end row -->
                            </fieldset>
                            <!-- end fieldset -->
                        </div>
                        <!-- end step-3 -->
                        <!-- begin step-4 -->
                        <div id="step-4">
                            <!-- begin fieldset -->
                            <fieldset>
                                <!-- begin row -->
                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-lg-5">
                                        <div class="row form-group m-b-10">
                                            <label class="col-form-label col-md-4 text-md-right">Email Address<span class="text-danger"> *</span></label>
                                            <div class="col-md-8">
                                                <input id="txt_email" type="text" placeholder="Email Address" class="form-control">
                                            </div>
                                        </div>
                                        <div class="row form-group m-b-10">
                                            <label class="col-form-label col-md-4 text-md-right">Password<span class="text-danger"> *</span></label>
                                            <div class="col-md-8">
                                                <input id="txt_pass" type="password" onkeyup="confirmPass()" placeholder="Password" data-parsley-group="step-4" data-parsley-required="true" data-parsley-minlength="8" class="form-control">
                                                <div class="invalid-feedback">Must choose 8 or higher length of password.</div>

                                            </div>
                                        </div>
                                        <div class="row form-group m-b-10">
                                            <label class="col-form-label col-md-4 text-md-right">Confirm Password<span class="text-danger"> *</span></label>
                                            <div class="col-md-8">
                                                <input id="txt_confirmPass" type="password" onkeyup="confirmPass()" placeholder="Confirm Password" class="form-control">
                                                <div class="invalid-feedback">Does not match with your password</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <!-- end fieldset -->
                        </div>
                        <!-- end step-4 -->
                        <!-- begin step-5 -->
                        <div id="step-5">
                            <div class=" m-b-0 text-center">
                                <h3 class="text-inverse">Register Successfully</h3>
                                <div class="row">
                                    <div class="col-lg-3"></div>
                                    <div class="col-lg-6 m-b-20">
                                        <div class="checkbox checkbox-css">
                                            <input type="checkbox" id="cb_Accept"/>
                                            <label for="cb_Accept">I certify that the given information provided are accurate and I agree to the Terms and Agreement.</label>
                                        </div>
                                    </div>
                                </div>
                                <p><button id="btn-submit" onclick="javascript:;" class="btn btn-primary disabled">Submit Registration Form</button></p>
                            </div>
                        </div>
                        <!-- end step-5 -->
                    </div>
                    <!-- end wizard-content -->
                </div>

                <!-- end wizard -->
            {{--</form>--}}
            <!-- end wizard-form -->
        </div>
    </div>
</div>

<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="{{ asset('assets/backend/plugins/jquery/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/bootstrap/4.0.0/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/js-cookie/js.cookie.js') }}"></script>
<script src="{{ asset('assets/backend/js/apps.min.js') }}"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="{{ asset('assets/sweetalert/sweetalert2.min.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/select2/dist/js/select2.min.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/parsley/dist/parsley.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/jquery-smart-wizard/src/js/jquery.smartWizard.js') }}"></script>
<script src="{{ asset('assets/backend/js/demo/form-wizards.demo.min.js') }}"></script>
<script src="{{ asset('assets/backend/js/demo/form-plugins.demo.min.js')}}"></script>
<!-- ================== END PAGE LEVEL JS ================== -->

<script>
    $(document).ready(function() {
        App.init();
        FormWizard.init();
    });

    function addFormValidation() {
        $("input[id='txt_fname']").parsley();
    }

    function deleteRow(row) {
        $(row).closest("tr").remove();
    };

    function confirmPass() {
        var pass = $("input[id='txt_pass']"),
            cPass = $("input[id='txt_confirmPass']");

        //if (pass.val().length >= 8) {
        pass.attr("class","form-control");
        if (cPass.val() != pass.val()) {
            cPass.attr("class","form-control is-invalid");
        } else {
            cPass.attr("class","form-control");
        }
        /*} else {
            pass.attr("class","form-control is-invalid");
        }*/
    }

    function submitForm() {
        var data = {
            _token: $("meta[name='csrf-token']").attr('content'),
            fname : $("input[id='txt_fname']").val(),
            mname : $("input[id='txt_mname']").val(),
            lname : $("input[id='txt_lname']").val(),
            address : $("input[id='txt_address']").val(),
            region : $("select[id='opt_region']").val(),
            gender : $("input[name='radio_gender']:checked").val(),
            bdate : $("input[id='txt_bdate']").val(),
            telno : $("input[id='txt_telno']").val(),
            celno : $("input[id='txt_celno']").val(),
            su : $("select[id='txt_su']").val(),
            empstat : $("select[id='opt_empstat']").val(),
            degree : $("input[id='txt_degree']").val(),
            gradSchool : $("input[id='txt_gradSchool']").val(),
            gradYear : $("input[id='txt_gradYear']").val(),
            seminars : getSeminarsTrainings(),
            awards : $("input[id='txt_awards']").val(),
            email : $("input[id='txt_email']").val(),
            pass : $("input[id='txt_pass']").val()
        };
        console.log(data);
        $.ajax({
            type: 'POST',
            url: '{{ route("registration.su-personnel.submit") }}',
            data: data,
            success: function(response) {
                console.log(response);
                swal({
                    title: '<span class="f-s-18">'+response.message+'</span>',
                    type: 'success',
                }).then((success) => {
                    location.replace('{{ route("home") }}');
                });
            }, error: function(data) {
                console.log("error: "+data);
            }
        });
    }

    function getSeminarsTrainings() {
        var st = [];
        $.each($("table[id='tbl_seminar'] tbody").find("tr"), function() {
            var title = $(this).find("input[id='txt_sTitle']").val(),
                sponsor = $(this).find("input[id='txt_sSponsor']").val(),
                level = $(this).find("input[id='txt_sLevel']").val(),
                date = $(this).find("input[id='txt_sDate']").val(),
                venue = $(this).find("input[id='txt_sVenue']").val();
            st.push({title: title, sponsor: sponsor, level: level, date: date, venue: venue});
        });
        return st;
    }

    function clearForm() {
        fname = $("input[id='txt_fname']").val("");
        mname = $("input[id='txt_mname']").val("");
        lname = $("input[id='txt_lname']").val("");
        address = $("input[id='txt_address']").val("");
        region = $("select[id='opt_region']").val("");
        gender = $("input[name='radio_gender']").val("");
        bdate = $("input[id='txt_bdate']").val("");
        telno = $("input[id='txt_telno']").val("");
        celno = $("input[id='txt_celno']").val("");
        su = $("select[id='txt_su']").val("");
        empstat = $("select[id='opt_empstat']").val("");
        degree = $("input[id='txt_degree']").val("");
        gradSchool = $("input[id='txt_gradSchool']").val("");
        gradYear = $("input[id='txt_gradYear']").val("");
        awards = $("input[id='txt_awards']").val("");
        email = $("input[id='txt_email']").val("");
        pass = $("input[id='txt_pass']").val("");
    }

    function clearTable() {
        var row = $("table[id='tbl_seminar'] tbody").first("td");
        row.nextAll().remove();
    }

    $("button[id='btn_sAddRow']").on('click',function() {
        var table = $("table[id='tbl_seminar'] tbody");
        table.append(
            '<tr id="tbl_row">' +
            '   <td><input id="txt_sTitle" type="text" placeholder="Seminar/Training Title" class="form-control"></td>' +
            '   <td><input id="txt_sSponsor" type="text" placeholder="Sponsoring Agency" class="form-control"></td>' +
            '   <td><input id="txt_sLevel" type="text" placeholder="Level of the Seminar" class="form-control"></td>' +
            '   <td><input id="txt_sDate" type="date" class="form-control"></td>' +
            '   <td><input id="txt_sVenue" type="text" placeholder="Venue" class="form-control"></td>' +
            '   <td>' +
            '       <button onclick="deleteRow(this)" class="btn btn-icon btn-danger" title="delete">' +
            '           <i class="fa fa-trash"></i>' +
            '       </button>' +
            '   </td>' +
            '</tr>'
        );
    });

    $("#cb_Accept").on('click',function() {
        var submit = $("#btn-submit");
        if ($("input[id='cb_Accept']:checked").length > 0) {
            submit.attr('class','btn btn-primary');
            submit.attr('onclick','submitForm()');
        } else {
            submit.attr('class','btn btn-primary disabled');
            submit.attr('onclick','javascript:;');
        }
    });
</script>
</body>
</html>