@extends('layouts.users')

@section('title','Host Training Establishment')

@section('css')
    <link href="{{ asset('assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css')}}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/DataTables/media/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/DataTables/extensions/Buttons/css/buttons.bootstrap.min.css') }}" rel="stylesheet" />
@endsection

@section('page-level-js')
    <link href="{{asset('assets/backend/plugins/parsley/src/parsley.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/backend//plugins/gritter/css/jquery.gritter.css')}}" rel="stylesheet" />
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">

            <!-- begin nav-pills -->
            <ul class="nav nav-pills">
                <li class="nav-items">
                    <a href="#nav-tab-hte-partners" data-toggle="tab" class="nav-link active">
                        <span class="d-sm-none">List of HTE Partners</span>
                        <span class="d-sm-block d-none">List of HTE Partners</span>
                    </a>
                </li>
                <li class="nav-items">
                    <a href="#nav-tab-record" data-toggle="tab" class="nav-link">
                        <span class="d-sm-none">Enrolled Record</span>
                        <span class="d-sm-block d-none">Enrolled Record</span>
                    </a>
                </li>
            </ul>
            <!-- end nav-pills -->
            <!-- begin tab-content -->
            <div class="tab-content">
                <!-- begin tab-pane -->
                {{--@if ($tab == 'add')
                    <div class="tab-pane fade active show" id="nav-tab-add">
                @else
                <div class="tab-pane fade" id="nav-tab-add">
                @endif--}}
                <div class="tab-pane fade active show" id="nav-tab-hte-partners">
                    <legend class="m-b-20">Host Training Establishment List of Partners</legend>
                    <div class="row">
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-4">
                            <div class="row form-group">
                                <div class="col-md-12">
                                    <a href="#modal-partnership-add" class="btn btn-primary col-sm-12" data-toggle="modal">Add Partnership</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <legend></legend>
                    <div class="row">
                        <div class="col-md-12">
                            <table id="data-table-buttons" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th class="text-nowrap">HTE Name</th>
                                    <th class="text-nowrap">Partnership Status</th>
                                    <th class="text-nowrap" data-orderable="false">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($partnerships as $partnership)
                                    <tr>
                                        <td>{{ $htes->where('hte_id', $partnership->hte_id)->first()->hte_name }}</td>
                                        <td>{{ $partnership->affiliated_status }}</td>
                                        <td>
                                            <a href="#modal-erview{{ $partnership->sh_id }}" class="btn btn-icon btn-info" data-toggle="modal">
                                                <i class="fas fa-eye"></i>
                                            </a>
                                            <a href="#modal-eredit{{ $partnership->sh_id }}" class="btn btn-icon btn-primary" data-toggle="modal" id="btn-edit">
                                                <i class="fas fa-pencil-alt"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="nav-tab-record">
                    <legend>Intern Add Record Form</legend>
                {{--<div class="alert alert-success fade show m-b-5">
                    <span class="close" data-dismiss="alert">×</span>
                    <center><span>Message goes here!</span></center>
                </div>--}}
                <!-- begin card -->
                    <div class="card">
                        <div class="card-header bg-aqua-darker text-white text-center pointer-cursor" data-toggle="collapse" data-target="#import">
                            Import Excel File
                        </div>
                        <div id="import" class="collapse" data-parent="#accordion">
                            <div class="card-body bg-grey-transparent-4">

                                {{ Form::open(array('method' => 'POST')) }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row form-group">
                                            <div class="width-30"></div>
                                            <label class="col-form-label text-md-right col-md-1">Program</label>
                                            <div class="col-md-3">
                                                <select name="program" class="form-control col-sm-12">
                                                    <option>-- Select Program Year & Section --</option>
                                                </select>
                                            </div>
                                            <label class="col-form-label text-md-right width-80">Select File</label>
                                            <div class="col-md-3 m-b-15">
                                                <input type="file" name="importfile" class="form-control col-sm-12">
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <button class="btn btn-primary col-sm-12">Import <File></File></button>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <button class="btn btn-white col-sm-12">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                    <!-- end card -->
                    {{--{{ Form::open(array('method' => 'POST')) }}--}}
                    <div class="row m-b-20">
                        <div class="col-md-12">
                            <div class="row form-group m-b-10">
                                <label class="col-form-label text-md-right width-150">Student Number</label>
                                <div class="col-md-4">
                                    <input type="text" name="studno" placeholder="Student Number" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <div class="row form-group">
                                        <label class="col-form-label text-md-right col-md-3">Program</label>
                                        <div class="col-md-6">
                                            <select name="program" class="form-control">
                                                <option>--- Select Program Year & Section ---</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row form-group m-b-10">
                                <label class="col-form-label text-md-right width-150">Student Name</label>
                                <div class="col-md-4">
                                    <div class="row">
                                        <div class="col-sm-12 m-b-5">
                                            <input type="text" name="firstname" placeholder="First Name" class="form-control">
                                        </div>
                                        <div class="col-sm-12 m-b-5">
                                            <input type="text" name="middlename" placeholder="Middle Name" class="form-control">
                                        </div>
                                        <div class="col-sm-12 m-b-5">
                                            <input type="text" name="lastname" placeholder="Last Name" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row form-group m-b-5">
                                        <label class="col-form-label text-md-right col-md-3">Gender</label>
                                        <div class="col-md-4">
                                            <div class="radio radio-css radio-inline">
                                                <input type="radio" name="gender" id="male" value="M">
                                                <label for="male">Male</label>
                                            </div>
                                            <div class="radio radio-css radio-inline">
                                                <input type="radio" name="gender" id="female" value="F">
                                                <label for="female">Female</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group m-b-10">
                                        <label class="col-form-label text-md-right col-md-3">Birth Date</label>
                                        <div class="col-md-6">
                                            <input type="text" name="birthdate" id="datepicker-autoClose" placeholder="mm/dd/yyyy" class="form-control">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-form-label text-md-right col-md-3">E-mail Address</label>
                                        <div class="col-md-6">
                                            <input type="text" name="email" placeholder="E-mail Address" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-form-label text-md-right width-150">Home Address</label>
                                <div class="col-md-4">
                                    <input type="text" name="address" placeholder="Home Address" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <div class="row form-group">
                                        <label class="col-form-label text-md-right col-md-3">Contact Number</label>
                                        <div class="col-md-6">
                                            <input type="text" name="contact" placeholder="Contact Number" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-3">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <button data-click="swal-record" class="btn btn-primary col-sm-12">Submit Form</button>
                                </div>
                                <div class="form-group col-md-6">
                                    <button class="btn btn-white col-sm-12">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--{{ Form::close() }}--}}
                </div>
                <!-- end tab-pane -->
            </div>
            <!-- end tab-content -->
        </div>
    </div>

    <!-- #modal-dialogs -->
    {{-- View Profile --}}
    @foreach($partnerships as $partnership)
        <div class="modal fade" id="modal-erview{{ $partnership->sh_id }}">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">&nbsp</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <div class="row m-b-10">
                            <div class="col-md-1"></div>
                            <div class="col-md-3">
                                <img src="{{ Avatar::create($htes->where('hte_id', $partnership->hte_id)->first()->hte_name)->toBase64() }}" class="width-100 height-100 m-l-100 m-r-40"></img>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-sm-12 m-t-5">
                                        <div class="row"><b><span class="f-s-15">{{ $htes->where('hte_id', $partnership->hte_id)->first()->hte_code }}</span></b></div>
                                        <div class="row"><span class="f-s-16">{{ $htes->where('hte_id', $partnership->hte_id)->first()->hte_name }}</span></div>
                                        <div class="row"><b><span class="f-s-14">&nbsp;</span></b></div>
                                        {{--<div class="row"><span class="label label-primary f-s-14">Enrolled</span></div>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <legend></legend>
                        <div class="row">
                            <div class="col-md-4 text-md-right">
                                <b><span class="f-s-15">Mission:</span></b>
                            </div>
                            <div class="col-md-7">
                                <span class="f-s-15">{{ $htes->where('hte_id', $partnership->hte_id)->first()->mission or 'N/A' }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 text-md-right">
                                <b><span class="f-s-15">Vision:</span></b>
                            </div>
                            <div class="col-md-7">
                                <span class="f-s-15">{{ $htes->where('hte_id', $partnership->hte_id)->first()->vision or 'N/A' }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 text-md-right">
                                <b><span class="f-s-15">Objectives:</span></b>
                            </div>
                            <div class="col-md-7">
                                <span class="f-s-15">{{ $htes->where('hte_id', $partnership->hte_id)->first()->objectives or 'N/A' }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 text-md-right">
                                <b><span class="f-s-15">Address:</span></b>
                            </div>
                            <div class="col-md-7">
                                <span class="f-s-15">{{ $htes->where('hte_id', $partnership->hte_id)->first()->address }}</span>
                            </div>
                        </div>
                        {{--<div class="row">--}}
                            {{--<div class="col-md-4 text-md-right">--}}
                                {{--<b><span class="f-s-15">Contact Person:</span></b>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-7">--}}
                                {{--<span class="f-s-15">{{ $contacts->where('hte_id', $partnership->hte_id)->first()->contact_person.' - '.$contacts->where('hte_id', $partnership->hte_id)->first()->contact_no }}</span>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <legend></legend>
                        <div class="row">
                            <div class="col-md-5"></div>
                            <div class="col-md-2">
                                <a href="javascript:;" class="btn btn-white col-sm-12" data-dismiss="modal">OK</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    {{-- Edit Profile --}}
    <div class="modal fade" id="modal-partnership-add">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">&nbsp</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-10">
                            {{ Form::open(['route' => 'partnership']) }}
                            <div class="row form-group m-b-10">
                                <label class="col-form-label text-md-right col-md-4">Host Training Establishments</label>
                                <div class="col-md-8">
                                    <select name="hte_partner" class="form-control" required>
                                        <option selected disabled="disabled">--HTEs available for Partnership--</option>
                                        @foreach($htes as $hte)
                                            @if(!$partnerships->where('hte_id', $hte->hte_id)->first())
                                                <option value="{{ $hte->hte_code }}">{{ $hte->hte_name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group m-b-10">
                                <label class="col-form-label text-md-right col-md-4">Attachment (MOA)</label>
                                <div class="col-md-8">
                                    <input type="file" name="hte_suc_moa"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary col-sm-12" >Add Partnership</buttons>
                                </div>
                                <div class="col-md-4">
                                    <a href="javascript:;" class="btn btn-white col-sm-12" data-dismiss="modal">Cancel</a>
                                </div>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{asset('assets/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/DataTables/media/js/jquery.dataTables.js')}}"></script>
    <script src="{{ asset('assets/backend/plugins/DataTables/media/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/bootstrap-sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/backend/js/demo/table-manage-buttons.demo.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/DataTables/extensions/Buttons/js/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js') }}"></script>
@endsection

@section('custom-js')
    <script>
        $(document).ready(function() {
            App.init();
            /*TableManageDefault.init();*/
            // $("table.display").DataTable({
            //     fixedHeader: true
            // });
            // FormPlugins.init();
            TableManageButtons.init();
        });
    </script>

    <script>
        $('button').on('click',function() {
            if(this.id == 'er-btn-loadlist') {
                alert("Enrolled List");
            }
        });
    </script>

    <script>
        $("input[id='datepicker-autoClose']").datepicker({todayHighlight:!0,autoclose:!0});
    </script>

    <script>
        $("[data-click='swal-record']").on('click',function(e){
            e.preventDefault(),
                swal({
                    title:"Are you sure?",
                    text:"Form will be submit!",
                    icon:"warning",
                    buttons:{
                        cancel:{
                            id: "swal-record-success",
                            text:"Cancel",
                            value:null,
                            visible:!0,
                            className:"btn btn-default",
                            closeModal:!0
                        },confirm:{
                            text:"OK",
                            value:!0,
                            visible:!0,
                            className:"btn btn-primary",
                            closeModal:!0
                        }
                    }
                })
        });
        /*handleSweetNotification=function(){$('[data-click="swal-primary"]').click(function(e){
         e.preventDefault(),
         swal({
         title:"Are you sure?",
         text:"You will not be able to recover this imaginary file!",
         icon:"info",
         buttons:{
         cancel:{
         text:"Cancel",
         value:null,
         visible:!0,
         className:"btn btn-default",
         closeModal:!0
         },confirm:{
         text:"Primary",
         value:!0,
         visible:!0,
         className:"btn btn-primary",
         closeModal:!0
         }
         }
         })
         })},AddRecordSwal=function(){
         "use strict";
         return{
         init:function(){
         handleSweetNotification()
         }
         }
         }();*/
    </script>
@endsection