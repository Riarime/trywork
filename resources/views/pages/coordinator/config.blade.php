@extends('layouts.users')

@section('title','University Configuration')

@section('css')
    <link href="{{ asset('assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css')}}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/DataTables/media/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css') }}" rel="stylesheet" />
@endsection

@section('page-level-js')
    {{--<link href="{{asset('assets/backend/plugins/parsley/src/parsley.css')}}" rel="stylesheet" />--}}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">

            <!-- begin nav-pills -->
            <ul class="nav nav-pills">
                <li class="nav-items">
                    <a href="#nav-tab-active" data-toggle="tab" class="nav-link active">
                        <span class="d-sm-none">Active Details</span>
                        <span class="d-sm-block d-none">Active Details</span>
                    </a>
                </li>
                <li class="nav-items">
                    <a href="#nav-tab-acdmcyr" data-toggle="tab" class="nav-link">
                        <span class="d-sm-none">Academic Year</span>
                        <span class="d-sm-block d-none">Academic Year</span>
                    </a>
                </li>
                <li class="nav-items">
                    <a href="#nav-tab-semester" data-toggle="tab" class="nav-link">
                        <span class="d-sm-none">Semester</span>
                        <span class="d-sm-block d-none">Semester</span>
                    </a>
                </li>
                <li class="nav-items">
                    <a href="#nav-tab-program" data-toggle="tab" class="nav-link">
                        <span class="d-sm-none">Program</span>
                        <span class="d-sm-block d-none">Program</span>
                    </a>
                </li>
            </ul>
            <!-- end nav-pills -->
            <!-- begin tab-content -->
            <div class="tab-content">
                <!-- begin ACTIVE DETAILS tab-pane -->
                <div class="tab-pane fade active show" id="nav-tab-active">
                    <legend>Active Details Configuration</legend>
                    <!--<div class="alert alert-success fade show m-b-5">
                        <span class="close" data-dismiss="alert">×</span>
                        <center><span>Message goes here!</span></center>
                    </div>-->
                    <div class="row">
                        <div class="col-md-6 p-t-20">
                            <div class="row form-group m-b-10">
                                <label class="col-form-label col-md-5 text-md-right">Select Active Academic Year</label>
                                <div class="col-md-5">
                                    <select id="activedetail-ay" class="form-control">
                                        <option>--Select Academic Year--</option>
                                        @if(count($ays) > 0)
                                            @foreach($ays as $ay)
                                                <option value="{{ $ay["ay_code"] }}">{{ $ay["academic_yr"] }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group m-b-10">
                                <label class="col-form-label col-md-5 text-md-right">Select Active Semester</label>
                                <div class="col-md-5">
                                    <select id="activedetail-sem" class="form-control">
                                        <option>--Select Semester--</option>
                                        @if(count($semesters) > 0)
                                            @foreach($semesters as $sem)
                                                <option value="{{ $sem["sem_code"] }}">{{ $sem["semester"] }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group m-b-10">
                                <div class="col-md-1"></div>
                                <div class="col-md-4 m-b-10">
                                    <button id="activedetails-btn-submit" class="btn btn-primary col-sm-12" disabled>Save Active Setup</button>
                                </div>
                                <div class="col-md-4">
                                    <button id="activedetails-btn-cancel" class="btn btn-white col-sm-12">Cancel Setup</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5 m-t-20">
                            {{--<div class="card bg-white card-outline-secondary text-center height-100">
                                <div class="card-block">
                                    <h3 class="card-title m-b-0 m-t-10"><span name="active-ay">{{ "A.Y. ".$active->academic_yr }}</span></h3>
                                    <span name="active-sem" class="f-s-20">{{ $active->semester }}</span>
                                </div>
                            </div>--}}
                            <div class="card bg-white card-outline-secondary m-l-10 m-r-10">
                                <div class="card-header text-center">ACTIVE ACADEMIC YEAR & SEMESTER</div>
                                <div class="row m-t-20 m-b-20 m-l-10 m-r-10 text-center">
                                    @if(count($active) > 0)
                                        <h3 id="display-active-ay" class="width-full">A.Y. {{ $active[0]->academic_yr }}</h3>
                                        <span id="display-active-sem" class="f-s-20 width-full">{{ $active[0]->semester }}</span>
                                    @else
                                        <h3 id="display-active-ay" class="width-full">No Active</h3>
                                        <span id="display-active-sem" class="f-s-20 width-full">A.Y. and Semester</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end tab-pane -->

                <!-- begin ACADEMIC YEAR tab-pane -->
                <div class="tab-pane fade show" id="nav-tab-acdmcyr">
                    <legend>Academic Year Configuration</legend>
                {{--<div class="alert alert-success fade show m-b-5">
                    <span class="close" data-dismiss="alert">×</span>
                    <center><span>Message goes here!</span></center>
                </div>--}}
                    <div class="row">
                        <div class="col-lg-5 m-b-10 m-t-20">
                            <div class="row">
                                <div class="card bg-white card-outline-secondary width-full m-l-10 m-r-10 p-r-15 p-l-15">
                                    <div class="row form-group m-t-20 m-b-10">
                                        <label class="col-form-label col-md-4 text-md-right">Academic Year</label>
                                        <div class="col-md-6">
                                            <input id="input-update-aycode" hidden>
                                            <input type="text" id="input-add-ay" placeholder="Academic Year" class="form-control">
                                        </div>
                                    </div>
                                    <div class="row m-b-10">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-3 m-b-5">
                                            <button id="btn-addupdate-ay" class="btn btn-primary col-sm-12">Add</button>
                                        </div>
                                        <div class="col-md-3 m-b-10">
                                            <button id="btn-cancel-ay" class="btn btn-grey col-sm-12">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <table id="tbl-ay" class="table">
                                <thead>
                                <tr>
                                    <th width="10%">No.</th>
                                    <th>Academic Year</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="odd">
                                    <td valign="top" colspan="5" class="dataTables_empty">No data available in table</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- end tab-pane -->

                <!-- begin SEMESTER tab-pane -->
                <div class="tab-pane fade" id="nav-tab-semester">
                    <legend>Semester Configuration</legend>
                    <div class="row">
                        <div class="col-lg-5 m-b-10 m-t-20">
                            <div class="row">
                                <div class="card bg-white card-outline-secondary width-full m-l-10 m-r-10 p-r-15 p-l-15">
                                    <div class="row form-group m-t-20 m-b-10">
                                        <label class="col-form-label col-md-4 text-md-right">Semester</label>
                                        <div class="col-md-6">
                                            <input id="input-update-semcode" hidden>
                                            <input type="text" id="input-add-sem" placeholder="Semester" class="form-control">
                                        </div>
                                    </div>
                                    <div class="row m-b-10">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-3 m-b-5">
                                            <button id="btn-addupdate-sem" class="btn btn-primary col-sm-12">Add</button>
                                        </div>
                                        <div class="col-md-3 m-b-10">
                                            <button id="btn-cancel-sem" class="btn btn-grey col-sm-12">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <table id="tbl-sem" class="table">
                                <thead>
                                <tr>
                                    <th width="10%">No.</th>
                                    <th>Academic Year</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="odd">
                                    <td valign="top" colspan="5" class="dataTables_empty">No data available in table</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- end tab-pane -->

                <!-- begin PROGRAM tab-pane -->
                <div class="tab-pane fade" id="nav-tab-program">
                    <legend>Program Configuration</legend>
                    <div class="row">
                        <div class="col-lg-5 m-b-10 m-t-20">
                            <div class="row">
                                <div class="card bg-white card-outline-secondary width-full m-l-10 m-r-10 p-r-15 p-l-15">
                                    <div class="row form-group m-t-20 m-b-10">
                                        <label class="col-form-label col-md-4 text-md-right">Program</label>
                                        <div class="col-md-6">
                                            <input id="input-update-programcode" hidden>
                                            <select id="opt-add-program" class="form-control">
                                                <option>--Select Program--</option>
                                                @if(count($programs) > 0)
                                                    @foreach($programs as $program)
                                                        <option value="{{ $program["course_code"] }}">{{ $program["course_abbrv"] }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row form-group m-t-20 m-b-10">
                                        <label class="col-form-label col-md-4 text-md-right">Year & Section</label>
                                        <div class="col-md-6">
                                            <input type="text" id="input-add-yrsec" placeholder="Year - Section" class="form-control">
                                        </div>
                                    </div>
                                    <div class="row m-b-10">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-3 m-b-5">
                                            <button id="btn-addupdate-program" class="btn btn-primary col-sm-12">Add</button>
                                        </div>
                                        <div class="col-md-3 m-b-10">
                                            <button id="btn-cancel-program" class="btn btn-grey col-sm-12">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <table id="tbl-program" class="table">
                                <thead>
                                <tr>
                                    <th width="10%">No.</th>
                                    <th width="30%">Program</th>
                                    <th width="20%">Year & Section</th>
                                    <th width="15%">Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="odd">
                                    <td valign="top" colspan="5" class="dataTables_empty">No data available in table</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- end tab-pane -->
            </div>
            <!-- end tab-content -->
        </div>
    </div>
@endsection

@section('js')
    <script src="{{asset('assets/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
    {{--<script src="{{asset('assets/backend/js/demo/form-plugins.demo.min.js')}}"></script>--}}
    <script src="{{asset('assets/backend/plugins/DataTables/media/js/jquery.dataTables.js')}}"></script>
    <script src="{{ asset('assets/backend/plugins/DataTables/media/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>
    {{--<script src="{{ asset('assets/backend/js/demo/table-manage-default.demo.min.js') }}"></script>--}}
@endsection

@section('custom-js')
    <script>
        $(document).ready(function() {
            App.init();
            loadAYTable();
            loadSemTable();
            loadProgramTable();
        });

        function setActiveAYSelect() {
            var ayselect = $("select[id='activedetail-ay']");
            var first = ayselect.find("option").first();
            first.nextAll().remove();
            $.ajax({
                type: "GET",
                url: "{{ route("config.getselectableactiveay") }}",
                success: function(response) {
                    //console.log(response);
                    $.each(response, function(index,data) {
                        ayselect.append('<option value="'+data["ay_code"]+'">'+data["academic_yr"]+'</option>');
                    });
                },
                error: function(data) { console.log("error:"+data); }
            });
        }

        function setActiveSemSelect() {
            var semselect = $("select[id='activedetail-sem']");
            var first = semselect.find("option").first();
            first.nextAll().remove();
            $.ajax({
                type: "GET",
                url: "{{ route("config.getselectableactivesem") }}",
                success: function(response) {
                    //console.log(response);
                    $.each(response, function(index,data) {
                        semselect.append('<option value="'+data["sem_code"]+'">'+data["semester"]+'</option>');
                    });
                },
                error: function(data) { console.log("error:"+data); }
            });
        }

        function loadAYTable() {
            var table = $("table[id='tbl-ay'] tbody");
            var first = table.find("tr[class='odd']");
            var count = 1;
            var status = '';
            var buttons = '';
            $.ajax({
                type: "GET",
                url: "{{ route("config.ay") }}",
                success: function(response) {
                    //console.log(response);
                    first.nextAll().remove();
                    first.hide();
                    $.each(response, function(index,data) {
                        //console.log(data["ay_stat"]);
                        if (data["ay_stat"] == 'Active') {
                            status = "Enabled";
                            buttons = '       <a id="btn-ayenabled" title="enable academic year" class="btn btn-icon btn-info disabled"><i class="fas fa-check text-white"></i></a>' +
                                '       <a id="btn-aydisabled" onclick="disableAY(this)" title="disable academic year" class="btn btn-icon btn-danger"><i class="fas fa-times text-white"></i></a>';
                        } else {
                            status = "Disabled";
                            buttons = '       <a id="btn-ayenabled" onclick="enableAY(this)" title="enable academic year"  class="btn btn-icon btn-info"><i class="fas fa-check text-white"></i></a>' +
                                '       <a id="btn-aydisabled" title="disable academic year" class="btn btn-icon btn-danger disabled"><i class="fas fa-times text-white"></i></a>';
                        }

                        table.append(
                            '<tr id="'+data["ay_code"]+'">' +
                            '   <td>'+count+'</td>' +
                            '   <td id="row-ay">'+data["academic_yr"]+'</td>' +
                            '   <td id="row-aystat">'+status+'</td>' +
                            '   <td>' +
                            '       <a onclick="editAY(this)" title="edit academic year" class="btn btn-icon btn-primary"><i class="fa fa-pencil-alt text-white"></i></a>' +
                                    buttons +
                            '       <i id="aylist-loading" class="fas fa-spinner fa-pulse fa-lg m-l-10" hidden></i>' +
                            '   </td>' +
                            '</tr>'
                        );
                        count++;
                    });
                },
                error: function(data) { console.log("error:"+data); }
            });
        }

        function loadSemTable() {
            var table = $("table[id='tbl-sem'] tbody");
            var first = table.find("tr[class='odd']");
            var count = 1;
            var status = '';
            var buttons = '';
            $.ajax({
                type: "GET",
                url: "{{ route("config.sem") }}",
                success: function(response) {
                    //console.log(response);
                    first.nextAll().remove();
                    first.hide();
                    $.each(response, function(index, data) {
                        if (data["sem_stat"] == 'Active') {
                            status = 'Enabled';
                            buttons = '       <a id="btn-semenabled" title="enable semester" class="btn btn-icon btn-info disabled"><i class="fas fa-check text-white"></i></a>' +
                                '       <a id="btn-semdisabled" onclick="disableSem(this)" title="disable semester" class="btn btn-icon btn-danger"><i class="fas fa-times text-white"></i></a>';
                        } else {
                            status = "Disabled";
                            buttons = '       <a id="btn-semenabled" onclick="enableSem(this)" title="enable semester"  class="btn btn-icon btn-info"><i class="fas fa-check text-white"></i></a>' +
                                '       <a id="btn-semdisabled" title="disable semester" class="btn btn-icon btn-danger disabled"><i class="fas fa-times text-white"></i></a>';
                        }

                        table.append(
                            '<tr id="'+data["sem_code"]+'">' +
                            '   <td>'+count+'</td>' +
                            '   <td id="row-sem">'+data["semester"]+'</td>' +
                            '   <td id="row-semstat">'+status+'</td>' +
                            '   <td>' +
                            '       <a onclick="editSem(this)" title="edit semester" class="btn btn-icon btn-primary"><i class="fa fa-pencil-alt text-white"></i></a>' +
                                    buttons +
                            '       <i id="semlist-loading" class="fas fa-spinner fa-pulse fa-lg m-l-10" hidden></i>' +
                            '   </td>' +
                            '</tr>'
                        );
                        count++;
                    });
                },
                error: function(data) { console.log("error:"+data); }
            });
        }

        function loadProgramTable() {
            var table = $("table[id='tbl-program'] tbody");
            var first = table.find("tr[class='odd']");
            var count = 1;
            var status = '';
            var buttons = '';
            $.ajax({
                type: "GET",
                url: "{{ route("config.program") }}",
                success: function(response) {
                    //console.log(response);
                    first.nextAll().remove();
                    first.hide();
                    $.each(response, function(index, data) {
                        if (data["bc_stat"] == 'Active') {
                            status = 'Enabled';
                            buttons = '       <a id="btn-programenabled" title="enable program" class="btn btn-icon btn-info disabled"><i class="fas fa-check text-white"></i></a>' +
                                '       <a id="btn-programdisabled" onclick="disableProgram(this)" title="disable program" class="btn btn-icon btn-danger"><i class="fas fa-times text-white"></i></a>';
                        } else {
                            status = "Disabled";
                            buttons = '       <a id="btn-programenabled" onclick="enableProgram(this)" title="enable program"  class="btn btn-icon btn-info"><i class="fas fa-check text-white"></i></a>' +
                                '       <a id="btn-programdisabled" title="disable program" class="btn btn-icon btn-danger disabled"><i class="fas fa-times text-white"></i></a>';
                        }

                        table.append(
                            '<tr id="'+data["bc_id"]+'">' +
                            '   <td>'+count+'</td>' +
                            '   <td id="row-programcode" hidden>'+data["course_code"]+'</td>' +
                            '   <td>'+data["course_abbrv"]+'</td>' +
                            '   <td id="row-yrsec">'+data["year_section"]+'</td>' +
                            '   <td id="row-programstat">'+status+'</td>' +
                            '   <td>' +
                            '       <a onclick="editProgram(this)" title="edit semester" class="btn btn-icon btn-primary"><i class="fa fa-pencil-alt text-white"></i></a>' +
                            buttons +
                            '       <i id="programlist-loading" class="fas fa-spinner fa-pulse fa-lg m-l-10" hidden></i>' +
                            '   </td>' +
                            '</tr>'
                        );
                        count++;
                    });
                },
                error: function(data) { console.log("error:"+data); }
            });
        }

        function setDisplayActive() {
            $.ajax({
                type: "GET",
                url: "{{ route("config.getactive") }}",
                //data: {_token: $("meta[name='csrf-token']").attr('content'),}
                success:function(response) {
                    //console.log("response: "+response["semester"]);
                    $("h3[id='display-active-ay']").text("A.Y. "+response["academic_yr"]);
                    $("span[id='display-active-sem']").text(response["semester"]);
                },
                error: function(data) {console.log("error: "+ data);}
            });
        }

        function enableSaveActiveSetup() {
            var ay = $("select[id='activedetail-ay']").val();
            var sem = $("select[id='activedetail-sem']").val();
            if (ay != "--Select Academic Year--" && sem != "--Select Semester--") {
                $("button[id='activedetails-btn-submit']").removeAttr("disabled","disabled");
            } else {
                $("button[id='activedetails-btn-submit']").attr("disabled","disabled");
            }
        }

        function resetAYFields() {
            $("input[id='input-update-aycode']").val("");
            $("input[id='input-add-ay']").val("");
            $("button[id='btn-addupdate-ay']").text("Add");
        }

        function resetSemFields() {
            $("input[id='input-update-semcode']").val("");
            $("input[id='input-add-sem']").val("");
            $("button[id='btn-addupdate-sem']").text("Add");
        }

        function resetProgramFields() {
            $("input[id='input-update-programcode']").val("");
            $("select[id='opt-add-program']").val("--Select Program--");
            $("input[id='input-add-yrsec']").val("");
            $("button[id='btn-addupdate-program']").text("Add");
        }

        function editAY(row) {
            //console.log($("button[id='btn-addupdate-ay']").text());
            $(row).closest('tr').find("i[id='aylist-loading']").removeAttr('hidden');
            $("input[id='input-update-aycode']").val($(row).closest("tr").attr('id'));
            $("input[id='input-add-ay']").val($(row).closest("tr").find("td[id='row-ay']").text());
            $("input[id='input-add-ay']").focus();
            $("button[id='btn-addupdate-ay']").text("Update");
            $(row).closest('tr').find("i[id='aylist-loading']").attr('hidden','hidden');
            setActiveAYSelect();
        }

        function editSem(row) {
            //console.log($("button[id='btn-addupdate-sem']").text());
            $(row).closest('tr').find("i[id='semlist-loading']").removeAttr('hidden');
            $("input[id='input-update-semcode']").val($(row).closest("tr").attr('id'));
            $("input[id='input-add-sem']").val($(row).closest("tr").find("td[id='row-sem']").text());
            $("input[id='input-add-sem']").focus();
            $("button[id='btn-addupdate-sem']").text("Update");
            $(row).closest('tr').find("i[id='semlist-loading']").attr('hidden','hidden');
            setActiveSemSelect();
        }

        function editProgram(row) {
            $(row).closest('tr').find("i[id='programlist-loading']").removeAttr('hidden');
            $("input[id='input-update-programcode']").val($(row).closest("tr").attr('id'));
            $("select[id='opt-add-program']").val($(row).closest("tr").find("td[id='row-programcode']").text());
            $("input[id='input-add-yrsec']").val($(row).closest("tr").find("td[id='row-yrsec']").text());
            $("input[id='input-add-yrsec']").focus();
            $("button[id='btn-addupdate-program']").text("Update");
            $(row).closest('tr').find("i[id='programlist-loading']").attr('hidden','hidden');
        }

        function enableAY(row) {
            var data = {
                _token: $("meta[name='csrf-token']").attr('content'),
                ay_code: $(row).closest('tr').attr("id"),
                status: $(row).closest('tr').find("td[id='row-aystat']").text()
            };
            var loading = $(row).closest('tr').find("i[id='aylist-loading']");
            loading.removeAttr('hidden');
            //console.log(data);
            $.ajax({
                type: "POST",
                url: "{{ route("config.updateaystat") }}",
                data: data,
                success: function(response) {
                    alert(response.message);
                    loading.attr('hidden','hidden');
                    $(row).closest('tr').find("td[id='row-aystat']").text("Enabled");
                    $(row).closest("tr").find("a[id='btn-aydisabled']").attr('class','btn btn-icon btn-danger');
                    $(row).closest("tr").find("a[id='btn-aydisabled']").attr('onclick','disableAY(this)');
                    $(row).attr('class','btn btn-icon btn-info disabled');
                    $(row).removeAttr('onclick');
                    setActiveAYSelect();
                },
                error: function(data) {
                    console.log("error: "+data);
                    loading.attr('hidden','hidden');
                }
            });
        }

        function enableSem(row) {
            var data = {
                _token: $("meta[name='csrf-token']").attr('content'),
                sem_code: $(row).closest('tr').attr("id"),
                status: $(row).closest('tr').find("td[id='row-semstat']").text()
            };
            var loading = $(row).closest('tr').find("i[id='semlist-loading']");
            loading.removeAttr('hidden');
            console.log(data);
            $.ajax({
                type: "POST",
                url: "{{ route("config.updatesemstat") }}",
                data: data,
                success: function(response) {
                    alert(response.message);
                    loading.attr('hidden','hidden');
                    $(row).closest('tr').find("td[id='row-semstat']").text("Enabled");
                    $(row).closest("tr").find("a[id='btn-semdisabled']").attr('class','btn btn-icon btn-danger');
                    $(row).closest("tr").find("a[id='btn-semdisabled']").attr('onclick','disableSem(this)');
                    $(row).attr('class','btn btn-icon btn-info disabled');
                    $(row).removeAttr('onclick');
                    setActiveSemSelect();
                },
                error: function(data) {
                    console.log("error: "+data);
                    loading.attr('hidden','hidden');
                }
            });
        }

        function enableProgram(row) {
            var data = {
                _token: $("meta[name='csrf-token']").attr('content'),
                program_id: $(row).closest('tr').attr("id"),
                status: $(row).closest('tr').find("td[id='row-programstat']").text()
            };
            var loading = $(row).closest('tr').find("i[id='programlist-loading']");
            loading.removeAttr('hidden');
            //console.log(data);
            $.ajax({
                type: "POST",
                url: "{{ route("config.updateprogramstat") }}",
                data: data,
                success: function(response) {
                    alert(response.message);
                    loading.attr('hidden','hidden');
                    $(row).closest('tr').find("td[id='row-programstat']").text("Enabled");
                    $(row).closest("tr").find("a[id='btn-programdisabled']").attr('class','btn btn-icon btn-danger');
                    $(row).closest("tr").find("a[id='btn-programdisabled']").attr('onclick','disableProgram(this)');
                    $(row).attr('class','btn btn-icon btn-info disabled');
                    $(row).removeAttr('onclick');
                },
                error: function(data) {
                    console.log("error: "+data);
                    loading.attr('hidden','hidden');
                }
            });
        }

        function disableAY(row) {
            var data = {
                _token: $("meta[name='csrf-token']").attr('content'),
                ay_code: $(row).closest('tr').attr("id"),
                status: $(row).closest('tr').find("td[id='row-aystat']").text()
            };
            var loading = $(row).closest('tr').find("i[id='aylist-loading']");
            loading.removeAttr('hidden');
            //console.log(data);
            $.ajax({
                type: "POST",
                url: "{{ route("config.updateaystat") }}",
                data: data,
                success: function(response) {
                    alert(response.message);
                    loading.attr('hidden','hidden');
                    $(row).closest('tr').find("td[id='row-aystat']").text("Disabled");
                    $(row).closest("tr").find("a[id='btn-ayenabled']").attr('class','btn btn-icon btn-info');
                    $(row).closest("tr").find("a[id='btn-ayenabled']").attr('onclick','enableAY(this)');
                    $(row).attr('class','btn btn-icon btn-danger disabled');
                    $(row).removeAttr('onclick');
                    setActiveAYSelect();
                },
                error: function(data) {
                    loading.attr('hidden','hidden');
                    console.log("error: "+data);
                }
            });
        }

        function disableSem(row) {
            var data = {
                _token: $("meta[name='csrf-token']").attr('content'),
                sem_code: $(row).closest('tr').attr("id"),
                status: $(row).closest('tr').find("td[id='row-semstat']").text()
            };
            var loading = $(row).closest('tr').find("i[id='semlist-loading']");
            loading.removeAttr('hidden');
            //console.log(data);
            $.ajax({
                type: "POST",
                url: "{{ route("config.updatesemstat") }}",
                data: data,
                success: function(response) {
                    alert(response.message);
                    loading.attr('hidden','hidden');
                    $(row).closest('tr').find("td[id='row-semstat']").text("Disabled");
                    $(row).closest("tr").find("a[id='btn-semenabled']").attr('class','btn btn-icon btn-info');
                    $(row).closest("tr").find("a[id='btn-semenabled']").attr('onclick','enableSem(this)');
                    $(row).attr('class','btn btn-icon btn-danger disabled');
                    $(row).removeAttr('onclick');
                    setActiveSemSelect();
                },
                error: function(data) {
                    console.log("error: "+data);
                    loading.attr('hidden','hidden');
                }
            });
        }

        function disableProgram(row) {
            var data = {
                _token: $("meta[name='csrf-token']").attr('content'),
                program_code: $(row).closest('tr').attr("id"),
                status: $(row).closest('tr').find("td[id='row-programstat']").text()
            };
            var loading = $(row).closest('tr').find("i[id='programlist-loading']");
            loading.removeAttr('hidden');
            //console.log(data);
            $.ajax({
                type: "POST",
                url: "{{ route("config.updateprogramstat") }}",
                data: data,
                success: function(response) {
                    alert(response.message);
                    loading.attr('hidden','hidden');
                    $(row).closest('tr').find("td[id='row-programstat']").text("Disabled");
                    $(row).closest("tr").find("a[id='btn-programenabled']").attr('class','btn btn-icon btn-info');
                    $(row).closest("tr").find("a[id='btn-programenabled']").attr('onclick','enableProgram(this)');
                    $(row).attr('class','btn btn-icon btn-danger disabled');
                    $(row).removeAttr('onclick');
                },
                error: function(data) {
                    console.log("error: "+data);
                    loading.attr('hidden','hidden');
                }
            });
        }

        $("select[id='activedetail-ay']").change(function() {
            enableSaveActiveSetup();
        });

        $("select[id='activedetail-sem']").change(function() {
            enableSaveActiveSetup();
        });

        $("button[id='activedetails-btn-submit']").on('click',function() {
            var ay = $("select[id='activedetail-ay']").val();
            var sem = $("select[id='activedetail-sem']").val();
            if (ay != "--Select Academic Year--" && sem != "--Select Semester--") {
                data = { _token: $("meta[name='csrf-token']").attr('content'), ay: ay, sem: sem };
                //console.log(data);
                $.ajax({
                    type: "POST",
                    url: "{{ route("config.active") }}",
                    data: data,
                    success: function(response) {
                        //console.log("Response:"+response.message);
                        $("select[id='activedetail-ay']").val("--Select Academic Year--");
                        setDisplayActive();
                        alert(response.message);
                    },
                    error: function(data) {console.log("error:"+data)}
                });
            }
        });

        $("button[id='activedetails-btn-cancel']").on('click',function() {
            $("select[id='activedetail-ay']").val("--Select Academic Year--");
            $("select[id='activedetail-sem']").val("--Select Semester--");
            $("button[id='activedetails-btn-submit']").attr("disabled","disabled");
        })

        $("button[id='btn-addupdate-ay']").on('click',function() {
            if ($("input[id='input-add-ay']").val() != "") {
                if ($(this).text() == "Add") {
                    data = { _token: $("meta[name='csrf-token']").attr('content'), ay: $("input[id='input-add-ay']").val() };
                    $.ajax({
                        type: "POST",
                        url: "{{ route("config.adday") }}",
                        data: data,
                        success: function(response) {
                            //console.log(response);
                            alert(response);
                            resetAYFields();
                            loadAYTable();
                            setActiveAYSelect();
                        },
                        error: function(data) {console.log("error:"+data);}
                    });
                } else {
                    data = {
                        _token: $("meta[name='csrf-token']").attr('content'),
                        ay_code: $("input[id='input-update-aycode']").val(),
                        ay: $("input[id='input-add-ay']").val()
                    };
                    $.ajax({
                        type: "POST",
                        url: "{{ route("config.updateay") }}",
                        data: data,
                        success: function(response) {
                            //console.log(response.message);
                            alert(response);
                            resetAYFields();
                            loadAYTable();
                            setActiveAYSelect();
                        },
                        error: function(data) {
                            console.log("error: "+ data);
                        }
                    });
                }
            }
            else {
                alert("Academic year input field is required!");
            }
        });

        $("button[id='btn-addupdate-sem']").on('click',function() {
            if ($("input[id='input-add-sem']").val() != "") {
                if ($(this).text() == "Add") {
                    data = { _token: $("meta[name='csrf-token']").attr('content'), sem: $("input[id='input-add-sem']").val() };
                    //console.log(data);
                    $.ajax({
                        type: "POST",
                        url: "{{ route("config.addsem") }}",
                        data: data,
                        success: function(response) {
                            //console.log(response);
                            alert(response.message);
                            resetSemFields();
                            loadSemTable();
                            setActiveSemSelect();
                        },
                        error: function(data) {console.log("error:"+data);}
                    });
                } else {
                    data = {
                        _token: $("meta[name='csrf-token']").attr('content'),
                        sem_code: $("input[id='input-update-semcode']").val(),
                        sem: $("input[id='input-add-sem']").val()
                    };
                    $.ajax({
                        type: "POST",
                        url: "{{ route("config.updatesem") }}",
                        data: data,
                        success: function(response) {
                            //console.log(response.message);
                            resetSemFields();
                            loadSemTable();
                            alert(response.message);
                            setActiveSemSelect();
                        },
                        error: function(data) {
                            console.log("error: "+ data);
                        }
                    });
                }
            } else {
                alert("Semester input field is required!");
            }
        });

        $("button[id='btn-addupdate-program']").on('click',function() {
            if ($("select[id='opt-add-program']").val() != "--Select Program--" && $("input[id='input-add-yrsec']").val() != "") {
                if ($(this).text() == "Add") {
                    data = {
                        _token: $("meta[name='csrf-token']").attr('content'),
                        program: $("select[id='opt-add-program']").val(),
                        yearsec: $("input[id='input-add-yrsec']").val()
                    };
                    //console.log(data);
                    $.ajax({
                        type: "POST",
                        url: "{{ route("config.addprogram") }}",
                        data: data,
                        success: function(response) {
                            //console.log(response);
                            resetProgramFields();
                            loadProgramTable();
                            alert(response["message"]);
                        },
                        error: function(data) {console.log("error:"+data);}
                    });
                } else {
                    data = {
                        _token: $("meta[name='csrf-token']").attr('content'),
                        program_id : $("input[id='input-update-programcode']").val(),
                        program_code: $("select[id='opt-add-program']").val(),
                        yearsec: $("input[id='input-add-yrsec']").val()
                    };
                    //console.log(data);
                    $.ajax({
                        type: "POST",
                        url: "{{ route("config.updateprogram") }}",
                        data: data,
                        success: function(response) {
                            //console.log(response.message);
                            resetProgramFields();
                            loadProgramTable();
                            alert(response.message);
                        },
                        error: function(data) {
                            console.log("error: "+ data);
                        }
                    });
                }
            } else {
                alert("Program select and input field is required!");
            }
        });

        $("button[id='btn-cancel-ay']").on('click',function() {
            resetAYFields();
        });

        $("button[id='btn-cancel-sem']").on('click',function() {
            resetSemFields();
        });

        $("button[id='btn-cancel-program']").on('click',function() {
            resetProgramFields();
        });
    </script>
{{--

        $("button[id='btn-setactive-ay']").on('click',function() {
            //console.log($("select[id='opt-active-ay']").val());
            var ay = $("select[id='opt-active-ay']").val();
            var sem = $("select[id='opt-active-sem']").val();
            if (ay != "--Select Academic Year--") {
                data = {
                    _token: $("meta[name='csrf-token']").attr('content'),
                    ay: $("select[id='opt-active-ay']").val()
                };
                console.log(data);
                $.ajax({
                    type: "POST",
                    url: "{{ route("config.active") }}",
                    data: data,
                    success: function(response) {
                        console.log("Response:"+response);
                        $("select[id='activedetail-ay']").val("--Select Academic Year--");
                        setDisplayActive();
                    },
                    error: function(data) {console.log("error:"+data)}
                });
            }
        });

    <script>
        $('button').on('click',function() {
            if(this.id == 'er-btn-loadlist') {
                alert("Enrolled List");
            }
            if (this.id == 'placement-btn-loadlist') {
                alert("OJT Placement");
            }
            if (this.id == 'assessment-btn-loadlist') {
                alert('Grade Assessment');
            }
        });
    </script>--}}
@endsection