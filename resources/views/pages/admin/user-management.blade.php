@extends('layouts.master-backend-admin')
@section('title', 'User Management')

@section('css')
    <link href="{{ asset('assets/backend/plugins/DataTables/media/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css') }}" rel="stylesheet" />
@endsection

@section('js')
    <script src="{{ asset('assets/backend/plugins/DataTables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/DataTables/media/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/DataTables/media/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>
@endsection

@section('document.ready')
    //setDataTable();
@endsection

@section('custom-js')
    <script>
        function setDataTable() {
            /*$("table[id='tbl_1']").dataTable({
                pagination: true,
                responsive: true
            });
            $("table[id='tbl_2']").dataTable({
                pagination: true,
            });*/
        }
    </script>
@endsection

@section('content')
    <div class="row">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-left">
            <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
            <li class="breadcrumb-item"><a href="javascript:;">UI Elements</a></li>
            <li class="breadcrumb-item active">Tabs & Accordions</li>
        </ol>
        <!-- end breadcrumb -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            <!-- begin nav-pills -->
            <ul class="nav nav-pills">
                <li class="nav-items">
                    <a href="#nav-pills-tab-1" data-toggle="tab" class="nav-link active">
                        <span class="d-sm-none">Host Training Establishment</span>
                        <span class="d-sm-block d-none">Host Training Establishment</span>
                    </a>
                </li>
                <li class="nav-items">
                    <a href="#nav-pills-tab-2" data-toggle="tab" class="nav-link">
                        <span class="d-sm-none">University Personnel</span>
                        <span class="d-sm-block d-none">University Personnel</span>
                    </a>
                </li>
                <li class="nav-items">
                    <a href="#nav-pills-tab-3" data-toggle="tab" class="nav-link">
                        <span class="d-sm-none">Student Trainee</span>
                        <span class="d-sm-block d-none">Student Trainee</span>
                    </a>
                </li>
            </ul>
            <!-- end nav-pills -->
            <!-- begin tab-content -->
            <div class="tab-content">
                <!-- begin tab-pane -->
                <div class="tab-pane fade active show" id="nav-pills-tab-1">
                    <legend>HTE User List</legend>
                </div>
                <!-- end tab-pane -->
                <!-- begin tab-pane -->
                <div class="tab-pane fade" id="nav-pills-tab-2">
                    <legend>University User List</legend>
                </div>
                <!-- end tab-pane -->
                <!-- begin tab-pane -->
                <div class="tab-pane fade" id="nav-pills-tab-3">
                    <legend>Student Trainee User List</legend>
                </div>
                <!-- end tab-pane -->
            </div>
            <!-- end tab-content -->
        </div>
    </div>
@endsection
