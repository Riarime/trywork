@extends('layouts.master-backend-admin')

@section('title', 'Audit Trail')
@section('css')
    <link href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css') }} rel="stylesheet" />
    <link href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/jquery-jvectormap/jquery-jvectormap.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/bootstrap-calendar/css/bootstrap_calendar.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/gritter/css/jquery.gritter.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/nvd3/build/nv.d3.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/DataTables/media/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css') }}" rel="stylesheet" />
@endsection

@section('js')
    <script src="{{asset('assets/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/DataTables/media/js/jquery.dataTables.js')}}"></script>
    <script src="{{ asset('assets/backend/plugins/DataTables/media/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/bootstrap-sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/backend/js/demo/table-manage-buttons.demo.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/DataTables/extensions/Buttons/js/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js') }}"></script>
@endsection
@section('document.ready')
    TableManageButtons.init();
    Highlight.init();
@endsection
@section('content')
    <div class="panel panel-inverse" data-sortable-id="ui-widget-1">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="#" class="btn btn-xs btn-icon btn-circle btn-default"
                   data-click="panel-expand">
                    <i class="fa fa-expand"></i>
                </a>
            </div>
            <h4 class="panel-title" style="font-size: 18px">Audit Trail</h4>
        </div>
        <div class="panel-body">
            <table id="data-table-buttons" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th></th>
                    <th width="15%">Date</th>
                    <th width="10%">Time</th>
                    <th class="text-nowrap">Details</th>
                </tr>
                </thead>
                <tbody>
                @foreach($auditTrails as $auditTrail)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td><b>{{ date('F d, Y', strtotime($auditTrail->created_at)) }}</b></td>
                        <td><b>{{ date('h:iA', strtotime($auditTrail->created_at)) }}</b></td>
                        <td>
                            <div class="row">
                                <div class="col-md-8">
                                    {{--{!! str_replace(['%USER%', '%DATE%'], ['<b>\''.ucwords(strtolower(\App\User::getName($auditTrail->user_id))).'\'</b>', '<b>'.date('h:ia', strtotime($auditTrail->created_at)).'</b>'], $details->where('message_id', $auditTrail->audit_details)->first()->details) !!}--}}
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection