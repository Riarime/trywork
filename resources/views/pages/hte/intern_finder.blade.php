@extends('layouts.master-backend-hte')

@section('title', 'Intern Finder')

@section('css')
    <link href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/jquery-jvectormap/jquery-jvectormap.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/bootstrap-calendar/css/bootstrap_calendar.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/gritter/css/jquery.gritter.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/nvd3/build/nv.d3.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/DataTables/media/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css') }}" rel="stylesheet" />
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.2/d3.min.js"></script>
    <script src="{{ asset('assets/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/backend/js/demo/table-manage-default.demo.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/nvd3/build/nv.d3.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-jvectormap/jquery-jvectormap.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-jvectormap/jquery-jvectormap-world-merc-en.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/bootstrap-calendar/js/bootstrap_calendar.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/gritter/js/jquery.gritter.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/bootstrap-sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/DataTables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/DataTables/media/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/backend/js/demo/dashboard-v2.min.js') }}"></script>
    <script src="{{ asset('assets/backend/js/demo/form-plugins.demo.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/highlight/highlight.common.js') }}"></script>
    <script src="{{ asset('assets/backend/js/demo/render.highlight.js') }}"></script>
    <script>
        var handleSweetNotification = function() {
            $('[data-click="swal-submit"]').click(function (e) {
                e.preventDefault();
                swal({
                    title: 'Are you sure?',
                    text: '',
                    icon: 'info',
                    buttons: {
                        cancel: {
                            text: 'Cancel',
                            value: null,
                            visible: true,
                            className: 'btn btn-default',
                            closeModal: true,
                        },
                        confirm: {
                            text: 'Submit',
                            value: true,
                            visible: true,
                            className: 'btn btn-lime text-black',
                            closeModal: true
                        }
                    }
                });
            });
        }
    </script>
@endsection

@section('document.ready')
    handleSweetNotification();
    TableManageDefault.init();
    handleDatepicker().init();
    Highlight.init();
@endsection

@section('content')
    <div class="panel panel-inverse" data-sortable-id="ui-widget-1">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="#" class="btn btn-xs btn-icon btn-circle btn-default"
                   data-click="panel-expand">
                    <i class="fa fa-expand"></i>
                </a>
            </div>
            <h4 class="panel-title">Intern Finder</h4>
        </div>
        <div class="panel-body">
            <table id="data-table-default" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>State University/College</th>
                    <th width="15%">Course/Major</th>
                    <th>Region</th>
                    <th>GPA</th>
                    <th>Intern Skills</th>
                    <th width="15%">Job Training Title</th>
                    <th width="20%">Description</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($jobTrainings as $jobTraining)
                        @foreach($recommendedStudents as $recommendedStudent)
                            @foreach($recommendedStudent as $item)
                            @if(null !== $internSkillSets->where('intern_id', $item)->first())
                                <tr>
                                    <td>{{ $sucs->where('id', $students->where('intern_id', $item)->first()->bc_id)->first()->name }}</td>
                                    <td>{{ $sucs->where('id', $students->where('intern_id', $item)->first()->bc_id)->first()->course }}</td>
                                    <td>{{ $regions->where('region_id', $students->where('intern_id', $item)->first()->region_id)->first()->name }}</td>
                                    <td>{{ $students->where('intern_id', $item)->first()->gpa }}</td>
                                    <td>
                                        <ul>
                                            @foreach($internSkillSets->where('intern_id', $item) as $student_skill)
                                                <li>{{ $internSkills->where('skill_id', $student_skill->skill_id)->first()->name }}</li>
                                            @endforeach
                                        </ul>
                                    </td>
                                    <td>{{ $jobTraining->name }}</td>
                                    <td>{{ $jobTraining->description }}</td>
                                    <td>
                                        @if(null == $internApplications->where('job_training_id', $jobTraining->id)->where('intern_id', $item)->where('status', true)->first())
                                        <form id="frm-interview" action="{{ route('application-manager.store') }}" method="POST">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="status" value="TWIAS-3"/>
                                            <input type="hidden" name="student_id" value="{{ $item }}"/>
                                            <input type="hidden" name="job_training_id" value="{{ $jobTraining->id }}"/>
                                            <button class="btn btn-primary btn-sm btn-block" type="submit">Recruit Intern for Interview</button>
                                        </form>
                                        @else
                                            <button class="btn btn-secondary btn-sm btn-block" disabled="disabled">Intern Unavailable</button>
                                        @endif
                                    </td>
                                </tr>
                            @endif
                                @endforeach
                        @endforeach
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection