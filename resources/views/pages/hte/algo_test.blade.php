<html>
<head>
    <title>algotest</title>
    <link href="{{ asset('assets/backend/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet"/>
</head>
<body>
<div class="miner" style="display: inline">
    <select name="branchID" id="jsonSelector" style="width: 300px"></select>
    &nbsp;&nbsp;&nbsp;&nbsp;
    <button id="suggestionMiner" style="height: 30px; width: 150px">Mine Suggestions</button>
</div>
<div class="results">
    <p id="branch_name"></p>
</div>
<script src="{{ asset('assets/frontend/plugins/jquery/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/select2/dist/js/select2.min.js') }}"></script>
<script>
    $(document).ready(function () {
        $('select#jsonSelector').select2({
            ajax: {
                method: 'POST',
                url: '{{ route('api.search.sucs') }}',
                delay: 500,
                dataType: 'json',
                data: function (params) {
                    var queryParameters = {
                        query: params.term,
                        page: params.page
                    };
                    return queryParameters;
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 10) < data.total_count
                        }
                    }
                },
                cache: true
            },
            allowClear: true,
            placeholder: 'Search State Universities/Colleges',
            minimumInputLength: 3,
            escapeMarkup: function (markup) {
                return markup;
            },
            templateResult: formatRepo,
            templateSelection: formatRepoSelection
        });
        function formatRepo(data) {
            if (data.loading) {
                return data.text;
            }

            var markup = "<p>" + data.branch_name + "</p>";

            return markup;
        }

        function formatRepoSelection(data) {
            return data.branch_name || data.id;
        }
    });
    $('button#suggestionMiner').click(function () {
        $('p#branch_name').text($('select#jsonSelector').find(":selected").attr('value'))
    });
    $('select#jsonSelector').on("select2:select", function (e) {
        $.ajax({
            method: 'POST',
            url: '{{ route('api.search.sucs') }}',
            dataType: 'json',
            data: {
                id: $('select#jsonSelector').find(":selected").attr('value')
            },
            success: function (response) {
                $('p#branch_name').text(response.branch_name)
            },
        });
    });
</script>
</body>
</html>