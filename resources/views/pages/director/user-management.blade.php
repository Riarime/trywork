@extends('layouts.users')

@section('title','User Management')

@section('base-js')

@endsection

@section('content')

@endsection

@section('js')
    <script src="{{ asset('assets/sweetalert/sweetalert2.min.js') }}"></script>
@endsection

@section('custom-js')
<script>
    $(document).ready(function() {
        App.init();
        setInternComparisonRateChart();
        setInternPerBranchChart();
        setInternPerHTEChart();
    });

</script>
@endsection