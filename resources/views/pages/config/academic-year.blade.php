@extends('layouts.users')

@section('title', 'Academic Year')

@section('css')
<link href="{{ asset('assets/backend/plugins/DataTables/media/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/backend/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css') }}" rel="stylesheet" />
@endsection

@section('base-js')
@endsection

@section('content')
    <!-- begin breadcrumb -->
    <div class="row">
        <ol class="breadcrumb pull-left">
            <li class="breadcrumb-item"><a href="javascript:;"><i class="fa fa-home"></i>&nbsp&nbspHome</a></li>
            <li class="breadcrumb-item"><i class="fa fa-th-large"></i>&nbsp&nbspConfiguration</li>
            <li class="breadcrumb-item active"><i class="far fa-calendar"></i>&nbsp&Academic Year</li>
        </ol>
    </div><br>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Academic Year</h1>
    <!-- end page-header -->
    
    <!-- begin panel -->
    <div class="panel panel-info">
        <div class="panel-heading">
            <h4 class="panel-title">Academic Year Record</h4>
        </div>
        <div class="panel-body">
            {{-- {!! Form::open(array('route' => '', 'method' => 'POST')) !!} --}}
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-5">
                    <div class="form-row">
                        <label class="col-form-label col-md-4"></label>
                        <div class="col-md-8">
                            <input id="txt_code" type="text" class="form-control" name="txt_academic_code" required disabled hidden>
                        </div>
                    </div><br>
                    <div class="form-row">
                        <label class="col-form-label col-md-4">Academic Year</label>
                        <div class="col-md-8">
                            <input type="text" id="txt_acdmcyr" class="form-control" name="academic_year" placeholder="Academic Year" disabled required>
                        </div>
                    </div><br>
                    <div class="form-row">
                        <div class="col-md-4"></div>
                        <div class="col-form-label">
                            <button id="btn_add" class="form-control btn btn-primary">Add</button>
                        </div>
                        <div class="col-form-label">
                            <button id="btn_submit" class="form-control btn btn-primary" name="submit" type="submit" disabled>Submit</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- {!! Form::close() !!} --}}
            <div class="row m-t-40">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                {{-- <div class="col-md-12"> --}}
                    <table id="data-table-default" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                {{-- <th width="1%"></th> --}}
                                <th width="14%" class="text-nowrap" hidden></th>
                                <th class="text-nowrap">Academic Year</th>
                                <th width="20%" class="text-nowrap" data-orderable="false">Status</th>
                                <th width="20%" class="text-nowrap" data-orderable="false">Controls</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($ay) > 0)
                                @foreach($ay as $value)
                                    <tr>
                                        <td width="14%" hidden>{{ $value->Code }}</td>
                                        <td>{{ $value->AY }}</td>
                                        <td>{{ $value->Status }}</td>
                                        <td>
                                            <div class="row pull-right">
                                                <div class="col-md-3">
                                                    <button id="btn_edit" class="btn btn-primary btn-icon"><i class="fas fa-edit"></i></button>
                                                </div>
                                                <div class="col-md-3">
                                                    <button id="btn_enable" class="btn btn-success btn-icon"><i class="fas fa-check"></i></button>
                                                </div>
                                                <div class="col-md-3">
                                                    <button id="btn_disable" class="btn btn-danger btn-icon"><i class="fas fa-times"></i></button>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- end panel -->
@endsection

@section('js')
    <script src="{{asset('assets/backend/plugins/DataTables/media/js/jquery.dataTables.js')}}"></script>
    <script src="{{ asset('assets/backend/plugins/DataTables/media/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/backend/js/demo/table-manage-default.demo.min.js') }}"></script>
@endsection

@section('custom-js')
<script>
    $(document).ready(function() {
        App.init();
        TableManageDefault.init();
    });
</script>
<script>
    $('#btn_add').on('click',function() {
        enableForm();
    });
    $('#btn_submit').on('click',function() {
        disableForm();
    });
    $('#btn_edit').on('click',function() {
        enableForm();
    });

    function enableForm() {
        var input = $('#txt_acdmcyr');
        var btn = $('#btn_submit');
        $('#btn_add').attr('disabled','disabled');
        input.removeAttr('disabled');
        btn.removeAttr('disabled');
        input.focus();
    }
    function disableForm() {
        $('#txt_acdmcyr').attr('disabled','disabled');
        $('#btn_submit').attr('disabled','disabled');
        $('#btn_add').removeAttr('disabled');
    }
</script>
@endsection