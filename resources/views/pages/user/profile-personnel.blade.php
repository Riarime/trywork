@extends('layouts.users')

@section('title', 'Semester')

@section('css')
    <link href="{{ asset('assets/backend/plugins/jquery-smart-wizard/src/css/smart_wizard.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backend/plugins/plugins/parsley/src/parsley.css') }}" rel="stylesheet" />
@endsection

@section('base-js')
@endsection

@section('content')
    <!-- begin breadcrumb -->
    <div class="row">
        <ol class="breadcrumb pull-left">
            <li class="breadcrumb-item"><a href="javascript:;"><i class="fa fa-home"></i>&nbsp&nbspHome</a></li>
            <li class="breadcrumb-item active"><i class="fas fa-user-circle"></i>&nbsp&nbspProfile</li>
        </ol>
    </div><br>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Account Profile Settings</h1>
    <!-- end page-header -->
    {{--begin row--}}
    <div class="row">
        <div class="col">
            <div class="row">
                {{--<div class="panel panel-info width-full">
                    <div class="panel-heading">
                        <h4 class="panel-title">Personal Information</h4>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <img src="" class="col-md-5 m-l-30 width-200 height-200">
                                        </div><br>
                                        <div class="row">
                                            <button class="m-l-40 btn btn-primary">Update Picture</button>
                                        </div>
                                    </div>
                                </div><br>
                            </div>
                            --}}{{--{!! Form::open() !!}--}}{{--
                            <div class="col-lg-9">
                                <div class="form-group row">
                                    <label class="col-form-label col-md-2">First Name</label>
                                    <div class="col-md-4">
                                        <input type="text" placeholder="First Name" class="form-control" required>
                                    </div>
                                    <label class="col-form-label col-md-2">Designation</label>
                                    <div class="col-md-4">
                                        <div>
                                            <input type="text" placeholder="Designation" class="form-control" required>
                                        </div>
                                    </div>
                                </div><br>
                                <div class="form-group row">
                                    <label class="col-form-label col-md-2">Middle Name</label>
                                    <div class="col-md-4">
                                        <input type="text" placeholder="Middle Name" class="form-control" required>
                                    </div>
                                    <label class="col-form-label col-md-2">Academic Rank</label>
                                    <div class="col-md-4">
                                        <div>
                                            <select class="form-control">
                                                <option>--Select Academic Rank--</option>
                                            </select>
                                        </div>
                                    </div>
                                </div><br>
                                <div class="form-group row">
                                    <label class="col-form-label col-md-2">Last Name</label>
                                    <div class="col-md-4">
                                        <input type="text" placeholder="Last Name" class="form-control" required>
                                    </div>
                                    <label class="col-form-label col-md-2">Employment Status</label>
                                    <div class="col-md-4">
                                        <div>
                                            <select class="form-control">
                                                <option>--Select Employment Status--</option>
                                            </select>
                                        </div>
                                    </div>
                                </div><br>
                                <div class="form-group row">
                                    <label class="col-form-label col-md-2">Designation</label>
                                    <div class="col-md-4">
                                        <input type="text" placeholder="Designation" class="form-control" required>
                                    </div>
                                    <label class="col-form-label col-md-2">State University</label>
                                    <div class="col-md-4">
                                        <div>
                                            <select class="form-control">
                                                <option>--Select State University--</option>
                                            </select>
                                        </div>
                                    </div>
                                </div><br>
                                <div class="form-group row">
                                    <label class="col-form-label col-md-2">Email</label>
                                    <div class="col-md-4">
                                        <input type="text" placeholder="Email" class="form-control" required>
                                    </div>
                                    <label class="col-form-label col-md-2">College</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" placeholder="College">
                                    </div>
                                </div><br>
                                <div class="form-group row">
                                    <label class="col-form-label col-md-2">Contact No</label>
                                    <div class="col-md-4">
                                        <input type="text" placeholder="Contact No" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <legend class="m-t-10"></legend>
                            <div class="col-lg-9">
                                <div class="form-group row">
                                    <label class="col-form-label col-md-2">Degree</label>
                                    <div class="col-md-4">
                                        <select class="form-control">
                                            <option>--Select Degree--</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            --}}{{--{!! Form::close() !!}--}}{{--
                        </div>
                    </div>
                </div>--}}

            </div>
            <!-- begin wizard-form -->
            <form method="POST" name="form-wizard" class="form-control-with-bg">
                <!-- begin wizard -->
                <div id="wizard">
                    <!-- begin wizard-step -->
                    <ul>
                        <li class="col-md-3 col-sm-4 col-6">
                            <a href="#step-1">
                                <span class="number">1</span>
                                <span class="info text-ellipsis">
									Personal Info
									<small class="text-ellipsis">Name, Address, IC No and DOB</small>
								</span>
                            </a>
                        </li>
                        <li class="col-md-3 col-sm-4 col-6">
                            <a href="#step-2">
                                <span class="number">2</span>
                                <span class="info text-ellipsis">
									Enter your contact
									<small class="text-ellipsis">Email and phone no. is required</small>
								</span>
                            </a>
                        </li>
                        <li class="col-md-3 col-sm-4 col-6">
                            <a href="#step-3">
                                <span class="number">3</span>
                                <span class="info text-ellipsis">
									Login Account
									<small class="text-ellipsis">Enter your username and password</small>
								</span>
                            </a>
                        </li>
                        <li class="col-md-3 col-sm-4 col-6">
                            <a href="#step-4">
                                <span class="number">4</span>
                                <span class="info text-ellipsis">
									Completed
									<small class="text-ellipsis">Complete Registration</small>
								</span>
                            </a>
                        </li>
                    </ul>
                    <!-- end wizard-step -->
                    <!-- begin wizard-content -->
                    <div>
                        <!-- begin step-1 -->
                        <div id="step-1">
                            <!-- begin fieldset -->
                            <fieldset>
                                <!-- begin row -->
                                <div class="row">
                                    <!-- begin col-8 -->
                                    <div class="col-md-8 offset-md-2">
                                        <legend class="no-border f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse">Personal info about yourself</legend>
                                        <!-- begin form-group -->
                                        <div class="form-group row m-b-10">
                                            <label class="col-md-3 col-form-label text-md-right">First Name <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                                <input type="text" name="firstname" placeholder="John" data-parsley-group="step-1" data-parsley-required="true" class="form-control" />
                                            </div>
                                        </div>
                                        <!-- end form-group -->
                                        <!-- begin form-group -->
                                        <div class="form-group row m-b-10">
                                            <label class="col-md-3 col-form-label text-md-right">Last Name <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                                <input type="text" name="lastname" placeholder="Smith" data-parsley-group="step-1" data-parsley-required="true" class="form-control" />
                                            </div>
                                        </div>
                                        <!-- end form-group -->
                                        <!-- begin form-group -->
                                        <div class="form-group row m-b-10">
                                            <label class="col-md-3 col-form-label text-md-right">Date of Birth <span class="text-danger">&nbsp;</span></label>
                                            <div class="col-md-6">
                                                <div class="row row-space-6">
                                                    <div class="col-4">
                                                        <select class="form-control" name="year">
                                                            <option>-- Year --</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-4">
                                                        <select class="form-control" name="month">
                                                            <option>-- Month --</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-4">
                                                        <select class="form-control" name="day">
                                                            <option>-- Day --</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- end form-group -->
                                        <!-- begin form-group -->
                                        <div class="form-group row m-b-10">
                                            <label class="col-md-3 col-form-label text-md-right">IC No <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                                <input type="text" name="ic" placeholder="" class="form-control" data-parsley-group="step-1" data-parsley-required="true" />
                                            </div>
                                        </div>
                                        <!-- end form-group -->
                                        <!-- begin form-group -->
                                        <div class="form-group row m-b-10">
                                            <label class="col-md-3 col-form-label text-md-right">Address</label>
                                            <div class="col-md-9">
                                                <input type="text" name="address1" placeholder="Address 1" class="form-control m-b-10" />
                                                <input type="text" name="address2" placeholder="Address 2" class="form-control" />
                                            </div>
                                        </div>
                                        <!-- end form-group -->
                                    </div>
                                    <!-- end col-8 -->
                                </div>
                                <!-- end row -->
                            </fieldset>
                            <!-- end fieldset -->
                        </div>
                        <!-- end step-1 -->
                        <!-- begin step-2 -->
                        <div id="step-2">
                            <!-- begin fieldset -->
                            <fieldset>
                                <!-- begin row -->
                                <div class="row">
                                    <!-- begin col-8 -->
                                    <div class="col-md-8 md-offset-2">
                                        <legend class="no-border f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse">You contact info, so that we can easily reach you</legend>
                                        <!-- begin form-group -->
                                        <div class="form-group row m-b-10">
                                            <label class="col-md-3 col-form-label text-md-right">Phone Number <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                                <input type="number" name="phone" placeholder="123-456-7890" data-parsley-group="step-2" data-parsley-required="true" data-parsley-type="number" class="form-control" />
                                            </div>
                                        </div>
                                        <!-- end form-group -->
                                        <!-- begin form-group -->
                                        <div class="form-group row m-b-10">
                                            <label class="col-md-3 col-form-label text-md-right">Email Address <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                                <input type="email" name="email" placeholder="someone@example.com" class="form-control" data-parsley-group="step-2" data-parsley-required="true" data-parsley-type="email" />
                                            </div>
                                        </div>
                                        <!-- end form-group -->
                                    </div>
                                    <!-- end col-8 -->
                                </div>
                                <!-- end row -->
                            </fieldset>
                            <!-- end fieldset -->
                        </div>
                        <!-- end step-2 -->
                        <!-- begin step-3 -->
                        <div id="step-3">
                            <!-- begin fieldset -->
                            <fieldset>
                                <!-- begin row -->
                                <div class="row">
                                    <!-- begin col-8 -->
                                    <div class="col-md-8 offset-md-2">
                                        <legend class="no-border f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse">Select your login username and password</legend>
                                        <!-- begin form-group -->
                                        <div class="form-group row m-b-10">
                                            <label class="col-md-3 col-form-label text-md-right">Username <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                                <input type="text" name="username" placeholder="johnsmithy" class="form-control" data-parsley-group="step-3" data-parsley-required="true" data-parsley-type="alphanum" />
                                            </div>
                                        </div>
                                        <!-- end form-group -->
                                        <!-- begin form-group -->
                                        <div class="form-group row m-b-10">
                                            <label class="col-md-3 col-form-label text-md-right">Pasword <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                                <input type="password" name="password" placeholder="Your password" class="form-control" data-parsley-group="step-3" data-parsley-required="true" />
                                            </div>
                                        </div>
                                        <!-- end form-group -->
                                        <!-- begin form-group -->
                                        <div class="form-group row m-b-10">
                                            <label class="col-md-3 col-form-label text-md-right">Confirm Pasword <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                                <input type="password" name="password2" placeholder="Confirmed password" class="form-control" data-parsley-group="step-3" data-parsley-required="true" />
                                            </div>
                                        </div>
                                        <!-- end form-group -->
                                    </div>
                                    <!-- end col-8 -->
                                </div>
                                <!-- end row -->
                            </fieldset>
                            <!-- end fieldset -->
                        </div>
                        <!-- end step-3 -->
                        <!-- begin step-4 -->
                        <div id="step-4">
                            <div class="jumbotron m-b-0 text-center">
                                <h2 class="text-inverse">Register Successfully</h2>
                                <p class="m-b-30 f-s-16">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris consequat commodo porttitor. <br />Vivamus eleifend, arcu in tincidunt semper, lorem odio molestie lacus, sed malesuada est lacus ac ligula. Aliquam bibendum felis id purus ullamcorper, quis luctus leo sollicitudin. </p>
                                <p><a href="#" class="btn btn-primary btn-lg">Proceed to User Profile</a></p>
                            </div>
                        </div>
                        <!-- end step-4 -->
                    </div>
                    <!-- end wizard-content -->
                </div>
                <!-- end wizard -->
            </form>
            <!-- end wizard-form -->
        </div>
    </div>
    {{--end row--}}
@endsection

@section('js')
    <script src="{{ asset('assets/backend/plugins/parsley/dist/parsley.js') }}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-smart-wizard/src/js/jquery.smartWizard.js')}}"></script>
    <script src="{{ asset('assets/backend/js/demo/form-wizards-validation.demo.min.js') }}"></script>
@endsection

@section('custom-js')
    <script>
        $(document).ready(function() {
            App.init();
            FormWizardValidation.init();
        });
    </script>
@endsection