<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobTraining extends Model
{
    protected $table = 'r_hte_job_trainings';
}
