<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InternGrading extends Model
{
    //  table
    protected $table = 't_intern_grading';
    protected $primaryKey = 'grading_id';

    public $timestamps = false;
}
