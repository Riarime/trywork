<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InternApplication extends Model
{
    //
    protected $table = 't_intern_application';
    protected $primaryKey = 'iapp_id';
}
