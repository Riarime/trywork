<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HTEContact extends Model
{
    //  Table Name
    protected $table = 'r_hte_contact';
    //  Timestamps
    public $timestamps = false;
}
