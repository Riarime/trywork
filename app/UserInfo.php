<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserInfo extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'title', 'first_name', 'middle_name', 'last_name', 'gender', 'bdate', 'tel_no', 'cel_no'
    ];
}
