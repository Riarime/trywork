<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnivUserInfo extends Model
{
    //  Table
    protected $table = 'r_su_user_info';

    //  Timestamps
    public $timestamps = false;
}
