<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    //
    protected $table = 'r_regions';

    public static function getCoordinates($address){
        $url = "https://geocoder.api.here.com/6.2/geocode.json?app_id=xpiObBngnxcok8LPnJHB&app_code=xr-tZl551VoWIrRWLP8TOg&country=PHL&searchtext=".str_replace(' ', '+', $address);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response);
        $return = $response_a;

        if(isset($return->Response->View[0]->Result[0])){
            $coords['lat'] = $return->Response->View[0]->Result[0]->Location->DisplayPosition->Latitude;
            $coords['long'] = $return->Response->View[0]->Result[0]->Location->DisplayPosition->Longitude;
            return $coords;
        }
        else{
            return 'ERROR';
        }
    }

    public static function getDistance($latStart, $longStart, $latDestination, $longDestination){
        $url = "https://matrix.route.api.here.com/routing/7.2/calculatematrix.json?app_id=xpiObBngnxcok8LPnJHB&app_code=xr-tZl551VoWIrRWLP8TOg&start0=".$latStart.",".$longStart."&destination0=".$latDestination.",".$longDestination."&mode=balanced;car;traffic:disabled&summaryAttributes=distance";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response, true);
        $return = $response_a;
        return $return;
    }
}
