<?php

namespace App;


use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class HTEJobTrainings extends Model
{
    // Table
    protected $table = 'r_hte_job_trainings';
    protected static $hteUsers;
    protected static $skillSets;
    protected static $univIntern;
    protected static $jobTrainings;

    public function getHTEUsers()
    {
        if(!$this->hteUsers)  {
            $this->hteUsers = HTEUsers::all();
        }
        return $this->hteUsers;
    }

    public function getSkillSets()
    {
        if(!$this->skillSets)  {
            $this->skillSets = collect(DB::select("SELECT\n".
                "user_roles.ur_id AS student_id,\n".
                "r_intern_skills.name AS skill_name,\n".
                "r_su_intern.intern_id AS intern_id\n".
                "FROM\n".
                "r_intern_skills\n".
                "INNER JOIN r_intern_skill_sets ON r_intern_skill_sets.skill_id = r_intern_skills.skill_id\n".
                "INNER JOIN r_su_intern ON r_intern_skill_sets.intern_id = r_su_intern.intern_id\n".
                "INNER JOIN user_infos ON r_su_intern.info_id = user_infos.info_id\n".
                "INNER JOIN user_roles ON user_roles.info_id = user_infos.info_id"));
        }
        return $this->skillSets;
    }

    public function getUnivIntern()
    {
        if(!$this->univIntern)  {
            $this->univIntern = UnivIntern::all();
        }
        return $this->univIntern;
    }

    public function getAllJobTrainings()
    {
        if(!$this->jobTrainings)  {
            $this->jobTrainings = collect(DB::select("SELECT\n".
                "r_hte.hte_id AS `hte_id`,\n".
                "r_hte_job_trainings.job_training_id AS `id`,\n".
                "r_hte_job_trainings.training_code AS `code`,\n".
                "r_hte_job_trainings.job_training_name AS `name`,\n".
                "r_hte_job_trainings.description AS description\n".
                "FROM\n".
                "r_hte\n".
                "INNER JOIN r_hte_job_trainings ON r_hte_job_trainings.hte_id = r_hte.hte_id"));
        }
        return $this->jobTrainings;
    }

    public static function getJobTrainings()
    {
        $hte = (new HTEJobTrainings)->getHTEUsers()->first()->hte_id;
        $jobtrainings = (new HTEJobTrainings)->getAllJobTrainings()->where('hte_id', $hte);

        return $jobtrainings;
    }

    public static function getRecommendedStudents($jobID, $studentID = NULL)
    {
            $jobtraining = self::getJobTrainings()->where('id', $jobID)->first();
            if(null !== $studentID)
                $skillsets = (new HTEJobTrainings)->getSkillSets()->where('student_id', $studentID);
            else
                $skillsets = (new HTEJobTrainings)->getSkillSets();
            $fuzz = new \FuzzyWuzzy\Fuzz();
            $process = new \FuzzyWuzzy\Process($fuzz);
            $i = 0;
            $choices = array();
            foreach ((new HTEJobTrainings)->getUnivIntern() as $student){
                foreach ($skillsets->where('intern_id', $student->intern_id) as $skillset) {
                    $choices[$i]['intern_id'] = $student->intern_id;
                    $choices[$i]['skill_name'] = $skillset->skill_name;
                    $i++;
                }
            }
            $i = 0;
            $recommendation_array = array();
            foreach ($choices as $choice) {
                if($c = $process->extract($jobtraining->description, [$choice['skill_name']], null, [$fuzz, 'tokenSetRatio'], null)){
                    $recommendation_array[$i][0] = $choice['skill_name'];
                    $recommendation_array[$i][1] = $c->toArray();
                    $i++;
                }
            }
            $final_recommendation = array();
            $i = 0;
            $j = 0;
            $semi_final = array();
            foreach ($recommendation_array as $item) {
                foreach ($item as $value) {
                    if($item[1][0][1] > 30){
                        $semi_final[$i] = ['intern_id' => $item[0]];
                        $i++;
                    }
                }
            }
            $i = 0;
            if(array() !== $semi_final){
                $recommendations = collect($semi_final);
                foreach((new HTEJobTrainings)->getUnivIntern() as $student){
                    if(null !== $recommendations->where('intern_id', $student->intern_id))
                        $final_recommendation[$i] = $student->intern_id;
                    $i++;
                }
                return $final_recommendation;
            }
            else{
                return null;
            }
    }
}