<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InternApplicationStatus extends Model
{
    //
    protected $table = 'r_intern_application_status';
}
