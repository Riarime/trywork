<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GPA extends Model
{
    protected $table = "r_su_intern_gpa";
}
