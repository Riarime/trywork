<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnivUserAcademicRank extends Model
{
    //
    protected $table = 'r_su_academic_rank';
}
