<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollegeCourses extends Model
{
    //Table Name
    protected $table = 'r_su_college_courses';
}
