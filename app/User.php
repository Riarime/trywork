<?php

namespace App;

use Dompdf\Exception;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function getName($internid = null){
        if(null !== $internid){
            $user = UserInfo::where('info_id', UserRole::where('user_id', $internid)->first()->info_id)->first();
        }
        else{
            $user = UserInfo::where('info_id', UserRole::where('user_id', \Auth::user()->id)->first()->info_id)->first();
        }
        $fullname = $user->first_name.' '.$user->last_name;
        return $fullname;
    }

    public static function getInternName($userid = null){
        if(null !== $userid){
            $user = UserInfo::where('info_id', UnivIntern::where('intern_id', $userid)->first()->info_id)->first();
        }
        else{
            $user = UserInfo::where('info_id', UnivIntern::where('intern_id', \Auth::user()->id)->first()->info_id)->first();
        }
        $fullname = $user->first_name.' '.$user->last_name;
        return $fullname;
    }

    public static function getProgram($userid = null){
        if(null !== $userid){
            $user = UnivIntern::where('intern_id',  $userid)->first();
        }
        else{
            $user = UnivIntern::where('info_id', UserRole::where('user_id', \Auth::user()->id)->first()->info_id)->first();
        }
        $program = CollegeCourses::where('course_id', $user->bc_id)->first();
        return $program->course_name;
    }

    public static function getRegion($userid = null){
        if(null !== $userid){
            $user = UnivIntern::where('intern_id',  $userid)->first();
        }
        else{
            $user = UnivIntern::where('info_id', UserRole::where('user_id', \Auth::user()->id)->first()->info_id)->first();
        }
        $region = $user->region_id;
        return $region;
    }

    public static function getSUC($userid = null){
        if(null !== $userid){
            $user = UnivIntern::where('intern_id', $userid)->first();
        }
        else{
            $user = UnivIntern::where('info_id', UserRole::where('user_id', \Auth::user()->id)->first()->info_id)->first();
        }
        $suc = StateUniversity::where('su_id', UnivColleges::where('college_id', CollegeCourses::where('course_id', $user->bc_id)->first()->college_id)->first()->su_id)->first();
        return $suc->su_name;
    }


    public static function getSUCAbbrev($userid = null){
        if(null !== $userid){
            $user = UnivIntern::where('intern_id', $userid)->first();
        }
        else{
            $user = UnivIntern::where('info_id', UserRole::where('user_id', \Auth::user()->id)->first()->info_id)->first();
        }
        $suc = StateUniversity::where('su_id', UnivColleges::where('college_id', CollegeCourses::where('course_id', $user->bc_id)->first()->college_id)->first()->su_id)->first();
        return $suc->su_abbrv;
    }

    public static function getStudentSkills($userid = null){
        if(null !== $userid){
            $student_skillsets = InternSkillSet::where('intern_id', $userid)->get();
        }
        else{
            $student_skillsets = InternSkillSet::where('intern_id', UnivIntern::where('info_id', UserRole::where('user_id', \Auth::user()->id)->first()->info_id)->first()->intern_id)->get();
        }
        $i = 0;
        foreach ($student_skillsets as $skillset){
            $skills[$i] = InternSkill::where('skill_id', $skillset->skill_id)->first()->name;
            $i++;
        }
        return $skills;
    }

    public static function getApplicationStatus($job_training, $intern = null, $active = null){
        if(null !== $intern && null !== $active){
            $application = InternApplication::where('job_training_id', $job_training)->where('intern_id', $intern)->where('status', $active)->orderBy('created_at','desc')->first();
        }
        else if(isset($intern)){
            $application = InternApplication::where('job_training_id', $job_training)->where('intern_id', $intern)->orderBy('created_at','desc')->first();
        }
        else{
            $application = InternApplication::where('job_training_id', $job_training)->where('intern_id', UserRole::where('user_id', \Auth::user()->id)->first()->intern_id)->orderBy('created_at','desc')->first();
        }
        $status = InternApplicationStatus::where('iappstat_id', $application->iappstat_id)->first();
        return $status;
    }

    public static function getHTEName(){
        $hte = \App\HTE::where('hte_id', \App\HTEUsers::where('ur_id', \App\UserRole::where('user_id', \Auth::user()->id)->first()->ur_id)->first()->hte_id)->first();
        $name = $hte->hte_name;
        return $name;
    }

    public static function getHTEid(){
        $hte = \App\HTE::where('hte_id', \App\HTEUsers::where('ur_id', \App\UserRole::where('user_id', \Auth::user()->id)->first()->ur_id)->first()->hte_id)->first();
        $id = $hte->hte_id;
        return $id;
    }

    public static function getUserInfo(){
        $hte = \App\HTE::where('hte_id', \App\HTEUsers::where('ur_id', \App\UserRole::where('user_id', \Auth::user()->id)->first()->ur_id)->first()->hte_id)->first();
        $id = $hte->id;
        return $id;
    }

    public static function getHTEGrade($intern){
        $grading_id = InternGrading::where('intern_id', $intern)->orderBy('grading_id')->first()->grading_id;
        $hte_grade = HTEGrade::where('grading_id', $grading_id)->first();

        return $hte_grade;
    }
}
