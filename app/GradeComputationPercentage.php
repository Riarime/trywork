<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GradeComputationPercentage extends Model
{
    //
    protected $table = 'r_grade_percentage';
}
