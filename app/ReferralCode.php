<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReferralCode extends Model
{
    //
    protected $table = 'r_intern_referral';

    public $timestamps = false;
}
