<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BranchCourse extends Model
{
    //
    protected $table = 'r_branch_courses';
}
