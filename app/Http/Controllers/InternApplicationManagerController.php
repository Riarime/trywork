<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InternApplication;
use App\InternApplicationStatus;

class InternApplicationManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(null != $existing = InternApplication::where('intern_id', $request->student_id)->where('job_training_id', $request->job_training_id)->where('status',true)->first()){
            $existing->status = false;
            $existing->save();
        }
        $update = new InternApplication();
        $update->intern_id = $request->student_id;
        $update->job_training_id =$request->job_training_id;
        $update->iappstat_id = InternApplicationStatus::where('iappstat_code', $request->status)->first()->iappstat_id;
        $update->save();

        return redirect(route('hte.job-trainings-management'))->with('message', 'Changed the intern to '.InternApplicationStatus::where('iappstat_code', $request->status)->first()->name);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
