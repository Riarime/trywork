<?php

namespace App\Http\Controllers;

use App\BranchCourse;
use App\BranchSem;
use App\CollegeCourses;
use App\GPA;
use App\GradeComputationPercentage;
use App\HTEJobTrainings;
use App\HTEUsers;
use App\InternApplication;
use App\InternSkill;
use App\InternSkillSet;
use App\Region;
use App\role;
use App\StateUniversity;
use App\UnivHTE;
use App\UnivIntern;
use App\User;
use App\UserInfo;
use App\UserRole;
use Illuminate\Http\Request;
use App\BranchAY;
use App\BranchAYSem;
use App\UnivBranches;
use App\HTE;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PageController extends Controller
{
    //  Registration Page

    public function registration()
    {
        return view('pages.registration.register');
    }

    public function su_register()
    {
        $regions = Region::select('region_id', 'code', 'name')->get()->toArray();
        $univs = StateUniversity::select('su_code', 'su_name')->get()->toArray();
        return view('pages.registration.ojt-personnel', compact('regions', 'univs'));
    }

    public function hte_register()
    {
        return view('pages.registration.hte');
    }

    public function intern_register($code)
    {

        $resp = DB::select("SELECT
r_intern_referral.referral_code,
r_su_intern.intern_id,
r_su_intern.stud_no,
r_su_intern.info_id,
r_su_intern.contact_email,
r_su_intern.intern_address,
r_branch_acdmcyr.academic_yr,
r_regions.`name`,
r_su_college_courses.course_name,
r_su_branches.branch_name,
CONCAT(first_name,' ',
user_infos.middle_name,' ',
user_infos.last_name) Stud_Name,
user_infos.gender,
user_infos.bdate,
user_infos.tel_no,
user_infos.cel_no,
r_intern_referral.referral_code,
r_su_colleges.college_name
FROM
r_su_intern
INNER JOIN r_regions ON r_su_intern.region_id = r_regions.region_id
INNER JOIN r_branch_acdmcyr ON r_su_intern.ay_id = r_branch_acdmcyr.ay_id
INNER JOIN r_su_branches ON r_su_intern.branch_id = r_su_branches.branch_id AND r_branch_acdmcyr.branch_id = r_su_branches.branch_id
INNER JOIN user_infos ON r_su_intern.info_id = user_infos.info_id
INNER JOIN r_intern_referral ON r_intern_referral.intern_id = r_su_intern.intern_id
INNER JOIN r_su_college_courses ON r_su_intern.bc_id = r_su_college_courses.course_id
INNER JOIN r_su_colleges ON r_su_college_courses.college_id = r_su_colleges.college_id
WHERE r_intern_referral.referral_code = '$code'
AND r_intern_referral.Status IS NULL");
//        dd($resp);
        if (count($resp) != 0)
            return view('pages.registration.intern', compact('resp'));
        else
            return 'No credentials found';
    }

    //  Configuration Page
    public function configs()
    {
        $user = 'coordinator';
        $view = 'config';
        $su = session('su_id');
        $branch = session('branch_id');
        $college = session('college_id');
        $name = UserInfo::select('first_name', 'middle_name', 'last_name')
            ->where('info_id', UserRole::where('user_id', Auth::user()->id)->first()->info_id)
            ->first()->toArray();
        $user_role = role::where('role_id', UserRole::where('user_id', Auth::user()->id)->first()->role_id)->first()->name;
        $active = DB::table('r_branch_active_ay_sem as Active')
            ->select('Active.aysem_id', 'AY.ay_code', 'AY.academic_yr', 'Sem.semester')
            ->join('r_branch_acdmcyr as AY', 'AY.ay_id', '=', 'Active.ay_id')
            ->join('r_branch_sem as Sem', 'Sem.sem_id', '=', 'Active.sem_id')
            ->where('Active.branch_id', '=', $branch)
            ->where('Active.aysem_stat', '=', 'Active')
            ->get()->toArray();
        $ays = BranchAY::select('ay_code', 'academic_yr')
            ->where('branch_id', '=', $branch)
            ->where('ay_stat', '=', 'Active')
            ->get()->toArray();
        $semesters = BranchSem::select('sem_code', 'semester')
            ->where('branch_id', '=', $branch)
            ->where('sem_stat', '=', 'Active')
            ->get()->toArray();
        $programs = CollegeCourses::select('course_code', 'course_abbrv', 'course_name')
            ->where('college_id', '=', $college)
            ->where('course_stat', '=', 'Active')
            ->get()->toArray();
        $yearsection = DB::table('r_branch_courses as BC')
            ->select('BC.bc_id', 'CC.course_abbrv', 'BC.year_section', 'BC.bc_stat')
            ->join('r_su_college_courses as CC', 'CC.course_id', '=', 'BC.course_id')
            ->where('CC.college_id', '=', $college)
            ->get()->toArray();
        //dd($programs);
        return view('pages.coordinator.config', compact('ays', 'user', 'view', 'active', 'name', 'user_role', 'semesters', 'programs', 'yearsection'));
    }

    public function intern()
    {
        $user = 'coordinator';
        $view = 'intern';
        $branch = session('branch_id');
        $college = session('college_id');
        $name = UserInfo::select('first_name', 'middle_name', 'last_name')
            ->where('info_id', UserRole::where('user_id', Auth::user()->id)->first()->info_id)
            ->first()->toArray();
        $user_role = role::where('role_id', UserRole::where('user_id', Auth::user()->id)->first()->role_id)->first()->name;
        $active = DB::table('r_branch_active_ay_sem as Active')
            ->select('Active.aysem_id', 'AY.ay_code', 'Active.ay_id')
            ->join('r_branch_acdmcyr as AY', 'AY.ay_id', '=', 'Active.ay_id')
            ->join('r_branch_sem as Sem', 'Sem.sem_id', '=', 'Active.sem_id')
            ->where('Active.branch_id', '=', $branch)
            ->where('Active.aysem_stat', '=', 'Active')
            ->get()->toArray();
        if (isset($active)) {
            $ays = BranchAY::select('ay_code as code', 'academic_yr as ay')
                ->where('branch_id', '=', $branch)
                ->where('ay_id', '<=', (BranchAY::where('ay_code', '=', $active[0]->ay_code)->get()->pluck('ay_id')->toArray()))
                ->where('ay_stat', '=', 'Active')
                ->get()->toArray();
        }
        $programs = json_encode(
            DB::table('r_branch_courses as BC')
                ->select('BC.bc_id as program_id', 'CC.course_abbrv as course', 'BC.year_section')
                ->join('r_su_branches as B', 'B.branch_id', '=', 'BC.branch_id')
                ->join('r_su_college_courses as CC', 'CC.course_id', '=', 'BC.course_id')
                ->join('r_su_colleges as C', 'C.college_id', '=', 'CC.college_id')
                ->where('BC.branch_id', '=', $branch)
                ->where('C.college_id', '=', $college)
                ->where('BC.bc_stat', '=', 'Active')
                ->get()->toArray()
        );
        $gpa = json_encode(GPA::pluck('gpa')->toArray());
        //dd(json_encode($programs));
        return view('pages.coordinator.intern', compact('user', 'view', 'active', 'name', 'user_role', 'ays', 'programs', 'gpa'));
    }

    //Coordinator Services
    public function coordinator_services()
    {
        $user = 'coordinator';
        $view = 'hte';
        $branch = session('branch_id');
        $name = UserInfo::select('first_name', 'middle_name', 'last_name')
            ->where('info_id', UserRole::where('user_id', Auth::user()->id)->first()->info_id)
            ->first()->toArray();
        $user_role = role::where('role_id', UserRole::where('user_id', Auth::user()->id)->first()->role_id)->first()->name;
        $contacts = collect(DB::select("SELECT *\n" .
            "FROM\n" .
            "r_hte_contact"));
        $htes = collect(DB::select("SELECT *\n" .
            "FROM\n" .
            "r_hte"));
        $partnerships = collect(DB::select("SELECT *\n" .
            "FROM\n" .
            "t_su_hte\n" .
            "INNER JOIN r_su_branches ON t_su_hte.branch_id = r_su_branches.branch_id\n" .
            "INNER JOIN r_hte ON t_su_hte.hte_id = r_hte.hte_id\n" .
            "WHERE\n" .
            "t_su_hte.branch_id = 1"));
        return view('pages.coordinator.hte', compact('user', 'view', 'partnerships', 'htes', 'contacts', 'name', 'user_role'));
    }

    public function partnership_process(Request $request)
    {
        $partnership = new UnivHTE();
        $partnership->branch_id = session('branch_id');
        $partnership->college_id = session('college_id');
        $partnership->hte_id = HTE::where('hte_code', $request->hte_partner)->first()->hte_id;
        $partnership->save();

        return redirect(route('services'));
    }

    public function hte_dashboard()
    {
        $deployed = InternApplication::where('iappstat_id', 4)->where('status', true)->count();
        $partnership = UnivHTE::where('hte_id', HTEUsers::where('ur_id', UserRole::where('user_id', Auth::user()->id)->first()->ur_id)->first()->hte_id)->where('affiliated_status', 'Active')->count();
        return view('pages.hte.dashboard', compact('deployed', 'partnership'));
    }

    public function intern_finder()
    {
        $students = \App\UnivIntern::all();
        $sucs = collect(DB::select("SELECT\n" .
            "	r_su_college_courses.course_id AS id,\n" .
            "	r_su.su_name AS name, \n" .
            "	r_su_college_courses.course_name AS course \n" .
            "FROM\n" .
            "	r_su_intern\n" .
            "	INNER JOIN r_su_college_courses ON r_su_intern.bc_id = r_su_college_courses.course_id\n" .
            "	INNER JOIN r_su_colleges ON r_su_college_courses.college_id = r_su_colleges.college_id\n" .
            "	INNER JOIN r_su ON r_su_colleges.su_id = r_su.su_id"));
        $jobTrainings = HTEJobTrainings::getJobTrainings();
        $regions = Region::all();
        $internSkillSets = InternSkillSet::all();
        $internSkills = InternSkill::all();
        $internApplications = InternApplication::all();
        $i = 0;
        foreach ($jobTrainings as $jobTraining) {
            $recommendedStudents[$i] = HTEJobTrainings::getRecommendedStudents($jobTraining->id);
            if (isset($recommendedStudents[$i]))
                $i++;
        }
        $recommendedStudents = collect($recommendedStudents);
        return view('pages.hte.intern_finder', compact('students', 'sucs', 'jobTrainings', 'regions', 'internSkills', 'internSkillSets', 'internApplications', 'recommendedStudents'));
    }

    public function hte_intern_finder()
    {
        $interns = UnivIntern::all();
        $activeAYSem = BranchAYSem::where('aysem_stat', 'Active')->get();
        $internInfos = UserInfo::whereIn('info_id', array_column($interns->toArray(), 'info_id'))->get();
        $partners = UnivHTE::where('hte_id', User::getHTEid())->where('affiliated_status', 'Active')->get();
        $branchPartners = UnivBranches::whereIn('branch_id', array_column($partners->toArray(), 'branch_id'))->get();
        return view('pages.hte.intern_finder_ui', compact('interns', 'internInfos', 'branchPartners', 'activeAYSem'));
    }

    public function algo_test()
    {
        return view('pages.hte.algo_test');
    }
}
