<?php

namespace App\Http\Controllers\Auth;

use App\AuditTrail;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
//    protected $redirectTo = '/system/redirect';
    public function redirectTo(){
        $role_id = \App\UserRole::where('user_id', \Auth::user()->id)->first()->role_id;

        $audit = new AuditTrail();
        $audit->user_id = \Auth::user()->id;
        $audit->audit_details = 1;
        $audit->save();

        switch($role_id){
            case \App\role::where('name', 'System Administrator')->first()->role_id:
                return route('admin.audit-trail');
            case \App\role::where('name', 'Student Trainee')->first()->role_id:
                return route('intern.home');
            case \App\role::where('name', 'HTE Representative')->first()->role_id:
                return route('hte.home');
            case \App\role::where('name', 'OJT Coordinator')->first()->role_id:
                session()->put('su_id',
                    DB::table('r_su_users as UU')
                        ->join('user_roles as UR','UR.ur_id','=','UU.ur_id')
                        ->where('UR.user_id','=',Auth::user()->id)
                        ->get()->pluck('su_id')->toArray()[0]);

                session()->put('branch_id',
                    DB::table('r_su_users as UU')
                    ->join('user_roles as UR','UR.ur_id','=','UU.ur_id')
                    ->where('UR.user_id','=',Auth::user()->id)
                    ->get()->pluck('branch_id')->toArray()[0]);

                $college_id = DB::table('r_su_user_info as UU_Info')
                    ->join('r_su_users as UU','UU.uu_id','=','UU_Info.uu_id')
                    ->join('user_roles as UR','UR.ur_id','=','UU.ur_id')
                    ->where('UR.user_id','=',Auth::user()->id)
                    ->get()->pluck('college_id')->toArray();

                if(isset($college_id[0])){
                    $college_id = $college_id[0];
                }
                else{
                    $college_id = null;
                }

                session()->put('college_id', $college_id);

                return route('coordinator.dashboard');
            case \App\role::where('name', 'OJT Head')->first()->role_id:
                session()->put('su_id',
                    DB::table('r_su_users as UU')
                        ->join('user_roles as UR','UR.ur_id','=','UU.ur_id')
                        ->where('UR.user_id','=',Auth::user()->id)
                        ->get()->pluck('su_id')->toArray()[0]);

                session()->put('branch_id',
                    DB::table('r_su_users as UU')
                        ->join('user_roles as UR','UR.ur_id','=','UU.ur_id')
                        ->where('UR.user_id','=',Auth::user()->id)
                        ->get()->pluck('branch_id')->toArray()[0]);

                return route('head.dashboard');
            case \App\role::where('name', 'OJT Director')->first()->role_id:
                session()->put('su_id',
                    DB::table('r_su_users as UU')
                        ->join('user_roles as UR','UR.ur_id','=','UU.ur_id')
                        ->where('UR.user_id','=',Auth::user()->id)
                        ->get()->pluck('su_id')->toArray()[0]);

                return route('director.dashboard');
            default:
                return route('home');
        }
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
