<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\BranchAYSem;
use App\BranchSem;
use App\UserInfo;
use App\UnivIntern;


class FileController extends Controller
{
    public function downloadExcel($type) {
        return Excel::create('Student Trainee - ('.''.')', function($excel){
            $excel->setCreator('i-Interno');
        })->download();
    }

    public function importExcel(Request $request) {

        if ($request->hasFile('intern_application')) {
            
            $path = $request->file('intern_application')->getRealPath();
            $data = Excel::selectSheetsByIndex(0)->load($path, function($reader) {})->get();
            $given_program = $request->select_program;

            if (!empty($data) && $data->count()) {
                $interns = array();
                $invalid = array();
                $error = array();

                foreach($data->toArray() as $key => $value) {
                    if (!empty($value)) {
                        $semester = BranchAYSem::where('ay_id', BranchActiveAY::pluck('ay_id')->first())->pluck('aysem_id')->first();
                        $section = BCYearSection::where('ys_code',$given_program)->pluck('ys_id');
                        $code = 'BSU-I'.Carbon::now()->format('y').(UnivIntern::get()->count() + 1);
/*
                        try {
                            if ($value['program'] == $given_program) {
                                array_push($interns, array(
                                    'firstname' => $value['first_name'],
                                    'middlename' => $value['middle_name'],
                                    'lastname' => $value['last_name'],
                                    'gender' => $value['gender'],
                                    'bdate' => $value['birth_date'],
                                    'tel_no' => $value['telephone_number'],
                                    'cel_no' => $value['cellphone_number']
                                ));
                            } else {
                                array_push($invalid, array(
                                    'invalid' => function($data) {
                                        return (is_null($data->$value['middle_name'])) ? $data->$value['first_name']." ".$data->$value['last_name'] : $data->$value['first_name']." ".$data->$value['middle_name']." ".$data->$value['last_name'];
                                    }
                                ));
                            }
                        } catch (Exception $e) {
                            array_push($error, array(
                                'error' => $e
                            ));
                        }*/

                         $info = new UserInfo();
                         $info->first_name = $value['first_name'];
                         $info->middle_name = $value['middle_name'];
                         $info->last_name = $value['last_name'];
                         $info->gender = $value['gender'];
                         $info->bdate = $value['birth_date'];
                         $info->tel_no = $value['telephone_number'];
                         $info->cel_no = $value['cellphone_number'];
                         $info->GPA = $value['gpa'];
                         $info->save();
                         $intern_info = $info->id;
                         $intern = UnivIntern::create(['aysem_id' => $semester, 'ys_id' => $section[0], 'info_id' => $intern_info, 'intern_code' => $code, 'stud_no' => $value['student_number'], 'contact_email' => $value['email'], 'intern_address' => $value['address'], 'su_usr_id' => 1]);
                    }
                }
                 return back()->with('success','Successfully Imported');
                /*print('interns:<br>');
                print_r($interns);
                print('<br><br>invalid:<br>');
                print_r($invalid);
                print('<br><br>error:<br>');
                print_r($error);*/
            }
        }
         return back()->with('error','File is Invalid');
    }

}
