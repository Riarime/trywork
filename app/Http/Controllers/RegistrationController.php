<?php

namespace App\Http\Controllers;

use App\role;
use App\StateUniversity;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RegistrationController extends Controller
{


    function check(Request $request){

    }

    function ojt_personnel(Request $request) {
        $datetime = Carbon::now()->format('Y-m-d H:i:s');
        $role = role::where('name','=','OJT Director')->pluck('role_id')->toArray()[0];
        $su = StateUniversity::where('su_code','=',$request->su)->pluck('su_id')->toArray()[0];
        $user_code = 'TWU'.Carbon::now()->format('y-md-').(User::get()->count() + 1);;
        $user = new User();
        $user->usr_code = $user_code;
        $user->email = $request->email;
        $user->password = bcrypt($request->pass);
        $user->save();
        $account = $user->id;
        $info = DB::table('user_infos')->insertGetId([
            'first_name' => $request->fname,
            'middle_name' => $request->mname,
            'last_name' => $request->lname,
            'gender' => $request->gender,
            'bdate' => $request->bdate,
            'cel_no' => $request->celno,
            'tel_no' => $request->telno
        ], 'info_id');
        $usr_role = DB::table('user_roles')->insertGetId(['user_id' => $account, 'role_id' => $role, 'info_id' => $info],'ur_id');
        $su_user = DB::table('r_su_users')->insertGetId(['su_id' => $su, 'ur_id' => $usr_role],'uu_id');
        DB::table('r_su_user_info')->insert(['uu_id' => $su_user, 'emp_stat' => $request->empstat]);
        DB::table('r_su_usr_educbg')->insert([
            'uu_id' => $su_user,
            'degree' => $request->degree,
            'school_graduated' => $request->gradSchool,
            'year_graduated' => $request->gradYear,
            'received_awards' => $request->awards
        ]);
        return response()->json(['message' => 'OJT Personnel Registration Successfully!']);
    }

    function intern(Request $request){
//
//        DB::table('user_infos')->insert([
//            'first_name'      => $request->firstname,
//            'middle_name'             => $request->middlename,
//            'last_name' => $request->lastname,
//            'gender'      => $request->input('gender'),
//            'bdate'             => date('Y-m-d', strtotime($request->year.'-'.$request->month.'-'.$request->day)),
//            'cel_no'       => $request->contact
//        ]);
//
//        $arr =array_map('intval',explode(',',$request->college));
//        $UsrInflatestId = DB::SELECT('SELECT MAX(info_id) ss FROM user_infos')[0]->ss;
//
        DB::table('r_su_intern')
            ->where('intern_id', $request->intern_id)
            ->update([
            'intern_address'       => $request->address,
            'region_id'       => $request->input('region'),
        ]);
        DB::table('user_infos')
            ->where('info_id', $request->info_id)
            ->update([
            'gender'       => $request->gender,
            'bdate'       => $request->bday,
            'tel_no'       => $request->telno,
            'cel_no'       => $request->celno,
        ]);

        $SUInlatestId = $request->intern_id;


        for($i=0; $i<count($request->skills);$i++){
            DB::table('r_intern_skill_sets')->insert([
                'intern_id' =>$SUInlatestId ,
                'skill_id' =>$request->skills[$i],

            ]);
        }

        DB::table('users')->insert([
            'usr_code' =>'TWU18-101'.($SUInlatestId+1).'-'.($SUInlatestId+1),
            'email' =>$request->email,
            'password' =>bcrypt($request->password),
        ]);


        $UsrlatestId = DB::SELECT('SELECT MAX(id) ss FROM users')[0]->ss;


        DB::table('user_roles')->insert([
            'user_id' =>$UsrlatestId,
            'role_id' =>5,
            'info_id' =>$SUInlatestId,
        ]);

        DB::table('r_intern_referral')
            ->where('referral_code', $request->referral_code)
            ->where('intern_id', $SUInlatestId)
            ->update(['status' => 'Used']);

        return redirect(route('login'));
    }
}