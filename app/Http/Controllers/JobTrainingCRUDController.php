<?php

namespace App\Http\Controllers;

use App\User;
use App\HTEJobTrainings;
use Illuminate\Http\Request;

class JobTrainingCRUDController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hte_id = \App\HTE::where('hte_id', \App\HTEUsers::where('ur_id', \App\UserRole::where('user_id', \Auth::user()->id)->first()->ur_id)->first()->hte_id)->first()->hte_id;
        $jobTraining_id = HTEJobTrainings::where('hte_id', $hte_id)->count();
        $code = 'OJT-'.str_pad($hte_id, 4,'0', STR_PAD_LEFT).'-'.str_pad($jobTraining_id, 3, '0', STR_PAD_LEFT);
        $jobtraining = new HTEJobTrainings();
        $jobtraining->hte_id = $hte_id;
        $jobtraining->training_code = $code;
        $jobtraining->job_training_name = $request->name;
        $jobtraining->description = $request->description;
        $jobtraining->start = date('Y-m-d', strtotime($request->start));
        $jobtraining->save();

        return redirect(route('hte.intern-finder'))->with('message', 'Job Training Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
