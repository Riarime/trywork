<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserManagementController extends Controller
{
    function admin_recordlist() {
        $view = 'record-list';
        return view('pages.admin.user-management', compact('view'));
    }
}
