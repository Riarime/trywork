<?php

namespace App\Http\Controllers;

use App\HTEGrade;
use App\HTEUsers;
use App\InternGrading;
use App\User;
use Illuminate\Http\Request;

class HTEGradingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $intern_grade = new InternGrading();
        $intern_grade->grading_code = 'INTGRD-'.str_pad(InternGrading::count(), 6, '0', STR_PAD_LEFT);
        $intern_grade->intern_id = $request->student_id;
        $intern_grade->save();

        $hte_grade = new HTEGrade();
        $hte_grade->grading_id = $intern_grade->grading_id;
        $hte_grade->hte_id = \App\HTEUsers::where('ur_id', \App\UserRole::where('user_id', \Auth::user()->id)->first()->ur_id)->first()->hte_id;
        $hte_grade->intern_grade = $request->grade;
        $hte_grade->remarks = $request->remarks;
        $hte_grade->save();

        return redirect(route('hte.job-trainings-management'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
