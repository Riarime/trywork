<?php

namespace App\Http\Controllers;

use App\BranchAY;
use App\ReferralCode;
use App\UnivIntern;
use App\UserInfo;
use App\InternGrading;
use App\BCGrade;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TableDataController extends Controller
{
    public function getInternList(Request $request) {
        if ($request->isMethod('post')) {
            $ay = $request->ay_code;
            $program = $request->program_id;
            $branch = session()->get('branch_id');
            $record = DB::table('r_branch_active_ay_sem as Active')
                ->join('r_branch_acdmcyr as AY','AY.ay_id','=','Active.ay_id')
                ->where('Active.branch_id','=',$branch)
                ->pluck('ay_code')->toArray()[0];
            $interns = DB::table('r_su_intern as Intern')
                ->select('Intern.intern_code','Intern.stud_no','Info.first_name','Info.middle_name','Info.last_name','Intern.contact_email as email','Intern.GPA','Intern.intern_stat')
                ->join('user_infos as Info','Info.info_id','=','Intern.info_id')
                ->join('r_branch_acdmcyr as AY','AY.ay_id','=','Intern.ay_id')
                ->where('Intern.branch_id','=',$branch)
                ->where('AY.ay_code','=',$ay)
                ->where('Intern.bc_id','=',$program)
                ->orderBy('Intern.created_at','desc')
                ->get();
            if (count($record) > 0 && $ay == $record) {
                return response()->json(['record' => 'current','interns' => $interns]);
            } else {
                return response()->json(['record' => 'past','interns' => $interns]);
            }
        }
    }

    public function preAssess(Request $request) {
        $branch = session()->get('branch_id');
        $code = UnivIntern::where('branch_id','=',$branch)->get()->count();
        $id = UnivIntern::where('branch_id','=',$branch)
            ->where('intern_code','=',$request->intern)
            ->pluck('intern_id')->toArray()[0];
        UnivIntern::where('branch_id','=',$branch)
            ->where('intern_id','=',$id)
            ->update(['intern_stat' => $request->status]);

        $referral_code = new ReferralCode();
        $referral_code->intern_id = $id;
        $referral_code->referral_code = Carbon::now()->format('ydm-His').$code;
        $referral_code->save();

        return response()->json(['message' => 'Student successfully assessed!']);
    }

    public function getReferralCode(Request $request) {
        $intern_code = $request->intern;
        if (ReferralCode::where('intern_id','=',(UnivIntern::where('intern_code','=',$request->intern)->pluck('intern_id')->toArray()[0]))->count() > 0) {
            $referral_code = DB::table('r_su_intern as Intern')
                ->select('Referral.referral_code')
                ->join('r_intern_referral as Referral','Intern.intern_id','=','Referral.intern_id')
                ->where('Intern.intern_code','=',$intern_code)
                ->get()->toArray()[0];
            return response()->json(['return' => true,'code' => $referral_code->referral_code]);
        } else {
            return response()->json(['return' => false,'message' => 'Student not yet set for internship. Pre-assess first!']);
        }
    }

    public function setUpdateIntern(Request $request) {
        if ($request->isMethod('post')) {
            $intern_code = $request->intern;
            $intern = DB::table('r_su_intern as Intern')
                ->select('Intern.intern_code','Intern.stud_no','Intern.contact_email','Info.first_name','Info.middle_name','Info.last_name','Intern.bc_id','Intern.intern_stat')
                ->join('user_infos as Info','Info.info_id','=','Intern.info_id')
                ->where('Intern.intern_code','=',$intern_code)
                ->get();
            return response()->json($intern[0]);
        }
    }

    public function updateIntern(Request $request) {
        if ($request->isMethod('post')) {
            $code = $request->code;
            $fname = $request->fname;
            $mname = $request->mname;
            $lname = $request->lname;
            UnivIntern::where('intern_code','=',$code)
                ->update(['bc_id' => $request->program, 'stud_no' => $request->studno, 'contact_email' => $request->email, 'GPA' => $request->gpa]);

            $info = UserInfo::where('info_id','=',UnivIntern::where('intern_code','=',$code)->get()->pluck('info_id')[0])
                ->update(['first_name' => $fname, 'middle_name' => $mname, 'last_name' => $lname]);

            return response()->json(['message' => 'Student details successfully updated!']);
        }
    }

    public function getPlacementList(Request $request) {
        if ($request->isMethod('post')) {
            $branch = session()->get('branch_id');
            $ay = $request->ay_code;
            $program = $request->program_id;
            /*$placements = DB::table('t_hte_accepted_intern as Accepted')
                ->select('Accepted.ai_id','Intern.stud_no','Info.first_name','Info.middle_name','Info.last_name','HTE.hte_name','Accepted.su_grade','Accepted.hte_grade','Accepted.ai_stat')
                ->join('r_hte as HTE','HTE.hte_id','=','Accepted.hte_id')
                ->join('r_su_intern as Intern','Intern.intern_id','=','Accepted.intern_id')
                ->join('user_infos as Info','Info.info_id','=','Intern.info_id')
                ->join('r_branch_acdmcyr as AY','AY.ay_id','=','Intern.ay_id')
                ->where('Intern.branch_id','=',1)
                ->where('AY.ay_code','=',$ay)
                ->where('Intern.bc_id','=',$program)
                ->get();*/
            $interns = DB::table('r_su_intern as Intern')
                ->select('Intern.intern_id','Intern.intern_code','Intern.stud_no','Info.first_name','Info.middle_name','Info.last_name','Intern.intern_stat')
                ->join('user_infos as Info','Info.info_id','=','Intern.info_id')
                ->join('r_branch_acdmcyr as AY','AY.ay_id','=','Intern.ay_id')
                ->where('Intern.branch_id','=',$branch)
                ->where('AY.ay_code','=',$ay)
                ->where('Intern.bc_id','=',$program)
                ->where('intern_stat', '!=' ,'Enrolled')
                ->orWhere('intern_stat','!=', 'Not Qualified')
                ->get()->toArray();
            $placements = [];
            foreach($interns as $intern) {
                array_push(
                    $placements,
                    DB::table('r_su_intern as Intern')
                        ->select('Accepted.ai_id','HTE.hte_name','Accepted.su_grade','Accepted.hte_grade','Accepted.ai_stat')
                        ->join('t_hte_accepted_intern as Accepted','Intern.intern_id','=','Accepted.intern_id')
                        ->join('r_hte as HTE','HTE.hte_id','=','Accepted.hte_id')
                        ->where('Intern.branch_id','=',$branch)
                        ->where('Intern.intern_id','=',$intern->intern_id)
                        ->get()->toArray()
                );
            }
            return response()->json(['interns' => $interns, 'placement' => $placements]);
        }
    }

    public function getPlacementDetails(Request $request) {
        if ($request->isMethod('post')) {
            $details = DB::table('t_hte_accepted_intern as Accepted')
                ->select('Intern.stud_no','Info.first_name','Info.middle_name','Info.last_name','Program.course_abbrv','YearSec.year_section','Intern.intern_stat','HTE.hte_name','HTE.address','Contact.contact_person','Contact.designation','Contact.contact_no')
                ->join('r_hte as HTE','HTE.hte_id','=','Accepted.hte_id')
                ->join('r_hte_contact as Contact','HTE.hte_id','=','Contact.hte_id')
                ->join('r_su_intern as Intern','Intern.intern_id','=','Accepted.intern_id')
                ->join('user_infos as Info','Info.info_id','=','Intern.info_id')
                ->join('r_branch_courses as YearSec','YearSec.bc_id','=','Intern.bc_id')
                ->join('r_su_college_courses as Program','Program.course_id','=','YearSec.course_id')
                ->where('Intern.intern_id','=',$request->intern)
                ->get();
            return response()->json($details[0]);
        }
    }

    public function gradeAssessment(Request $request) {
        if ($request->isMethod('post')) {
            $branch = session()->get('branch_id');
            $grading_id = InternGrading::select('grading_id')
                ->where('intern_id','=',$request->intern)
                ->get()[0]->grading_id;
            $assessment = new BCGrade();
            $assessment->grading_id = $grading_id;
            $assessment->branch_id = $branch;
            $assessment->intern_grade = $request->grade;
            $assessment->remarks = $request->remarks;
            $assessment->save();

            return response()->json(['Feedback assessment successfully submitted!']);
        }
    }
}
