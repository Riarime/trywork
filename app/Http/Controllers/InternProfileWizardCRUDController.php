<?php

namespace App\Http\Controllers;

use App\CourseMajor;
use App\CourseProgram;
use App\StateUniversityCollege;
use App\StudentInfo;
use App\StudentSkill;
use App\StudentSkillSet;
use App\SUCProgramMajor;
use App\UserRole;
use Illuminate\Http\Request;

class InternProfileWizardCRUDController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sucs = StateUniversityCollege::all();
        $courses = SUCProgramMajor::all();
        $programs = CourseProgram::all();
        $majors = CourseMajor::all();
        $skills = StudentSkill::all();
        return view('pages.intern.profile-wizard', compact('sucs', 'courses', 'programs', 'majors', 'skills'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $studentinfo = new StudentInfo();
        $studentinfo->first_name = $request->firstname;
        $studentinfo->middle_name = $request->middlename or null;
        $studentinfo->last_name = $request->lastname;
        $studentinfo->birthdate = date('Y-m-d', strtotime($request->year.'-'.$request->month.'-'.$request->day));
        $studentinfo->gender = $request->gender;
        $studentinfo->address1 = $request->address1;
        $studentinfo->address2 = $request->address2;
        $studentinfo->suc_program_major_id = $request->sucmajor;
        $studentinfo->save();

        $userinfo = UserRole::where('user_id', \Auth::user()->id)->first();
        $userinfo->info_id = $studentinfo->id;
        $userinfo->save();

        foreach($request->skills as $skill){
            $skillset = new StudentSkillSet();
            $skillset->student_id = $studentinfo->id;
            $skillset->student_skill_id = $skill;
            $skillset->save();
        }

        return redirect(route('intern.home'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
