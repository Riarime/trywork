<?php

namespace App\Http\Controllers;

use App\BranchCourse;
use App\role;
use App\UnivHTE;
use App\UnivIntern;
use App\UserInfo;
use App\UserRole;
use Illuminate\Http\Request;
use App\UnivBranches;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function coordinator() {
        $branch = session('branch_id');
        $college = session('college_id');
        $user = 'coordinator';
        $view = 'dashboard';
        $name = UserInfo::select('first_name','middle_name','last_name')
            ->where('info_id',UserRole::where('user_id',Auth::user()->id)->first()->info_id)
            ->first()->toArray();
        $user_role = role::where('role_id',UserRole::where('user_id',Auth::user()->id)->first()->role_id)->first()->name;
        $active = DB::table('r_branch_active_ay_sem as Active')
            ->select('Active.ay_id','AY.academic_yr','Active.sem_id','Sem.semester')
            ->join('r_branch_acdmcyr as AY','AY.ay_id','=','Active.ay_id')
            ->join('r_branch_sem as Sem','Sem.sem_id','=','Active.sem_id')
            ->where('Active.aysem_stat','=','Active')
            ->first();
        //dd($active->academic_yr);
        $intern_total = DB::table('r_su_intern as Intern')
            ->join('r_branch_courses as BC','BC.bc_id','=','Intern.bc_id')
            ->join('r_su_college_courses as CC','CC.course_id','BC.course_id')
            ->where('Intern.branch_id','=',$branch)
            ->where('CC.college_id','=',$college)
            ->where('ay_id','=',$active->ay_id)
            ->get()->count();
        $hte_total = UnivHTE::where('branch_id','=',$branch)
            ->where('college_id','=',$college)
            ->where('affiliated_status','=','Active')
            ->get()->count();
        $programs = DB::table('r_branch_courses as BC')
            ->select('BC.bc_id','CC.course_abbrv','CC.course_name')
            ->join('r_su_college_courses as CC','CC.course_id','=','BC.course_id')
            ->where('BC.branch_id','=',$branch)
            ->where('CC.college_id','=',$college)
            ->get()->toArray();
        $categories = [];
        $enrolled = [];
        $internship = [];
        $complete = [];
        foreach($programs as $program) {
            array_push($categories, $program->course_abbrv);
            array_push(
                $enrolled,
                UnivIntern::where('branch_id','=',$branch)
                    ->where('branch_id','=',$branch)
                    ->where('ay_id','=',$active->ay_id)
                    ->where('bc_id','=',$program->bc_id)
                    ->get()->count()
            );
            array_push(
                $internship,
                UnivIntern::where('branch_id','=',$branch)
                    ->where('ay_id','=',$active->ay_id)
                    ->where('bc_id','=',$program->bc_id)
                    ->where('intern_stat','=','Internship')
                    ->orWhere('intern_stat','=','Completed')
                    ->get()->count()
            );
            array_push(
                $complete,
                UnivIntern::where('branch_id','=',$branch)
                    ->where('ay_id','=',$active->ay_id)
                    ->where('bc_id','=',$program->bc_id)
                    ->where('intern_stat','=','Completed')
                    ->get()->count()
            );
        }
        $comparison = [
            'categories' => $categories,
            'enrolled' => $enrolled,
            'internship' => $internship,
            'complete' => $complete
        ];
        //dd($comparison);
        $intern_no = [];
        foreach($programs as $program) {
            array_push(
                $intern_no,
                DB::table('r_su_intern as Intern')
                    ->join('r_branch_courses as BC','BC.bc_id','=','Intern.bc_id')
                    ->join('r_su_college_courses as CC','CC.course_id','BC.course_id')
                    ->where('Intern.branch_id','=',$branch)
                    ->where('Intern.ay_id','=',$active->ay_id)
                    ->where('Intern.bc_id','=',$program->bc_id)
                    ->where('Intern.intern_stat','=','Internship')
                    ->orWhere('Intern.intern_stat','=','Completed')
                    ->get()->count()
            );
        }
        $intern = [
            'programs' => $programs,
            'intern_no' => $intern_no
        ];
        $hte = DB::table('t_hte_accepted_intern as OJT')
            ->select('HTE.hte_id','HTE.hte_name')
            ->join('r_hte as HTE','HTE.hte_id','=','OJT.hte_id')
            ->join('t_su_hte as Affiliated','HTE.hte_id','=','Affiliated.hte_id')
            ->join('r_su_intern as Intern','Intern.intern_id','OJT.intern_id')
            ->where('Affiliated.branch_id','=',$branch)
            ->where('Intern.ay_id','=',$active->ay_id)
            ->where('Affiliated.college_id','=',$college)
            ->groupBy('HTE.hte_id','HTE.hte_name')
            ->get()->toArray();
        $hte_intern = [];
        foreach($hte as $company) {
            array_push(
                $hte_intern,
                DB::table('t_hte_accepted_intern as OJT')
                    ->join('r_su_intern as Intern','Intern.intern_id','=','OJT.intern_id')
                    ->join('r_branch_courses as BC','BC.bc_id','=','Intern.bc_id')
                    ->join('r_su_college_courses as Course','Course.course_id','=','BC.course_id')
                    ->where('Intern.branch_id','=',$branch)
                    ->where('Intern.ay_id','=',$active->ay_id)
                    ->where('Course.college_id','=',$college)
                    ->where('OJT.hte_id','=',$company->hte_id)
                    ->get()->count()
            );
        }
        $affiliated = [
            'hte' => $hte,
            'hte_intern' => $hte_intern
        ];
        //dd($affiliated);
        return view('pages.coordinator.dashboard',compact('user','view','active','name','user_role','intern_total','hte_total','comparison','intern','affiliated'));
    }

    public function head() {
        $branch = session('branch_id');
        $user = 'head';
        $view = 'dashboard';
        $name = UserInfo::select('first_name','middle_name','last_name')
            ->where('info_id',UserRole::where('user_id',Auth::user()->id)->first()->info_id)
            ->first()->toArray();
        $user_role = role::where('role_id',UserRole::where('user_id',Auth::user()->id)->first()->role_id)->first()->name;
        $active = DB::table('r_branch_active_ay_sem as Active')
            ->select('Active.ay_id','AY.academic_yr','Active.sem_id','Sem.semester')
            ->join('r_branch_acdmcyr as AY','AY.ay_id','=','Active.ay_id')
            ->join('r_branch_sem as Sem','Sem.sem_id','=','Active.sem_id')
            ->where('Active.aysem_stat','=','Active')
            ->get()->toArray()[0];
        $intern_total = UnivIntern::where('branch_id','=',$branch)
            ->where('ay_id','=',$active->ay_id)
            ->get()->count();
        $hte_total = UnivHTE::where('branch_id','=',$branch)
            ->where('affiliated_status','=','Active')
            ->get()->count();
        $colleges = DB::table('r_branch_courses as BC')
            ->select('BC.branch_id','College.college_id','College.college_abbrv','College.college_name')
            ->join('r_su_college_courses as Course','Course.course_id','=','BC.course_id')
            ->join('r_su_colleges as College','College.college_id','=','Course.college_id')
            ->where('BC.branch_id','=',$branch)
            ->groupBy('BC.branch_id','College.college_id','College.college_abbrv','College.college_name')
            ->get()->toArray();
        $programs = DB::table('r_su_intern as Intern')
            ->select('BC.branch_id','BC.bc_id','College.college_id','College.college_abbrv','CC.course_abbrv','CC.course_name')
            ->join('r_branch_courses as BC','BC.bc_id','=','Intern.bc_id')
            ->join('r_su_college_courses as CC','CC.course_id','=','BC.course_id')
            ->join('r_su_colleges as College','College.college_id','=','CC.college_id')
            ->where('BC.branch_id','=',$branch)
            ->groupBy('BC.branch_id','BC.bc_id','College.college_id','College.college_abbrv','CC.course_abbrv','CC.course_name')
            ->get()->toArray();
        $categories = [];
        $enrolled = [];
        $internship = [];
        $complete = [];
        foreach($programs as $program) {
            array_push($categories, $program->course_abbrv);
            array_push(
                $enrolled,
                UnivIntern::where('branch_id','=',$branch)
                    ->where('ay_id','=',$active->ay_id)
                    ->where('bc_id','=',$program->bc_id)
                    ->get()->count()
            );
            array_push(
                $internship,
                UnivIntern::where('branch_id','=',$branch)
                    ->where('ay_id','=',$active->ay_id)
                    ->where('bc_id','=',$program->bc_id)
                    ->where('intern_stat','=','Internship')
                    ->orWhere('intern_stat','=','Completed')
                    ->get()->count()
            );
            array_push(
                $complete,
                UnivIntern::where('branch_id','=',$branch)
                    ->where('ay_id','=',$active->ay_id)
                    ->where('bc_id','=',$program->bc_id)
                    ->where('intern_stat','=','Completed')
                    ->get()->count()
            );
        }
        $comparison = [
            'categories' => $categories,
            'enrolled' => $enrolled,
            'internship' => $internship,
            'complete' => $complete
        ];
        $college_no = [];
        $intern_no = [];
        foreach($colleges as $college) {
            if ($college->branch_id == $branch) {
                $college_count = 0;
                foreach($programs as $program) {
                    if ($program->branch_id == $branch && $program->college_id == $college->college_id) {
                        $college_count += DB::table('r_su_intern as Intern')
                            ->join('r_branch_courses as BC','BC.bc_id','=','Intern.bc_id')
                            ->join('r_su_college_courses as CC','CC.course_id','BC.course_id')
                            ->where('Intern.branch_id','=',$branch)
                            ->where('Intern.ay_id','=',$active->ay_id)
                            ->where('Intern.bc_id','=',$program->bc_id)
                            ->where('Intern.intern_stat','=','Internship')
                            ->orWhere('Intern.intern_stat','=','Completed')
                            ->get()->count();
                    }
                }
                array_push($college_no, $college_count);
            }
        }
        //dd($programs, $colleges ,$college_no);
        foreach($programs as $program) {
            $intern_count = 0;
            array_push(
                $intern_no,
                DB::table('r_su_intern as Intern')
                    ->join('r_branch_courses as BC','BC.bc_id','=','Intern.bc_id')
                    ->join('r_su_college_courses as CC','CC.course_id','BC.course_id')
                    ->where('Intern.branch_id','=',$branch)
                    ->where('Intern.ay_id','=',$active->ay_id)
                    ->where('Intern.bc_id','=',$program->bc_id)
                    ->where('Intern.intern_stat','=','Internship')
                    ->orWhere('Intern.intern_stat','=','Completed')
                    ->get()->count()
            );
        }
        $intern = [
            'colleges' => $colleges,
            'programs' => $programs,
            'college_no' => $college_no,
            'intern_no' => $intern_no
        ];
        $hte = DB::table('t_hte_accepted_intern as OJT')
            ->select('HTE.hte_id','HTE.hte_name')
            ->join('r_hte as HTE','HTE.hte_id','=','OJT.hte_id')
            ->join('t_su_hte as Affiliated','HTE.hte_id','=','Affiliated.hte_id')
            ->join('r_su_intern as Intern','Intern.intern_id','OJT.intern_id')
            ->where('Affiliated.branch_id','=',$branch)
            ->where('Intern.ay_id','=',$active->ay_id)
            ->groupBy('HTE.hte_id','HTE.hte_name')
            ->get()->toArray();
        $hte_intern = [];
        foreach($hte as $company) {
            array_push(
                $hte_intern,
                DB::table('t_hte_accepted_intern as OJT')
                    ->join('r_su_intern as Intern','Intern.intern_id','=','OJT.intern_id')
                    ->join('r_branch_courses as BC','BC.bc_id','=','Intern.bc_id')
                    ->join('r_su_college_courses as Course','Course.course_id','=','BC.course_id')
                    ->where('Intern.branch_id','=',$branch)
                    ->where('Intern.ay_id','=',$active->ay_id)
                    ->where('OJT.hte_id','=',$company->hte_id)
                    ->get()->count()
            );
        }
        $affiliated = [
            'hte' => $hte,
            'hte_intern' => $hte_intern
        ];
        //dd($intern);
        return view('pages.head.dashboard',compact('user','view','active','name','user_role','intern_total','hte_total','comparison','intern','affiliated'));
    }

    public function director() {
        $user = 'director';
        $view = 'dashboard';
        $university = session('su_id');
        $name = UserInfo::select('first_name','middle_name','last_name')
            ->where('info_id',UserRole::where('user_id',Auth::user()->id)->first()->info_id)
            ->first()->toArray();
        $user_role = role::where('role_id',UserRole::where('user_id',Auth::user()->id)->first()->role_id)->first()->name;
        $active = DB::table('r_branch_active_ay_sem as Active')
            ->select('Active.ay_id','AY.academic_yr','Active.sem_id','Sem.semester')
            ->join('r_branch_acdmcyr as AY','AY.ay_id','=','Active.ay_id')
            ->join('r_branch_sem as Sem','Sem.sem_id','=','Active.sem_id')
            ->where('Active.aysem_stat','=','Active')
            ->get()->toArray()[0];
        $branches = UnivBranches::select('branch_id', 'branch_abbrv','branch_name')
            ->where('su_id','=',$university)
            ->where('branch_stat','=','Active')
            ->get()->toArray();
        $intern_total = 0;
        foreach($branches as $branch) {
            $intern_total += DB::table('r_su_intern')
                ->where('branch_id','=',$branch["branch_id"])
                ->where('ay_id','=',$active->ay_id)
                ->get()->count();
        }
        $hte_total = DB::table('t_su_hte as Affiliated')
            ->join('r_su_colleges as College','College.college_id','=','Affiliated.college_id')
            ->where('College.su_id','=',$university)
            ->get()->count();
        $colleges = DB::table('r_branch_courses as BC')
            ->select('BC.branch_id','College.college_id','College.college_abbrv','College.college_name')
            ->join('r_su_college_courses as Course','Course.course_id','=','BC.course_id')
            ->join('r_su_colleges as College','College.college_id','=','Course.college_id')
            ->where('College.su_id','=',$university)
            ->groupBy('BC.branch_id','College.college_id','College.college_abbrv','College.college_name')
            ->get()->toArray();
        $programs = DB::table('r_su_intern as Intern')
            ->select('CC.college_id','BC.bc_id','BC.branch_id','College.college_abbrv','CC.course_abbrv','CC.course_name')
            ->join('r_branch_courses as BC','BC.bc_id','=','Intern.bc_id')
            ->join('r_su_college_courses as CC','CC.course_id','=','BC.course_id')
            ->join('r_su_colleges as College','College.college_id','=','CC.college_id')
            ->groupBy('CC.college_id','BC.bc_id','BC.branch_id','College.college_abbrv','CC.course_abbrv','CC.course_name')
            ->get()->toArray();
            /*DB::table('r_branch_courses as BC')
            ->select('CC.college_id','BC.bc_id','BC.branch_id','College.college_abbrv','CC.course_abbrv','CC.course_name')
            ->join('r_su_college_courses as CC','CC.course_id','=','BC.course_id')
            ->join('r_su_colleges as College','College.college_id','=','CC.college_id')
            ->where('BC.branch_id','=',$branch)
            ->get()->toArray();*/
        //
        //dd($programs);
        $categories = [];
        $enrolled = [];
        $internship = [];
        $complete = [];
        foreach($branches as $branch) {
            foreach($programs as $program) {
                if ($program->branch_id == $program->branch_id) {
                    array_push($categories, $program->course_abbrv);
                    array_push(
                        $enrolled,
                        UnivIntern::where('branch_id','=',$branch["branch_id"])
                            ->where('ay_id','=',$active->ay_id)
                            ->where('bc_id','=',$program->bc_id)
                            ->get()->count()
                    );
                    array_push(
                        $internship,
                        UnivIntern::where('branch_id','=',$branch["branch_id"])
                            ->where('ay_id','=',$active->ay_id)
                            ->where('bc_id','=',$program->bc_id)
                            ->where('intern_stat','=','Internship')
                            ->orWhere('intern_stat','=','Completed')
                            ->get()->count()
                    );
                    array_push(
                        $complete,
                        UnivIntern::where('branch_id','=',$branch["branch_id"])
                            ->where('ay_id','=',$active->ay_id)
                            ->where('bc_id','=',$program->bc_id)
                            ->where('intern_stat','=','Completed')
                            ->get()->count()
                    );
                }
            }
        }
        $comparison = [
            'categories' => $categories,
            'enrolled' => $enrolled,
            'internship' => $internship,
            'complete' => $complete
        ];
        $branch_no = [];
        $college_no = [];
        $intern_no = [];
        foreach($branches as $branch) {
            $branch_count = 0;
            foreach($programs as $program) {
                if ($program->branch_id == $branch["branch_id"]) {
                    $branch_count += DB::table('r_su_intern as Intern')
                        ->join('r_branch_courses as BC','BC.bc_id','=','Intern.bc_id')
                        ->join('r_su_college_courses as CC','CC.course_id','BC.course_id')
                        ->where('Intern.branch_id','=',$branch["branch_id"])
                        ->where('Intern.ay_id','=',$active->ay_id)
                        ->where('Intern.bc_id','=',$program->bc_id)
                        ->where('Intern.intern_stat','=','Internship')
                        ->orWhere('Intern.intern_stat','=','Completed')
                        ->get()->count();
                }
            }
            array_push($branch_no, $branch_count);
        }
        foreach($branches as $branch) {
            $college_count = 0;
            foreach($colleges as $college) {
                if ($college->branch_id == $branch["branch_id"]) {
                    foreach($programs as $program) {
                        if ($program->branch_id == $branch["branch_id"] && $program->college_id == $college->college_id) {
                            $college_count += DB::table('r_su_intern as Intern')
                                ->join('r_branch_courses as BC','BC.bc_id','=','Intern.bc_id')
                                ->join('r_su_college_courses as CC','CC.course_id','BC.course_id')
                                ->where('Intern.branch_id','=',$branch["branch_id"])
                                ->where('Intern.ay_id','=',$active->ay_id)
                                ->where('Intern.bc_id','=',$program->bc_id)
                                ->where('Intern.intern_stat','=','Internship')
                                ->orWhere('Intern.intern_stat','=','Completed')
                                ->get()->count();
                        }
                    }
                    array_push($college_no, $college_count);
                }
            }
        }
        foreach($branches as $branch) {
            foreach($programs as $program) {
                if ($program->branch_id == $branch["branch_id"]) {
                    array_push(
                        $intern_no,
                        DB::table('r_su_intern as Intern')
                            ->join('r_branch_courses as BC','BC.bc_id','=','Intern.bc_id')
                            ->join('r_su_college_courses as CC','CC.course_id','BC.course_id')
                            ->where('Intern.branch_id','=',$branch["branch_id"])
                            ->where('Intern.ay_id','=',$active->ay_id)
                            ->where('Intern.bc_id','=',$program->bc_id)
                            ->where('Intern.intern_stat','=','Internship')
                            ->orWhere('Intern.intern_stat','=','Completed')
                            ->get()->count()
                    );
                }
            }
        }
        $intern = [
            'branches' => $branches,
            'colleges' => $colleges,
            'programs' => $programs,
            'branch_no' => $branch_no,
            'college_no' => $college_no,
            'intern_no' => $intern_no
        ];
        $hte = DB::table('t_hte_accepted_intern as OJT')
            ->select('HTE.hte_id','HTE.hte_name')
            ->join('r_hte as HTE','HTE.hte_id','=','OJT.hte_id')
            ->join('t_su_hte as Affiliated','HTE.hte_id','=','Affiliated.hte_id')
            ->join('r_su_intern as Intern','Intern.intern_id','OJT.intern_id')
            ->join('r_su_branches as Branch','Branch.branch_id','=','Affiliated.branch_id')
            ->where('Branch.su_id','=',$university)
            ->where('Intern.ay_id','=',$active->ay_id)
            ->groupBy('HTE.hte_id','HTE.hte_name')
            ->get()->toArray();
        $hte_intern = [];
        foreach($hte as $company) {
            array_push(
                $hte_intern,
                DB::table('t_hte_accepted_intern as OJT')
                    ->join('r_su_intern as Intern','Intern.intern_id','=','OJT.intern_id')
                    ->join('r_branch_courses as BC','BC.bc_id','=','Intern.bc_id')
                    ->join('r_su_college_courses as Course','Course.course_id','=','BC.course_id')
                    ->join('r_su_branches as Branch','Branch.branch_id','=','Intern.branch_id')
                    ->where('Branch.su_id','=',$university)
                    ->where('Intern.ay_id','=',$active->ay_id)
                    ->where('OJT.hte_id','=',$company->hte_id)
                    ->get()->count()
            );
        }
        $affiliated = [
            'hte' => $hte,
            'hte_intern' => $hte_intern
        ];
        //dd($intern);
        //dd($branches, $intern_total,$hte_total, $colleges, $programs, $comparison, $intern, $affiliated);
        return view('pages.director.dashboard',compact('user','view','active','name','user_role','intern_total','hte_total','comparison','intern','affiliated'));
    }
}
