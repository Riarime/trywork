<?php

namespace App\Http\Controllers;

use App\BranchCourse;
use App\CollegeCourses;
use App\UnivUsers;
use Illuminate\Http\Request;
use App\BranchAY;
use App\BranchSem;
use App\BranchAYSem;
use Carbon\Carbon;
use App\UnivBranches;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ConfigController extends Controller
{
    //
    public function getActiveAYSelect() {
        $branch = session('branch_id');
        $active_ay = BranchAY::select('ay_code','academic_yr')
            ->where('branch_id','=',$branch)
            ->where('ay_stat','=','Active')
            ->get()->toArray();
        return response()->json($active_ay);
    }

    public function getActiveSemSelect() {
        $branch = session('branch_id');
        $active_sem = BranchSem::select('sem_code','semester')
            ->where('branch_id','=',$branch)
            ->where('sem_stat','=','Active')
            ->get()->toArray();
        return response()->json($active_sem);
    }

    public function setActiveDetails(Request $request) {
        if ($request->isMethod('post')) {
            $branch = session('branch_id');
            $ay = BranchAY::where('ay_code','=',$request->ay)
                ->get()->pluck('ay_id')->toArray()[0];
            $sem = BranchSem::where('sem_code','=',$request->sem)
                ->get()->pluck('sem_id')->toArray()[0];
            $active = BranchAYSem::where('branch_id','=', $branch)
                ->where('aysem_stat','=','Active')
                ->get()->pluck('aysem_id');
            /*if (BranchAYSem::where('ay_id','=',$ay)
                ->where('sem_id','=',$sem)
                ->get()->count() > 0) {
                return response()->json(['already exist']);
            }*/
            if (count($active) > 0) {
                BranchAYSem::where('aysem_id','=',$active)
                    ->update(['aysem_stat' => 'Inactive']);
            }

            $insert = new BranchAYSem();
            $insert->branch_id = $branch;
            $insert->ay_id = $ay;
            $insert->sem_id = $sem;
            $insert->save();

            return response()->json(['message' => 'Active details successfully set!']);
        }
    }

    public function getActiveDetails() {
        $branch = session('branch_id');
        $active = DB::table('r_branch_active_ay_sem as Active')
            ->select('Sem.semester','AY.academic_yr')
            ->join('r_branch_acdmcyr as AY','AY.ay_id','=','Active.ay_id')
            ->join('r_branch_sem as Sem','Sem.sem_id','=','Active.sem_id')
            ->where('Active.branch_id','=',$branch)
            ->where('aysem_stat','=','Active')
            ->get()->toArray()[0];
        return response()->json($active);
    }

    public function getAcademicYear() {
        $branch = session('branch_id');
        $ays = BranchAY::select('ay_code','academic_yr','ay_stat')
            ->where('branch_id','=',$branch)
            ->get()->toArray();
        return response()->json($ays);
    }

    public function getSemester() {
        $branch = session('branch_id');
        $semester = BranchSem::select('sem_code','semester','sem_stat')
            ->where('branch_id','=',$branch)
            ->get()->toArray();
        return response()->json($semester);
    }

    public function getProgram() {
        $branch = session('branch_id');
        $college = session('college_id');
        $program = DB::table('r_branch_courses as BC')
            ->select('BC.bc_id','CC.course_code','CC.course_abbrv','BC.year_section','BC.bc_stat')
            ->join('r_su_college_courses as CC','CC.course_id','=','BC.course_id')
            ->where('BC.branch_id','=',$branch)
            ->where('CC.college_id','=',$college)
            ->get()->toArray();
        return response()->json($program);
    }

    public function addAY(Request $request) {
        $branch_id = session('branch_id');
        $branch = \App\UnivBranches::where('branch_id','=',$branch_id)->pluck('branch_abbrv');
        $insert = new BranchAY();
        $insert->branch_id = $branch_id;
        $insert->ay_code = $branch[0].'-'.'AY'.(Carbon::now()->format('y')).(BranchAY::count() + 1);   //'BSUM1-AY181'
        $insert->academic_yr = $request->ay;
        $insert->save();
        return response()->json(["Academic year succecssfully added!"]);
    }

    public function addSem(Request $request) {
        $branch_id = session('branch_id');
        $branch = session('branch_id');
        $sem = new \App\BranchSem();
        $sem->branch_id = $branch_id;
        $sem->sem_code = $branch[0].'-SEM'.(Carbon::now()->format('y')).(BranchSem::get()->count() + 1);     //'BSUM1-SEM181';
        $sem->semester = $request->sem;
        $sem->save();

        return response()->json(["message" => "Semester successfully added!"]);
    }

    public function addProgram(Request $request) {
        $branch = session('branch_id');
        $course = CollegeCourses::where('course_code','=',$request->program)
            ->pluck('course_id')->toArray()[0];
        $branch_course = new BranchCourse();
        $branch_course->branch_id = $branch;
        $branch_course->uu_id = UnivUsers::where('ur_id','=', Auth::user()->id)->pluck('uu_id')->toArray()[0];
        $branch_course->course_id = $course;
        $branch_course->year_section = $request->yearsec;
        $branch_course->save();

        return response()->json(['message' => 'Program successfully added!']);
    }

    public function updateAY(Request $request) {
        BranchAY::where('ay_code','=',$request->ay_code)
            ->update(['academic_yr' => $request->ay]);

        return response()->json(['message' => 'Academic year successfully updated!']);
    }

    public function updateSem(Request $request) {
        BranchSem::where('sem_code','=',$request->sem_code)
            ->update(['semester' => $request->sem]);

        return response()->json(['message' => 'Semester successfully updated!']);
    }

    public function updateProgram(Request $request) {
        $course = CollegeCourses::where('course_code','=',$request->program_code)
            ->get()->pluck('course_id')->toArray()[0];
        BranchCourse::where('bc_id','=',$request->program_id)
            ->update(['course_id' => $course, 'year_section' => $request->yearsec]);
        return response()->json(['message' => 'Program successfully updated!']);
    }

    public function updateAYStat(Request $request) {
        if ($request->status == 'Enabled') {
            BranchAY::where('ay_code','=',$request->ay_code)
                ->update(['ay_stat' => 'Inactive']);
        } else {
            BranchAY::where('ay_code','=',$request->ay_code)
                ->update(['ay_stat' => 'Active']);
        }
        return response()->json(['message' => 'Academic year status successfully changed!']);
    }

    public function updateSemStat(Request $request) {
        if ($request->status == 'Enabled') {
            BranchSem::where('sem_code','=',$request->sem_code)
                ->update(['sem_stat' => 'Inactive']);
        } else {
            BranchSem::where('sem_code','=',$request->sem_code)
                ->update(['sem_stat' => 'Active']);
        }
        return response()->json(['message' => 'Semester status successfully changed!']);
    }

    public function updateProgramStat(Request $request) {
        if ($request->status == 'Enabled') {
            BranchCourse::where('bc_id','=',$request->program_id)
                ->update(['bc_stat' => 'Inactive']);
        } else {
            BranchCourse::where('bc_id','=',$request->program_id)
                ->update(['bc_stat' => 'Active']);
        }
        return response()->json(['message' => 'Program status successfully changed!']);
    }
}
