<?php

namespace App\Http\Controllers;

use App\BranchAY;
use Illuminate\Http\Request;
use App\UserInfo;
use App\UnivIntern;
use App\ReferralCode;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Excel;

class SwalController extends Controller
{
    //
    function AddIntern(Request $request) {
        /*if ($request->isMethod('post')) {
            $branch = 1;
            $count = UnivIntern::where('branch_id','=',$branch)->get()->count();
            $code = 'TW-I'.$branch.'-'.$count;
            $ay = BranchAY::where('ay_code','=',$request->ay)->get()->pluck('ay_id')->toArray();
            $insert = [$ay[0], $request->studno, $request->fname, $request->mname, $request->lname, $request->email, $request->program];

            $intern_insert = [$insert[0], $branch, $insert[6], 'TW-I'.$branch.'-'.$code, $insert[1], $insert[5]];

            $info = new UserInfo();
            $info->first_name =$insert[2];
            $info->middle_name = $insert[3];
            $info->last_name = $insert[4];
            $info->save();
            $id = $info->id;

            $intern = new UnivIntern();
            $intern->branch_id = $branch;
            $intern->ay_id = $insert[0];
            $intern->bc_id = $insert[6];
            $intern->info_id = $id;
            $intern->intern_code = $code;
            $intern->stud_no = $insert[1];
            $intern->contact_email = $insert[5];
            $intern->save();
            $intern_id = $intern->id;

            $referral_code = new ReferralCode();
            $referral_code->intern_id = $intern_id;
            $referral_code->referral_code = Carbon::now()->format('ydm-His').$count;
            $referral_code->save();

            return response()->json(['Successfully Added!']);
        }*/
        $branch = session()->get('branch_id');
        $count = UnivIntern::where('branch_id','=',$branch)->get()->count();
        $code = 'TW-I'.$branch.'-'.$count;
        $ay = BranchAY::where('ay_code','=',$request->ay)->get()->pluck('ay_id')->toArray();
        /*$insert = [$ay[0], $request->studno, $request->fname, $request->mname, $request->lname, $request->email, $request->program];
        $intern_insert = [$insert[0], $branch, $insert[6], 'TW-I'.$branch.'-'.$code, $insert[1], $insert[5]];*/

        $info = new UserInfo();
        $info->first_name = $request->fname;
        $info->middle_name = $request->mname;
        $info->last_name = $request->lname;
        $info->save();
        $id = $info->id;

        DB::table('r_su_intern')->insert([
            'branch_id' => $branch,
            'ay_id' => $ay[0],
            'bc_id' => $request->program,
            'info_id' => $id,
            'intern_code' => $code,
            'stud_no' => $request->studno,
            'contact_email' => $request->email
        ]);

        return response()->json(['Successfully Added!']);
    }

    function importIntern(Request $request) {
        if ($request->hasFile('excel')) {
            $path = $request->file('excel')->getRealPath();
            $data = \Excel::load($path)->get();
            $branch = session()->get('branch_id');
            $bc_id = $request->program;
            $ay_id = BranchAY::where('ay_code','=',$request->ay)
                ->pluck('ay_id')->toArray()[0];
            $program_yrsec = DB::table('r_branch_courses as BC')
                ->select('CC.course_abbrv','BC.year_section')
                ->join('r_su_college_courses as CC','CC.course_id','BC.course_id')
                ->where('BC.branch_id','=',$branch)
                ->where('CC.college_id','=',session()->get('college_id'))
                ->where('BC.bc_id','=',$bc_id)
                ->get()->toArray()[0];
            $insert = [];
            $error = [];
            //dd($path, $data);
            if (!empty($data) && $data->count()) {
                foreach($data->toArray() as $key => $value) {
                    if (!empty($value)) {
                        $program = $value['program'];
                        $yearsec = $value['year_and_section'];
                        $studno = $value['student_number'];
                        $fname = $value['first_name'];
                        $mname = $value['middle_name'];
                        $lname = $value['last_name'];
                        $email = $value['email'];

                        if ($program.' '.$yearsec == $program_yrsec->course_abbrv.' '.$program_yrsec->year_section) {
                            array_push($insert,[
                                'bc_id' => $bc_id,
                                'studno' => $studno,
                                'fname' => $fname,
                                'mname' => $mname,
                                'lname' => $lname,
                                'email' => $email
                            ]);
                        } else {
                            array_push($error,array([
                                'bc_id' => $bc_id,
                                'studno' => $studno,
                                'fname' => $fname,
                                'mname' => $mname,
                                'lname' => $lname,
                                'email' => $email
                            ]));
                        }
                    }
                }

                foreach($insert as $data) {
                    $count = UnivIntern::where('branch_id','=',$branch)->get()->count();
                    $code = 'TW-I'.$branch.'-'.$count;

                    $info = new UserInfo();
                    $info->first_name = $data["fname"];
                    $info->middle_name = $data["mname"];
                    $info->last_name = $data["lname"];
                    $info->save();
                    $info_id = $info->id;

                    DB::table('r_su_intern')->insert([
                        'branch_id' => $branch,
                        'ay_id' => $ay_id,
                        'bc_id' => $data["bc_id"],
                        'info_id' => $info_id,
                        'intern_code' => $code,
                        'stud_no' => $data["studno"],
                        'contact_email' => $data["email"]
                    ]);
                }
                return response()->json(['result' => 'success','message' => 'Successfully imported!']);
            }
            return response()->json(['result' => 'failed','message' => 'Empty file!']);
        }
    }
}
