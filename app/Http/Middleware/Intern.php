<?php

namespace App\Http\Middleware;

use App\role;
use App\User;
use App\UserRole;
use Closure;
use Illuminate\Support\Facades\Auth;

class Intern
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()){
            if (UserRole::where('user_id', Auth::user()->id)->first()->role_id == role::where('name', 'Student Trainee')->first()->role_id && UserRole::where('user_id', Auth::user()->id)->first()->info_id != null) {
                return $next($request);
            }
        }
        return response('NOT ENOUGH CREDENTIALS TO LOG IN');
    }
}
