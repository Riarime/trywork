<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\UserRole;
use App\role;

class HTE
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()){
            if ((UserRole::where('user_id', Auth::user()->id)->first()->role_id == role::where('name', 'HTE Supervisor')->first()->role_id || UserRole::where('user_id', Auth::user()->id)->first()->role_id == role::where('name', 'HTE Representative')->first()->role_id) && UserRole::where('user_id', Auth::user()->id)->first()->info_id != null) {
                return $next($request);
            }
        }
        return response('NOT ENOUGH CREDENTIALS TO LOG IN');
    }
}
