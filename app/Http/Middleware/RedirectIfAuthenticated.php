<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            switch(\App\UserRole::where('user_id', Auth::user()->id)->first()->role_id){
                case \App\role::where('name', 'System Administrator')->first()->role_id:
                    return redirect(route('admin.audit-trail'));
                case \App\role::where('name', 'Student Trainee')->first()->role_id:
                    return redirect(route('intern.home'));
                case \App\role::where('name', 'HTE Representative')->first()->role_id:
                    return redirect(route('hte.home'));
                case \App\role::where('name', 'OJT Coordinator')->first()->role_id:
                    return redirect(route('coordinator.dashboard'));
                case \App\role::where('name', 'OJT Head')->first()->role_id:
                    return redirect(route('head.dashboard'));
                case \App\role::where('name', 'OJT Director')->first()->role_id:
                    return redirect(route('director.dashboard'));
                default:
                    return redirect(route('home'));
            }
        }
        return $next($request);
    }
}
