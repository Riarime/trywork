<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnivUsers extends Model
{
    // Table Name
    protected $table = 'r_su_users';
    // Timestamps
    public $timestamps = false;
}
