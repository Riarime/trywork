<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InternSkill extends Model
{
    //  Table
    protected $table = 'r_intern_skills';

    public $timestamps = false;
}
