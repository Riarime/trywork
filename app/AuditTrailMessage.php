<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditTrailMessage extends Model
{
    protected $table = 'r_audit_trail_messages';
}
