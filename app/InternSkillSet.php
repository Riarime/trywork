<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InternSkillSet extends Model
{
    protected $table = 'r_intern_skill_sets';

    public $timestamps = false;
}
