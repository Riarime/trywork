<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnivBranches extends Model
{
    //Table Name
    protected $table = 'r_su_branches';
}
