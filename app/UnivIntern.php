<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnivIntern extends Model
{
    // Table Name
    protected $table = 'r_su_intern';

    // public $timestamps = false;
}
