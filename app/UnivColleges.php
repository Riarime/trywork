<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnivColleges extends Model
{
    // Table Name
    protected $table = 'r_su_colleges';
}
