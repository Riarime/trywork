<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnivUserEducBackground extends Model
{
    //
    protected $table = 'r_su_usr_educbg';

    public $timestamps = false;
}
