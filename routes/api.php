<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('/recommender/suggestions', function ($hte = 1, $branch = 1) {
    $jobTrainings = \App\JobTraining::where('hte_id', $hte)->where('job_training_stat', 'Active')->get();
    $branchCourses = \App\BranchCourse::where('branch_id', $branch)->get();
    $courses = \App\CollegeCourses::wherein('course_id', array_column($branchCourses->toArray(), 'course_id'))->get();
    $interns = \App\UnivIntern::where('branch_id', $branch)->whereIn('bc_id', array_column($branchCourses->toArray(), 'bc_id'))->where('intern_stat', 'Approved')->get();
    $internInfos = \App\UserInfo::whereIn('info_id', array_column($interns->toArray(), 'info_id'))->get();
    $skillsets = \App\InternSkillSet::whereIn('intern_id', array_column($interns->toArray(), 'intern_id'))->get();
    $skills = \App\InternSkill::whereIn('skill_id', array_column($skillsets->toArray(), 'skill_id'))->get();

    foreach ($jobTrainings as $ojtIndex => $jobTraining) {
        foreach ($interns as $internIndex => $intern) {
            if ($skillsets->where('intern_id', $intern->intern_id)->count() > 0) {
                $fuzz = new \FuzzyWuzzy\Fuzz();
                $process = new \FuzzyWuzzy\Process($fuzz);
                $results['skills'] = null;
                foreach (collect($skills->whereIn('skill_id', array_column($skillsets->where('intern_id', $intern->intern_id)->toArray(), 'skill_id'))) as $skill) {
                    $results['skills'][] =
                        [
                            'skill_id' => $skill->skill_id,
                            'skill_name' => $skill->name
                        ];
                }

                //Process the Skills and Description Matching
                if (isset($results)) {
                    $tempVar = [
                        'id' => $intern->intern_id,
                        'course' => $courses->where('course_id', $branchCourses->where('bc_id', $intern->bc_id)->first()->course_id)->first()->course_name,
                        'skills' => []
                    ];

                    foreach (collect($results['skills'])->toArray() as $skill) {
                        if ($c = $process->extract($jobTraining->description, [$skill['skill_name']], null, [$fuzz, 'tokenSetRatio'], null)) {
                            $tempArray = $c->toArray()[0];
                            if ($tempArray[1] > 50) {
                                $temp[$ojtIndex][$internIndex] = $tempVar;
                                $temp[$ojtIndex][$internIndex]['skills'][] = [
                                    'id' => $skill['skill_id'],
                                    'skill' => $skill['skill_name'],
                                    'ratio' => $tempArray[1]
                                ];
                            }
                        }
                    }
                }
            }
        }
        if (isset($temp[$ojtIndex])) {
            $results['data'][$ojtIndex] = [
                'total_interns_count' => collect($temp[$ojtIndex])->count(),
                'attributes' => [
                    $jobTraining,
                    'img' => '' . Avatar::create($jobTraining->job_training_name)->toBase64() . ''
                ],
                'suggestions' => [
                    $temp[$ojtIndex]
                ]
            ];
        } else {
            $results['data'][$ojtIndex] = [
                'total_interns_count' => 0,
                'attributes' => [
                    $jobTraining,
                    'img' => '' . Avatar::create($jobTraining->job_training_name)->toBase64() . ''
                ],
                'suggestions' => [

                ]
            ];
        }
    }
    return $results;
//    return collect($results)->toJson();
})->name('api.recommender.suggestions');
Route::post('/search/state-universities-colleges/', function ($hte = 1) {
    if (null !== $query = request('query')) {
        $branches = \App\UnivBranches::where('branch_name', 'LIKE', '%' . $query . '%')->paginate(10);

        $data = [
            'total_count' => $branches->count(),
            'incomplete_results' => false,
            'items' => [],
        ];
        $activeAYSem = \App\BranchAYSem::where('aysem_stat', 'Active')->get();

        foreach ($branches as $key => $branch) {
            $data['items'][$key] = [
                'id' => $branch->branch_id,
                'branch_name' => $branch->branch_name,
                'address' => $branch->branch_address,
                'student_count' => \App\UnivIntern::where('branch_id', '=', $branch->branch_id)->where('ay_id', '=', $activeAYSem->where('branch_id', $branch->branch_id)->first()->ay_id)->where('intern_stat', '=', 'Approved')->get()->count(),
                'img' => '' . Avatar::create($branch->branch_name)->toBase64() . ''
            ];
        }
    } else if (null !== $id = request('id')) {
        $branch = \App\UnivBranches::where('branch_id', $id)->first();
        $suc = \App\StateUniversity::where('su_id', $branch->su_id)->first();
        $data = [
            'data' => [
                'type' => 'branch',
                'id' => $branch->branch_id,
                'attributes' => [
                    'code' => $branch->branch_code,
                    'abbr' => $branch->branch_abbrv,
                    'name' => $branch->branch_name,
                    'address' => $branch->branch_address,
                    'img' => '' . Avatar::create($branch->branch_name)->toBase64() . '',
                    'parent' => [
                        'type' => 'suc',
                        'id' => $branch->su_id,
                        'attributes' => [
                            'code' => $suc->su_code,
                            'abbr' => $suc->su_abbrv,
                            'name' => $suc->su_name
                        ]
                    ]
                ]
            ],
        ];
    } else {
        $data = ['error' => 'TW-IRS-ER-001'];
    }

    return response()->json($data);
})->name('api.search.sucs');

//New Algorithm for Intern Finder via API (WIP)
Route::get('/fetch/intern/{job_training_id}/{intern_id}', function ($job_training_id, $intern_id) {
    //Initialize Student/Intern Info
    $intern = \App\UnivIntern::where('intern_id', $intern_id)->first();
    $internInfo = \App\UserInfo::where('info_id', $intern->info_id)->first();
    $skillset = \App\InternSkillSet::where('intern_id', $intern_id)->get();
    $skills = \App\InternSkill::whereIn('skill_id', array_column($skillset->toArray(), 'skill_id'))->get();

    $results['intern_info'] =
        [
            'intern_id' => $intern->intern_id,
            'name' => ucwords(strtolower($internInfo->first_name . ' ' . $internInfo->last_name)),
        ];

    $index = 0;

    //Initialize Job Training Info
    $jobTraining = \App\JobTraining::where('job_training_id', $job_training_id)->first();

    //Map out results to array
    if ($skills->count() > 0) {
        //Initialize the Fuzzy Wuzzy Algorithm
        $fuzz = new \FuzzyWuzzy\Fuzz();
        $process = new \FuzzyWuzzy\Process($fuzz);

        foreach ($skills as $skill) {
            $results['skills'][$index] =
                [
                    'skill_id' => $skill->skill_id,
                    'skill_name' => $skill->name
                ];
            $index++;
        }

        $index = 0;

        //Process the Skills and Descrpition Matching
        foreach ($results['skills'] as $skill) {
            if ($c = $process->extract($jobTraining->description, [$skill['skill_name']], null, [$fuzz, 'tokenSetRatio'], null)) {
                $temp = $c->toArray()[0];
                for ($innerIndex = 0; $innerIndex < $c->count(); $innerIndex++) {
                    if ($temp[1] > 30) {
                        $results['results'][$index] = [
                            'job_training_id' => $jobTraining->job_training_id,
                            'skill_name' => $temp[0],
                            'ratio' => $temp[1]
                        ];
                    }
                }
                $index++;
            }
        }
    } else {
        $results = [
            'error' => 'TW-IRS-ER-001'
        ];
    }

    return collect($results)->toJson();
})->name('api.hte.fetch-intern');

Route::get('/fetch/recommendations/job-training/{job_training_id}', function ($job_training_id) {
    //Initialize Student/Intern Info
    $interns = \App\UnivIntern::all();
    $internInfos = \App\UserInfo::whereIn('info_id', array_column($interns->toArray(), 'info_id'))->get();
    $skillsets = \App\InternSkillSet::whereIn('intern_id', array_column($interns->toArray(), 'intern_id'))->get();
    $skills = \App\InternSkill::whereIn('skill_id', array_column($skillsets->toArray(), 'skill_id'))->get();

    //Initialize State Universities/Colleges and their Branches
    $sucs = collect(\Illuminate\Support\Facades\DB::select(
        "SELECT\n" .
        "r_su.su_id,\n" .
        "r_su.su_code,\n" .
        "r_su.su_abbrv,\n" .
        "r_su.su_name,\n" .
        "r_su.su_stat,\n" .
        "r_su_branches.branch_stat,\n" .
        "r_su_branches.branch_add_zipcode,\n" .
        "r_su_branches.branch_address,\n" .
        "r_su_branches.branch_abbrv,\n" .
        "r_su_branches.branch_code,\n" .
        "r_su_branches.branch_name,\n" .
        "r_su_branches.branch_id\n" .
        "FROM\n" .
        "r_su\n" .
        "INNER JOIN r_su_branches ON r_su_branches.su_id = r_su.su_id"
    ));

    //Initialize Job Training Info
    $jobTraining = \App\JobTraining::where('job_training_id', $job_training_id)->first();

    //Map out results to array
    if ($sucs->count() > 0) {
        foreach ($sucs as $suc) {
            $results['sucs'][$suc->su_id] = $suc;
        }
    }

    if ($interns->count() > 0) {
        //Initialize the Fuzzy Wuzzy Algorithm
        $fuzz = new \FuzzyWuzzy\Fuzz();
        $process = new \FuzzyWuzzy\Process($fuzz);

        foreach ($interns as $intern) {
            $results['interns'][$intern->intern_id] =
                [
                    'intern_id' => $intern->intern_id,
                    'bc_id' => $intern->bc_id,
                    'name' => ucwords(strtolower($internInfos->where('info_id', $intern->info_id)->first()->first_name . ' ' . $internInfos->where('info_id', $intern->info_id)->first()->last_name)),
                ];

            $studentSkills = $skillsets->where('intern_id', $intern->intern_id);
            if ($studentSkills->count > 0) {
                $index = 0;
                foreach ($studentSkills as $studentSkill) {
                    $results['interns'][$intern->intern_id]['skills'][$index] = [
                        'skill_id'
                    ];
                }
            }
        }
    }

    $index = 0;
    if ($skills->count() > 0) {
        //Initialize the Fuzzy Wuzzy Algorithm
        $fuzz = new \FuzzyWuzzy\Fuzz();
        $process = new \FuzzyWuzzy\Process($fuzz);

        foreach ($skills as $skill) {
            $results['skills'][$index] =
                [
                    'skill_id' => $skill->skill_id,
                    'skill_name' => $skill->name
                ];
            $index++;
        }

        $index = 0;

        //Process the Skills and Descrpition Matching
        foreach ($results['skills'] as $skill) {
            if ($c = $process->extract($jobTraining->description, [$skill['skill_name']], null, [$fuzz, 'tokenSetRatio'], null)) {
                $temp = $c->toArray()[0];
                for ($innerIndex = 0; $innerIndex < $c->count(); $innerIndex++) {
                    if ($temp[1] > 30) {
                        $results['results'][$index] = [
                            'job_training_id' => $jobTraining->job_training_id,
                            'skill_name' => $temp[0],
                            'ratio' => $temp[1]
                        ];
                    }
                }
                $index++;
            }
        }
    } else {
        $results = [
            'error' => 'TW-IRS-ER-001'
        ];
    }

    return collect($results)->toJson();
});

Route::get('/test', function () {
    $url = route('api.hte.fetch-intern', [1, 1]);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
})->name('api.hte.fetch-suggestion');

Route::get('/fetch/suggestion/intern/', function () {
    $var = <<<HTML
    <body>
    <p id="time"</p>
<script src="http://localhost/trywork/public/assets/frontend/plugins/jquery/jquery-3.2.1.min.js"></script>
<script src="http://localhost/trywork/public/assets/timeago/jquery.timeago.js"></script>
<script>
        var time = jQuery.timeago(new Date());
        $('p#time').text(time);
        time.render();
</script>
</body>
HTML;

    echo $var;
})->name('test');
