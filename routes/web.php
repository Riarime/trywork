<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//    return view('welcome');
    return redirect(route('home-trywork'));
})->name('home');

Route::get('/algotest', 'PageController@algo_test');

Route::get('/home', function () {
    $htes = \App\HTE::all();
    $jobTrainings = \App\JobTraining::all();
    return view('guest', compact('htes', 'jobTrainings'));
})->name('home-trywork');

Route::get('/page', function () {
    //$su = App\UserRole::where('user_id',Auth::user()->id)->first()->role_id;
    $branch = session('branch_id');
    $ay = 'PUPQC-AY188';
    $program = 11;
    $college_id = DB::table('r_su_user_info as UU_Info')
        ->join('r_su_users as UU','UU.uu_id','=','UU_Info.uu_id')
        ->join('user_roles as UR','UR.ur_id','=','UU.ur_id')
        ->where('UR.user_id','=',Auth::user()->id)
        ->get()->pluck('college_id')->toArray();
    dd($college_id);
    //return view('pages.user.register');
});
Route::group(['prefix' => '/reg'], function () {
    Route::get('/', function () {
        $su = DB::table('r_su')
            ->select('su_code', 'su_name')
            ->get();
        $details = array(
            'su' => $su
        );
        return view('pages.intern.profile-wizard')->with($details);
    })->name('registration');
});
Route::get('/faq', function () {
    return view('faq');
})->name('faq');
Route::group(['prefix' => '/registration'], function () {
    Route::get('/', 'PageController@registration')->name('Registration');
    Route::get('/check/{id}', function ($id) {
        return DB::select("SELECT
r_su_intern.stud_no,
r_su_intern.contact_email,
r_su_intern.intern_address,
r_branch_acdmcyr.academic_yr,
r_regions.`name`,
r_su_college_courses.course_name,
r_su_branches.branch_name,
CONCAT(first_name,' ',
user_infos.middle_name,' ',
user_infos.last_name) Stud_Name,
user_infos.gender,
user_infos.bdate,
user_infos.tel_no,
user_infos.cel_no,
r_intern_referral.referral_code,
r_su_colleges.college_name
FROM
r_su_intern
INNER JOIN r_regions ON r_su_intern.region_id = r_regions.region_id
INNER JOIN r_branch_acdmcyr ON r_su_intern.ay_id = r_branch_acdmcyr.ay_id
INNER JOIN r_su_branches ON r_su_intern.branch_id = r_su_branches.branch_id AND r_branch_acdmcyr.branch_id = r_su_branches.branch_id
INNER JOIN user_infos ON r_su_intern.info_id = user_infos.info_id
INNER JOIN r_intern_referral ON r_intern_referral.intern_id = r_su_intern.intern_id
INNER JOIN r_su_college_courses ON r_su_intern.bc_id = r_su_college_courses.course_id
INNER JOIN r_su_colleges ON r_su_college_courses.college_id = r_su_colleges.college_id
WHERE r_intern_referral.referral_code = '$id'
AND r_intern_referral.Status IS NULL");
    })->name('Referral Check');
    Route::post('/internReg', 'RegistrationController@intern')->name('internReg');
    Route::get('/host-training-company', 'PageController@hte_register')->name('Registration HTE');
    Route::get('/state-university', 'PageController@su_register')->name('registration.su-personnel');
    Route::post('/state-u-submit', 'RegistrationController@ojt_personnel')->name('registration.su-personnel.submit');
    Route::get('/university-student/{code}', 'PageController@intern_register')->name('Registration INTERN');
});
/*
Route::group(['prefix' => 'register'], function() {
    Route::get('/', function () { return redirect(route('hte.home')); });
    Route::post('/intern-referral-code','RegistrationController@intern')->name('register.intern-referral');
});*/
Route::group(['prefix' => 'director'], function () {
    Route::get('/', 'DashboardController@director')->name('director.dashboard');
});
Route::group(['prefix' => 'head'], function () {
    Route::get('/', 'DashboardController@head')->name('head.dashboard');
});
Route::group(['prefix' => 'coordinator'], function () {
    Route::get('/', 'DashboardController@coordinator')->name('coordinator.dashboard');
    Route::get('/configuration', 'PageController@configs')->name('config');
    Route::get('/get-activeay-selectable', 'ConfigController@getActiveAYSelect')->name('config.getselectableactiveay');
    Route::get('/get-activesem-selectable', 'ConfigController@getActiveSemSelect')->name('config.getselectableactivesem');
    Route::post('/config-active', 'ConfigController@setActiveDetails')->name('config.active');
    Route::get('/get-active', 'ConfigController@getActiveDetails')->name('config.getactive');
    Route::get('/aylist', 'ConfigController@getAcademicYear')->name('config.ay');
    Route::get('/semlist', 'ConfigController@getSemester')->name('config.sem');
    Route::get('/programlist', 'ConfigController@getProgram')->name('config.program');
    Route::post('/add-ay', 'ConfigController@addAY')->name('config.adday');
    Route::post('/add-sem', 'ConfigController@addSem')->name('config.addsem');
    Route::post('/add-program', 'ConfigController@addProgram')->name('config.addprogram');
    Route::post('/update-ay', 'ConfigController@updateAY')->name('config.updateay');
    Route::post('/update-sem', 'ConfigController@updateSem')->name('config.updatesem');
    Route::post('/update-program', 'ConfigController@updateProgram')->name('config.updateprogram');
    Route::post('/update-aystat', 'ConfigController@updateAYStat')->name('config.updateaystat');
    Route::post('/update-semstat', 'ConfigController@updateSemStat')->name('config.updatesemstat');
    Route::post('/update-programstat', 'ConfigController@updateProgramStat')->name('config.updateprogramstat');
    Route::get('/intern', 'PageController@intern')->name('intern');
    Route::post('/addintern', 'SwalController@AddIntern')->name('intern.add');
    Route::post('/importintern', 'SwalController@importIntern')->name('intern.import');
    Route::post('/internlist', 'TableDataController@getInternList')->name('intern.list');
    Route::post('/internpre-assess', 'TableDataController@preAssess')->name('intern.preassess');
    Route::post('/referralcode', 'TableDataController@getReferralCode')->name('intern.referralcode');
    Route::post('/setupdateintern', 'TableDataController@setUpdateIntern')->name('intern.setupdate');
    Route::post('/updateintern', 'TableDataController@updateIntern')->name('intern.update');
    Route::post('/placement', 'TableDataController@getPlacementList')->name('intern.placement');
    Route::post('/viewplacement', 'TableDataController@getPlacementDetails')->name('intern.ojtdetails');
    Route::post('/assessment', 'TableDataController@gradeAssessment')->name('intern.assessment');
    Route::get('/services', 'PageController@coordinator_services')->name('services');
    Route::post('/services/submit-partnership', 'PageController@partnership_process')->name('partnership');
    /*Route::get('/profile', function() {
        $details = array(
            'user' => 'coordinator',
            'view' => 'profile'
        );
        return view('pages.user.profile-personnel')->with($details);
    })->name('profile');
    Route::post('/update-profile', 'FormController@UserDetailsUpdate')->name('profile-update');
    Route::get('/dashboard', function() {
        return redirect(route('dashboard'));
    });
    Route::get('/academic-year', 'PageController@ConfigAY')->name('config.acadyr');
    Route::get('/semester', function() {
        $sem = DB::table('r_branch_sem')
            ->select('sem_code as Code','semester as Sem', 'sem_stat as Status')
            ->where('branch_id', '=', '1')
            ->get();
        $details = array(
            'user' => 'coordinator',
            'view' => 'config',
            'sub' => 'sem',
            'sem' => $sem
        );
        return view('pages.config.b-sem')->with($details);
    })->name('config.sem');
    Route::get('/colleges', function() {
        $details = array(
            'user' => 'coordinator',
            'view' => 'config',
            'sub' => 'colleges'
        );
        return view('pages.config.univ-colleges')->with($details);
    })->name('config.college');
    Route::get('/college-courses', function() {
        $details = array(
            'user' => 'coordinator',
            'view' => 'config',
            'sub' => 'course'
        );
        return view('pages.config.college-courses')->with($details);
    })->name('config.course');
    Route::get('/branch-course', function() {
        $details = array(
            'user' => 'coordinator',
            'view' => 'config',
            'sub' => 'yrsec'
        );
        return view('pages.config.b-year-sec')->with($details);
    })->name('config.yearsec');
    Route::group(['prefix' => 'intern'], function() {
        Route::get('/', function() {
            return redirect(route('dashboard'));
        })->name('intern.redirect');
        Route::get('/assessment', function() {
            $programs = DB::table('r_branch_courses as BC')
                ->select('bc_id as id', 'course_abbrv as abbrv', 'year_section as ys')
                ->join('r_su_college_courses as CC','BC.course_id','=','CC.course_id')
                ->where('branch_id','=',1)
                ->where('bc_stat', '=', 'Active')
                ->get();
            $details = array(
                'user' => 'coordinator',
                'view' => 'intern',
                'sub' => 'assess',
                'programs' => $programs
            );
            return view('pages.coordinator.i-app-assess')->with($details);
            // return $programs;~
        })->name('intern.assessment');
        Route::post('/assessment', 'FormController@')->name('intern.add');
        Route::post('/import','FileController@importExcel')->name('intern.import');
        Route::get('/list','PageController@ViewInternList')->name('intern.list');
        Route::post('/list','PageController@ViewInternList')->name('intern.list');
        Route::get('/grade', function() {
            return "grade";
        })->name('intern.grade');
    });*/
});

//Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'user-portal'], function () {
    Auth::routes();
});
Route::group(['prefix' => 'system'], function () {
    Route::group(['prefix' => 'intern', 'middleware' => 'intern'], function () {
        Route::get('/', function () {
            return redirect(route('intern.hte-finder'));
        })->name('intern.home');
        Route::get('/hte-finder/{region?}', function ($region = null) {
            if (null !== Request::get('query')) {
                $queryoriginal = Request::get('query');
                $query = '%' . Request::get('query') . '%';
                $results = collect(DB::select("SELECT\n" .
                    "                    r_hte_job_trainings.description AS description,\n" .
                    "                    r_hte.hte_name AS hte_name,\n" .
                    "                    r_hte_job_trainings.job_training_id AS job_id,\n" .
                    "                    r_hte_job_trainings.job_training_name AS ojt_name,\n" .
                    "                    r_hte_job_trainings.`start` AS date,\n" .
                    "                    r_hte_job_trainings.training_code AS code,\n" .
                    "                    r_hte.hte_id AS hte_id,\n" .
                    "                    r_hte.region_id AS region\n" .
                    "FROM\n" .
                    "r_hte\n" .
                    "INNER JOIN r_hte_job_trainings ON r_hte_job_trainings.hte_id = r_hte.hte_id\n" .
                    "WHERE r_hte.hte_name LIKE ?\n" .
                    "OR \n" .
                    "r_hte_job_trainings.job_training_name LIKE ?\n" .
                    "OR \n" .
                    "r_hte_job_trainings.description LIKE ?\n" .
                    "OR\n" .
                    "r_hte.address LIKE ?", [$query, $query, $query, $query]));

                $htes = \App\HTE::all();
                return view('pages.intern.home-search', compact('htes', 'results', 'queryoriginal'));
            }
            $recommendation_array = array();
            if (null == $region) {
                $region_id = \App\UnivIntern::where('intern_id', \App\UserRole::where('user_id', \Auth::user()->id)->first()->info_id)->first()->region_id;
                $ojts = collect(DB::select("SELECT\n" .
                    "	r_hte_job_trainings.description AS description,\n" .
                    "	r_hte.hte_name AS company_name,\n" .
                    "	r_hte_job_trainings.job_training_id AS job_id,\n" .
                    "	r_hte_job_trainings.job_training_name AS ojt_name,\n" .
                    "	r_hte_job_trainings.`start` AS date,\n" .
                    "	r_hte_job_trainings.training_code AS `code`,\n" .
                    "	r_hte.hte_id AS hte,\n" .
                    "   r_hte.address AS address," .
                    "	r_hte.region_id AS region \n" .
                    "FROM\n" .
                    "	r_hte\n" .
                    "	INNER JOIN r_hte_job_trainings ON r_hte_job_trainings.hte_id = r_hte.hte_id\n" .
                    "WHERE\n" .
                    "	r_hte.region_id = ?", [$region_id]));
            } else {
                $region_id = $region;
                $ojts = collect(DB::select("SELECT\n" .
                    "	r_hte_job_trainings.description AS description,\n" .
                    "	r_hte.hte_name AS company_name,\n" .
                    "	r_hte_job_trainings.job_training_id AS job_id,\n" .
                    "	r_hte_job_trainings.job_training_name AS ojt_name,\n" .
                    "	r_hte_job_trainings.`start` AS date,\n" .
                    "	r_hte_job_trainings.training_code AS `code`,\n" .
                    "	r_hte.hte_id AS hte,\n" .
                    "   r_hte.address AS address," .
                    "	r_hte.region_id AS region \n" .
                    "FROM\n" .
                    "	r_hte\n" .
                    "	INNER JOIN r_hte_job_trainings ON r_hte_job_trainings.hte_id = r_hte.hte_id \n" .
                    "WHERE\n" .
                    "	r_hte.region_id = ?", [$region_id]));
            }
            $fuzz = new \FuzzyWuzzy\Fuzz();
            $process = new \FuzzyWuzzy\Process($fuzz);
            $i = 0;
            foreach ($ojts as $ojt) {
                $descriptions[$i] = $ojt->description;
                $i++;
            }
            $skillsets = collect(DB::select("SELECT\n" .
                "r_intern_skills.`name` AS skill_name\n" .
                "FROM\n" .
                "r_intern_skills\n" .
                "INNER JOIN r_intern_skill_sets ON r_intern_skill_sets.skill_id = r_intern_skills.skill_id\n" .
                "INNER JOIN r_su_intern ON r_intern_skill_sets.intern_id = r_su_intern.intern_id\n" .
                "INNER JOIN user_infos ON r_su_intern.info_id = user_infos.info_id\n" .
                "INNER JOIN user_roles ON user_roles.info_id = user_infos.info_id\n" .
                "WHERE\n" .
                "user_roles.ur_id = ?", [\Illuminate\Support\Facades\Auth::user()->id]));
            $i = 0;
            $choices = array();
            foreach ($skillsets as $skillset) {
                $choices[$i] = $skillset->skill_name;
                $i++;
            }
            $i = 0;
            foreach ($ojts as $ojt) {
                $c = $process->extract($ojt->description, $choices, null, null, null);
                $recommendation_array[$i] = [[$ojt->description], $c->toArray()];
                $i++;
            }
            $final_recommendation = null;
            $i = 0;
            $j = 0;
            foreach ($recommendation_array as $item) {
                foreach ($item as $value) {
                    foreach ($value as $cart) {
                        if ($cart[1] > 50) {
                            $final_recommendation[$j] = $item[0];
                            $j++;
                            break;
                        }
                    }
                }
            }
            $unsortedHTEs = \App\HTE::all();
            $coordsIntern = \App\Region::getCoordinates(\App\UnivIntern::where('info_id', \App\UserRole::where('user_id', \Illuminate\Support\Facades\Auth::user()->id)->first()->info_id)->first()->intern_address);
            foreach ($unsortedHTEs as $hte) {
                if ('ERROR' !== $coordsHTE[$hte->hte_id] = \App\Region::getCoordinates($hte->address)) {
                    if (null !== $distance = \App\Region::getDistance($coordsIntern['lat'], $coordsIntern['long'], $coordsHTE[$hte->hte_id]['lat'], $coordsHTE[$hte->hte_id]['long'])['response']['matrixEntry'][0]['summary']['distance']) {
                        $hte->distance = floatval($distance / 1000);
                    } else {
                        $hte->distance = 999999;
                    }
                } else {
                    $hte->distance = 999999;
                }
            }
            $htes = $unsortedHTEs->sortBy('distance');
            $descriptions = array_column($final_recommendation, 0);
            return view('pages.intern.home', compact('ojts', 'final_recommendation', 'htes', 'region_id', 'coordsIntern', 'coordsHTE', 'descriptions'));
        })->name('intern.hte-finder');
        Route::resource('/profile', 'InternProfileCRUDController');
        Route::get('/ojt-list', function () {
            $ojts = DB::table('h_t_e_o_j_ts as ho')
                ->select('o.name as ojtname', 'h.company_name as company_name', 'o.description as description', 'o.start as date')
                ->join('job_trainings as o', 'ho.job_training_id', '=', 'o.id')
                ->join('host_training_establishment_infos as h', 'ho.hte_id', '=', 'h.id')
                ->get();
            return view('pages.intern.job-training-list', compact('ojts'));
        })->name('intern.ojt-list');
        Route::get('/application-list', function () {
            $user_applications = array();
            $user_applications = \App\InternApplication::where('intern_id', \App\UnivIntern::where('info_id', \App\UserRole::where('user_id', Auth::user()->id)->first()->info_id)->first()->intern_id)->where('status', true)->get();
            $statuses = \App\InternApplicationStatus::all();
            return view('pages.intern.application-list', compact('user_applications', 'statuses', 'htes'));
        })->name('intern.application-list');
        Route::get('/profile', function () {
            return view('pages.intern.profile');
        })->name('profile.index');
        Route::resource('/application-controller', 'ApplicationController');
        Route::resource('/view-hte', 'HTEViewerControllerForIntern');
    });
    Route::group(['prefix' => 'hte', 'middleware' => 'hte'], function () {
        Route::get('/', function () {
            return redirect(route('hte.dashboard'));
        })->name('hte.home');
        Route::get('/dashboard', 'PageController@hte_dashboard')->name('hte.dashboard');
        Route::get('/job-trainings-management', function () {
            $students = \App\UnivIntern::all();
            $sucs = collect(DB::select("SELECT\n" .
                "	r_su_college_courses.course_id AS id,\n" .
                "	r_su.su_name AS name, \n" .
                "	r_su_college_courses.course_name AS course \n" .
                "FROM\n" .
                "	r_su_intern\n" .
                "	INNER JOIN r_su_college_courses ON r_su_intern.bc_id = r_su_college_courses.course_id\n" .
                "	INNER JOIN r_su_colleges ON r_su_college_courses.college_id = r_su_colleges.college_id\n" .
                "	INNER JOIN r_su ON r_su_colleges.su_id = r_su.su_id"));
            return view('pages.hte.home', compact('students', 'sucs'));
        })->name('hte.job-trainings-management');
        Route::get('/intern-finder2', 'PageController@intern_finder')->name('hte.intern-finder2');
        Route::get('/intern-finder', 'PageController@hte_intern_finder')->name('hte.intern-finder');
        Route::resource('/add-job-trainings', 'JobTrainingCRUDController');
        Route::resource('/application-manager', 'InternApplicationManagerController');
        Route::resource('/grades', 'HTEGradingController');
    });
    Route::group(['prefix' => 'admin'], function () {
        Route::get('/audit-trail', function () {
            $auditTrails = \App\AuditTrail::orderByDesc('created_at')->get();
            $details = \App\AuditTrailMessage::all();
            $view = 'audittrail';
            return view('pages.admin.audit-trail', compact('auditTrails', 'details', 'view'));
        })->name('admin.audit-trail');
        Route::get('/user-management', 'UserManagementController@admin_recordlist')->name('admin.userrecord');
    });
});

Route::get('/test-map', function () {
    $origin = \App\Region::getCoordinates('Quezon City');
    $to = \App\Region::getCoordinates('Mandaluyong City');
    $distance = \App\Region::getDistance($origin['lat'], $origin['long'], $to['lat'], $to['long']);
    return floatval($distance['response']['matrixEntry'][0]['summary']['distance'] / 1000);
});
