<?php $__env->startSection('title', 'Intern List'); ?>

<?php $__env->startSection('css'); ?>
<link href="<?php echo e(asset('assets/backend/plugins/DataTables/media/css/dataTables.bootstrap.min.css')); ?>" rel="stylesheet" />
<link href="<?php echo e(asset('assets/backend/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css')); ?>" rel="stylesheet" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page-level-js'); ?>
    <script src="<?php echo e(asset('assets/backend/plugins/pace/pace.min.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <!-- begin breadcrumb -->
    <div class="row">
        <ol class="breadcrumb pull-left">
            <li class="breadcrumb-item"><a href="javascript:;"><i class="fa fa-home"></i>&nbsp&nbspHome</a></li>
            <li class="breadcrumb-item"><i class="fa fa-hdd"></i>&nbsp&nbspInterns</li>
            <li class="breadcrumb-item active"><i class="fa fa-list"></i>&nbsp&nbspRecord List</li>
        </ol>
    </div><br>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Intern Record List</h1>
    <!-- end page-header -->
    
    <!-- begin panel -->
    <div class="panel panel-inverse">
        <!-- begin panel-heading -->
        <div class="panel-heading">
            <h4 class="panel-title">Data Table - Default</h4>
        </div>
        <!-- end panel-heading -->
        <!-- begin panel-body -->
        <div class="panel-body">
            <table id="data-table-default" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        
                        <th width="1%" class="text-nowrap" hidden></th>
                        <th width="7%" class="text-nowrap" data-orderable="false">Student Number</th>
                        <th width="25%" class="text-nowrap">Student Trainee</th>
                        <th width="10%" class="text-nowrap" data-orderable="false">Gender</th>
                        <th width="10%" class="text-nowrap" data-orderable="false">Birth Date</th>
                        <th width="15%" class="text-nowrap" data-orderable="false">E-mail</th>
                        <th class="text-nowrap" data-orderable="false">Address</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(count($interns) > 0): ?>
                        <?php $__currentLoopData = $interns; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $intern): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr class="odd gradeX">
                            <td width="1%" hidden><?php echo e($intern->IntCode); ?></td>
                            <td width="7%"><?php echo e($intern->StudNo); ?></td>
                            <td width="25%"><?php echo e($intern->Fname.' '.$intern->Lname); ?></td>
                            <?php if($intern->Gender == 'M'): ?>
                                <td width="10%">Male</td>
                            <?php else: ?>
                                <td width="10%">Female</td>
                            <?php endif; ?>
                            <td width="10%"><?php echo e(\Carbon\Carbon::parse($intern->BirthDate)->format('m-d-Y')); ?></td>
                            <td width="15%"><?php echo e($intern->Email); ?></td>
                            <td><?php echo e($intern->Address); ?></td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
        <!-- end panel-body -->
    </div>
    <!-- end panel -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('assets/backend/plugins/DataTables/media/js/jquery.dataTables.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/DataTables/media/js/dataTables.bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/js/demo/table-manage-default.demo.min.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-js'); ?>
    <script>
        $(document).ready(function() {
            App.init();
			TableManageDefault.init();
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.coordinator', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>