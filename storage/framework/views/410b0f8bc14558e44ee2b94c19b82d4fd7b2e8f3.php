<!DOCTYPE html>
<!--[if IE 8]> <html lang="<?php echo e(app()->getLocale()); ?>" class="ie8"> <![endif]-->

<!--[if !IE]><!-->
<html lang="<?php echo e(app()->getLocale()); ?>">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <title><?php echo $__env->yieldContent('title'); ?> | i-Interno</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/frontend/plugins/bootstrap3/css/bootstrap.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/frontend/plugins/font-awesome/css/font-awesome.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/frontend/plugins/animate/animate.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/frontend/css/e-commerce/style.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/frontend/css/e-commerce/style-responsive.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/frontend/css/e-commerce/theme/default.css')); ?>" id="theme" rel="stylesheet" />
    <!-- ================== END BASE CSS STYLE ================== -->

    <!-- ================== BEGIN EXTRA CSS STYLE ================== -->
    <?php $__env->startSection('css'); ?>
    <?php echo $__env->yieldSection(); ?>
    <!-- ================== END EXTRA CSS STYLE ================== -->

    <!-- ================== BEGIN BASE JS ================== -->
    <script src="<?php echo e(asset('assets/frontend/plugins/pace/pace.min.js')); ?>"></script>
    <!-- ================== END BASE JS ================== -->
</head>
<body>
<!-- BEGIN #page-container -->
<div id="page-container" class="fade">
    <!-- BEGIN #top-nav -->
    <div id="top-nav" class="top-nav">
        <!-- BEGIN container -->
        <div class="container">
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="#">Link #1</a></li>
                    <li><a href="#">Link #2</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">Link #3</a></li>
                    <li><a href="#">Link #4</a></li>
                    <li><a href="#">Link #5</a></li>
                    <li><a href="#"><i class="fa fa-facebook f-s-14"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter f-s-14"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram f-s-14"></i></a></li>
                    <li><a href="#"><i class="fa fa-dribbble f-s-14"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus f-s-14"></i></a></li>
                </ul>
            </div>
        </div>
        <!-- END container -->
    </div>
    <!-- END #top-nav -->

    <!-- BEGIN #header -->
    <div id="header" class="header" data-fixed-top="true">
        <!-- BEGIN container -->
        <div class="container">
            <!-- BEGIN header-container -->
            <div class="header-container">
                <!-- BEGIN navbar-header -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="header-logo">
                        <a href="<?php echo e(route('home')); ?>">
                            <span class="brand"></span>
                            <span>i</span>-Interno
                            <small>Recommendation System</small>
                        </a>
                    </div>
                </div>
                <!-- END navbar-header -->
                <!-- BEGIN header-nav -->
                <div class="header-nav">
                    <div class=" collapse navbar-collapse" id="navbar-collapse">
                        <ul class="nav">
                            <?php $__env->startSection('navbar'); ?>
                            <?php echo $__env->yieldSection(); ?>
                        </ul>
                    </div>
                </div>
                <!-- END header-nav -->
                <!-- BEGIN header-nav -->
                <div class="header-nav">
                    <ul class="nav pull-right">
                        <li>
                            <?php $__env->startSection('account'); ?>
                            <?php echo $__env->yieldSection(); ?>
                        </li>
                    </ul>
                </div>
                <!-- END header-nav -->
            </div>
            <!-- END header-container -->
        </div>
        <!-- END container -->
    </div>
    <!-- END #header -->

    <?php $__env->startSection('content'); ?>
    <?php echo $__env->yieldSection(); ?>

    <!-- BEGIN #footer -->
    <div id="footer" class="footer">
        <!-- BEGIN container -->
        <div class="container">
            <!-- BEGIN row -->
            <div class="row">
                <!-- BEGIN col-3 -->
                <div class="col-md-3">
                    <h4 class="footer-header">ABOUT US</h4>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nec tristique dolor, ac efficitur velit. Nulla lobortis tempus convallis. Nulla aliquam lectus eu porta pulvinar. Mauris semper justo erat.
                    </p>
                    <p>
                        Vestibulum porttitor lorem et vestibulum pharetra. Phasellus sit amet mi congue, hendrerit mi ut, dignissim eros.
                    </p>
                </div>
                <!-- END col-3 -->
                <!-- BEGIN col-3 -->
                <div class="col-md-3">
                    <h4 class="footer-header">RELATED LINKS</h4>
                    <ul class="fa-ul">
                        <li><i class="fa fa-li fa-angle-right"></i> <a href="#">Shopping Help</a></li>
                        <li><i class="fa fa-li fa-angle-right"></i> <a href="#">Terms of Use</a></li>
                        <li><i class="fa fa-li fa-angle-right"></i> <a href="#">Contact Us</a></li>
                        <li><i class="fa fa-li fa-angle-right"></i> <a href="#">Careers</a></li>
                        <li><i class="fa fa-li fa-angle-right"></i> <a href="#">Payment Method</a></li>
                        <li><i class="fa fa-li fa-angle-right"></i> <a href="#">Sales & Refund</a></li>
                        <li><i class="fa fa-li fa-angle-right"></i> <a href="#">Sitemap</a></li>
                        <li><i class="fa fa-li fa-angle-right"></i> <a href="#">Privacy & Policy</a></li>
                    </ul>
                </div>
                <!-- END col-3 -->
                <!-- BEGIN col-3 -->
                <div class="col-md-3">
                    <h4 class="footer-header">LATEST PRODUCT</h4>
                    <ul class="list-unstyled list-product">
                        <li>
                            <div class="image">
                                <img src="../assets/img/product/product-iphone-6s.jpg" alt="" />
                            </div>
                            <div class="info">
                                <h4 class="info-title">Iphone 6s</h4>
                                <div class="price">$1200.00</div>
                            </div>
                        </li>
                        <li>
                            <div class="image">
                                <img src="../assets/img/product/product-galaxy-s6.jpg" alt="" />
                            </div>
                            <div class="info">
                                <h4 class="info-title">Samsung Galaxy s7</h4>
                                <div class="price">$850.00</div>
                            </div>
                        </li>

                        <li>
                            <div class="image">
                                <img src="../assets/img/product/product-ipad-pro.jpg" alt="" />
                            </div>
                            <div class="info">
                                <h4 class="info-title">Ipad Pro</h4>
                                <div class="price">$800.00</div>
                            </div>
                        </li>
                        <li>
                            <div class="image">
                                <img src="../assets/img/product/product-galaxy-note5.jpg" alt="" />
                            </div>
                            <div class="info">
                                <h4 class="info-title">Samsung Galaxy Note 5</h4>
                                <div class="price">$1200.00</div>
                            </div>
                        </li>
                    </ul>
                </div>
                <!-- END col-3 -->
                <!-- BEGIN col-3 -->
                <div class="col-md-3">
                    <h4 class="footer-header">OUR CONTACT</h4>
                    <address>
                        <strong>Twitter, Inc.</strong><br />
                        1355 Market Street, Suite 900<br />
                        San Francisco, CA 94103<br /><br />
                        <abbr title="Phone">Phone:</abbr> (123) 456-7890<br />
                        <abbr title="Fax">Fax:</abbr> (123) 456-7891<br />
                        <abbr title="Email">Email:</abbr> <a href="mailto:sales@myshop.com">sales@myshop.com</a><br />
                        <abbr title="Skype">Skype:</abbr> <a href="skype:myshop">myshop</a>
                    </address>
                </div>
                <!-- END col-3 -->
            </div>
            <!-- END row -->
        </div>
        <!-- END container -->
    </div>
    <!-- END #footer -->

    <!-- BEGIN #footer-copyright -->
    <div id="footer-copyright" class="footer-copyright">
        <!-- BEGIN container -->
        <div class="container">
            <div class="copyright">
                Copyright &copy; 2018 SRG 7th Generation. All rights reserved.
            </div>
        </div>
        <!-- END container -->
    </div>
    <!-- END #footer-copyright -->
</div>
<!-- END #page-container -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="<?php echo e(asset('assets/frontend/plugins/jquery/jquery-3.2.1.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/frontend/plugins/bootstrap3/js/bootstrap.min.js')); ?>"></script>
<!--[if lt IE 9]>
<script src="<?php echo e(asset('assets/frontend/crossbrowserjs/html5shiv.js')); ?>"></script>
<script src="<?php echo e(asset('assets/frontend/crossbrowserjs/respond.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/frontend/crossbrowserjs/excanvas.min.js')); ?>"></script>
<![endif]-->
<script src="<?php echo e(asset('assets/frontend/plugins/js-cookie/js.cookie.js')); ?>"></script>
<script src="<?php echo e(asset('assets/frontend/js/e-commerce/apps.min.js')); ?>"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN EXTRA JS STYLE  ================== -->
<?php $__env->startSection('js'); ?>
<?php echo $__env->yieldSection(); ?>
<!-- ================== END EXTRA JS STYLE ================== -->

<script>
    $(document).ready(function() {
        App.init();
    });
</script>
</body>
</html>
