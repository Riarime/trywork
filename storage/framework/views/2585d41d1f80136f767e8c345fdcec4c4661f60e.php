<?php $__env->startSection('title', 'Home'); ?>

<?php $__env->startSection('css'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('navbar'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('account'); ?>
    <a href="<?php echo e(route('login')); ?>">
        <img src="<?php echo e(asset('assets/frontend/img/user/user-1.jpg')); ?>" class="user-img" alt="" />
        <span class="hidden-md hidden-sm hidden-xs">Login</span>
    </a>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <!-- BEGIN #slider -->
    <div id="slider" class="section-container p-0 bg-black-darker">
        <!-- BEGIN carousel -->
        <div id="main-carousel" class="carousel slide" data-ride="carousel">
            <!-- BEGIN carousel-inner -->
            <div class="carousel-inner">
                <!-- BEGIN item -->
                <div class="item active">
                    <img src="<?php echo e(asset('assets/frontend/img/slider/slider-1-cover.jpg')); ?>" class="bg-cover-img" alt="" />
                    <div class="container">
                        <img src="../assets/img/slider/slider-1-product.png" class="product-img right bottom fadeInRight animated" alt="" />
                    </div>
                    <div class="carousel-caption carousel-caption-left">
                        <div class="container">
                            <h3 class="title m-b-5 fadeInLeftBig animated">i-Interno</h3>
                            <p class="m-b-15 fadeInLeftBig animated">Intelligent system to make internship application a lot faster.<br/><small>Over thousands of Host Training Establishments are available.</small></p>
                            
                            <a href="#" class="btn btn-outline btn-lg fadeInLeftBig animated">Join Now</a>
                        </div>
                    </div>
                </div>
                <!-- END item -->
            </div>
            <!-- END carousel-inner -->
        </div>
        <!-- END carousel -->
    </div>
    <!-- END #slider -->

    <!-- BEGIN #promotions -->
    <div id="promotions" class="section-container bg-white">
        <!-- BEGIN container -->
        <div class="container">
            <!-- BEGIN section-title -->
            <h4 class="section-title clearfix">
                <a href="#" class="pull-right">SHOW ALL</a>
                Features
                <small>of i-Interno Intern Recommendation System and Management Services</small>
            </h4>
            <!-- END section-title -->
            <!-- BEGIN #policy -->
            <div id="policy" class="section-container p-t-30 p-b-30">
                <!-- BEGIN container -->
                <div class="container">
                    <!-- BEGIN row -->
                    <div class="row">
                        <!-- BEGIN col-4 -->
                        <div class="col-md-4 col-sm-4">
                            <!-- BEGIN policy -->
                            <div class="policy">
                                <div class="policy-icon"><i class="fa fa-graduation-cap"></i></div>
                                <div class="policy-info">
                                    <h4>Intelligently recommends interns</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                </div>
                            </div>
                            <!-- END policy -->
                        </div>
                        <!-- END col-4 -->
                        <!-- BEGIN col-4 -->
                        <div class="col-md-4 col-sm-4">
                            <!-- BEGIN policy -->
                            <div class="policy">
                                <div class="policy-icon"><i class="fa fa-building-o"></i></div>
                                <div class="policy-info">
                                    <h4>Suggests Training Establishments</h4>
                                    <p>Cras laoreet urna id dui malesuada gravida. <br />Duis a lobortis dui.</p>
                                </div>
                            </div>
                            <!-- END policy -->
                        </div>
                        <!-- END col-4 -->
                        <!-- BEGIN col-4 -->
                        <div class="col-md-4 col-sm-4">
                            <!-- BEGIN policy -->
                            <div class="policy">
                                <div class="policy-icon"><i class="fa fa-users"></i></div>
                                <div class="policy-info">
                                    <h4>i-Interno tracks interns and HTEs</h4>
                                    <p>Fusce ut euismod orci. Morbi auctor, sapien non eleifend iaculis.</p>
                                </div>
                            </div>
                            <!-- END policy -->
                        </div>
                        <!-- END col-4 -->
                    </div>
                    <!-- END row -->
                </div>
                <!-- END container -->
            </div>
            <!-- END #policy -->
            <!-- BEGIN section-title -->
            <h4 class="section-title clearfix">
                <a href="#" class="pull-right">SHOW ALL</a>
                Announcements
                <small>for the month of August</small>
            </h4>
            <!-- END section-title -->
            <!-- BEGIN row -->
            <div class="row row-space-10">
                <!-- BEGIN col-6 -->
                <div class="col-md-6">
                    <!-- BEGIN promotion -->
                    <div class="promotion promotion-lg bg-black-darker">
                        <div class="promotion-image text-right promotion-image-overflow-bottom">
                            <img src="<?php echo e(asset('assets/frontend/img/product/product-iphone-se.png')); ?>" alt="" />
                        </div>
                        <div class="promotion-caption promotion-caption-inverse">
                            <h4 class="promotion-title">i-Interno Development</h4>
                            
                            <p class="promotion-desc">A new way to match interns with companies.<br />Made with love and Laravel.</p>
                            <a href="#" class="promotion-btn">View More</a>
                        </div>
                    </div>
                    <!-- END promotion -->
                </div>
                <!-- END col-6 -->
                <!-- BEGIN col-3 -->
                <div class="col-md-3 col-sm-6">
                    <!-- BEGIN promotion -->
                    <div class="promotion bg-blue">
                        <div class="promotion-image promotion-image-overflow-bottom promotion-image-overflow-top">
                            <img src="<?php echo e(asset('assets/frontend/img/product/product-apple-watch-sm.png')); ?>" alt="" />
                        </div>
                        <div class="promotion-caption promotion-caption-inverse text-right">
                            <h4 class="promotion-title">Version 0.1</h4>
                            
                            
                            <a href="#" class="promotion-btn">View More</a>
                        </div>
                    </div>
                    <!-- END promotion -->
                    <!-- BEGIN promotion -->
                    <div class="promotion bg-silver">
                        <div class="promotion-image text-center promotion-image-overflow-bottom">
                            <img src="<?php echo e(asset('assets/frontend/img/product/product-mac-mini.png')); ?>" alt="" />
                        </div>
                        <div class="promotion-caption text-center">
                            <h4 class="promotion-title">Version 0.2</h4>
                            
                            
                            <a href="#" class="promotion-btn">View More</a>
                        </div>
                    </div>
                    <!-- END promotion -->
                </div>
                <!-- END col-3 -->
                <!-- BEGIN col-3 -->
                <div class="col-md-3 col-sm-6">
                    <!-- BEGIN promotion -->
                    <div class="promotion bg-silver">
                        <div class="promotion-image promotion-image-overflow-right promotion-image-overflow-bottom text-right">
                            <img src="<?php echo e(asset('assets/frontend/img/product/product-mac-accessories.png')); ?>" alt="" />
                        </div>
                        <div class="promotion-caption text-center">
                            <h4 class="promotion-title">Version 0.3</h4>
                            
                            
                            <a href="#" class="promotion-btn">View More</a>
                        </div>
                    </div>
                    <!-- END promotion -->
                    <!-- BEGIN promotion -->
                    <div class="promotion bg-black">
                        <div class="promotion-image text-right">
                            <img src="<?php echo e(asset('assets/frontend/img/product/product-mac-pro.png')); ?>" alt="" />
                        </div>
                        <div class="promotion-caption promotion-caption-inverse">
                            <h4 class="promotion-title">Version 0.4</h4>
                            
                            
                            <a href="#" class="promotion-btn">View More</a>
                        </div>
                    </div>
                    <!-- END promotion -->
                </div>
                <!-- END col-3 -->
            </div>
            <!-- END row -->
        </div>
        <!-- END container -->
    </div>
    <!-- END #promotions -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>