<?php $__env->startSection('title', 'Job Trainings Manager'); ?>

<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css')); ?> rel="stylesheet" />
    <link href="<?php echo e(asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/plugins/jquery-jvectormap/jquery-jvectormap.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/plugins/bootstrap-calendar/css/bootstrap_calendar.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/plugins/gritter/css/jquery.gritter.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/plugins/nvd3/build/nv.d3.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/plugins/DataTables/media/css/dataTables.bootstrap.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css')); ?>" rel="stylesheet" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.2/d3.min.js"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/js/demo/table-manage-default.demo.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/nvd3/build/nv.d3.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/jquery-jvectormap/jquery-jvectormap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/jquery-jvectormap/jquery-jvectormap-world-merc-en.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/bootstrap-calendar/js/bootstrap_calendar.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/gritter/js/jquery.gritter.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/bootstrap-sweetalert/sweetalert.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/DataTables/media/js/jquery.dataTables.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/DataTables/media/js/dataTables.bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/js/demo/dashboard-v2.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/js/demo/form-plugins.demo.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/highlight/highlight.common.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/js/demo/render.highlight.js')); ?>"></script>
    <script>
        var handleSweetNotification = function() {
            $('[data-click="swal-submit"]').click(function (e) {
                e.preventDefault();
                swal({
                    title: 'Are you sure?',
                    text: '',
                    icon: 'info',
                    buttons: {
                        cancel: {
                            text: 'Cancel',
                            value: null,
                            visible: true,
                            className: 'btn btn-default',
                            closeModal: true,
                        },
                        confirm: {
                            text: 'Submit',
                            value: true,
                            visible: true,
                            className: 'btn btn-lime text-black',
                            closeModal: true
                        }
                    }
                });
            });
        }
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('document.ready'); ?>
    handleSweetNotification();
    TableManageDefault.init();
    handleDatepicker().init();
    Highlight.init();
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="panel panel-inverse" data-sortable-id="ui-widget-1">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="#" class="btn btn-xs btn-icon btn-circle btn-default"
                   data-click="panel-expand">
                    <i class="fa fa-expand"></i>
                </a>
            </div>
            <h4 class="panel-title">Job Training Management</h4>
        </div>
        <div class="panel-body">
            <a href="#addNewJobTraining" class="btn btn-primary pull-right" data-toggle="modal">Add New Job Training</a>
            <table id="data-table-default" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th width="1%">Code</th>
                    <th class="text-nowrap">Job Training Name</th>
                    <th class="text-nowrap">Description</th>
                    <th class="text-nowrap" width="15%">Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php if(null !== \App\HTEJobTrainings::getJobTrainings()): ?>
                <?php $__currentLoopData = \App\HTEJobTrainings::getJobTrainings(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jobTraining): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($jobTraining->code); ?></td>
                        <td><?php echo e($jobTraining->name); ?></td>
                        <td><?php echo e($jobTraining->description); ?></td>
                        <td>
                            <a href="#ApplicationManager<?php echo e($jobTraining->code); ?>" class="btn btn-sm btn-warning" data-toggle="modal">Intern Management</a>
                        </td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="modal fade" id="addNewJobTraining">
        <form method="post" action="<?php echo e(route('add-job-trainings.store')); ?>" class="modal-dialog">
            <div class="modal-content">
                <?php echo e(csrf_field()); ?>

                <input type="hidden" name="_method" value="POST" />
                <div class="modal-header">
                    <h4 class="modal-title">Add New Job Training</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Job Training Code</label>
                        <div class="col-md-9">
                            <input type="text" name="code" class="form-control m-b-5" placeholder="Enter Code" />
                        </div>
                    </div>
                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Name</label>
                        <div class="col-md-9">
                            <input type="text" name="name" class="form-control m-b-5" placeholder="Enter Name" />
                        </div>
                    </div>
                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Description</label>
                        <div class="col-md-9">
                            <input type="text" name="description" class="form-control m-b-5" placeholder="Enter Description" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
                    <button class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
    </div>
    <?php if(null !== \App\HTEJobTrainings::getJobTrainings()): ?>
    <?php $__currentLoopData = \App\HTEJobTrainings::getJobTrainings(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jobTraining): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="modal fade modal-message" id="ApplicationManager<?php echo e($jobTraining->code); ?>">
            <div class="modal-dialog height-lg">
                <div class="modal-content height-lg">
                    <div class="modal-header">
                        <h4 class="modal-title">Interns that applied for <?php echo e($jobTraining->name); ?></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <table class="table table-striped">
                            <thead>
                            <th width="30%">Name, Course and College/University</th>
                            <th>GPA</th>
                            <th>Skills</th>
                            <th>Status</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                            <?php if(null !== \App\InternApplication::where('job_training_id', $jobTraining->id)->where('status', true)->first()): ?>
                            <?php $__currentLoopData = \App\InternApplication::where('job_training_id', $jobTraining->id)->where('status', true)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $application): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td>
                                        <strong><?php echo e(ucwords(strtolower(\App\User::getInternName($application->intern_id)))); ?></strong><br/>
                                        <small><?php echo e(\App\User::getProgram($application->intern_id)); ?><br/><?php echo e(\App\User::getSUC($application->intern_id)); ?></small>
                                    </td>
                                    <td><?php echo e(\App\UnivIntern::where('intern_id', $application->intern_id)->first()->gpa); ?></td>
                                    <td>
                                        <ul>
                                            <?php $__currentLoopData = \App\User::getStudentSkills($application->intern_id); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $studentSkill): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li><?php echo e($studentSkill); ?></li>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ul>
                                    </td>
                                    <td>
                                        <?php if(!\App\InternGrading::where('intern_id', $application->intern_id)->first()): ?>
                                            <span class="label label-primary">
                                                <?php echo e(\App\User::getApplicationStatus($jobTraining->id, $application->intern_id, true)->name); ?>

                                        </span>
                                        <?php elseif(\App\HTEGrade::where('hte_id', \App\HTEJobTrainings::where('job_training_id', $jobTraining->id)->first()->hte_id)->where('grading_id', \App\InternGrading::where('intern_id', $application->intern_id)->first()->grading_id)->first() && \App\HTEGrade::where('hte_id', \App\HTEJobTrainings::where('job_training_id', $jobTraining->id)->first()->hte_id)->where('grading_id', \App\InternGrading::where('intern_id', $application->intern_id)->first()->grading_id)->first()  ->intern_grade >= 75.0): ?>
                                            <span class="label label-primary">
                                                <?php echo e(\App\User::getApplicationStatus($jobTraining->id, $application->intern_id, true)->name); ?>

                                        </span>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if(\App\User::getApplicationStatus($jobTraining->id, $application->intern_id)->iappstat_code == 'TWIAS-1'): ?>
                                            <a href="#" onclick="event.preventDefault(); document.getElementById('frm-interview').submit();" class="btn btn-primary btn-sm">For Interview</a>
                                            <a href="#" onclick="event.preventDefault(); document.getElementById('frm-not-suitable').submit();" class="btn btn-danger btn-sm">Skills Not Suitable</a>
                                            <form id="frm-interview" action="<?php echo e(route('application-manager.store')); ?>" method="POST" style="display: none;">
                                                <?php echo e(csrf_field()); ?>

                                                <input type="hidden" name="status" value="TWIAS-3"/>
                                                <input type="hidden" name="student_id" value="<?php echo e($application->intern_id); ?>"/>
                                                <input type="hidden" name="job_training_id" value="<?php echo e($jobTraining->id); ?>"/>
                                            </form>
                                            <form id="frm-not-suitable" action="<?php echo e(route('application-manager.store')); ?>" method="POST" style="display: none;">
                                                <?php echo e(csrf_field()); ?>

                                                <input type="hidden" name="status" value="TWIAS-2"/>
                                                <input type="hidden" name="student_id" value="<?php echo e($application->intern_id); ?>"/>
                                                <input type="hidden" name="job_training_id" value="<?php echo e($jobTraining->id); ?>"/>
                                            </form>
                                        <?php elseif(\App\User::getApplicationStatus($jobTraining->id, $application->intern_id)->iappstat_code == 'TWIAS-3'): ?>
                                            <a href="#" onclick="event.preventDefault(); document.getElementById('frm-passed').submit();" class="btn btn-lime btn-sm">Passed</a>
                                            <a href="#" onclick="event.preventDefault(); document.getElementById('frm-not-suitable').submit();" class="btn btn-danger btn-sm">Skills Not Suitable</a>
                                            <form id="frm-passed" action="<?php echo e(route('application-manager.store')); ?>" method="POST" style="display: none;">
                                                <?php echo e(csrf_field()); ?>

                                                <input type="hidden" name="status" value="TWIAS-4"/>
                                                <input type="hidden" name="student_id" value="<?php echo e($application->intern_id); ?>"/>
                                                <input type="hidden" name="job_training_id" value="<?php echo e($jobTraining->id); ?>"/>
                                            </form>
                                            <form id="frm-not-suitable" action="<?php echo e(route('application-manager.store')); ?>" method="POST" style="display: none;">
                                                <?php echo e(csrf_field()); ?>

                                                <input type="hidden" name="status" value="TWIAS-2"/>
                                                <input type="hidden" name="student_id" value="<?php echo e($application->intern_id); ?>"/>
                                                <input type="hidden" name="job_training_id" value="<?php echo e($jobTraining->id); ?>"/>
                                            </form>

                                        <?php elseif(\App\User::getApplicationStatus($jobTraining->id, $application->intern_id)->iappstat_code == 'TWIAS-4'): ?>
                                            <?php if(null == \App\InternGrading::where('intern_id', $application->intern_id)->first()): ?>
                                                <a href="#Grading<?php echo e($jobTraining->code); ?>" class="btn btn-sm btn-purple" data-toggle="modal">Set Grade</a>
                                                <div class="modal fade modal-message" id="Grading<?php echo e($jobTraining->code); ?>">
                                                    <div class="modal-dialog">
                                                        <form class="form" method="post" action="<?php echo e(route('grades.store')); ?>">
                                                            <?php echo e(csrf_field()); ?>

                                                            <input type="hidden" name="hte_id" value="<?php echo e(\App\HTE::where('hte_id', \App\HTEUsers::where('ur_id', \App\UserRole::where('user_id',Auth::user()->id)->first()->ur_id)->first()->hte_id)->first()->hte_id); ?>">
                                                            <input type="hidden" name="student_id" value="<?php echo e($application->intern_id); ?>">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title">Set Grade for <?php echo e(ucwords(strtolower(\App\User::getInternName($application->intern_id)))); ?></h4>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="note note-warning note-with-right-icon">
                                                                                <div class="note-icon"><i class="fa fa-exclamation-triangle"></i></div>
                                                                                <div class="note-content text-right">
                                                                                    <h4><b>Warning!</b></h4>
                                                                                    <p>Setting a grade to the intern signifies the end of the internship of the particular student, please double check before proceeding.</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label>Set Grade:</label>
                                                                                <div class="input-group m-b-10">
                                                                                    <input type="text" class="form-control" placeholder="0 - 100" name="grade">
                                                                                    <div class="input-group-append"><span class="input-group-text">%</span></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <div class="form-group">
                                                                                <label>Remarks:</label>
                                                                                <div class="input-group m-b-10">
                                                                                    <textarea class="form-control" rows="3" name="remarks"></textarea>
                                                                                </div>
                                                                            </div>
                                                                            <div class="checkbox checkbox-css m-b-20">
                                                                                <input type="checkbox" id="<?php echo e($jobTraining->code.'-'.$application->intern_id); ?>" required>
                                                                                <label for="<?php echo e($jobTraining->code.'-'.$application->intern_id); ?>">I hereby certify that the intern named <?php echo e(ucwords(strtolower(\App\User::getInternName($application->intern_id)))); ?> finished our internship program entitled '<?php echo e($jobTraining->name); ?>'.</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <a href="javascript:;" class="btn btn-white" data-dismiss="modal">Cancel</a>
                                                                    <button type="submit" class="btn btn-primary">Submit Grade to Coordinator</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            <?php else: ?>

                                                <a href="#ViewGrade<?php echo e($jobTraining->code); ?>" class="btn btn-sm btn-purple" data-toggle="modal">View Grade</a>
                                                <div class="modal fade" id="ViewGrade<?php echo e($jobTraining->code); ?>">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">View <?php echo e(\App\User::getInternName($application->intern_id)); ?>'s Grades</h4>
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <table class="table table-striped">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>Designated User</th>
                                                                        <th>Remarks</th>
                                                                        <th>Grade</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    <tr>
                                                                        <td>HTE Representative</td>
                                                                        <td><?php echo e(\App\User::getHTEGrade($application->intern_id)->remarks); ?></td>
                                                                        <td><?php echo e(sprintf("%.2f%%", \App\User::getHTEGrade($application->intern_id)->intern_grade)); ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>State University/College</td>
                                                                        
                                                                        
                                                                        <td>
                                                                            <span class="label label-warning">Pending</span>
                                                                        </td>
                                                                        <td>
                                                                            <span class="label label-warning">Pending</span>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master-backend-hte', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>