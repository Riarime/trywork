<?php $__env->startSection('title', 'Intern Finder'); ?>

<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(asset('assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css')); ?>"
          rel="stylesheet"/>
    <link href="<?php echo e(asset('assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css')); ?>"
          rel="stylesheet"/>
    <link href="<?php echo e(asset('assets/backend/plugins/jquery-jvectormap/jquery-jvectormap.css')); ?>" rel="stylesheet"/>
    <link href="<?php echo e(asset('assets/backend/plugins/bootstrap-calendar/css/bootstrap_calendar.css')); ?>" rel="stylesheet"/>
    <link href="<?php echo e(asset('assets/backend/plugins/gritter/css/jquery.gritter.css')); ?>" rel="stylesheet"/>
    <link href="<?php echo e(asset('assets/backend/plugins/nvd3/build/nv.d3.css')); ?>" rel="stylesheet"/>
    <link href="<?php echo e(asset('assets/backend/plugins/DataTables/media/css/dataTables.bootstrap.min.css')); ?>"
          rel="stylesheet"/>
    <link href="<?php echo e(asset('assets/backend/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css')); ?>"
          rel="stylesheet"/>
    <link href="<?php echo e(asset('assets/backend/plugins/select2/dist/css/select2.min.css')); ?>" rel="stylesheet"/>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.2/d3.min.js"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/js/demo/table-manage-default.demo.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/nvd3/build/nv.d3.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/jquery-jvectormap/jquery-jvectormap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/jquery-jvectormap/jquery-jvectormap-world-merc-en.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/bootstrap-calendar/js/bootstrap_calendar.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/gritter/js/jquery.gritter.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/bootstrap-sweetalert/sweetalert.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/DataTables/media/js/jquery.dataTables.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/DataTables/media/js/dataTables.bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/js/demo/dashboard-v2.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/js/demo/form-plugins.demo.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/highlight/highlight.common.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/select2/dist/js/select2.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/js/demo/render.highlight.js')); ?>"></script>
    <script>
        const branchResult = $('div#branchResult');
        const branchName = $('#branchName');
        const branchAddress = $('#branchAddress');
        const branchImg = $('#branchImg');
        const branchSUC = $('#branchSUC');
        const placeholderEmpty = $('#placeholderEmpty');
        const loader = $('#loader');
        const suggestions = $('#suggestions');
        const analytics = $('#analytics');
        const jobTrainingTable = $('#jobTrainingTable > tbody');
        const suggestionsModal = $('#suggestionsModal');
        const internSuggestionTable = $('#internSuggestionTable');
        var results;

        $(document).ready(function () {
            branchResult.hide();
            loader.hide();
            suggestions.hide();
            $('select#branchSelector').select2({
                ajax: {
                    method: 'POST',
                    url: '<?php echo e(route('api.search.sucs')); ?>',
                    delay: 500,
                    dataType: 'json',
                    data: function (params) {
                        var queryParameters = {
                            query: params.term,
                            page: params.page
                        };
                        return queryParameters;
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 10) < data.total_count
                            }
                        }
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                },
                templateResult: formatRepo,
                templateSelection: formatRepoSelection,
                allowClear: true,
                placeholder: 'Search State Universities/Colleges',
                minimumInputLength: 3
            });
            function formatRepo(data) {
                if (data.loading) {
                    return data.text;
                }

                var markup = "<div style='display: inline-block;'>" +
                    "<div class='pull-left m-r-10'>" +
                    "<img src='" + data.img + "' style='width: 80px;height: auto'  />" +
                    "</div>" +
                    "<div class='pull-left m-t-5 m-b-5 m-r-10'><b>" + data.branch_name + "</b><br/>" +
                    "<span>" + data.address + "</span><br/><br/>" +
                    "<span><small><b>Available for Internship: </b>" + data.student_count + "</small></span></div>" +
                    "</div>";

                return markup;
            }

            function formatRepoSelection(data) {
                return data.branch_name || data.id;
            }
        });
        $('select#branchSelector').on("select2:select", function (e) {
            $.ajax({
                method: 'POST',
                url: '<?php echo e(route('api.search.sucs')); ?>',
                dataType: 'json',
                data: {
                    id: $('select#branchSelector').find(":selected").attr('value')
                },
                success: function (response) {
                    branchResult.hide();
                    analytics.hide();
                    branchName.html('<b>' + response.data.attributes.name + '</b>');
                    branchAddress.html('<b>Address:</b> ' + response.data.attributes.address);
                    branchSUC.html('<b>State University/College Code:</b> ' + response.data.attributes.parent.attributes.code + '<br/><b>State University/College Name:</b> ' + response.data.attributes.parent.attributes.name);
                    branchImg.attr('src', response.data.attributes.img);
                    placeholderEmpty.hide();
                    loader.show();

                    setTimeout(function () {
                        loader.hide();
                        branchResult.show();
                    }, 1500);
                }
            });
        });
        $('button#processSuggestion').on('click', function (e) {
            suggestions.hide();
            loader.show();

            $.ajax({
                method: 'POST',
                url: '<?php echo e(route('api.recommender.suggestions')); ?>',
                dataType: 'json',
//                data: {
//                    id: $('select#branchSeslector').find(":selected").attr('value')
//                },
                success: function (response) {
                    results = response;
                    $.each(response.data, function (k, e) {
                        var html = '<tr>' +
                            '<td>' +
                            '<img src="' + e.attributes.img + '" style="height:50px;width: auto;"  />' +
                            '</td>' +
                            '<td>' +
                            e.attributes[0].training_code +
                            '</td>' +
                            '<td>' +
                            e.attributes[0].job_training_name +
                            '</td>' +
                            '<td>' +
                            e.attributes[0].description +
                            '</td>' +
                            '<td>' +
                            e.total_interns_count +
                            '</td>' +
                            '<td>' +
                            '<button id="' + e.attributes[0].job_training_id + '" class="btn btn-sm btn-primary btn-block jobTrainingBtn">View Suggested Interns</button' +
                            '</td>' +
                            '</tr>';

                        jobTrainingTable.append(html);
                    });
                }
            });

            setTimeout(function () {
                loader.hide();
                suggestions.show();
            }, 2500);
        });

        var btnClick = function (e) {
//            alert("Button: "+e.currentTarget.id);
            var idBtn = e.currentTarget.id;
            $.each(results.data, function (k, e) {
                var html = '';
                var ratio = 0;
                var count = 0;
                var i = 0;
                if (e.attributes[0].job_training_id == idBtn) {
                    internSuggestionTable.children().remove();
                    if (e.suggestions.length > 0) {
                        $.each(e.suggestions[0], function (l, f) {
                            console.log(JSON.stringify(f));
                            html += '<tr>' +
                                '<td>' +
                                f.course +
                                '</td>' +
                                '<td>';
                            $.each(f.skills, function (m, g) {
                                html += g.skill + '<br/>';
                                ratio += parseInt(g.ratio);
                                count++;
                            });
                            html += '</td>' +
                                '<td>' +
                                (ratio / count) +
                                '%' +
                                '</td>' +
                                '<td>' +
                                '<button class="btn btn-primary btn-sm btn-block">Recruit Intern for interview</button>' +
                                '</td>' +
                                '</tr>';
                        });
                        internSuggestionTable.append(html);
                    }
                }
            });
            $('#suggestionsModal').modal().show();
        };
        $('#buttonClicker').on('click', 'button', btnClick);
    </script>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
    <div class="panel panel-inverse" data-sortable-id="ui-widget-1">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="#" class="btn btn-xs btn-icon btn-circle btn-default"
                   data-click="panel-expand">
                    <i class="fa fa-expand"></i>
                </a>
            </div>
            <h4 class="panel-title">Intern Finder</h4>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <b>Branch Selector:</b><select class="width-full" id="branchSelector"></select>
                    <div class="row" id="placeholderEmpty">
                        <div class="col-md-12">
                            <p class="text-center">Please select the branch first to see result.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
            <br/>
            <div class="row" id="analytics">
                <script type="text/javascript" src="<?php echo e(asset('assets/backend/js/highcharts/highcharts.js')); ?>"></script>
                <script type="text/javascript" src="<?php echo e(asset('assets/backend/js/highcharts/data.js')); ?>"></script>
                <script type="text/javascript" src="<?php echo e(asset('assets/backend/js/highcharts/drilldown.js')); ?>"></script>
                <div class="col-md-12">
                    <div id="branchesAnalytics" style="height: 100%; width: 100%;"></div>
                </div>
                <script>
                    Highcharts.chart('branchesAnalytics', {
                        chart: {
                            type: 'bar',
                        },
                        title: {
                            text: '<b>Number of Student Trainees for Deployment per State University/College</b>'
                        },
                        subtitle: {
                            text: 'A.Y. 2018 - 2019'
                        },
                        xAxis: {
                            type: 'category'
                        },
                        yAxis: {
                            min: 0,
                            tickInterval: 5,
                            title: {
                                text: null
                            }
                        },
                        tooltip: {
                            pointFormat: '{series.name}: <b>{point.y:f}</b>'
                        },
                        plotOptions: {
                            series: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: false
                                },
                                showInLegend: false
                            }
                        },
                        series: [{
                            name: 'Student Trainees',
                            colorByPoint: true,
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: false,
                            data: [
                                    <?php $__currentLoopData = $branchPartners; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $branchPartner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                {
                                    name: '<?php echo e($branchPartner->branch_name); ?>',
                                    y: <?php echo e(\App\UnivIntern::where('branch_id','=',$branchPartner->branch_id)->where('ay_id','=', $activeAYSem->where('branch_id', $branchPartner->branch_id)->first()->ay_id)->where('intern_stat','=','Approved')->get()->count()); ?>,
                                    drilldown: null
                                },
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            ]
                        }]
                    });
                </script>
            </div>
            <br/>
            <div class="row" id="branchResult">
                <div class="col-md-12">
                    <div class="profile">
                        <div class="profile-header">
                            <!-- BEGIN profile-header-cover -->
                            <div class="profile-header-cover"></div>
                            <!-- END profile-header-cover -->
                            <!-- BEGIN profile-header-content -->
                            <div class="profile-header-content rounded-corner">
                                <!-- BEGIN profile-header-img -->
                                <div class="profile-header-img">
                                    <img src="" id="branchImg" alt="">
                                </div>
                                <!-- END profile-header-img -->
                                <!-- BEGIN profile-header-info -->
                                <div class="profile-header-info">
                                    <h4 class="m-t-10 m-b-5" id="branchName"></h4>
                                    <p class="m-b-10" id="branchAddress"></p>
                                    <p class="m-b-10" id="branchSUC"></p>
                                    <button class="m-b-10 btn btn-success btn-lg" id="processSuggestion">Process
                                        Suggestions
                                    </button>
                                </div>
                                <!-- END profile-header-info -->
                            </div>
                            <!-- END profile-header-content -->
                            <!-- BEGIN profile-header-tab -->
                            <ul class="profile-header-tab nav nav-tabs">
                                <li class="nav-item"></li>
                            </ul>
                            <!-- END profile-header-tab -->
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <div class="row" id="loader">
                <div class="col-md-12 m-40 text-center">
                    <i class="fas fa-spinner fa-spin fa-5x"></i>
                    <br/>
                    <br/>
                    <br/>
                    <h3 class="text-muted">Loading... Please Wait.</h3>
                </div>
            </div>
            <div class="row" id="suggestions">
                <hr class="m-b-40"/>
                <div class="col-md-12">
                    <h3 class="text-center">SUGGESTIONS<br/>
                        <small>Per Job Trainings</small>
                    </h3>
                    <br/>
                    <table id="jobTrainingTable" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th width="1%" data-orderable="false"></th>
                            <th width="12%" class="text-nowrap">Code</th>
                            <th class="text-nowrap">Job Training Name</th>
                            <th width="30%" class="text-nowrap">Description</th>
                            <th class="text-nowrap">Suggested Interns</th>
                            <th class="text-nowrap">Action</th>
                        </tr>
                        </thead>
                        <tbody id="buttonClicker">

                        </tbody>
                    </table>
                </div>
            </div>
            <br/>
        </div>
    </div>

    <div class="modal fade modal-message" id="suggestionsModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Intern Recommendations per Job Training</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <table id="jobTrainingTable" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th class="text-nowrap">Course</th>
                            <th class="text-nowrap">Skills</th>
                            <th class="text-nowrap">Match Percentage</th>
                            <th class="text-nowrap">Action</th>
                        </tr>
                        </thead>
                        <tbody id="internSuggestionTable">

                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master-backend-hte', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>