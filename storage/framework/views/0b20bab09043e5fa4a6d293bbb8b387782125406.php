<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $__env->yieldContent('title'); ?> | TryWork</title>

	<!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link rel="stylesheet" href="<?php echo e(asset('assets/backend/plugins/jquery-ui/jquery-ui.min.css')); ?>">
	<link href="<?php echo e(asset('assets/backend/plugins/bootstrap/4.0.0/css/bootstrap.min.css')); ?>" rel="stylesheet" />
	<link href="<?php echo e(asset('assets/backend/plugins/font-awesome/5.0/css/fontawesome-all.min.css')); ?>" rel="stylesheet" />
	<link href="<?php echo e(asset('assets/backend/plugins/animate/animate.min.css')); ?>" rel="stylesheet" />
	<link href="<?php echo e(asset('assets/backend/css/default/style.min.css')); ?>" rel="stylesheet" />
	<link href="<?php echo e(asset('assets/backend/css/default/style-responsive.min.css')); ?>" rel="stylesheet" />
	<!-- ================== END BASE CSS STYLE ================== -->
    
    <!-- ================== BEGIN PAGE LEVEL CSS STYLE ================== -->
    <?php $__env->startSection('css'); ?>
    <?php echo $__env->yieldSection(); ?>
    <!-- ================== END PAGE LEVEL CSS STYLE ================== -->

    <!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo e(asset('assets/backend/plugins/pace/pace.min.js')); ?>"></script>
    <?php $__env->startSection('base-js'); ?>
    <?php echo $__env->yieldSection(); ?>
	<!-- ================== END BASE JS ================== -->
</head>
<body>


	<!-- begin #page-loader -->
	<div id="page-loader" class="fade show"><span class="spinner"></span></div>
	<!-- end #page-loader -->
	
	<!-- begin #page-container -->
	<div id="page-container" class="page-container fade page-sidebar-fixed page-header-fixed">
        <!-- begin #header -->
        
		<div id="header" class="header navbar-default">
			<!-- begin navbar-header -->
			<div class="navbar-header">
				<a href="" class="navbar-brand">
					<img src="<?php echo e(asset('assets/images/logo-light.png')); ?>" alt="" width="50" height="50" style="margin-top: -10px;"> <b>T</b><small><b>RY</b></small><b>W</b><small><b>ORK</b></small>
				</a>
				<button type="button" class="navbar-toggle" data-click="sidebar-toggled">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<!-- end navbar-header -->

			<!-- begin header-nav -->
			<ul class="navbar-nav navbar-right">
				<li class="dropdown navbar-user">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
						<img src="<?php echo e(asset('assets/backend/img/user/user-13.jpg')); ?>" alt="" />
						<span class="d-none d-md-inline">Riarime Gabriel</span> <b class="caret"></b>
					</a>
					<div class="dropdown-menu dropdown-menu-right">
						<a href="<?php echo e(route('profile')); ?>" class="dropdown-item">View Profile</a>
						<div class="dropdown-divider"></div>
						<a href="javascript:;" class="dropdown-item">Log Out</a>
					</div>
				</li>
			</ul>
			<!-- end header navigation right -->
		</div>
		<!-- end #header -->

        <!-- begin #sidebar -->
		<div id="sidebar" class="sidebar">
			<!-- begin sidebar scrollbar -->
			<div data-scrollbar="true" data-height="100%">
				<!-- begin sidebar user -->
				<ul class="nav">
					<li class="nav-profile">
						<a href="javascript:;">
							<div class="with-shadow"></div>
							<div class="image">
								<img src="<?php echo e(asset('assets/backend/img/user/user-13.jpg')); ?>" alt="" />
							</div>
							<div class="info">
								Riarime Gabriel
								<small>OJT Coordinator</small>
							</div>
						</a>
					</li>
				</ul>
				<!-- end sidebar user -->
				<!-- begin sidebar nav -->
				<ul class="nav">
                    <li class="nav-header">Navigation</li>
                    <?php if($view == 'dashboard'): ?>
                        <li class="active">
                            <a href="javascript:;">
                                <i class="fa fa-th-large"></i>
                                <span>Dashboard</span>
                            </a>
                        </li>
                    <?php else: ?>
                        <li>
                            <?php if($user == 'coordinator'): ?>
                                <a href="<?php echo e(route('dashboard')); ?>">
                            <?php elseif($user == 'director'): ?>
                                <a href="<?php echo e(route('dashboard')); ?>">
                            <?php endif; ?>
                                <i class="fa fa-th-large"></i>
                                <span>Dashboard</span>
                            </a>
                        </li>
                    <?php endif; ?>
					<?php if($user == 'coordinator'): ?>
						<?php if($view == 'config'): ?>
							<li class="has-sub active">
								<a href="javascript:;">
									<i class="fas fa-cog fa-fw"></i>
									<span>Configuration</span>
								</a>
								<ul class="sub-menu">
									<?php if($sub == 'ay'): ?>
										<li class="active"><a href="javascript:;">Academic Year</a></li>
									<?php else: ?>
										<li><a href="<?php echo e(route('config.acadyr')); ?>">Academic Year</a></li>
									<?php endif; ?>
									<?php if($sub == 'sem'): ?>
										<li class="active"><a href="javascript:;">Semester</a></li>
									<?php else: ?>
										<li><a href="<?php echo e(route('config.sem')); ?>">Semester</a></li>
									<?php endif; ?>
									<?php if($sub == 'colleges'): ?>
										<li class="active"><a href="javascript:;">Colleges</a></li>
									<?php else: ?>
										<li><a href="<?php echo e(route('config.college')); ?>">Colleges</a></li>
									<?php endif; ?>
									<?php if($sub == 'course'): ?>
										<li class="active"><a href="javascript:;">College Courses</a></li>
									<?php else: ?>
										<li><a href="<?php echo e(route('config.course')); ?>">College Courses</a></li>
									<?php endif; ?>
									<?php if($sub == 'yrsec'): ?>
										<li class="active"><a href="javascript:;">Year & Section</a></li>
									<?php else: ?>
										<li><a href="<?php echo e(route('config.yearsec')); ?>">Year & Section</a></li>
									<?php endif; ?>
								</ul>
							</li>
						<?php else: ?>
							<li class="has-sub">
								<a href="javascript:;">
									<i class="fas fa-cog fa-fw"></i>
									<span>Configuration</span>
								</a>
								<ul class="sub-menu">
									<li><a href="<?php echo e(route('config.acadyr')); ?>">Academic Year</a></li>
									<li><a href="<?php echo e(route('config.sem')); ?>">Semester</a></li>
									<li><a href="<?php echo e(route('config.college')); ?>">Colleges</a></li>
									<li><a href="<?php echo e(route('config.course')); ?>">College Courses</a></li>
									<li><a href="<?php echo e(route('config.yearsec')); ?>">Year & Section</a></li>
								</ul>
							</li>
						<?php endif; ?>
                        <?php if($view == 'intern'): ?>
                            <li class="has-sub active">
                                <a href="javascript:;">
                                    <i class="fa fa-hdd"></i>
                                    <span>Interns</span>
                                </a>
                                <ul class="sub-menu">
                                    <?php if($sub == "assess"): ?>
                                        <li class="active"><a href="javascript:;">Registration</a></li>
                                    <?php else: ?>
                                        <li><a href="<?php echo e(route('intern.assessment')); ?>">Registration</a></li>
                                    <?php endif; ?>
                                    <?php if($sub == "list"): ?>
                                        <li class="active"><a href="javascript:;">Record List</a></li>
                                    <?php else: ?>
                                        <li><a href="<?php echo e(route('intern.list')); ?>">Record List</a></li>
                                    <?php endif; ?>
                                    <?php if($sub == "grade"): ?>
                                        <li class="active"><a href="javascript:;">Grade Assessment</a></li>
                                    <?php else: ?>
                                        <li><a href="<?php echo e(route('intern.grade')); ?>">Grade Assessment</a></li>
                                    <?php endif; ?>
                                </ul>
                            </li>
                        <?php else: ?>
                            <li class="has-sub">
                                <a href="javascript:;">
                                    <i class="fa fa-hdd"></i>
                                    <span>Interns</span>
                                </a>
                                <ul class="sub-menu">
                                    <li><a href="<?php echo e(route('intern.assessment')); ?>">Registration</a></li>
                                    <li><a href="<?php echo e(route('intern.list')); ?>">Record List</a></li>
                                    <li><a href="<?php echo e(route('intern.grade')); ?>">Grade Assessment</a></li>
                                </ul>
                            </li>
                        <?php endif; ?>
                        <li>
                            <a href="javascript:;">
                                <i class="fa fa-print"></i>
                                <span>Reports</span>
                            </a>
                        </li>
                    <?php endif; ?>
				</ul>
				<!-- end sidebar nav -->
			</div>
			<!-- end sidebar scrollbar -->
		</div>
		<div class="sidebar-bg"></div>
		<!-- end #sidebar -->

        <!-- begin #content -->
		<div id="content" class="content">
            <?php $__env->startSection('content'); ?>
            <?php echo $__env->yieldSection(); ?>
        </div>
		<!-- end #content -->
		
		<!-- end #content -->
		<!-- begin scroll to top btn -->
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
		<!-- end scroll to top btn -->
	</div>
	<!-- end page container -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo e(asset('assets/backend/plugins/jquery/jquery-3.2.1.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/backend/plugins/jquery-ui/jquery-ui.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/backend/plugins/bootstrap/4.0.0/js/bootstrap.bundle.min.js')); ?>"></script>
	<!--[if lt IE 9]>
		<script src="../assets/crossbrowserjs/html5shiv.js"></script>
		<script src="../assets/crossbrowserjs/respond.min.js"></script>
		<script src="../assets/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
	<script src="<?php echo e(asset('assets/backend/plugins/slimscroll/jquery.slimscroll.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/backend/plugins/js-cookie/js.cookie.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/backend/js/theme/default.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/backend/js/apps.min.js')); ?>"></script>
	<!-- ================== END BASE JS ================== -->
	
    <!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script>
		App.setPageOption({
			pageSidebarTransparent: true,
			clearOptionOnLeave: true
		});
		App.restartGlobalFunction();
	</script>
    <?php $__env->startSection('js'); ?>
    <?php echo $__env->yieldSection(); ?>
    <!-- ================== END PAGE LEVEL JS ================== -->
    
    <?php $__env->startSection('custom-js'); ?>
    <?php echo $__env->yieldSection(); ?>

</body>
</html>