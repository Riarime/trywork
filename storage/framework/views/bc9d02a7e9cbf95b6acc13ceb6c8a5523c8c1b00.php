<!DOCTYPE html>
<html >
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="generator" content="Mobirise v4.7.10, mobirise.com">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="shortcut icon" href="<?php echo e(asset('assets/faq/images/logo-light-2-122x122.png')); ?>" type="image/x-icon">
    <meta name="description" content="">
    <title>FAQ | TryWork</title>
    <link rel="stylesheet" href="<?php echo e(asset('assets/faq/web/assets/mobirise-icons/mobirise-icons.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/faq/ether/tether.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/faq/bootstrap/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/faq/bootstrap/css/bootstrap-grid.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/faq/bootstrap/css/bootstrap-reboot.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/faq/dropdown/css/style.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/faq/animatecss/animate.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/faq/socicon/css/styles.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/faq/theme/css/style.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/faq/mobirise/css/mbr-additional.css')); ?>" type="text/css">



</head>
<body>
<section class="menu cid-r7m6v3eb4v" once="menu" id="menu1-2">
    <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm bg-color transparent">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <div class="hamburger">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </button>
        <div class="menu-logo">
            <div class="navbar-brand">
                <span class="navbar-logo">
                    <a href="<?php echo e(route('home')); ?>">
                         <img src="<?php echo e(asset('assets/faq/images/logo-light-2-122x122.png')); ?>" alt="" title="" style="height: 3.8rem;">
                    </a>
                </span>
                <span class="navbar-caption-wrap"><a class="navbar-caption text-white display-4" href="<?php echo e(route('home')); ?>">
                        TryWork</a></span>
            </div>
        </div>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
        </div>
    </nav>
</section>
<section class="engine"><a href="">responsive site templates</a></section><section class="mbr-section content5 cid-r7m6zI8YGU mbr-parallax-background" id="content5-3" style="background-image: url('<?php echo e(asset('assets/faq/images/slider-1-cover-1920x1080.jpg')); ?>')">
    <div class="container">
        <div class="media-container-row">
            <div class="title col-12 col-md-8">
                <h2 class="align-center mbr-bold mbr-white pb-3 mbr-fonts-style display-1">
                    TryWork</h2>
                <h3 class="mbr-section-subtitle align-center mbr-light mbr-white pb-3 mbr-fonts-style display-5">Student Training. Gain Experience. Work Ready.</h3>
            </div>
        </div>
    </div>
</section>
<section class="toggle2 cid-r7m6SxQbyc" id="toggle2-6">
    <div class="container">
        <div class="media-container-row">
            <div class="toggle-content">
                <h2 class="mbr-section-title pb-3 align-left mbr-fonts-style display-2">Getting trouble <em>Logging in?</em></h2>
                <h3 class="mbr-section-subtitle align-left mbr-fonts-style display-5">Check below the most common question that interfere your account on logging into the system.</h3>
                <div id="bootstrap-toggle" class="toggle-panel accordionStyles tab-content pt-5 mt-2">
                    <div class="card">
                        <div class="card-header" role="tab" id="headingOne">
                            <a role="button" class="collapsed panel-title text-black" data-toggle="collapse" data-parent="#accordion" data-core="" href="#collapse1_6" aria-expanded="false" aria-controls="collapse1">
                                <h4 class="mbr-fonts-style display-5">
                                    <span class="sign mbr-iconfont mbri-arrow-down inactive"></span>I can't login my account.</h4>
                            </a>
                        </div>
                        <div id="collapse1_6" class="panel-collapse noScroll collapse" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body p-4">
                                <p class="mbr-fonts-style panel-text display-7">
                                    Try checking your credentials inputted on the login fields. Try turning off your CapsLock function.&nbsp;</p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" role="tab" id="headingTwo">
                            <a role="button" class="collapsed panel-title text-black" data-toggle="collapse" data-parent="#accordion" data-core="" href="#collapse2_6" aria-expanded="false" aria-controls="collapse2">
                                <h4 class="mbr-fonts-style display-5">
                                    <span class="sign mbr-iconfont mbri-arrow-down inactive"></span> I forgot my password.</h4>
                            </a>
                        </div>
                        <div id="collapse2_6" class="panel-collapse noScroll collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-body p-4">
                                <p class="mbr-fonts-style panel-text display-7">Click the forgot password button on the login form to proceed on your account recovery.</p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" role="tab" id="headingThree">
                            <a role="button" class="collapsed panel-title text-black" data-toggle="collapse" data-parent="#accordion" data-core="" href="#collapse3_6" aria-expanded="true" aria-controls="collapse3">
                                <h4 class="mbr-fonts-style display-5">
                                    <span class="sign mbr-iconfont mbri-arrow-down inactive"></span> My intern account didn't exist.</h4>
                            </a>
                        </div>
                        <div id="collapse3_6" class="panel-collapse noScroll collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div class="panel-body p-4">
                                <p class="mbr-fonts-style panel-text display-7">Intern accounts need to be confirmed by the Coordinator. You can apply for an application, but the coordinator must first confirm that you are ready for deployement for you to have the verefied account that you can use logging in.</p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" role="tab" id="headingThree">
                            <a role="button" class="collapsed panel-title  text-black" data-toggle="collapse" data-parent="#accordion" data-core="" href="#collapse4_6" aria-expanded="false" aria-controls="collapse4">
                                <h4 class="mbr-fonts-style display-5">
                                    <span class="sign mbr-iconfont mbri-arrow-down inactive"></span>&nbsp;I can't access the site.</h4>
                            </a>
                        </div>
                        <div id="collapse4_6" class="panel-collapse noScroll collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div class="panel-body p-4">
                                <p class="mbr-fonts-style panel-text display-7">
                                    Try clearing your browser cache, or your internet connection.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mbr-figure" style="width: 80%;">
                <img src="<?php echo e(asset('assets/faq/images/login-1-1176x666.jpg')); ?>" alt="" title="">
            </div>
        </div>
    </div>
</section>
<section class="mbr-section info1 cid-r7md57WOBk" id="info1-7">
    <div class="container">
        <div class="row justify-content-center content-row">
            <div class="media-container-column title col-12 col-lg-7 col-md-6">
                <h3 class="mbr-section-subtitle align-left mbr-light pb-3 mbr-fonts-style display-5">Found your solution to your problem</h3>
                <h2 class="align-left mbr-bold mbr-fonts-style display-2">
                    Try logging in your account again.</h2>
            </div>
            <div class="media-container-column col-12 col-lg-3 col-md-4">
                <div class="mbr-section-btn align-right py-4"><a class="btn btn-primary display-4" href="http://trywork.cf/user-portal/login">Login</a></div>
            </div>
        </div>
    </div>
</section>
<section class="toggle2 cid-r7mg5xB64B" id="toggle2-8">
    <div class="container">
        <div class="media-container-row">
            <div class="toggle-content">
                <h2 class="mbr-section-title pb-3 align-left mbr-fonts-style display-2">I don't have updates on my applications.</h2>
                <div id="bootstrap-toggle" class="toggle-panel accordionStyles tab-content pt-5 mt-2">
                    <div class="card">
                        <div class="card-header" role="tab" id="headingOne">
                            <a role="button" class="collapsed panel-title text-black" data-toggle="collapse" data-parent="#accordion" data-core="" href="#collapse1_8" aria-expanded="false" aria-controls="collapse1">
                                <h4 class="mbr-fonts-style display-5">
                                    <span class="sign mbr-iconfont mbri-arrow-down inactive"></span>Pending Applications</h4>
                            </a>
                        </div>
                        <div id="collapse1_8" class="panel-collapse noScroll collapse" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body p-4">
                                <p class="mbr-fonts-style panel-text display-7">If your applications&nbsp;</p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" role="tab" id="headingTwo">
                            <a role="button" class="collapsed panel-title text-black" data-toggle="collapse" data-parent="#accordion" data-core="" href="#collapse2_8" aria-expanded="false" aria-controls="collapse2">
                                <h4 class="mbr-fonts-style display-5">
                                    <span class="sign mbr-iconfont mbri-arrow-down inactive"></span>For Interview</h4>
                            </a>
                        </div>
                        <div id="collapse2_8" class="panel-collapse noScroll collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-body p-4">
                                <p class="mbr-fonts-style panel-text display-7"></p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" role="tab" id="headingThree">
                            <a role="button" class="collapsed panel-title text-black" data-toggle="collapse" data-parent="#accordion" data-core="" href="#collapse3_8" aria-expanded="true" aria-controls="collapse3">
                                <h4 class="mbr-fonts-style display-5">
                                    <span class="sign mbr-iconfont mbri-arrow-down inactive"></span>Passed Applications</h4>
                            </a>
                        </div>
                        <div id="collapse3_8" class="panel-collapse noScroll collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div class="panel-body p-4">
                                <p class="mbr-fonts-style panel-text display-7"></p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" role="tab" id="headingThree">
                            <a role="button" class="collapsed panel-title  text-black" data-toggle="collapse" data-parent="#accordion" data-core="" href="#collapse4_8" aria-expanded="false" aria-controls="collapse4">
                                <h4 class="mbr-fonts-style display-5">
                                    <span class="sign mbr-iconfont mbri-arrow-down inactive"></span>Not Suitable</h4>
                            </a>
                        </div>
                        <div id="collapse4_8" class="panel-collapse noScroll collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div class="panel-body p-4">
                                <p class="mbr-fonts-style panel-text display-7"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mbr-figure" style="width: 141%;">
                <img src="<?php echo e(asset('assets/faq/images/screenshot-from-2018-10-24-10-32-56-950x324.png')); ?>" alt="" title="">
            </div>
        </div>
    </div>
</section>
<section class="toggle2 cid-r7mjs70KF3" id="toggle2-9">
    <div class="container">
        <div class="media-container-row">
            <div class="toggle-content">
                <h2 class="mbr-section-title pb-3 align-left mbr-fonts-style display-2">
                    Changing my account details</h2>
                <div id="bootstrap-toggle" class="toggle-panel accordionStyles tab-content pt-5 mt-2">
                    <div class="card">
                        <div class="card-header" role="tab" id="headingOne">
                            <a role="button" class="collapsed panel-title text-black" data-toggle="collapse" data-parent="#accordion" data-core="" href="#collapse1_9" aria-expanded="false" aria-controls="collapse1">
                                <h4 class="mbr-fonts-style display-5">
                                    <span class="sign mbr-iconfont mbri-arrow-down inactive"></span>Can i change my account details?</h4>
                            </a>
                        </div>
                        <div id="collapse1_9" class="panel-collapse noScroll collapse" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body p-4">
                                <p class="mbr-fonts-style panel-text display-7">
                                    No, your coordinator can only update your account details. You need to notify your coordinator for the details change.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mbr-figure" style="width: 89%;">
                <img src="<?php echo e(asset('assets/faq/images/screenshot-from-2018-10-24-10-25-18-950x401.png')); ?>" alt="" title="">
            </div>
        </div>
    </div>
</section>
<section class="footer4 cid-r7mmO9CRPf" id="footer4-b">
    <div class="container">
        <div class="media-container-row content mbr-white">
            <div class="col-md-3 col-sm-4">
                <div class="mb-3 img-logo">
                    <a href="">
                        <img src="<?php echo e(asset('assets/faq/images/logo-dark-192x192.png')); ?>" alt="" title="">
                    </a>
                </div>
                <p class="mb-3 mbr-fonts-style foot-title display-7">
                    TryWork</p>
                <p class="mbr-text mbr-fonts-style mbr-links-column display-7"></p>
            </div>
            <div class="col-md-4 col-sm-8">
                <p class="mb-4 foot-title mbr-fonts-style display-7">
                    ABOUT US
                </p>
                <p class="mbr-text mbr-fonts-style foot-text display-7">Trywork: Internship Recommender Serivces System is a website made for Interns, Host Training Establishments, OJT Directors, Heads, and Coordinators. This benefits them by making their job easily, by making the system to help them with suggesting ideal interns/companies.
                    <br>
                    <br>This site is under maintenance by SRG 7th Generation</p>
            </div>
            <div class="col-md-4 offset-md-1 col-sm-12">
                <p class="mb-4 foot-title mbr-fonts-style display-7">OUR CONTACT
                    <br></p>
                <p class="mbr-text mbr-fonts-style form-text display-7">TryWork
                    <br>#123 Address
                    <br>City, Province, Zip Code
                    <br>
                    <br>Phone: (123) 456-7890
                    <br>Fax: (123) 456-7891
                    <br>Email: admin@trywork.com
                    <br>Skype: trywork</p>
                <div class="media-container-column" data-form-type="formoid">
                    <div data-form-alert="" hidden="" class="align-center">You'll gonna hear updates from us, everynow and then.,</div>

                    <form class="form-inline" action="" method="post" data-form-title="">
                        <input type="hidden" value="5Zheb+p7W9KOpJJpcYECouv8q7XGlVmNHKCW9lT2lWyQft8DEa47xxxT1snWqgMFAeH9/rIFLiZo4xZrV2yFiDt6AwczA3Lg1xsb/2/rpndCLdb4PAV8V4tynAQeIkz9" data-form-email="true">
                        <div class="form-group">
                            <input type="email" class="form-control input-sm input-inverse my-2" name="email" required="" data-form-field="Email" placeholder="Email" id="email-footer4-b">
                        </div>
                        <div class="input-group-btn m-2"><button href="" class="btn btn-success-outline display-4" type="submit" role="button">Subscribe</button></div>
                    </form>
                </div>
                <p class="mb-4 mbr-fonts-style foot-title display-7">
                    CONNECT WITH US
                </p>
                <div class="social-list pl-0 mb-0">
                    <div class="soc-item">
                        <a href="" target="_blank">
                            <span class="mbr-iconfont mbr-iconfont-social socicon-skype socicon"></span>
                        </a>
                    </div>
                    <div class="soc-item">
                        <a href="" target="_blank">
                            <span class="socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span>
                        </a>
                    </div>
                    <div class="soc-item">
                        <a href="" target="_blank">
                            <span class="socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
                        </a>
                    </div>
                    <div class="soc-item">
                        <a href="" target="_blank">
                            <span class="socicon-instagram socicon mbr-iconfont mbr-iconfont-social"></span>
                        </a>
                    </div>
                    <div class="soc-item">
                        <a href="" target="_blank">
                            <span class="socicon-googleplus socicon mbr-iconfont mbr-iconfont-social"></span>
                        </a>
                    </div>
                    <div class="soc-item">
                        <a href="" target="_blank">
                            <span class="socicon-behance socicon mbr-iconfont mbr-iconfont-social"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-lower">
            <div class="media-container-row">
                <div class="col-sm-12">
                    <hr>
                </div>
            </div>
            <div class="media-container-row mbr-white">
                <div class="col-sm-12 copyright">
                    <p class="mbr-text mbr-fonts-style display-7">
                        Copyright © 2018 SRG 7th Generation. All rights reserved.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="<?php echo e(asset('assets/faq/web/assets/jquery/jquery.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/faq/popper/popper.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/faq/tether/tether.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/faq/bootstrap/js/bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/faq/smoothscroll/smooth-scroll.js')); ?>"></script>
<script src="<?php echo e(asset('assets/faq/dropdown/js/script.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/faq/touchswipe/jquery.touch-swipe.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/faq/viewportchecker/jquery.viewportchecker.js')); ?>"></script>
<script src="<?php echo e(asset('assets/faq/parallax/jarallax.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/faq/mbr-switch-arrow/mbr-switch-arrow.js')); ?>"></script>
<script src="<?php echo e(asset('assets/faq/theme/js/script.js')); ?>"></script>
<script src="<?php echo e(asset('assets/faq/formoid/formoid.min.js')); ?>"></script>
<div id="scrollToTop" class="scrollToTop mbr-arrow-up"><a style="text-align: center;"><i></i></a></div>
<input name="animation" type="hidden">
</body>
</html>