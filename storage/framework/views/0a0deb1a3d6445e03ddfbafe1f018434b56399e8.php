<?php $__env->startSection('title', 'Dashboard'); ?>

<?php $__env->startSection('base-js'); ?>
    <script type="text/javascript" src="<?php echo e(asset('assets/backend/js/highcharts/highcharts.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/backend/js/highcharts/data.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/backend/js/highcharts/drilldown.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <h1 class="page-header">Dashboard | Host Training Establishment</h1>
    <div class="row">
        <div class="col-lg-4 col-md-6">
            <div class="widget widget-stats bg-blue">
                <div class="stats-icon"><i class="fa fa-users"></i></div>
                <div class="stats-info height-60">
                    <p><?php echo e($deployed); ?></p>
                </div>
                <div class="stats-link">
                    <a href="javascript:;">INTERNS CURRENTLY ON JOB TRAINING</a>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="widget widget-stats bg-gradient-green">
                <div class="stats-icon"><i class="fa fa-building"></i></div>
                <div class="stats-info height-60">
                    <p><?php echo e($partnership); ?></p>
                </div>
                <div class="stats-link">
                    <a href="javascript:;">STATE UNIVERSITY/COLLEGE PARTNERSHIPS</a>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-info">
        <div class="panel-heading">
            <h4 class="panel-title">Company Internship History</h4>
        </div>
        <div class="panel-body">
            <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-js'); ?>
<script>
    Highcharts.chart('container', {
        chart: {
            type: 'area'
        },
        title: {
            text: 'Company Internship History'
        },
        subtitle: {
            text: 'Source: <a href="http://trywork.cf/">' +
            'trywork.cf</a>'
        },
        xAxis: {
            allowDecimals: false,
            labels: {
                formatter: function () {
                    return this.value; // clean, unformatted number for year
                }
            }
        },
        yAxis: {
            title: {
                text: 'Number of Student Interns'
            },
            labels: {
//                formatter: function () {
//                    return this.value / 1000 + 'k';
//                }
            }
        },
        tooltip: {
            pointFormat: '{series.name} peaked at <b>{point.y:,.0f}</b><br/> in {point.x}'
        },
        plotOptions: {
            area: {
                pointStart: 2010,
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
        series: [{
            name: 'Student Interns',
            data: [
                100, 83, 92, 105, 120,
                95, 60, 180, 150
            ]
        }]
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master-backend-hte', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>