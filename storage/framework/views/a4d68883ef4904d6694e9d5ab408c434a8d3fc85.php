<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <title>Color Admin | Wizards + Validation</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    
    <link href="<?php echo e(asset('assets/backend/plugins/jquery-ui/jquery-ui.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/plugins/bootstrap/4.0.0/css/bootstrap.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/plugins/font-awesome/5.0/css/fontawesome-all.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/plugins/animate/animate.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/css/default/style.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/css/default/style-responsive.min.css')); ?>" rel="stylesheet" />
    <!-- ================== END BASE CSS STYLE ================== -->

    <!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
    <link href="<?php echo e(asset('assets/backend/plugins/jquery-smart-wizard/src/css/smart_wizard.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/plugins/parsley/src/parsley.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/plugins/gritter/css/jquery.gritter.css')); ?>" rel="stylesheet" />
    <!-- ================== END PAGE LEVEL STYLE ================== -->

    <!-- ================== BEGIN BASE JS ================== -->
    <script src="<?php echo e(asset('assets/backend/plugins/pace/pace.min.js')); ?>"></script>
    <!-- ================== END BASE JS ================== -->
</head>
<body>
<!-- begin #page-loader -->
<div id="page-loader" class="fade show"><span class="spinner"></span></div>
<!-- end #page-loader -->

<!-- begin #page-container -->
<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
    <!-- begin #header -->
    <div id="header" class="header navbar-default">
        <!-- begin navbar-header -->
        <div class="navbar-header">
            <a href="" class="navbar-brand">
                <img src="<?php echo e(asset('assets/images/logo-light.png')); ?>" alt="" width="50" height="50" style="margin-top: -10px;"> <b>T</b><small><b>RY</b></small><b>W</b><small><b>ORK</b></small>
            </a>
        </div>
        <!-- end navbar-header -->
    </div>
    <!-- end #header -->

    <!-- begin #content -->
    <div style="padding: 20px 30px">
        <div class="row">
            <div class="col-lg-12">
                <!-- begin page-header -->
                <h1 class="page-header">Registration</h1>
                <!-- end page-header -->

                <div class="card card-outline-primary">
                    <div class="row">
                        <div class="col-md-12">

                            <!-- begin wizard-form -->
                            <form action="<?php echo e(route('register')); ?>" method="POST" name="form-wizard" class="form-control-with-bg">
                                <?php echo Form::token();; ?>

                                <!-- begin wizard -->
                                <div id="wizard">
                                    <!-- begin wizard-step -->
                                    <ul>
                                        <li class="col-md-2 col-sm-4 col-6">
                                            <a href="#step-1">
                                                <span class="number">1</span>
                                                <span class="info text-ellipsis">
                                                    Profile Information
                                                    <small class="text-ellipsis">Register Your Profile</small>
                                                </span>
                                            </a>
                                        </li>
                                        <li class="col-md-3 col-sm-4 col-6">
                                            <a href="#step-2">
                                                <span class="number">2</span>
                                                <span class="info text-ellipsis">
                                                    Educational Background
                                                    <small class="text-ellipsis">Educational Background is required</small>
								                </span>
                                            </a>
                                        </li>
                                        <li class="col-md-3 col-sm-4 col-6">
                                            <a href="#step-3">
                                                <span class="number">3</span>
                                                <span class="info text-ellipsis">
                                                    Attended Seminars and Trainings
                                                    <small class="text-ellipsis">Seminars and Trainings attended for last 2 years</small>
								                </span>
                                            </a>
                                        </li>
                                        <li class="col-md-2 col-sm-4 col-6">
                                            <a href="#step-4">
                                                <span class="number">4</span>
                                                <span class="info text-ellipsis">
                                                    Login Account
                                                    <small class="text-ellipsis">Enter your username and password</small>
								                </span>
                                            </a>
                                        </li>
                                        <li class="col-md-2 col-sm-4 col-6">
                                            <a href="#step-5">
                                                <span class="number">5</span>
                                                <span class="info text-ellipsis">
                                                    Completed
                                                    <small class="text-ellipsis">Complete Registration</small>
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                    <!-- end wizard-step -->
                                    <!-- begin wizard-content -->
                                    <div>
                                        <!-- begin step-1 -->
                                        <div id="step-1">
                                            <!-- begin fieldset -->
                                            <fieldset>
                                                <!-- begin row -->
                                                <div class="row">
                                                    <!-- begin col-8 -->
                                                    
                                                    <div class="col-md-6"> <!-- class="col-md-8 offset-md-2" -->
                                                        <!-- begin form-group -->
                                                        <div class="form-group row m-b-10">
                                                            <label class="col-md-3 col-form-label text-md-right">First Name <span class="text-danger">*</span></label>
                                                            <div class="col-md-9">
                                                                <input autofocus type="text" name="firstname" placeholder="First Name" data-parsley-group="step-1"  class="form-control" />
                                                            </div>
                                                        </div>
                                                        <!-- end form-group -->
                                                        <!-- begin form-group -->
                                                        <div class="form-group row m-b-10">
                                                            <label class="col-md-3 col-form-label text-md-right">Middle Name <span class="text-danger">*</span></label>
                                                            <div class="col-md-9">
                                                                <input type="text" name="middlename" placeholder="Middle Name" data-parsley-group="step-1" class="form-control" />
                                                            </div>
                                                        </div>
                                                        <!-- end form-group -->
                                                        <!-- begin form-group -->
                                                        <div class="form-group row m-b-10">
                                                            <label class="col-md-3 col-form-label text-md-right">Last Name <span class="text-danger">*</span></label>
                                                            <div class="col-md-9">
                                                                <input type="text" name="lastname" placeholder="Last Name" data-parsley-group="step-1" class="form-control" />
                                                            </div>
                                                        </div>
                                                        <!-- end form-group -->
                                                        <!-- begin form-group -->
                                                        <div class="form-group row m-b-10">
                                                            <label class="col-md-3 col-form-label text-md-right">Date of Birth <span class="text-danger">*</span></label>
                                                            <div class="col-md-9">
                                                                <input type="date" name="birthdate" data-parsley-group="step-1" class="form-control">
                                                                
                                                            </div>
                                                        </div>
                                                        <!-- end form-group -->
                                                        <!-- begin form-group -->
                                                        <div class="form-group row m-b-10">
                                                            <label class="col-md-3 col-form-label text-md-right">Phone Number <span class="text-danger">&nbsp</span></label>
                                                            <div class="col-md-9">
                                                                <input type="number" name="phone" placeholder="Phone Number" data-parsley-group="step-1" data-parsley-type="number" class="form-control" />
                                                            </div>
                                                        </div>
                                                        <!-- end form-group -->
                                                        <!-- begin form-group -->
                                                        <div class="form-group row m-b-10">
                                                            <label class="col-md-3 col-form-label text-md-right">Email Address <span class="text-danger">*</span></label>
                                                            <div class="col-md-9">
                                                                <input type="email" name="email" placeholder="Email Address" class="form-control" data-parsley-group="step-1"  data-parsley-type="email" />
                                                            </div>
                                                        </div>
                                                        <!-- end form-group -->
                                                    </div>
                                                    <!-- end col-6 -->
                                                    <!-- begin col-6 -->
                                                    <div class="col-md-6">
                                                        <!-- begin form-group -->
                                                        <div class="form-group row m-b-10">
                                                            <label class="col-md-3 col-form-label text-md-right">Designation <span class="text-danger">*</span></label>
                                                            <div class="col-md-9">
                                                                <div class="col-9">
                                                                    <input type="text" name="designation" placeholder="Designation" class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- begin form-group -->
                                                        <div class="form-group row m-b-10">
                                                            <label class="col-md-3 col-form-label text-md-right">Academic Rank <span class="text-danger">*</span></label>
                                                            <div class="col-md-9">
                                                                <div class="col-9">
                                                                    <select class="form-control" name="academic-rank" data-parsley-group="step-1" >
                                                                        <option>-- Academic Rank --</option>
                                                                        <option>Instructor I</option>
                                                                        <option>Instructor II</option>
                                                                        <option>Instructor III</option>
                                                                        <option>Assistant Professor I</option>
                                                                        <option>Assistant Professor II</option>
                                                                        <option>Assistant Professor III</option>
                                                                        <option>Assistant Professor IV</option>
                                                                        <option>Associate Professor I</option>
                                                                        <option>Associate Professor II</option>
                                                                        <option>Associate Professor III</option>
                                                                        <option>Associate Professor IV</option>
                                                                        <option>Associate Professor V</option>
                                                                        <option>Professor I</option>
                                                                        <option>Professor II</option>
                                                                        <option>Professor III</option>
                                                                        <option>Professor IV</option>
                                                                        <option>Professor V</option>
                                                                        <option>Professor VI</option>
                                                                        <option>College/University Professor</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- end form-group -->
                                                        <!-- begin form-group -->
                                                        <div class="form-group row m-b-10">
                                                            <label class="col-md-3 col-form-label text-md-right">Employment Status <span class="text-danger">*</span></label>
                                                            <div class="col-md-9">
                                                                <div class="col-9">
                                                                    <select class="form-control" name="employment-stat" data-parsley-group="step-1" >
                                                                        <option>-- Employment Status --</option>
                                                                        <option value="emp-contractual">Contractual</option>
                                                                        <option value="emp-temporary">Temporary</option>
                                                                        <option value="emp-permanent">Permanent</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- end form-group -->
                                                        <!-- begin form-group -->
                                                        <div class="form-group row m-b-10">
                                                            <label class="col-md-3 col-form-label text-md-right">State University <span class="text-danger">*</span></label>
                                                            <div class="col-md-9">
                                                                <div class="col-9">
                                                                    <select class="form-control" name="university" data-parsley-group="step-1" >
                                                                        <option>-- State University --</option>
                                                                        
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- end form-group -->
                                                        <!-- begin form-group -->
                                                        <div class="form-group row m-b-10">
                                                            <label class="col-md-3 col-form-label text-md-right">Campus/Branch Name <span class="text-danger">*</span></label>
                                                            <div class="col-md-9">
                                                                <div class="col-9">
                                                                    <input type="text" name="branch-name" placeholder="Campus/Branch Name" data-parsley-group="step-1"  class="form-control" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- begin form-group -->
                                                        <div class="form-group row m-b-10">
                                                            <label class="col-md-3 col-form-label text-md-right">College <span class="text-danger">*</span></label>
                                                            <div class="col-md-9">
                                                                <div class="col-9">
                                                                    <input type="text" name="college" placeholder="College" data-parsley-group="step-1"  class="form-control" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- end form-group -->
                                                    </div>
                                                    <!-- end col-6 -->
                                                </div>
                                                <!-- end row -->
                                            </fieldset>
                                            <!-- end fieldset -->
                                        </div>
                                        <!-- end step-1 -->
                                        <!-- begin step-2 -->
                                        <div id="step-2">
                                            <!-- begin fieldset -->
                                            <fieldset>
                                                <!-- begin row -->
                                                <div class="row">
                                                    <!-- begin col-8 -->

                                                    <div class="col-md-8 offset-md-2">
                                                        <legend class="no-border f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse">Your Educational Background Information</legend>
                                                        <!-- begin form-group -->
                                                        <div class="form-group row m-b-10">
                                                            <label class="col-md-3 col-form-label text-md-right">Degree <span class="text-danger">*</span></label>
                                                            <div class="col-md-6">
                                                                <select name="degree" class="form-control">
                                                                    <option>Bachelor's Degree</option>
                                                                    <option>Master's Degree</option>
                                                                    <option>Doctorate Degree</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <!-- end form-group -->
                                                        <!-- begin form-group -->
                                                        <div class="form-group row m-b-10">
                                                            <label class="col-md-3 col-form-label text-md-right">School <span class="text-danger">*</span></label>
                                                            <div class="col-md-6">
                                                                <input type="text" name="graduated-school" placeholder="School Graduated" class="form-control" data-parsley-group="step-3" />
                                                            </div>
                                                        </div>
                                                        <!-- end form-group -->
                                                        <!-- begin form-group -->
                                                        <div class="form-group row m-b-10">
                                                            <label class="col-md-3 col-form-label text-md-right">Year Graduated <span class="text-danger">*</span></label>
                                                            <div class="col-md-6">
                                                                <input type="text" name="graduated-year" placeholder="Year Graduated" class="form-control" data-parsley-group="step-3"  />
                                                            </div>
                                                        </div>
                                                        <!-- end form-group -->
                                                        <!-- begin form-group -->
                                                        <div class="form-group row m-b-10">
                                                            <label class="col-md-3 col-form-label text-md-right">Received Awards <span class="text-danger">*</span></label>
                                                            <div class="col-md-6">
                                                                <input type="text" name="awards" placeholder="Received Awards" class="form-control" data-parsley-group="step-3"  />
                                                            </div>
                                                        </div>
                                                        <!-- end form-group -->
                                                    </div>
                                                    <!-- end col-8 -->
                                                </div>
                                                <!-- end row -->
                                            </fieldset>
                                            <!-- end fieldset -->
                                        </div>
                                        <!-- end step-2 -->
                                        <!-- begin step-3 -->
                                        <div id="step-3">
                                            <!-- begin fieldset -->
                                            <fieldset>
                                                <!-- begin row -->
                                                <div class="row">
                                                    <!-- begin col-8 -->

                                                    <div class="col-md-8 offset-md-2">
                                                    <legend class="no-border f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse">Provide the Seminars and Trainings that you attended for the last 2 years</legend>
                                                        <!-- begin form-group -->
                                                        <div class="form-group row m-b-10">
                                                            <label class="col-md-3 col-form-label text-md-right">Title <span class="text-danger">*</span></label>
                                                            <div class="col-md-6">
                                                                <input type="text" name="title" placeholder="Seminar Title" class="form-control" data-parsley-group="step-3" />
                                                            </div>
                                                        </div>
                                                        <!-- end form-group -->
                                                        <!-- begin form-group -->
                                                        <div class="form-group row m-b-10">
                                                            <label class="col-md-3 col-form-label text-md-right">Sponsoring Agency <span class="text-danger">*</span></label>
                                                            <div class="col-md-6">
                                                                <input type="text" name="agency-sponsor" placeholder="Sponsoring Agency" class="form-control" data-parsley-group="step-3" />
                                                            </div>
                                                        </div>
                                                        <!-- end form-group -->
                                                        <!-- begin form-group -->
                                                        <div class="form-group row m-b-10">
                                                            <label class="col-md-3 col-form-label text-md-right">Level of the Seminar <span class="text-danger">*</span></label>
                                                            <div class="col-md-6">
                                                                <select class="form-control" name="seminar-lvl" data-parsley-group="step-3">
                                                                    <option>--Level--</option>
                                                                    <option value="lvl-international">International</option>
                                                                    <option value="lvl-regional">Regional</option>
                                                                    <option value="lvl-local">Local</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <!-- end form-group -->
                                                        <!-- begin form-group -->
                                                        <div class="form-group row m-b-10">
                                                            <label class="col-md-3 col-form-label text-md-right">Date <span class="text-danger">*</span></label>
                                                            <div class="col-md-6">
                                                                <input type="date" name="seminar-date" placeholder="Seminar Title" class="form-control" data-parsley-group="step-3" data-parsley-type="alphanum" />
                                                            </div>
                                                        </div>
                                                        <!-- end form-group -->
                                                        <!-- begin form-group -->
                                                        <div class="form-group row m-b-10">
                                                            <label class="col-md-3 col-form-label text-md-right">Venue <span class="text-danger">*</span></label>
                                                            <div class="col-md-6">
                                                                <input type="text" name="venue" placeholder="Seminar Title" class="form-control" data-parsley-group="step-3" data-parsley-type="alphanum" />
                                                            </div>
                                                        </div>
                                                        <!-- end form-group -->
                                                    </div>
                                                    <!-- end col-8 -->
                                                </div>
                                                <!-- end row -->
                                            </fieldset>
                                            <!-- end fieldset -->
                                        </div>
                                        <!-- end step-3 -->
                                        <!-- begin step-4 -->
                                        <div id="step-4">
                                            <!-- begin fieldset -->
                                            <fieldset>
                                                <!-- begin row -->
                                                <div class="row">
                                                    <!-- begin col-8 -->
                                                    <div class="col-md-8 offset-md-2">
                                                        <legend class="no-border f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse">Select your login username and password</legend>
                                                        <!-- begin form-group -->
                                                        <div class="form-group row m-b-10">
                                                            <label class="col-md-3 col-form-label text-md-right">Username <span class="text-danger">*</span></label>
                                                            <div class="col-md-6">
                                                                <input type="text" name="username" placeholder="Username" class="form-control" data-parsley-group="step-4" data-parsley-type="alphanum" />
                                                            </div>
                                                        </div>
                                                        <!-- end form-group -->
                                                        <!-- begin form-group -->
                                                        <div class="form-group row m-b-10">
                                                            <label class="col-md-3 col-form-label text-md-right">Pasword <span class="text-danger">*</span></label>
                                                            <div class="col-md-6">
                                                                <input type="password" name="password" placeholder="Password" class="form-control" data-parsley-group="step-4" />
                                                            </div>
                                                        </div>
                                                        <!-- end form-group -->
                                                        <!-- begin form-group -->
                                                        <div class="form-group row m-b-10">
                                                            <label class="col-md-3 col-form-label text-md-right">Confirm Pasword <span class="text-danger">*</span></label>
                                                            <div class="col-md-6">
                                                                <input type="password" name="password2" placeholder="Confirmed Password" class="form-control" data-parsley-group="step-4" />
                                                            </div>
                                                        </div>
                                                        <!-- end form-group -->
                                                    </div>
                                                    <!-- end col-8 -->
                                                </div>
                                                <!-- end row -->
                                            </fieldset>
                                            <!-- end fieldset -->
                                        </div>
                                        <!-- end step-3 -->
                                        <!-- begin step-4 -->
                                        <div id="step-5">
                                            <fieldset>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="jumbotron m-b-0 text-center">
                                                            <h2 class="text-inverse">Information Fillup Completed</h2>
                                                            <p class="f-s-16">
                                                                I hereby certify that the information provided here is correct to the best of my knowledge.
                                                            </p>
                                                            <p><a href="#modal-dialog" data-toggle="modal" class="btn btn-primary btn-lg">Proceed Registration</a></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                        <!-- end step-4 -->
                                    </div>
                                    <!-- end wizard-content -->
                                </div>
                                <!-- end wizard -->
                                <!-- #modal-dialog -->
                                <div class="modal fade" id="modal-dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Registration</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body">
                                                <p>
                                                    Submit Information?
                                                </p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary" data-dissmiss="modal">Submit</button>
                                                <a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- end wizard-form -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end #content -->

    <!-- begin scroll to top btn -->
    <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
    <!-- end scroll to top btn -->
</div>
<!-- end page container -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="<?php echo e(asset('assets/backend/plugins/jquery/jquery-3.2.1.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/backend/plugins/jquery-ui/jquery-ui.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/backend/plugins/bootstrap/4.0.0/js/bootstrap.bundle.min.js')); ?>"></script>
<!--[if lt IE 9]>
<script src="../assets/crossbrowserjs/html5shiv.js"></script>
<script src="../assets/crossbrowserjs/respond.min.js"></script>
<script src="../assets/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo e(asset('assets/backend/plugins/slimscroll/jquery.slimscroll.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/backend/plugins/js-cookie/js.cookie.js')); ?>"></script>
<script src="<?php echo e(asset('assets/backend/js/theme/default.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/backend/js/apps.min.js')); ?>"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="<?php echo e(asset('assets/backend/plugins/parsley/dist/parsley.js')); ?>"></script>
<script src="<?php echo e(asset('assets/backend/plugins/jquery-smart-wizard/src/js/jquery.smartWizard.js')); ?>"></script>
<script src="<?php echo e(asset('assets/backend/js/demo/form-wizards-validation.demo.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/backend/plugins/gritter/js/jquery.gritter.js')); ?>"></script>
<script src="<?php echo e(asset('assets/backend/js/demo/ui-modal-notification.demo.min.js')); ?>"></script>
<!-- ================== END PAGE LEVEL JS ================== -->

<script>
    $(document).ready(function() {
        App.init();
        FormWizardValidation.init();
        Notification.init();
    });
</script>
</body>
</html>
