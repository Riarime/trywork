<?php $__env->startSection('title', 'Dashboard'); ?>

<?php $__env->startSection('base-js'); ?>
	<script type="text/javascript" src="<?php echo e(asset('assets/backend/js/highcharts/highcharts.js')); ?>"></script>
	<script type="text/javascript" src="<?php echo e(asset('assets/backend/js/highcharts/data.js')); ?>"></script>
	<script type="text/javascript" src="<?php echo e(asset('assets/backend/js/highcharts/drilldown.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <!-- begin breadcrumb -->
    <div class="row">
        <ol class="breadcrumb pull-left">
            <li class="breadcrumb-item"><a href="javascript:;"><i class="fa fa-home"></i>&nbsp&nbspHome</a></li>
            <li class="breadcrumb-item active"><i class="fa fa-th-large"></i>&nbsp&nbspDashboard</li>
        </ol>
    </div><br>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <!-- <h1 class="page-header">Dashboard</h1> -->
    <!-- end page-header -->
    
    <!-- begin panel -->
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">Number of Interns</h4>
        </div>
        <div class="panel-body">
            <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        </div>
    </div>
    <!-- end panel -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-js'); ?>
<script>
    $(document).ready(function() {
        App.init();
        TableManageDefault.init();
    });
</script>

<script type="text/javascript">
		
    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Total percent market share'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
        },
        series: [{
            name: 'Colleges',
            colorByPoint: true,
            data: [{
                name: 'College of Business Administration',
                y: 172,
                drilldown: 'QC-CBA'
            }, {
                name: 'College of Computer Information Systems',
                y: 48,
                drilldown: 'QC-CCIS'
            }, {
                name: 'College of Education',
                y: 87,
                drilldown: 'QC-COED'
            }, {
                name: 'Institute of Technology',
                y: 44,
                drilldown: 'QC-ITECH'
            }]
        }],
        drilldown: {
            series: [
            // 1st Level of Drilldown - Courses
            {
                name: 'QC Branch Courses',
                id: 'QC-CCIS',
                data: [[
                    // 'Bachelor of Science in Information Technology',
                    'BSIT',
                    48
                ]]
            }, {
                name: 'QC Branch Courses',
                id: 'QC-CBA',
                data: [[
                    // 'Bachelor of Science in Business Administration Major in Marketing Management',
                    'BSBA-MM',
                    45
                ], [
                    // 'Bachelor of Science in Business Administration Major in Human Resources Development Management',
                    'BSBA-HRDM',
                    84
                ], [
                    // 'Bachelor of Science in Entrepreneurship',
                    'BSEntrep',
                    43
                ]]
            }, {
                name: 'QC Branch Courses',
                id: 'QC-COED',
                data: [[
                    // 'Bachelor in Business Teacher Education',
                    'BBTE',
                    87
                ]]
            }, {
                name: 'QC Branch Courses',
                id: 'QC-ITECH',
                data: [[
                    // 'Diploma in Information Communication Technology',
                    'DICT',
                    44
                ]]
            }]
        }
    })

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.coordinator', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>