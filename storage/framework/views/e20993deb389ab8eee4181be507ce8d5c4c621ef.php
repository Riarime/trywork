<?php $__env->startSection('title', 'HTE Finder'); ?>

<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(asset('assets/backend/plugins/jquery-jvectormap/jquery-jvectormap.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/plugins/bootstrap-calendar/css/bootstrap_calendar.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/plugins/gritter/css/jquery.gritter.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/plugins/nvd3/build/nv.d3.css')); ?>" rel="stylesheet" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.2/d3.min.js"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/nvd3/build/nv.d3.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/jquery-jvectormap/jquery-jvectormap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/jquery-jvectormap/jquery-jvectormap-world-merc-en.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/bootstrap-calendar/js/bootstrap_calendar.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/gritter/js/jquery.gritter.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/js/demo/dashboard-v2.min.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('document.ready'); ?>
    handleVisitorsAreaChart();
    handleVisitorsDonutChart();
    handleVisitorsVectorMap();
    handleScheduleCalendar();

    
    
    
    
    
    
    
    
    
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <!-- begin col-6 -->
        <div class="col-md-12">
            <div>
                <h4>HTE Finder</h4>
            </div>
            <div class="m-b-10 f-s-10 m-t-5"><b class="text-inverse">List of Suggested Host Training Establishments for you:</b></div>
            <div class="row">
                <?php if(isset($final_recommendation)): ?>
                <?php $__currentLoopData = $ojts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ojt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php $__currentLoopData = $final_recommendation; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($item[0] == $ojt->description): ?>
                            <div class="col-md-4">
                                <!-- begin card -->
                                <div class="card">
                                    <div class="card-block">
                                        <h4 class="card-title"><?php echo e($ojt->ojtname); ?><br/><small><?php echo e($ojt->company_name); ?></small></h4>
                                        <p class="card-text"><?php echo e($ojt->description); ?></p>
                                        <button class="btn btn-primary">Details</button>
                                        <p class="card-text" style="text-align: right"><small class="text-muted">Start Date: <?php echo e($ojt->date); ?></small></p>
                                    </div>
                                </div>
                                <!-- end card -->
                            </div>
                            <?php break; ?>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>
                    <?php echo e('There are currently no OJTs that we can suggest to you at the moment, please try again some other time.'); ?>

                    <?php endif; ?>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master-backend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>