<?php $__env->startSection('title', 'Dashboard'); ?>

<?php $__env->startSection('base-js'); ?>
	<script type="text/javascript" src="<?php echo e(asset('assets/backend/js/highcharts/highcharts.js')); ?>"></script>
	<script type="text/javascript" src="<?php echo e(asset('assets/backend/js/highcharts/data.js')); ?>"></script>
	<script type="text/javascript" src="<?php echo e(asset('assets/backend/js/highcharts/drilldown.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <!-- begin breadcrumb -->
    <div class="row">
        <ol class="breadcrumb pull-left">
            <li class="breadcrumb-item"><a href="javascript:;"><i class="fa fa-home"></i>&nbsp&nbspHome</a></li>
            <li class="breadcrumb-item active"><i class="fa fa-th-large"></i>&nbsp&nbspDashboard</li>
        </ol>
    </div><br>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">A.Y. <?php echo e($active->academic_yr.' ('.$active->semester.')'); ?></h1>
    <!-- end page-header -->
    
    <div class="row">
        <!-- begin col-3 -->
        <div class="col-lg-4 col-md-6">
            <div class="widget widget-stats bg-blue">
                <div class="stats-icon"><i class="fa fa-users"></i></div>
                <div class="stats-info height-60">
                    <p><?php echo e($intern_total); ?></p>
                </div>
                <div class="stats-link">
                    <a href="javascript:;">NUMBER OF STUDENT TRAINEE</a>
                    
                </div>
            </div>
        </div>
        <!-- end col-3 -->
        <!-- begin col-3 -->
        <div class="col-lg-4 col-md-6">
            <div class="widget widget-stats bg-gradient-green">
                <div class="stats-icon"><i class="fa fa-building"></i></div>
                <div class="stats-info height-60">
                    <p><?php echo e($hte_total); ?></p>
                </div>
                <div class="stats-link">
                    <a href="javascript:;">NUMBER OF HOST TRAINING ESTABLISHMENT</a>
                    
                </div>
            </div>
        </div>
        <!-- end col-3 -->
        <!-- begin col-3 -->
        
            
                
                
                    
                
                
                    
                     
                
            
        
        <!-- end col-3 -->
    </div>
    
    
    <div class="panel panel-info">
        <div class="panel-heading">
            <h4 class="panel-title">Student Trainee Comparison Rate</h4>
        </div>
        <div class="panel-body">
            <div id="intern-comparison-rate" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        </div>
    </div>
    
    <!-- begin panel -->
    <div class="panel panel-info">
        <div class="panel-heading">
            <h4 class="panel-title">Student Trainee</h4>
        </div>
        <div class="panel-body">
            
            <div id="intern-count" class="width-full" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        </div>
    </div>
    <div class="panel panel-info">
        <div class="panel-heading">
            <h4 class="panel-title">Host Training Establishment</h4>
        </div>
        <div class="panel-body">
            <div id="hte-intern-affiliated" class="width-full" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        </div>
    </div>
    <!-- end panel -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-js'); ?>
<script>
    $(document).ready(function() {
        App.init();
        setInternComparisonRateChart();
        setInternPerProgramChart();
        setInternPerHTEChart();
    });

    function setInternComparisonRateChart() {
        var categories = [];
        var enrolled = [];
        var internship = [];
        var complete = [];

        <?php $__currentLoopData = $comparison["categories"]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            categories.push('<?php echo e($category); ?>');
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php $__currentLoopData = $comparison["enrolled"]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $enrolled): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            enrolled.push(<?php echo e($enrolled); ?>);
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php $__currentLoopData = $comparison["internship"]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $internship): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            internship.push(<?php echo e($internship); ?>);
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php $__currentLoopData = $comparison["complete"]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $complete): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            complete.push(<?php echo e($complete); ?>);
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        //console.log(categories, enrolled, internship);

        Highcharts.chart('intern-comparison-rate', {
            chart: {
                type: 'column'
            },
            title: {
                text: '',
            },
            xAxis: {
                categories: categories,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: null
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat:
                    '<tr>' +
                    '   <td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '   <td style="padding:0"><b>{point.y:f}</b></td>' +
                    '</tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    // pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Enrolled Students',
                data: enrolled
            }, {
                name: 'Deployed Student Trainee',
                data: internship
            }, {
                name: 'Deployed Student Trainee',
                data: complete
            }]
        });
    }

    function setInternPerProgramChart() {
        Highcharts.chart('intern-count', {
            title: {
                text: '<b>Number of Student Trainee per Degree Program</b>'
            },
            subtitle: {
                text: 'A.Y. <?php echo e($active->academic_yr.' - '.$active->semester); ?>'
            },
            xAxis: {
                type: 'category'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },
            plotOptions: {
                series: {
                    //allowPointSelect: true,
                    cursor: 'pointer'
                }
            },
            series: [{
                name: 'Student Trainees',
                colorByPoint: true,
                type: 'pie',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true,
                data: [
                        <?php if(count($intern["intern_no"]) > 0): ?>
                    <?php for($i = 0; $i < count($intern); $i++): ?>
                        {
                            name: '<?php echo e($intern["programs"][$i]->course_name); ?>',
                            y: <?php echo e($intern["intern_no"][$i]); ?>,
                            drilldown: '<?php echo e($intern["programs"][$i]->course_abbrv); ?>'
                        },
                    <?php endfor; ?>
                    <?php endif; ?>
                ]
            }]
        });
    }

    function setInternPerHTEChart() {
        Highcharts.chart('hte-intern-affiliated', {
            title: {
                text: '<b>Number of Student Trainee per Host Training Company</b>'
            },
            subtitle: {
                text: 'A.Y. <?php echo e($active->academic_yr.' - '.$active->semester); ?>'
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                min: 0,
                tickInterval: 2,
                title: {
                    text: 'Student Trainee'
                },
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y:f}</b>'
            },
            plotOptions: {
                series: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: false
                }
            },
            series: [{
                name: 'Student Trainees',
                colorByPoint: true,
                type: 'bar',
                dataLabels: {
                    enabled: false
                },
                showInLegend: false,
                data: [
                    <?php for($i = 0; $i < count($affiliated["hte"]); $i++): ?>
                    {
                        name: '<?php echo e($affiliated["hte"][$i]->hte_name); ?>',
                        y: <?php echo e($affiliated["hte_intern"][$i]); ?>,
                        drilldown: null
                    },
                    <?php endfor; ?>
                ]
            }],
            /*drilldown: {
                series: [
                    // 1st Level of Drilldown - Courses
                    {
                        name: 'BSU-Main1 Courses',
                        id: 'BSU-CECS',
                        type: 'column',
                        dataLabels: {
                            enabled: true
                        },
                        showInLegend: false,
                        data: [{
                            // 'Bachelor of Science in Information Technology',
                            name: 'BSIT',
                            y: 48,
                            drilldown: null
                        }, {
                            name: 'BSCS',
                            y: 50,
                            drilldown: null
                        }, {
                            name: 'BSCOE',
                            y: 50,
                            drilldown: null
                        }]
                    }, {
                        name: 'BSU-Main1 Courses',
                        id: 'BSU-CBA',
                        type: 'column',
                        dataLabels: {
                            enabled: true
                        },
                        showInLegend: false,
                        data: [{
                            // 'Bachelor of Science in Business Administration Major in Marketing Management',
                            name: 'BSBA-MM',
                            y: 45,
                            drilldown: null
                        }, {
                            // 'Bachelor of Science in Business Administration Major in Human Resources Development Management',
                            name: 'BSBA-HRDM',
                            y: 84,
                            drilldown: null
                        }, {
                            // 'Bachelor of Science in Entrepreneurship',
                            name: 'BSEntrep',
                            y: 43,
                            drilldown: null
                        }]
                    }, {
                        name: 'BSU-Main1 Courses',
                        id: 'BSU-COED',
                        type: 'column',
                        dataLabels: {
                            enabled: true
                        },
                        showInLegend: false,
                        data: [{
                            // 'Bachelor in Business Teacher Education',
                            name: 'BBTE',
                            y: 87,
                            drilldown: null
                        }, {
                            // 'Bachelor in Business Teacher Education',
                            name: 'BSED-English',
                            y: 87,
                            drilldown: null
                        }]
                    }, {
                        name: 'BSU-Main1 Courses',
                        id: 'BSU-CAS',
                        type: 'column',
                        dataLabels: {
                            enabled: true
                        },
                        showInLegend: false,
                        data: [{
                            // 'Diploma in Information Communication Technology',
                            name: 'BSF',
                            y: 44,
                            drilldown: null
                        }]
                    }, {
                        name: 'BSU-Main1 Courses',
                        id: 'BSU-CABEIHM',
                        type: 'column',
                        dataLabels: {
                            enabled: true
                        },
                        showInLegend: false,
                        data: [{
                            // 'Diploma in Information Communication Technology',
                            name: 'BSTM',
                            y: 44,
                            drilldown: null
                        }, {
                            // 'Diploma in Information Communication Technology',
                            name: 'BSHRM',
                            y: 44,
                            drilldown: null
                        }]
                    }]
            }*/
        });
    }
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.users', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>