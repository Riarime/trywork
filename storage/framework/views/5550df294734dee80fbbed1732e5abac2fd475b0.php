<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <title>Users Portal | TryWork</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/plugins/jquery-ui/jquery-ui.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/plugins/bootstrap/4.0.0/css/bootstrap.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/plugins/font-awesome/5.0/css/fontawesome-all.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/plugins/animate/animate.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/css/default/style.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/css/default/style-responsive.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/css/default/theme/default.css')); ?>" rel="stylesheet" id="theme" />
    <!-- ================== END BASE CSS STYLE ================== -->

    <!-- ================== BEGIN BASE JS ================== -->
    <script src="<?php echo e(asset('assets/backend/plugins/pace/pace.min.js')); ?>"></script>
    <!-- ================== END BASE JS ================== -->
</head>
<body class="pace-top bg-white">
<!-- begin #page-loader -->
<div id="page-loader" class="fade show"><span class="spinner"></span></div>
<!-- end #page-loader -->

<!-- begin #page-container -->
<div id="page-container" class="fade">
    <!-- begin login -->
    <div class="login login-with-news-feed">
        <!-- begin news-feed -->
        <div class="news-feed">
            
            <div class="news-image" style="background: #118c8a"></div>
            <div class="news-caption">
                <h4 class="caption-title"><b>TryWork</b> Users Portal</h4>
                <p>

                </p>
            </div>
        </div>
        <!-- end news-feed -->
        <!-- begin right-content -->
        <div class="right-content">
            <!-- begin login-header -->
            <div class="login-header" style="padding-top: 30px">
                <div class="brand text-center">
                    <img src="<?php echo e(asset('assets/images/logo-light.png')); ?>" style="height: 100px"/><br/><b>Try</b>Work
                    <small>Internship Recommendation System</small>
                </div>
                <div class="icon">
                    <i class="fa fa-sign-in"></i>
                </div>
            </div>
            <!-- end login-header -->
            <!-- begin login-content -->
            <div class="login-content">
                <form action="<?php echo e(route('login')); ?>" method="POST" class="margin-bottom-0">
                    <?php echo e(csrf_field()); ?>

                    <div class="form-group m-b-15">
                        <input type="text" class="form-control form-control-sm  " name="email" placeholder="Email Address" required />
                        <?php if($errors->has('email')): ?>
                            <span class="help-block">
                                        <small class="text-danger"><?php echo e($errors->first('email')); ?></small>
                                    </span>
                        <?php endif; ?>
                    </div>
                    <div class="form-group m-b-15">
                        <input type="password" class="form-control form-control-sm" name="password" placeholder="Password" required />
                        <?php if($errors->has('password')): ?>
                            <span class="help-block">
                                        <small><?php echo e($errors->first('password')); ?></small>
                                    </span>
                        <?php endif; ?>
                    </div>
                    <div class="checkbox checkbox-css m-b-30">
                        <input type="checkbox" id="remember_me_checkbox" value="" />
                        <label for="remember_me_checkbox">
                            Remember Me
                        </label>
                    </div>
                    <div class="login-buttons">
                        <button type="submit" class="btn btn-success btn-block btn-sm">Sign me in</button>
                    </div>
                    <div class="m-t-20 m-b-40 p-b-40 text-inverse text-center">
                        Not a user yet? <a href="<?php echo e(route('Registration')); ?>" class="text-success">Click here</a> to register.

                        <hr />
                        <p class="text-center text-grey-darker">
                            &copy; SRG 7th Generation, All Right Reserved, <?php echo e(date('Y')); ?>

                        </p>
                    </div>
                </form>
            </div>
            <!-- end login-content -->
        </div>
        <!-- end right-container -->
    </div>
    <!-- end login -->
</div>
<!-- end page container -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="<?php echo e(asset('assets/backend/plugins/jquery/jquery-3.2.1.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/backend/plugins/jquery-ui/jquery-ui.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/backend/plugins/bootstrap/4.0.0/js/bootstrap.bundle.min.js')); ?>"></script>
<!--[if lt IE 9]>
<script src="<?php echo e(asset('assets/backend/crossbrowserjs/html5shiv.js')); ?>"></script>
<script src="<?php echo e(asset('assets/backend/crossbrowserjs/respond.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/backend/crossbrowserjs/excanvas.min.js')); ?>"></script>
<![endif]-->
<script src="<?php echo e(asset('assets/backend/plugins/slimscroll/jquery.slimscroll.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/backend/plugins/js-cookie/js.cookie.js')); ?>"></script>
<script src="<?php echo e(asset('assets/backend/js/theme/default.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/particleground/jquery.particleground.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/vaguejs/Vague.js')); ?>"></script>
<script src="<?php echo e(asset('assets/backend/js/apps.min.js')); ?>"></script>
<!-- ================== END BASE JS ================== -->

<script>
    $("div.news-image").children().remove();
    $("div.news-image").particleground({
        parallax: true,
        parallaxMultiplier: 10,
        dotColor: '#3abfbd',
        lineColor: '#3abfbd',
        density: 5000,
    });

    var vague = $("div.news-image").Vague({intensity:3});
    vague.blur();
</script>
<script>
    $(document).ready(function() {
        App.init();
    });
</script>
</body>
</html>