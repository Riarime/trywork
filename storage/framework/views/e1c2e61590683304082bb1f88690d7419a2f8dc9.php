<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="<?php echo e(asset('assets/backend/plugins/jquery-ui/jquery-ui.min.css')); ?>">
    <link href="<?php echo e(asset('assets/backend/plugins/bootstrap/4.0.0/css/bootstrap.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/plugins/font-awesome/5.0/css/fontawesome-all.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/plugins/animate/animate.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/css/default/style.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/css/default/style-responsive.min.css')); ?>" rel="stylesheet" />
    <title>Document</title>
</head>
<body>

    <div class="panel">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-4">
                    <img src="<?php echo e(asset('assets/images/logo-light (2).png')); ?>" width="30%" height="30%">
                </div>
                <div class="col-lg-5 m-t-40">
                    <h3>Hello, <?php echo e($role); ?> !</h3>
                    <p class="f-s-20">
                        Thank you for joining to <a href="<?php echo e(route('home')); ?>">TryWork</a>!<br>
                        Your registration for <?php echo e($company); ?> as Host Training Company has been evaluated and approved.<br>
                        Thank you for your patience!<br><br>

                        -TryWork Project Team
                    </p>
                </div>
            </div>
        </div>
    </div>

</body>
</html>