<?php $__env->startSection('title', 'Intern List'); ?>

<?php $__env->startSection('css'); ?>
<link href="<?php echo e(asset('assets/backend/plugins/DataTables/media/css/dataTables.bootstrap.min.css')); ?>" rel="stylesheet" />
<link href="<?php echo e(asset('assets/backend/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css')); ?>" rel="stylesheet" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('base-js'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <!-- begin breadcrumb -->
    <div class="row">
        <ol class="breadcrumb pull-left">
            <li class="breadcrumb-item"><a href="javascript:;"><i class="fa fa-home"></i>&nbsp&nbspHome</a></li>
            <li class="breadcrumb-item"><i class="fa fa-hdd"></i>&nbsp&nbspInterns</li>
            <li class="breadcrumb-item active"><i class="fa fa-list"></i>&nbsp&nbspRecord List</li>
        </ol>
    </div><br>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Intern Record List</h1>
    <!-- end page-header -->
    
    <!-- begin panel -->
    <div class="panel panel-inverse">
        <!-- begin panel-heading -->
        <div class="panel-heading">
            <h4 class="panel-title">Data Table - Default</h4>
        </div>
        <!-- end panel-heading -->
        <!-- begin panel-body -->
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <legend>
                            <?php echo Form::open(['route' => 'intern.list', 'method' => 'post']); ?>

                                <div class="col-md-9 offset-1">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label f-s-17">Academic Year</label>
                                        <div class="col-md-3">
                                            <select id="year" name="year" class="form-control" required>
                                                <option value="">--Select Academic Year--</option>
                                                <?php if(count($ays) > 0): ?>
                                                    <?php $__currentLoopData = $ays; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $year): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($year->ay_code); ?>"><?php echo e($year->academic_yr); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                        <label class="col-md-2 col-form-label f-s-17">Program</label>
                                        <div class="col-md-3">
                                            <select id="program" name="program" class="form-control" required>
                                                <option value="<?php echo e($selected_program); ?>">--Select Program--</option>
                                                <?php if(count($programs) > 0): ?>
                                                    <?php $__currentLoopData = $programs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $program): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($program->ys_code); ?>"><?php echo e($program->year_section); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                            </select>
                                        </div><br><br>
                                        <div class="col-md-1">
                                            <button id="load-record" type="submit" class="btn btn-primary m-b-10">Load Record</button>
                                        </div>
                                    </div>
                                </div>
                            <?php echo Form::close(); ?>

                        </legend>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="data-table-default" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th width="17%" class="text-nowrap" data-orderable="false">Student Number</th>
                                    <th class="text-nowrap" data-orderable="false">Student Name</th>
                                    <th width="11%" class="text-nowrap" data-orderable="false">Status</th>
                                    <th width="17%" class="text-nowrap" data-orderable="false">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php if(count($interns) > 0): ?>
                                        <?php $__currentLoopData = $interns; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr id="<?php echo e($row->IntCode); ?>">
                                                <td><?php echo e($row->StudNo); ?></td>
                                                <td><?php echo e($row->Fname.' '.$row->Lname); ?></td>
                                                <td><?php echo e($row->Status); ?></td>
                                                <td>
                                                    <a class="btn btn-icon btn-primary" href="javascript:;" id="btn-edit">
                                                        <i class="fas fa-pencil-alt"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end panel-body -->
    </div>
    <!-- end panel -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('assets/backend/plugins/DataTables/media/js/jquery.dataTables.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/DataTables/media/js/dataTables.bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/js/demo/table-manage-default.demo.min.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-js'); ?>
    <script>
        $(document).ready(function() {
            App.init();
			TableManageDefault.init();
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.users', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>