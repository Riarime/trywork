<?php $__env->startSection('title', 'University Colleges'); ?>

<?php $__env->startSection('css'); ?>
<link href="<?php echo e(asset('assets/backend/plugins/DataTables/media/css/dataTables.bootstrap.min.css')); ?>" rel="stylesheet" />
<link href="<?php echo e(asset('assets/backend/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css')); ?>" rel="stylesheet" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('base-js'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <!-- begin breadcrumb -->
    <div class="row">
        <ol class="breadcrumb pull-left">
            <li class="breadcrumb-item"><a href="javascript:;"><i class="fa fa-home"></i>&nbsp&nbspHome</a></li>
            <li class="breadcrumb-item"><i class="fa fa-th-large"></i>&nbsp&nbspConfiguration</li>
            <li class="breadcrumb-item active"><i class="fas fa-graduation-cap"></i>&nbsp&nbspUniversity Colleges</li>
        </ol>
    </div><br>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">University Colleges</h1>
    <!-- end page-header -->
    
    <!-- begin panel -->
    <div class="panel panel-info">
        <div class="panel-heading">
            <h4 class="panel-title">University Colleges Record</h4>
        </div>
        <div class="panel-body">
            
            <div class="row m-t-20">
                <div class="col-md-3"></div>
                <div class="col-md-5">
                    <div class="form-row">
                        <label class="col-form-label col-md-4"></label>
                        <div class="col-md-8">
                            <input id="txt_code" type="text" class="form-control" name="txt_academic_code" required disabled hidden>
                        </div>
                    </div><br>
                    <div class="form-row">
                        <label class="col-form-label col-md-4">University Colleges</label>
                        <div class="col-md-8">
                            <input id="txt_college" type="text" class="form-control" placeholder="Semster" required disabled>
                        </div>
                    </div><br>
                    <div class="form-row">
                        <div class="col-md-4"></div>
                        <div class="col-form-label">
                            <button id="btn_add" class="form-control btn btn-primary" type="submit">Add</button>
                        </div>
                        <div class="col-form-label">
                            <button id="btn_submit" class="form-control btn btn-primary" type="submit" disabled>Submit</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row m-t-40">
                <div class="col-md-12">
                    <table id="data-table-default" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                
                                <th width="1%" class="text-nowrap" hidden></th>
                                <th width="" class="text-nowrap">University Colleges</th>
                                <th width="20%" class="text-nowrap" data-orderable="false">Status</th>
                                <th width="" class="text-nowrap" data-orderable="false">Controls</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
            
        </div>
    </div>
    <!-- end panel -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('assets/backend/plugins/DataTables/media/js/jquery.dataTables.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/DataTables/media/js/dataTables.bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/js/demo/table-manage-default.demo.min.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-js'); ?>
<script>
    $(document).ready(function() {
        App.init();
        TableManageDefault.init();
    });
</script>
<script>
    $('#btn_add').on('click',function() {
        enableForm();
    });
    $('#btn_submit').on('click',function() {
        disableForm();
    });
    $('#btn_edit').on('click',function() {
        enableForm();
    });

    function enableForm() {
        var input = $('#txt_college');
        var btn = $('#btn_submit');
        $('#btn_add').attr('disabled','disabled');
        input.removeAttr('disabled');
        btn.removeAttr('disabled');
        input.focus();
    }
    function disableForm() {
        $('#txt_college').attr('disabled','disabled');
        $('#btn_submit').attr('disabled','disabled');
        $('#btn_add').removeAttr('disabled');
    }
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.users', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>