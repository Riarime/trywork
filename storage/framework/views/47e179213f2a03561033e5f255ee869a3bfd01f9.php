<?php $__env->startSection('title', 'Your Current Applications'); ?>

<?php $__env->startSection('css'); ?>
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('document.ready'); ?>
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-inverse" data-sortable-id="ui-widget-1">
                <div class="panel-heading bg-warning">
                    <div class="panel-heading-btn">
                        <a href="#" class="btn btn-xs btn-icon btn-circle btn-default"
                           data-click="panel-expand">
                            <i class="fa fa-expand"></i>
                        </a>
                    </div>
                    <h4 class="panel-title">Pending Applications</h4>
                </div>
                <div class="panel-body">
                    <div data-scrollbar="true" data-height="450px">
                        <div class="list-group">
                            <?php if(null != $user_applications): ?>
                                <?php $__currentLoopData = $user_applications; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $application): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($application->iappstat_id == \App\InternApplicationStatus::where('iappstat_code', 'TWIAS-1')->first()->iappstat_id): ?>
                                        <a href="#timeline<?php echo e($application->iapp_id); ?>" data-toggle="modal" class="list-group-item list-group-item-action flex-column align-items-start" >
                                            <div class="d-flex w-100 justify-content-between">
                                                <h5 class="mb-1">
                                                    <div>
                                                        <?php echo e(\App\HTEJobTrainings::where('job_training_id', $application->job_training_id)->first()->job_training_name); ?>

                                                    </div>
                                                    <small style="font-size: 10px;"><?php echo e(\App\HTE::where('hte_id', \App\HTEJobTrainings::where('job_training_id', $application->job_training_id)->first()->hte_id)->first()->hte_name); ?></small>
                                                </h5>

                                            </div>
                                            <p class="mb-1"><?php echo e(\App\HTEJobTrainings::where('job_training_id', $application->job_training_id)->first()->description); ?></p>
                                            <small style="font-size: 8px"><b><?php echo e(date('F d, Y', strtotime($application->created_at))); ?> | <?php echo e(date('H:ia', strtotime($application->created_at))); ?></b></small>
                                        </a>
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php if(null == $user_applications->where('iappstat_id', \App\InternApplicationStatus::where('iappstat_code', 'TWIAS-1')->first()->iappstat_id)->where('status', true)->first()): ?>
                                    <b class="text-muted">There are no pending applications to display.</b>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-inverse" data-sortable-id="ui-widget-2">
                <div class="panel-heading bg-primary">
                    <div class="panel-heading-btn">
                        <a href="#" class="btn btn-xs btn-icon btn-circle btn-default"
                           data-click="panel-expand">
                            <i class="fa fa-expand"></i>
                        </a>
                    </div>
                    <h4 class="panel-title">For Interview Applications</h4>
                </div>
                <div class="panel-body">
                    <div data-scrollbar="true" data-height="450px">
                        <div class="list-group">
                            <?php if(null != $user_applications): ?>
                                <?php $__currentLoopData = $user_applications; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $application): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($application->iappstat_id == \App\InternApplicationStatus::where('iappstat_code', 'TWIAS-3')->first()->iappstat_id): ?>
                                        <a  href="#timeline<?php echo e($application->iapp_id); ?>" data-toggle="modal" class="list-group-item list-group-item-action flex-column align-items-start">
                                            <div class="d-flex w-100 justify-content-between">
                                                <h5 class="mb-1">
                                                    <div>
                                                        <?php echo e(\App\HTEJobTrainings::where('job_training_id', $application->job_training_id)->first()->job_training_name); ?>

                                                    </div>
                                                    <small style="font-size: 10px;"><?php echo e(\App\HTE::where('hte_id', \App\JobTraining::where('job_training_id', $application->job_training_id)->first()->hte_id)->first()->hte_name); ?></small>
                                                </h5>

                                            </div>
                                            <p class="mb-1"><?php echo e(\App\JobTraining::where('job_training_id', $application->job_training_id)->first()->description); ?></p>
                                            <small style="font-size: 8px"><b><?php echo e(date('F d, Y', strtotime($application->created_at))); ?> | <?php echo e(date('H:ia', strtotime($application->created_at))); ?></b></small>
                                        </a>
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php if(null == $user_applications->where('iappstat_id', \App\InternApplicationStatus::where('iappstat_code', 'TWIAS-3')->first()->iappstat_id)->where('status', true)->first()): ?>
                                    <b class="text-muted">There are no for interview applications to display.</b>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-inverse" data-sortable-id="ui-widget-2">
                <div class="panel-heading bg-lime">
                    <div class="panel-heading-btn">
                        <a href="#" class="btn btn-xs btn-icon btn-circle btn-default"
                           data-click="panel-expand">
                            <i class="fa fa-expand"></i>
                        </a>
                    </div>
                    <h4 class="panel-title">Passed Applications</h4>
                </div>
                <div class="panel-body">
                    <div data-scrollbar="true" data-height="450px">
                        <div class="list-group">
                            <?php if(null != $user_applications): ?>
                                <?php $__currentLoopData = $user_applications; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $application): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($application->iappstat_id == \App\InternApplicationStatus::where('iappstat_code', 'TWIAS-4')->first()->iappstat_id): ?>
                                        <a href="#timeline<?php echo e($application->iapp_id); ?>" data-toggle="modal" class="list-group-item list-group-item-action flex-column align-items-start">
                                            <div class="d-flex w-100 justify-content-between">
                                                <h5 class="mb-1">
                                                    <div>
                                                        <?php echo e(\App\HTEJobTrainings::where('job_training_id', $application->job_training_id)->first()->job_training_name); ?>

                                                    </div>
                                                    <small style="font-size: 10px;"><?php echo e(\App\HTE::where('hte_id', \App\JobTraining::where('job_training_id', $application->job_training_id)->first()->hte_id)->first()->hte_name); ?></small>
                                                </h5>

                                            </div>
                                            <p class="mb-1"><?php echo e(\App\JobTraining::where('job_training_id', $application->job_training_id)->first()->description); ?></p>
                                            <small style="font-size: 8px"><b><?php echo e(date('F d, Y', strtotime($application->created_at))); ?> | <?php echo e(date('H:ia', strtotime($application->created_at))); ?></b></small>
                                        </a>
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php if(null == $user_applications->where('iappstat_id', \App\InternApplicationStatus::where('iappstat_code', 'TWIAS-4')->first()->iappstat_id)->where('status', true)->first()): ?>
                                    <b class="text-muted">There are no passed applications to display.</b>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-inverse" data-sortable-id="ui-widget-2">
                <div class="panel-heading bg-danger">
                    <div class="panel-heading-btn">
                        <a href="#" class="btn btn-xs btn-icon btn-circle btn-default"
                           data-click="panel-expand">
                            <i class="fa fa-expand"></i>
                        </a>
                    </div>
                    <h4 class="panel-title">Not Suitable Applications</h4>
                </div>
                <div class="panel-body">
                    <div data-scrollbar="true" data-height="450px">
                        <div class="list-group">
                            <?php if(null != $user_applications): ?>
                                <?php $__currentLoopData = $user_applications; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $application): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($application->iappstat_id == \App\InternApplicationStatus::where('iappstat_code', 'TWIAS-2')->first()->iappstat_id): ?>
                                        <a href="#timeline<?php echo e($application->iapp_id); ?>" data-toggle="modal" class="list-group-item list-group-item-action flex-column align-items-start">
                                            <div class="d-flex w-100 justify-content-between">
                                                <h5 class="mb-1">
                                                    <div>
                                                        <?php echo e(\App\HTEJobTrainings::where('job_training_id', $application->job_training_id)->first()->job_training_name); ?>

                                                    </div>
                                                    <small style="font-size: 10px;"><?php echo e(\App\HTE::where('hte_id', \App\JobTraining::where('job_training_id', $application->job_training_id)->first()->hte_id)->first()->hte_name); ?></small>
                                                </h5>

                                            </div>
                                            <p class="mb-1"><?php echo e(\App\JobTraining::where('job_training_id', $application->job_training_id)->first()->description); ?></p>
                                            <small style="font-size: 8px"><b><?php echo e(date('F d, Y', strtotime($application->created_at))); ?> | <?php echo e(date('H:ia', strtotime($application->created_at))); ?></b></small>
                                        </a>
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php if(null == $user_applications->where('iappstat_id', \App\InternApplicationStatus::where('iappstat_code', 'TWIAS-2')->first()->iappstat_id)->first()): ?>
                                    <b class="text-muted">There are no not suitable applications to display.</b>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <?php $__currentLoopData = $user_applications->where('status', true); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $application): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="modal fade" id="timeline<?php echo e($application->iapp_id); ?>">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">
                            Status Timeline for <?php echo e(\App\HTEJobTrainings::where('job_training_id', $application->job_training_id)->first()->job_training_name); ?>

                            <br/>
                            <small style="font-size: 12px;"><?php echo e(\App\HTE::where('hte_id', \App\HTEJobTrainings::where('job_training_id', $application->job_training_id)->first()->hte_id)->first()->hte_name); ?></small>
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body bg-grey-lighter">
                        <ul class="timeline">
                            <?php $__currentLoopData = \App\InternApplication::where('intern_id', $application->intern_id)->where('job_training_id', $application->job_training_id)->orderBy('created_at', 'asc')->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $training_application): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($loop->last): ?>
                                    <li>
                                        <!-- begin timeline-time -->
                                        <div class="timeline-time">
                                            
                                            
                                        </div>
                                        <!-- end timeline-time -->
                                        <!-- begin timeline-icon -->
                                        <div class="timeline-icon">
                                            <a href="javascript:;">&nbsp;</a>
                                        </div>
                                        <!-- end timeline-icon -->
                                        <!-- begin timeline-body -->
                                        <div class="timeline-body">
                                            <div class="timeline-content">
                                                <span class="label label-primary"><?php echo e(\App\InternApplicationStatus::where('iappstat_id', $training_application->iappstat_id)->first()->name); ?></span>
                                                <hr/>
                                                <span class="date"><?php echo e(date('F d, Y', strtotime($training_application->created_at))); ?></span>
                                                <span class="time"><?php echo e(date('H:ia', strtotime($training_application->created_at))); ?></span>
                                            </div>
                                        </div>
                                        <!-- end timeline-body -->
                                    </li>
                                <?php else: ?>
                                    <li>
                                        <!-- begin timeline-time -->
                                        <div class="timeline-time">
                                            
                                        </div>
                                        <!-- end timeline-time -->
                                        <!-- begin timeline-icon -->
                                        <div class="timeline-icon">
                                            <a href="javascript:;">&nbsp;</a>
                                        </div>
                                        <!-- end timeline-icon -->
                                        <!-- begin timeline-body -->
                                        <div class="timeline-body">
                                            <div class="timeline-content">
                                                <span class="label label-primary"><?php echo e(\App\InternApplicationStatus::where('iappstat_id', $training_application->iappstat_id)->first()->name); ?></span>
                                                <hr/>
                                                <span class="date"><?php echo e(date('F d, Y', strtotime($training_application->created_at))); ?></span>
                                                <span class="time"><?php echo e(date('H:ia', strtotime($training_application->created_at))); ?></span>
                                            </div>
                                        </div>
                                        <!-- end timeline-body -->
                                    </li>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master-backend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>