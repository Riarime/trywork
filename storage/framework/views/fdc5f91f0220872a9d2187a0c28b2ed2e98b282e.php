<?php $__env->startSection('title', 'HTE Finder'); ?>

<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(asset('assets/backend/plugins/jquery-jvectormap/jquery-jvectormap.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/plugins/bootstrap-calendar/css/bootstrap_calendar.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/plugins/gritter/css/jquery.gritter.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/plugins/nvd3/build/nv.d3.css')); ?>" rel="stylesheet" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.2/d3.min.js"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/nvd3/build/nv.d3.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/jquery-jvectormap/jquery-jvectormap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/jquery-jvectormap/jquery-jvectormap-world-merc-en.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/bootstrap-calendar/js/bootstrap_calendar.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/gritter/js/jquery.gritter.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/bootstrap-sweetalert/sweetalert.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/js/demo/dashboard-v2.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/js/demo/timeline.demo.min.js')); ?>"></script>
    <script>
        var handleSweetNotification = function() {
            $('[data-click="swal-submit"]').click(function (e) {
                e.preventDefault();
                swal({
                    title: 'Are you sure?',
                    text: '',
                    icon: 'info',
                    buttons: {
                        cancel: {
                            text: 'Cancel',
                            value: null,
                            visible: true,
                            className: 'btn btn-default',
                            closeModal: true,
                        },
                        confirm: {
                            text: 'Submit',
                            value: true,
                            visible: true,
                            className: 'btn btn-lime text-black',
                            closeModal: true
                        }
                    }
                });
            });
        }
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('document.ready'); ?>
    handleSweetNotification();
    handleVisitorsAreaChart();
    handleVisitorsDonutChart();
    handleVisitorsVectorMap();
    handleScheduleCalendar();

    
    
    
    
    
    
    
    
    
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="panel panel-inverse" data-sortable-id="ui-widget-1">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="#" class="btn btn-xs btn-icon btn-circle btn-default"
                   data-click="panel-expand">
                    <i class="fa fa-expand"></i>
                </a>
            </div>
            <h4 class="panel-title" style="font-size: 18px">HTE Finder<br/><small>Search Results</small></h1>
        </div>
        <div class="panel-body bg-silver bg-gradient-silver">
            
                <div>
                <div class="row">
                    <!-- begin col-12 -->
                    <div class="col-md-12">
                        <form class="row" id="frm-logout" action="<?php echo e(route('intern.hte-finder')); ?>" method="GET">
                            <div class="col-md-6">
                                <p>Job Training results for <b><i><?php echo e($queryoriginal); ?></i>, </b><a href="<?php echo e(route('intern.hte-finder')); ?>" class="link text-black">Go Back to HTE Finder</a>.</p>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="  form-control" name="query">
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-primary dropdown-toggle no-caret">
                                                Search
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- begin result-container -->
                        <div class="result-container">
                            <!-- begin result-list -->
                            <ul class="result-list">
                                <?php if(null !== $results->first()): ?>
                                <?php $__currentLoopData = $htes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $hte): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php $__currentLoopData = $results->where('hte_id', $hte->hte_id); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $result): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li>
                                                <div class="result-price" style="background-color: #e4e4e4">
                                                    
                                                    
                                                    
                                                    5<br/><small style="color: #101010;"> slots available</small>
                                                    
                                                    <?php if(\App\InternApplication::where('intern_id', \App\UnivIntern::where('info_id', \App\UserRole::where('user_id', \Auth::user()->id)->first()->info_id)->first()->intern_id)->where('job_training_id', \App\HTEJobTrainings::where('training_code', $result->code)->first()->job_training_id)->first()): ?>
                                                        <form id="frm-logout" action="<?php echo e(route('application-controller.store')); ?>" method="POST">
                                                            <?php echo e(csrf_field()); ?>

                                                            <input type="hidden" name="code" value="<?php echo e($result->code); ?>"/>
                                                            <button class="btn btn-block btn-secondary" type="submit" disabled="disabled">Application Submitted</button>
                                                        </form>
                                                    <?php else: ?>
                                                        <form id="frm-logout" action="<?php echo e(route('application-controller.store')); ?>" method="POST">
                                                            <?php echo e(csrf_field()); ?>

                                                            <input type="hidden" name="code" value="<?php echo e($result->code); ?>"/>
                                                            <button class="btn btn-block btn-lime" type="submit">Submit Application</button>
                                                        </form>
                                                    <?php endif; ?>
                                                </div>
                                                <a href="#" class="result-image " style="background-image: url(<?php echo e(Avatar::create($hte->hte_name)->toBase64()); ?>)"></a>
                                                <div class="result-info">
                                                    <h4 class="title"><?php echo e($result->ojt_name); ?></h4>
                                                    <p class="location"><a href="<?php echo e(route('view-hte.show', $hte->hte_id)); ?>"><?php echo e($hte->hte_name); ?></a>&nbsp;<br/><br/><b>Job Description:</b></p>
                                                    <p class="desc">
                                                        <?php echo e($result->description); ?>

                                                    </p>
                                                    <span><small><b>Address:</b> <?php echo e($hte->address); ?>, <?php echo e(\App\Region::where('region_id', $hte->region_id)->first()->name); ?></small></span>
                                                </div>
                                            </li>
                                            <br/>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php else: ?>
                                    <li style="text-align: center">
                                        <br/>
                                        <br/>
                                        <br/>
                                        <h2 class="text-muted">NO RESULTS FOUND</h2>
                                        <br/>
                                        <br/>
                                        <br/>
                                    </li>
                                <?php endif; ?>
                            </ul>
                            <!-- end result-list -->
                        </div>
                        <!-- end result-container -->
                    </div>
                    <!-- end col-12 -->
                </div>
            </div>
        </div>
    </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master-backend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>