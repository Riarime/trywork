<?php $__env->startSection('title', 'Intern Assessment'); ?>

<?php $__env->startSection('css'); ?>
<link href="<?php echo e(asset('assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css')); ?>" rel="stylesheet" />
<link href="<?php echo e(asset('assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css')); ?>" rel="stylesheet" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page-level-js'); ?>
	<link href="<?php echo e(asset('assets/backend/plugins/parsley/src/parsley.css')); ?>" rel="stylesheet" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <!-- begin breadcrumb -->
    <div class="row">
        <ol class="breadcrumb pull-left">
            <li class="breadcrumb-item"><a href="javascript:;"><i class="fa fa-home"></i>&nbsp&nbspHome</a></li>
            <li class="breadcrumb-item"><i class="fa fa-hdd"></i>&nbsp&nbspInterns</li>
            <li class="breadcrumb-item active"><i class="fa fa-print"></i>&nbsp&nbspApplication Assessment</li>
        </ol>
    </div><br>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Intern Application Assessment</h1>
    <!-- end page-header -->
    
    
    <div class="row">
        <div class="col-md-5">
            <!-- begin panel -->
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title">Import Excel</h4>
                    
                </div>
                <div class="panel-body">
                    <?php echo Form::open(array('route' => 'intern.import', 'method' => 'POST', 'files' => 'true')); ?>

                        <div class="form-group row">
                            <div class="input-group col-md-12">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Select Program</span>
                                </div>
                                <select name="select_program" id="program" class="form-control" required>
                                    <option value="">--Select Program--</option>
                                    <?php if(count($programs) > 0): ?>
                                        <?php $__currentLoopData = $programs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $program): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($program->id); ?>"><?php echo e($program->abbrv.' '.$program->ys); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div><br>
                        <div class="form-group row">
                            <div class="input-group col-md-12">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">&nbsp&nbsp&nbsp&nbspSelect File&nbsp&nbsp&nbsp&nbsp</span>
                                </div>
                                <input class="form-control" name="intern_application" type="file" required>
                            </div>
                        </div><br>
                        <div class="form-group row pull-right">
                            <div class="input-group col-md-12">
                                <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    <?php echo Form::close(); ?>

                </div>
            </div>
            <!-- end panel -->
        </div>
        <div class="col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">Format</h4>
                </div>
                <div class="panel-body">
                    <div class="note note-primary m-b-5">
                        <div class="note-icon"><i class="fa fa-download"></i></div>
                        <div class="note-content">
                            <h4><b>Download Format</b></h4>
                            <p>
                                Here is the provided excel file format for importing and fill-up form for manual of Intern. Just click the link to download excel or fill-up form.
                            </p>
                            <a href="">Download Excel File</a>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<a href="">Download Fill-up Form</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title">Manual Form</h4>
                </div>
                <div class="panel-body">
                     <?php echo Form::open(array('route' => 'intern.import', 'method' => 'POST')); ?>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-form-label">Student Number</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="txt_StudNo" name="txt_StudNo" placeholder="Student Number" />
                                    </div>
                                </div><br>
                                <div class="form-group row">
                                    <label class="col-form-label width-100 m-r-15">First Name</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="txt_FName" name="txt_FName" placeholder="First Name" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label width-100 m-r-15">Middle Name</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="txt_MName" name="txt_MName" placeholder="Middle Name" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label width-100 m-r-15">Last Name</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="txt_LName" name="txt_LName" placeholder="Last Namer" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-form-label col-md-3">Gender</label>
                                    <div class="col-md-6">
                                        <div class="radio radio-css radio-inline">
                                            <input type="radio" name="radio_css_inline" id="opt_Male" value="Male"/>
                                            <label for="opt_Male">Male</label>
                                        </div>
                                        <div class="radio radio-css radio-inline">
                                            <input type="radio" name="radio_css_inline" id="opt_Female" name="opt_Female" value="Female" />
                                            <label for="opt_Female">Female</label>
                                        </div>
                                    </div>
                                </div><br>
                                <div class="form-group row">
                                    <label class="col-form-label width-100 m-r-15">Birth Date</label>
                                    <div class="col-md-8">
                                            <input type="text" class="form-control" name="bdate" id="datepicker-autoClose" placeholder="mm/dd/yyyy">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label width-100 m-r-15">Email</label>
                                    <div class="col-md-8">
                                        <input type="email" class="form-control" name="txt_email" id="txt_MName" placeholder="Email Address" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label m-r-2">Contact Number</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="txt_contact" id="txt_contact" placeholder="Contact Number" />
                                    </div>
                                </div>
                            </div>
                        </div><br>
                        <div class="row pull-right">
                            <div class="col-md-1">
                                <div class="form-group row width-150 m-t-5">
                                    <div class="m-r-20">
                                        <button type="submit" class="btn btn-primary form-control">Add</button>
                                    </div>
                                    <div>
                                        <button type="submit" name="submit" class="btn btn-primary form-control">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                     <?php echo Form::close(); ?>

                </div>
            </div>
        </div>
    </div>
    

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script src="<?php echo e(asset('assets/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')); ?>"></script>
<script src="<?php echo e(asset('assets/backend/js/demo/form-plugins.demo.min.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-js'); ?>
    <script>
        $(document).ready(function() {
            App.init();
            FormPlugins.init();
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.users', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>