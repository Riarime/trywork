<?php $__env->startSection('title', 'Dashboard'); ?>

<?php $__env->startSection('base-js'); ?>
	<script type="text/javascript" src="<?php echo e(asset('assets/backend/js/highcharts/highcharts.js')); ?>"></script>
	<script type="text/javascript" src="<?php echo e(asset('assets/backend/js/highcharts/data.js')); ?>"></script>
	<script type="text/javascript" src="<?php echo e(asset('assets/backend/js/highcharts/drilldown.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <!-- begin breadcrumb -->
    <div class="row">
        <ol class="breadcrumb pull-left">
            <li class="breadcrumb-item"><a href="javascript:;"><i class="fa fa-home"></i>&nbsp&nbspHome</a></li>
            <li class="breadcrumb-item active"><i class="fa fa-th-large"></i>&nbsp&nbspDashboard</li>
        </ol>
    </div><br>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">A.Y. 2018-2019 (1st Semester)</h1>
    <!-- end page-header -->
    
    <div class="row">
        <!-- begin col-3 -->
        <div class="col-lg-4 col-md-6">
            <div class="widget widget-stats bg-blue">
                <div class="stats-icon"><i class="fa fa-users"></i></div>
                <div class="stats-info height-60">
                    <p>144</p>
                </div>
                <div class="stats-link">
                    <a href="javascript:;">NUMBER OF STUDENT TRAINEE</a>
                    
                </div>
            </div>
        </div>
        <!-- end col-3 -->
        <!-- begin col-3 -->
        <div class="col-lg-4 col-md-6">
            <div class="widget widget-stats bg-gradient-green">
                <div class="stats-icon"><i class="fa fa-building"></i></div>
                <div class="stats-info height-60">
                    <p>25</p>
                </div>
                <div class="stats-link">
                    <a href="javascript:;">NUMBER OF HOST TRAINING ESTABLISHMENT</a>
                    
                </div>
            </div>
        </div>
        <!-- end col-3 -->
        <!-- begin col-3 -->
        
            
                
                
                    
                
                
                    
                     
                
            
        
        <!-- end col-3 -->
    </div>
    
    
    <div class="panel panel-info">
        <div class="panel-heading">
            <h4 class="panel-title">Student Trainee Comparison Rate</h4>
        </div>
        <div class="panel-body">
            <div id="intern-comparison-rate" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        </div>
    </div>
    
    <!-- begin panel -->
    <div class="panel panel-info h-full">
        <div class="panel-heading">
            <h4 class="panel-title">Charts and Diagrams</h4>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <div id="intern-count" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div>
                <div class="col-md-6">
                    <div id="hte-intern-affiliated" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- end panel -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-js'); ?>
<script>
    $(document).ready(function() {
        App.init();
    });
</script>
<script>
    var enrolled = [60, 53, 50];
    var deployed = [48, 50, 50];
    var finished = []; //[48.9, 38.8, 39.3]
    Highcharts.chart('intern-comparison-rate', {
        chart: {
            type: 'column'
        },
        title: {
            text: null
        },
        xAxis: {
            categories: [
                'BSIT',
                'BSCS',
                'BSCOE'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: null
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Enrolled Students',
            data: enrolled

        }, {
            name: 'Deployed Student Trainee',
            data: deployed

        }, {
            name: 'Finish Internship',
            data: finished

        }]
    });
    Highcharts.chart('intern-count', {
        title: {
            text: '<b>Number of Student Trainee</b><br><b>per University Colleges</b>'
        },
        subtitle: {
            text: '1st Semester A.Y. 2018-2019'
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: null
            }
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y:f} Students</b>'
        },
        plotOptions: {
            series: {
                // allowPointSelect: true,
                cursor: 'pointer'
            }
        },
        series: [{
            name: 'University Colleges',
            colorByPoint: true,
            type: 'pie',
            dataLabels: {
                enabled: false
            },
            showInLegend: true,
            data: [{
                name: 'Bachelor of Science in Information Technology',
                y: 52,
                drilldown: null
            }, {
                name: 'Bachelor of Science in Computer Science',
                y: 48,
                drilldown: null
            }, {
                name: 'Bachelor of Science in College of Engineering',
                y: 44,
                drilldown: null
            }]
        }]
    });

    Highcharts.chart('hte-intern-affiliated', {
        title: {
            text: '<b>Number of Student Trainee per</br><br><b>Host Training Company</b>'
        },
        subtitle: {
            text: '1st Semester A.Y. 2018-2019'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y:f}</b>'
        },
        plotOptions: {
            series: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'HTE',
            colorByPoint: true,
            type: 'pie',
            dataLabels: {
                enabled: false
            },
            showInLegend: true,
            data: [{
                name: 'Absolute Distillers, Inc.',
                y: 172,
                drilldown: 'null'
            }, {
                name: 'Adventus IT Service Inc.',
                y: 48,
                drilldown: 'null'
            }, {
                name: 'Batangas I Electric Cooperative Inc.',
                y: 44,
                drilldown: 'BSU-CAS'
            }, {
                name: 'Bausas Surveying Office',
                y: 44,
                drilldown: 'null'
            }, {
                name: 'Bluecollar Manpower Services Inc.',
                y: 87,
                drilldown: 'null'
            }]
        }],
        drilldown: {
            series: [
            // 1st Level of Drilldown - Courses
            {
                name: 'BSU-Main1 Courses',
                id: 'BSU-CECS',
                type: 'column',
                dataLabels: {
                    enabled: true
                },
                showInLegend: false,
                data: [{
                    // 'Bachelor of Science in Information Technology',
                    name: 'BSIT',
                    y: 48,
                    drilldown: null
                }, {
                    name: 'BSCS',
                    y: 50,
                    drilldown: null
                }, {
                    name: 'BSCOE',
                    y: 50,
                    drilldown: null
                }]
            }, {
                name: 'BSU-Main1 Courses',
                id: 'BSU-CBA',
                type: 'column',
                dataLabels: {
                    enabled: true
                },
                showInLegend: false,
                data: [{
                    // 'Bachelor of Science in Business Administration Major in Marketing Management',
                    name: 'BSBA-MM',
                    y: 45,
                    drilldown: null
                }, {
                    // 'Bachelor of Science in Business Administration Major in Human Resources Development Management',
                    name: 'BSBA-HRDM',
                    y: 84,
                    drilldown: null
                }, {
                    // 'Bachelor of Science in Entrepreneurship',
                    name: 'BSEntrep',
                    y: 43,
                    drilldown: null
                }]
            }, {
                name: 'BSU-Main1 Courses',
                id: 'BSU-COED',
                type: 'column',
                dataLabels: {
                    enabled: true
                },
                showInLegend: false,
                data: [{
                    // 'Bachelor in Business Teacher Education',
                    name: 'BBTE',
                    y: 87,
                    drilldown: null
                }, {
                    // 'Bachelor in Business Teacher Education',
                    name: 'BSED-English',
                    y: 87,
                    drilldown: null
                }]
            }, {
                name: 'BSU-Main1 Courses',
                id: 'BSU-CAS',
                type: 'column',
                dataLabels: {
                    enabled: true
                },
                showInLegend: false,
                data: [{
                    // 'Diploma in Information Communication Technology',
                    name: 'BSF',
                    y: 44,
                    drilldown: null
                }]
            }, {
                name: 'BSU-Main1 Courses',
                id: 'BSU-CABEIHM',
                type: 'column',
                dataLabels: {
                    enabled: true
                },
                showInLegend: false,
                data: [{
                    // 'Diploma in Information Communication Technology',
                    name: 'BSTM',
                    y: 44,
                    drilldown: null
                }, {
                    // 'Diploma in Information Communication Technology',
                    name: 'BSHRM',
                    y: 44,
                    drilldown: null
                }]
            }]
        }
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.users', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>