<?php $__env->startSection('title', 'Home'); ?>

<?php $__env->startSection('css'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('navbar'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('account'); ?>
    <li>
        <a href="<?php echo e(route('login')); ?>">
                <?php if(Auth::user()): ?>
                    <img src="<?php echo e(Avatar::create(\App\User::getName())->toBase64()); ?>" class="user-img" alt="" />
                    <span class="hidden-md hidden-sm hidden-xs">Welcome, <?php echo e(ucwords(strtolower(\App\User::getName()))); ?></span>
                <?php else: ?>
                    <img src="<?php echo e(asset('assets/frontend/img/user/user-1.jpg')); ?>" class="user-img" alt="" />
                    <span class="hidden-md hidden-sm hidden-xs">Login</span>
                <?php endif; ?>
        </a>
    </li>
    <li>
        <a href="<?php echo e(route('faq')); ?>">
            <span class="hidden-md hidden-sm hidden-xs">FAQ</span>
        </a>
    </li>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <!-- BEGIN #slider -->
    <div id="slider" class="section-container p-0 bg-black-darker">
        <!-- BEGIN carousel -->
        <div id="main-carousel" class="carousel slide" data-ride="carousel">
            <!-- BEGIN carousel-inner -->
            <div class="carousel-inner">
                <!-- BEGIN item -->
                <div class="item active">
                    <img src="<?php echo e(asset('assets/frontend/img/slider/slider-1-cover.jpg')); ?>" class="bg-cover-img" alt="" />
                    <div class="container">
                        <img src="../assets/img/slider/slider-1-product.png" class="product-img right bottom fadeInRight animated" alt="" />
                    </div>
                    <div class="carousel-caption carousel-caption-left">
                        <div class="container">
                            <h3 class="title m-b-5 fadeInLeftBig animated"><b>Try</b>Work</h3>
                            <p class="m-b-15 fadeInLeftBig animated">Student Training. Gain Experience. Work Ready.</p>
                            
                            
                        </div>
                    </div>
                </div>
                <!-- END item -->
            </div>
            <!-- END carousel-inner -->
        </div>
        <!-- END carousel -->
    </div>
    <!-- END #slider -->

    <!-- BEGIN #promotions -->
    <div id="promotions" class="section-container bg-white">
        <!-- BEGIN container -->
        <div class="container">
            <!-- BEGIN section-title -->
            <h4 class="section-title clearfix">
                <a href="<?php echo e(route('Registration')); ?>" class="pull-right">REGISTRATION</a>
                Features
                <small>of TryWork Intern Recommendation System and Management Services</small>
            </h4>
            <!-- END section-title -->
            <!-- BEGIN #policy -->
            <div id="policy" class="section-container p-t-30 p-b-30">
                <!-- BEGIN container -->
                <div class="container">
                    <!-- BEGIN row -->
                    <div class="row">
                        <!-- BEGIN col-4 -->
                        <div class="col-md-4 col-sm-4">
                            <!-- BEGIN policy -->
                            <div class="policy">
                                <div class="policy-icon"><i class="fa fa-graduation-cap"></i></div>
                                <div class="policy-info">
                                    <h4>Intelligently recommends interns</h4>
                                    <p>Feature where a host training establishment can find potential interns.</p>
                                </div>
                            </div>
                            <!-- END policy -->
                        </div>
                        <!-- END col-4 -->
                        <!-- BEGIN col-4 -->
                        <div class="col-md-4 col-sm-4">
                            <!-- BEGIN policy -->
                            <div class="policy">
                                <div class="policy-icon"><i class="fa fa-building-o"></i></div>
                                <div class="policy-info">
                                    <h4>Suggests Training Establishments</h4>
                                    <p>feature where an intern can find the Host Training Establishments which will address their needs in Internship Training.</p>
                                </div>
                            </div>
                            <!-- END policy -->
                        </div>
                        <!-- END col-4 -->
                        <!-- BEGIN col-4 -->
                        <div class="col-md-4 col-sm-4">
                            <!-- BEGIN policy -->
                            <div class="policy">
                                <div class="policy-icon"><i class="fa fa-users"></i></div>
                                <div class="policy-info">
                                    <h4>TryWork tracks interns and HTEs</h4>
                                    <p>Feature where an analytics is displayed for the coordinators to keep in track with the intenrs and HTEs statuses.</p>
                                </div>
                            </div>
                            <!-- END policy -->
                        </div>
                        <!-- END col-4 -->

                    </div>
                    <!-- END row -->




                </div>
                <!-- END container -->
            <!-- END #policy -->
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        <!-- END col-3 -->
        </div>
        <!-- END row -->
    </div>
    <!-- END container -->
    </div>
    <!-- END #promotions -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>