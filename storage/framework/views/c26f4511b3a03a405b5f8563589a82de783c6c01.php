<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-7 m-l-40">
            <div class="row height-80"></div>
            <div class="card card-outline-primary bg-white m-l-20 height-350">
                <div class="card-block">
                    <div class="row">
                        <div class="col-md-6 p-l-40">
                            <img src="<?php echo e(asset('assets/images/logo-light (2).png')); ?>" class="width-300">
                        </div>
                        <div class="col-md-5">
                            <div class="row m-t-40"></div>
                            <div class="row m-b-10">
                                <b><span class="f-s-17 m-l-10">Company Name</span></b>
                            </div>
                            <div class="row">
                                <div class="col-md-12 m-b-10">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="row m-b-10">
                                <b><span class="f-s-17 m-l-10">Email</span></b>
                            </div>
                            <div class="row">
                                <div class="col-md-12 m-b-20">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="row pull-right">
                                <div class="col-md-10">
                                    <button class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.users-registration', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>