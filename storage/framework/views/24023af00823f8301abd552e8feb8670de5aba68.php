<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <title><?php echo $__env->yieldContent('title'); ?> | TryWork</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/plugins/jquery-ui/jquery-ui.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/plugins/bootstrap/4.0.0/css/bootstrap.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/plugins/font-awesome/5.0/css/fontawesome-all.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/plugins/animate/animate.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/css/default/style.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/css/default/style-responsive.min')); ?>.css" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/css/default/theme/default.css')); ?>" rel="stylesheet" id="theme" />
    <!-- ================== END BASE CSS STYLE ================== -->

    <!-- ================== BEGIN PAGE LEVEL CSS STYLE ================== -->
<?php $__env->startSection('css'); ?>
<?php echo $__env->yieldSection(); ?>
<!-- ================== END PAGE LEVEL CSS STYLE ================== -->

    <!-- ================== BEGIN BASE JS ================== -->
    <script src="<?php echo e(asset('assets/plugins/pace/pace.min.js')); ?>"></script>
    <!-- ================== END BASE JS ================== -->
</head>
<body>
<!-- begin #page-loader -->
<div id="page-loader" class="fade show"><span class="spinner"></span></div>
<!-- end #page-loader -->

<!-- begin #page-container -->
<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
    <!-- begin #header -->
    <div id="header" class="header navbar-default">
        <!-- begin navbar-header -->
        <div class="navbar-header">
            <a href="" class="navbar-brand">
                <img src="<?php echo e(asset('assets/images/logo-light.png')); ?>" alt="" width="50" height="50" style="margin-top: -10px;"> <b>T</b><small><b>RY</b></small><b>W</b><small><b>ORK</b></small>
            </a>
            <button type="button" class="navbar-toggle" data-click="sidebar-toggled">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <!-- end navbar-header -->
    </div>
    <!-- end #header -->

    <!-- begin #sidebar -->
    <div id="sidebar" class="sidebar sidebar-transparent">
        <!-- begin sidebar scrollbar -->
        <div data-scrollbar="true" data-height="100%">
            <!-- begin sidebar user -->
            <ul class="nav">
                <li class="nav-profile">
                    <a href="javascript:;" data-toggle="nav-profile">
                        <div class="cover with-shadow"></div>
                        <div class="image">
                            <img src="<?php echo e(Avatar::create(\App\User::getName(\Auth::user()->id))->toBase64()); ?>" alt="" />
                        </div>
                         <div class="info">
                            <b class="caret pull-right"></b>
                            <?php echo e(ucwords(strtolower(\App\User::getName(\Auth::user()->id)))); ?>

                            <small><?php echo e('Admin'); ?></small>
                        </div>
                    </a>
                </li>
                <li>
                    <ul class="nav nav-profile">
                        <li><a href="<?php echo e(route('profile.index')); ?>"><i class="fa fa-user-circle"></i> Profile</a></li>
                        <li><a href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();"><i class="fa fa-sign-out-alt"></i> Log Out</a></li>
                        <form id="frm-logout" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                            <?php echo e(csrf_field()); ?>

                        </form>
                    </ul>
                </li>
            </ul>
            <!-- end sidebar user -->
            <!-- begin sidebar nav -->
            <ul class="nav">
                <li class="nav-header">Navigation</li>
                <?php if($view == 'audittrail'): ?>
                    <li class="active">
                        <a href="javascript:;">
                            <i class="fa fa-search"></i>
                            <span>Audit Trail</span>
                        </a>
                    </li>
                <?php else: ?>
                    <li>
                        <a href="<?php echo e(route('admin.audit-trail')); ?>">
                            <i class="fa fa-search"></i>
                            <span>Audit Trail</span>
                        </a>
                    </li>
                <?php endif; ?>
                <?php if($view == 'request-list' || $view == 'record-list'): ?>
                    <li class="has-sub expanded active">
                <?php else: ?>
                    <li class="has-sub closed">
                <?php endif; ?>
                    <a href="javascript:;">
                        <b class="caret"></b>
                        <i class="fas fa-users"></i>
                        <span>User Management</span>
                    </a>
                    <?php if($view == 'request-list' || $view == 'record-list'): ?>
                        <ul class="sub-menu">
                    <?php else: ?>
                        <ul class="sub-menu" style="display: none;">
                    <?php endif; ?>
                        <?php if($view == 'request-list'): ?>
                            <li class="active">
                                <a href="javascript:;">
                                    <span>Registration Request</span>
                                </a>
                            </li>
                        <?php else: ?>
                            <li>
                                <a href="javascript:;">
                                    <span>Registration Request</span>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if($view == 'record-list'): ?>
                            <li class="active">
                                <a href="javascript:;">
                                    <span>User Record</span>
                                </a>
                            </li>
                        <?php else: ?>
                            <li>
                                <a href="<?php echo e(route('admin.userrecord')); ?>">
                                    <span>User Record</span>
                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </li>
                <!-- begin sidebar minify button -->
                <li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
                <!-- end sidebar minify button -->
            </ul>
            <!-- end sidebar nav -->
        </div>
        <!-- end sidebar scrollbar -->
    </div>
    <div class="sidebar-bg"></div>
    <!-- end #sidebar -->

    <!-- begin #content -->
    <div id="content" class="content">
        <?php $__env->startSection('content'); ?>
        <?php echo $__env->yieldSection(); ?>
    </div>
    <!-- end #content -->


    <!-- begin scroll to top btn -->
    <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
    <!-- end scroll to top btn -->
</div>
<!-- end page container -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="<?php echo e(asset('assets/backend/plugins/jquery/jquery-3.2.1.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/backend/plugins/jquery-ui/jquery-ui.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/backend/plugins/bootstrap/4.0.0/js/bootstrap.bundle.min.js')); ?>"></script>
<!--[if lt IE 9]>
<script src="<?php echo e(asset('assets/backend/crossbrowserjs/html5shiv.js')); ?>"></script>
<script src="<?php echo e(asset('assets/backend/crossbrowserjs/respond.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/backend/crossbrowserjs/excanvas.min.js')); ?>"></script>
<![endif]-->
<script src="<?php echo e(asset('assets/backend/plugins/slimscroll/jquery.slimscroll.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/backend/plugins/js-cookie/js.cookie.js')); ?>"></script>
<script src="<?php echo e(asset('assets/backend/js/theme/default.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/particleground/jquery.particleground.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/vaguejs/Vague.js')); ?>"></script>
<script src="<?php echo e(asset('assets/backend/js/apps.min.js')); ?>"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<?php $__env->startSection('js'); ?>
<?php echo $__env->yieldSection(); ?>
<!-- ================== END PAGE LEVEL JS ================== -->

<script>
    $("div.sidebar-bg").children().remove();
    $("div.sidebar-bg").particleground({
        parallax: true,
        parallaxMultiplier: 10,
        dotColor: '#3abfbd',
        lineColor: '#3abfbd',
        density: 2500,
    });

        var vague = $("div.sidebar-bg").Vague({intensity:1.5});
        vague.blur();
</script>
<script>
    $(document).ready(function() {
        App.init();
        <?php $__env->startSection('document.ready'); ?>
        <?php echo $__env->yieldSection(); ?>
    });
</script>
<?php $__env->startSection('custom-js'); ?>
<?php echo $__env->yieldSection(); ?>

</body>
</html>
