<?php $__env->startSection('title', 'Intern Assessment'); ?>

<?php $__env->startSection('page-level-js'); ?>
	<link href="<?php echo e(asset('assets/backend/plugins/parsley/src/parsley.css')); ?>" rel="stylesheet" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <!-- begin breadcrumb -->
    <div class="row">
        <ol class="breadcrumb pull-left">
            <li class="breadcrumb-item"><a href="javascript:;"><i class="fa fa-home"></i>&nbsp&nbspHome</a></li>
            <li class="breadcrumb-item"><i class="fa fa-hdd"></i>&nbsp&nbspInterns</li>
            <li class="breadcrumb-item active"><i class="fa fa-print"></i>&nbsp&nbspApplication Assessment</li>
        </ol>
    </div><br>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Intern Application Assessment</h1>
    <!-- end page-header -->
    
    <!-- begin panel -->
    <div class="panel panel-default">
        <div class="panel-heading">
            
            <h4 class="">Import Excel</h4>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-4">
                    <?php echo Form::open(array('route' => 'intern.import', 'method' => 'POST', 'files' => 'true')); ?>

                        <div class="form-group row">
                            <div class="input-group col-md-12">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Program</span>
                                </div>
                                <select name="program" id="program" class="form-control" required>
                                    <option value="">--Select Program--</option>
                                    <?php if(count($programs) > 0): ?>
                                        <?php $__currentLoopData = $programs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $program): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($program->code); ?>"><?php echo e($program->ys); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div><br>
                        <div class="form-group row">
                            <div class="input-group col-md-12">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">&nbsp&nbsp&nbsp&nbspFile&nbsp&nbsp&nbsp&nbsp</span>
                                </div>
                                <input class="form-control" type="file" required>
                            </div>
                        </div><br>
                        <div class="form-group row pull-right">
                            <div class="input-group col-md-12">
                                <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    <?php echo Form::close(); ?>

                </div>
                <div class="col-md-1"></div>
                <div class="col-md-7">
                    <div class="note note-primary m-b-5">
                        <div class="note-icon"><i class="fa fa-download"></i></div>
                        <div class="note-content">
                            <h4><b>Download Format</b></h4>
                            <p>
                                Here is the provided excel file format for importing and fill-up form for manual of Intern. Just click the link to download excel or fill-up form.
                            </p>
                            <a href="">Download Excel File</a>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<a href="">Download Fill-up Form</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end panel -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-js'); ?>
    <script>
        $(document).ready(function() {
            App.init();
			// Highlight.init();
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.coordinator', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>