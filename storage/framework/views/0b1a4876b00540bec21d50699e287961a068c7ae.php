<?php $__env->startSection('title', 'Registration'); ?>

<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(asset('assets/sweetalert/sweetalert2.css')); ?>" rel="stylesheet" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('navbar'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('account'); ?>
    <li>
        <a href="<?php echo e(route('login')); ?>">
            <span class="hidden-md hidden-sm hidden-xs">Login</span>
        </a>
    </li>
    <li>
        <a href="<?php echo e(route('faq')); ?>">
            <span class="hidden-md hidden-sm hidden-xs">FAQ</span>
        </a>
    </li>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('assets/sweetalert/sweetalert2.js')); ?>" ></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-js'); ?>
    <script>
        $("a[id=internReg]").on('click',function(){
            swal({
                title: '<span class="f-s-18">Referral Code</span>',
                html:
                    '<input id=referral_code type=text/>',
                showCancelButton: true,
                confirmButtonClass: "btn btn-primary",
                cancelButtonClass: "btn btn-secondary",
                confirmButtonText: "Submit",
                preConfirm: (result) => {
                var code = $("input[id='referral_code']").val();
            if (code.length == 0) {
                swal.showValidationMessage(
                    `Could not be null!`
                )
            }
            else {
                $.ajax({
                    type:'GET'
                    ,url:'<?php echo e(url('/registration/check')); ?>/'+code
                    ,dataType:'json'
                    ,success:function(response){
                        if(response.length==0){
                            alert('This referral code is invalid');
                        }else{
                            window.location = '<?php echo e(url('/registration/university-student')); ?>/'+code;
                        }
                    }

                });
            }
        }
        }).then((result) => {

            })
        });
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <!-- BEGIN #promotions -->
    <div style="background-color: #d9e0e7 !important;">
        <!-- BEGIN container -->
        <div class="container">
            <!-- BEGIN section-title -->
            <div class="row text-center">
                <h3 class="clearfix">REGISTRATION</h3>
                <small>Intern Recommendation System and Management Services</small>
            </div>
            <!-- END section-title -->
            <!-- BEGIN #policy -->
            <div id="policy" class="section-container p-t-30 p-b-30">
                <!-- BEGIN container -->
                <div class="container">
                    <!-- BEGIN row -->
                    <div class="row">
                        <!-- BEGIN col-4 -->
                        <div class="col-md-4 col-sm-4">
                            <!-- BEGIN policy -->
                            <div class="panel">
                                <div class="panel-body text-center">
                                    <div class="row">
                                        <i class="fa fa-graduation-cap fa-5x text-muted"></i>
                                    </div>
                                    <div class="row">
                                        <a id=internReg href="#"><h4>Student Registration</h4></a>
                                        <p>This feature will help the students to use the Intern Module of Trywork System</p>
                                    </div>
                                </div>
                            </div>
                            <!-- END policy -->
                        </div>
                        <!-- END col-4 -->
                        <!-- BEGIN col-4 -->
                        <div class="col-md-4 col-sm-4">
                            <!-- BEGIN policy -->
                            <div class="panel">
                                <div class="panel-body text-center">
                                    <div class="row">
                                        <i class="fa fa-building-o fa-5x text-muted"></i>
                                    </div>
                                    <div class="row">
                                        <a href="<?php echo e(route('Registration HTE')); ?>"><h4>HTE Registration</h4></a>
                                        <p>This feature will help the companies to use the Intern Module of Trywork System</p>
                                    </div>
                                </div>
                            </div>
                            <!-- END policy -->
                        </div>
                        <!-- END col-4 -->
                        <!-- BEGIN col-4 -->
                        <div class="col-md-4 col-sm-4">
                            <!-- BEGIN policy -->
                            <div class="panel">
                                <div class="panel-body text-center">
                                    <div class="row">
                                        <i class="fa fa-users fa-5x text-muted"></i>
                                    </div>
                                    <div class="row">
                                        <a href="<?php echo e(route('registration.su-personnel')); ?>"><h4>OJT Personnel Registration</h4></a>
                                        <p>This feature will help the companies to use the Intern Module of Trywork System</p>
                                    </div>
                                </div>
                            </div>
                        <div class="col-md-4 col-sm-4">
                            <!-- END policy -->
                        </div>
                        <!-- END col-4 -->

                    </div>
                    <!-- END row -->

                </div>
            </div>
            <!-- END row -->
        </div>
        <!-- END container -->
    </div>
    <!-- END #promotions -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>