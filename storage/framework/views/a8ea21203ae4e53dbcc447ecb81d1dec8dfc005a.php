<?php $__env->startSection('title','Intern Management'); ?>

<?php $__env->startSection('css'); ?>
    <!--suppress ALL -->
    <link href="<?php echo e(asset('assets/backend/plugins/DataTables/media/css/dataTables.bootstrap.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/sweetalert/sweetalert2.css')); ?>" rel="stylesheet" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page-level-js'); ?>
    <link href="<?php echo e(asset('assets/backend/plugins/parsley/src/parsley.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/sweetalert/sweetalert2.js')); ?>" rel="stylesheet" />

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <!-- begin breadcrumb -->
    <div class="row">
        <ol class="breadcrumb pull-left">
            <li class="breadcrumb-item"><a href="javascript:;"><i class="fa fa-home"></i>&nbsp&nbspHome</a></li>
            <li class="breadcrumb-item active"><i class="fas fa-users"></i>&nbsp&nbspStudent Trainee</li>
        </ol>
    </div><br>
    <!-- end breadcrumb -->

    <!-- begin row -->
    <div class="row">
        <div class="col-lg-12">

            <!-- begin nav-pills -->
            <ul class="nav nav-pills">
                <li class="nav-items">
                    <a href="#nav-tab-recordlist" data-toggle="tab" class="nav-link active">
                        <span class="d-sm-none">Record List</span>
                        <span class="d-sm-block d-none">Record List</span>
                    </a>
                </li>
                <li class="nav-items">
                    <a href="#nav-tab-placement" data-toggle="tab" class="nav-link">
                        <span class="d-sm-none">OJT Placement</span>
                        <span class="d-sm-block d-none">OJT Placement</span>
                    </a>
                </li>
            </ul>
            <!-- end nav-pills -->

            <!-- begin tab-content -->
            <div class="tab-content">
                <!-- begin STUDENT TRAINEE RECORD LIST tab-pane -->
                <div class="tab-pane fade active show" id="nav-tab-recordlist">
                    <legend class="m-b-20">Student Trainee Record List</legend>
                    <div class="row">
                        <div class="width-150"></div>
                        <div class="col-md-4">
                            <div class="row form-group">
                                <label class="col-form-label text-md-right col-sm-4">Academic Year</label>
                                <div class="col-sm-7">
                                    <select id="record-ay" class="form-control">
                                        <option>--Select Academic Year--</option>
                                        <?php if(isset($ays)): ?>
                                            <?php $__currentLoopData = $ays; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ay): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($ay['code']); ?>"><?php echo e($ay['ay']); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row form-group m-b-15">
                                <label class="col-form-label text-md-right col-sm-4">Program</label>
                                <div class="col-sm-8">
                                    <select id="record-program" class="form-control">
                                        <option>--Select Program--</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div id="record-loading" class="col-sm-2 m-t-5" hidden>
                            <i class="fas fa-spinner fa-pulse fa-2x"></i>
                        </div>
                    </div>
                    <legend></legend>
                    <div class="row m-b-10">
                        <div class="col-lg-12">
                            <div class="row width-300 pull-right">
                                <div class="col-md-2"></div>
                                <div class="col-md-5 m-b-10">
                                    <button disabled id="btn-addrecord" class="btn btn-primary col-sm-12"><i class="fa fa-plus"></i> Add Record</button>
                                </div>
                                <div class="col-md-5">
                                    <button disabled id="btn-import" class="btn btn-primary col-sm-12"><i class="fa fa-upload"></i>&nbsp&nbsp&nbsp&nbspImport</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table id="tbl_recordlist" class="table m-t-20">
                        <thead>
                        <tr>
                            <th width="15%">Student Number</th>
                            <th width="30%">Student Name</th>
                            <th width="25%">Email</th>
                            <th width="5%">GPA</th>
                            <th width="10%">Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="odd">
                            <td valign="top" colspan="5" class="dataTables_empty">No data available in table</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <!-- end tab-pane -->
                <!-- begin STUDENT TRAINEE OJT PLACEMENT tab-pane -->
                <div class="tab-pane fade" id="nav-tab-placement">
                    <legend class="m-b-20">Student Trainee OJT Placement</legend>
                    <div class="row">
                        <div class="width-150"></div>
                        <div class="col-md-4">
                            <div class="row form-group">
                                <label class="col-form-label text-md-right col-sm-4">Academic Year</label>
                                <div class="col-sm-7">
                                    <select id="placement-ay" class="form-control">
                                        <option>--Select Academic Year--</option>
                                        <?php if(null !== $ays): ?>
                                            <?php $__currentLoopData = $ays; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ay): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($ay['code']); ?>"><?php echo e($ay['ay']); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row form-group m-b-15">
                                <label class="col-form-label text-md-right col-sm-4">Program</label>
                                <div class="col-sm-8">
                                    <select id="placement-program" class="form-control">
                                        <option>--Select Program--</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div id="placement-loading" class="col-sm-2 m-t-5" hidden>
                            <i class="fas fa-spinner fa-pulse fa-2x"></i>
                        </div>
                    </div>
                    <legend></legend>
                    <div class="row m-b-10">
                        <div class="col-lg-12">
                            <div class="row width-200 pull-right">
                                <div class="col-md-12">
                                    <button id="btn-gradepercentage" class="btn btn-primary col-sm-12">
                                        Grade Percentage
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table id="tbl_ojtplacement" class="table m-t-20">
                        <thead>
                            <tr>
                                
                                <th width="">Student Number</th>
                                <th width="">Student Name</th>
                                <th width="">Company</th>
                                <th width="">Internship Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="odd">
                                <td valign="top" colspan="5" class="dataTables_empty">No data available in table</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- end tab-pane -->
            </div>
            <!-- end tab-content -->
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('assets/backend/plugins/DataTables/media/js/jquery.dataTables.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/DataTables/media/js/dataTables.bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/sweetalert/sweetalert2.min.js')); ?>"></script>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-js'); ?>
    <script>
        $(document).ready(function() {
            App.init();
            setSelectablePrograms();
            // setDataTable();
            console.log(<?php echo e($gpa); ?>);
        });

        function setDataTable() {
            // $("table[id='tbl_ojtplacement']").dataTable();
            // $("table[id='tbl_recordlist']").dataTable();

            $("table[id='tbl_ojtplacement']").dataTable({
                pagination: true,
                responsive: true
            });
            $("table[id='tbl_recordlist']").dataTable({
                pagination: true,
                responsive: true
            });
        }

        function setSelectablePrograms() {
            var programs = getPrograms();
            $("select[id='record-program']").append(programs);
            $("select[id='placement-program']").append(programs);
        }

        function getPrograms() {
            var programs = "";
            $.each(JSON.parse($('<div>').html('<?php echo e($programs); ?>')[0].textContent), function(index, value) {
                programs += '<option value="'+value["program_id"]+'">'+value["course"]+' '+value["year_section"]+'</option>';
            });
            return programs;
        }

        function getProgramsWithSelected(selected) {
            var programs = "";
            $.each(JSON.parse($('<div>').html('<?php echo e($programs); ?>')[0].textContent), function(index, value) {
                if (value["program_id"] == selected) {
                    programs += '<option value="'+value["program_id"]+'" selected>'+value["course"]+' '+value["year_section"]+'</option>';
                } else {
                    programs += '<option value="'+value["program_id"]+'">'+value["course"]+' '+value["year_section"]+'</option>';
                }
            });
            return programs;
        }

        function getGPAwithSelected(selected) {
            var gpa = "";
            $.each(JSON.parse($('<div>').html('<?php echo e($gpa); ?>')[0].textContent), function(index, value) {
                if (value == selected) {
                    gpa += '<option value="'+value+'" selected>'+value+'</option>';
                } else {
                    gpa += '<option value="'+value+'">'+value+'</option>';
                }
            });
            return gpa;
        }

        function getInternStatus(current) {
            var array = ['Enrolled','Approved','Not Qualified'];
            var status = '';
            $.each(array, function() {
                if (this == current) {
                    status += '<option value="'+this+'" selected>'+this+'</option>';
                }
                else {
                    status += '<option value="'+this+'">'+this+'</option>';
                }
            });
            return status;
        }

        function loadRecordTable() {
            var ay = $("select[id='record-ay']").val();
            var program = $("select[id='record-program']").val();
            var loading = $("#record-loading");
            var btnadd = $("button[id='btn-addrecord']");
            var btnimport = $("button[id='btn-import']");
            var table = $("table[id='tbl_recordlist'] tbody");
            var first = table.find("tr[class='odd']");
            if (ay != "--Select Academic Year--" && program != "--Select Program--") {
                loading.removeAttr('hidden');
                var data = {_token: $("meta[name='csrf-token']").attr('content'), ay_code: ay, program_id: program};
                console.log(data);
                $.ajax({
                    type: "POST",
                    url: "<?php echo e(route('intern.list')); ?>",
                    data: data,
                    success: function(data) {
                        var name = "";
                        first.nextAll().remove();
                        first.hide();
                        if (data["interns"].length > 0) {
                            $.each(data["interns"], function() {
                                //console.log(this["intern_code"] + "\n");
                                //console.log(data["active"]);
                                var buttons = '';
                                if (this["middle_name"] != null) {
                                    name = this["first_name"]+' '+this["middle_name"]+' '+this["last_name"];
                                }else {
                                    name = this["first_name"]+' '+this["last_name"];
                                }
                                if (data["record"] == "current") {
                                    if (this["intern_stat"] == "Enrolled") {
                                        buttons += '       <a onclick="recordListPreAssess(this)" title="Student Pre-assessment" class="btn btn-warning btn-sm text-white m-b-5">Pre-Assess</a>' +
                                            '       <a onclick="recordListEditUpdate(this)" title="Edit/Update" class="btn btn-primary btn-icon m-b-5"><i class="fa fa-pencil-alt text-white"></i></a>';
                                    } else {
                                        buttons += '       <a onclick="recordListView(this)" title="View Referral Code" class="btn btn-success btn-icon m-b-5"><i class="fa fa-eye text-white"></i></a>' +
                                            '       <a onclick="recordListEditUpdate(this)" title="Edit/Update" class="btn btn-primary btn-icon m-b-5"><i class="fa fa-pencil-alt text-white"></i></a>';
                                    }
                                } else {
                                    buttons = 'No Action Available';
                                }
                                table.append(
                                    '<tr id="'+this["intern_code"]+'">' +
                                    '   <td id="studno">'+this["stud_no"]+'</td>' +
                                    '   <td>'+name+'</td>' +
                                    '   <td>'+this["email"]+'</td>' +
                                    '   <td>'+this["GPA"]+'</td>' +
                                    '   <td>'+this["intern_stat"]+'</td>' +
                                    '   <td>' + buttons +
                                    //'       <a hidden onclick="recordListAccountChanges(this)" title="View Account Changes" class="btn btn-warning btn-icon"><i class="fa fa-info text-white"></i></a>' +
                                    '       <i class="fas fa-spinner fa-pulse m-l-10" hidden></i>' +
                                    '   </td>' +
                                    '</tr>');
                            });
                        }
                        else {
                            first.show();
                            if (!btnadd.attr('disabled')) {
                                btnadd.attr('disabled','disabled');
                            }
                            if (!btnimport.attr('disabled')) {
                                btnimport.attr('disabled','disabled');
                            }
                        }
                        btnadd.removeAttr('disabled');
                        btnimport.removeAttr('disabled');
                        loading.attr('hidden','hidden');
                    },
                    error: function(error) { loading.attr('hidden','hidden'); console.log(error); }
                });
            }
            else {
                first.show();
                first.nextAll().remove();
                if (!btnadd.attr('disabled')) {
                    btnadd.attr('disabled','disabled');
                }
                if (!btnimport.attr('disabled')) {
                    btnimport.attr('disabled','disabled');
                }
            }
        }

        function loadPlacementTable() {
            var ay = $("select[id='placement-ay']").val();
            var program = $("select[id='placement-program']").val();
            var loading = $("#placement-loading");
            if (ay != "--Select Academic Year--" && program != "--Select Program--") {
                loading.removeAttr('hidden');
                var data = {_token: $("meta[name='csrf-token']").attr('content'), ay_code: ay, program_id: program};
                console.log(data);
                $.ajax({
                    type: "POST",
                    url: "<?php echo e(route("intern.placement")); ?>",
                    data: data,
                    success: function(data) {
                        console.log(data);
                        var name = "";
                        var table = $("table[id='tbl_ojtplacement'] tbody");
                        var first = table.find("tr[class='odd']");
                        first.nextAll().remove();
                        first.hide();
                        if (data.interns.length > 0) {
                            for (i = 0; i < data.interns.length; i++) {
                                //console.log(data.placement[i][0]);
                                var assess = "";
                                if (data.interns[i]["middle_name"] != null) {
                                    name = data.interns[i]["first_name"]+' '+data.interns[i]["middle_name"]+' '+data.interns[i]["last_name"];
                                }else {
                                    name = data.interns[i]["first_name"]+' '+data.interns[i]["last_name"];
                                }
                                if (data.placement[i].length > 0) {
                                    if (data.placement[i][0]["su_grade"] == null) {
                                        if (data.placement[i][0]["hte_grade"] == null) {
                                            assess = '       <a onclick="javascript:;" class="btn btn-primary btn-icon btn disabled" title="Grade Assessment"><i class="fa fa-pencil-alt text-white"></i></a>';
                                        } else {
                                            assess = '       <a onclick="ojtplacementAssessment(this)" class="btn btn-primary btn-icon btn" title="Grade Assessment"><i class="fa fa-pencil-alt text-white"></i></a>';
                                        }
                                    }
                                    table.append(
                                        '<tr id="'+data.interns[i]["intern_id"]+'">' +
                                        '   <td id="ojtplacement-rowstudno">'+data.interns[i]["stud_no"]+'</td>' +
                                        '   <td id="ojtplacement-rowname">'+name+'</td>' +
                                        '   <td>'+data.placement[i][0]["hte_name"]+'</td>' +
                                        '   <td>'+data.placement[i][0]["ai_stat"]+'</td>' +
                                        '   <td>' +
                                        '       <a onclick="ojtplacementView(this)" class="btn btn-success btn-icon btn" title="View Details"><i class="fa fa-eye text-white"></i></a>' +
                                        assess +
                                        '       <i id="ojtplacement-rowloading" class="fas fa-spinner fa-pulse m-l-10" hidden></i>' +
                                        '   </td>' +
                                        '</tr>');
                                } else {
                                    table.append(
                                        '<tr id="'+data.interns[i]["intern_id"]+'">' +
                                        '   <td id="ojtplacement-rowstudno">'+data.interns[i]["stud_no"]+'</td>' +
                                        '   <td id="ojtplacement-rowname">'+name+'</td>' +
                                        '   <td>--Not Yet Available--</td>' +
                                        '   <td>'+data.interns[i]["intern_stat"]+'</td>' +
                                        '   <td>' +
                                        '       <a onclick="ojtplacementView(this)" class="btn btn-success btn-icon btn" title="View Details"><i class="fa fa-eye text-white"></i></a>' +
                                        assess +
                                        '       <i id="ojtplacement-rowloading" class="fas fa-spinner fa-pulse m-l-10" hidden></i>' +
                                        '   </td>' +
                                        '</tr>');
                                }
                            }
                        }
                        /*if (data.length > 0) {
                            $.each(data, function() {
                                //console.log(this["intern_code"] + "\n");
                                var assess = "";
                                if (this["middle_name"] != null) {
                                    name = this["first_name"]+' '+this["middle_name"]+' '+this["last_name"];
                                }else {
                                    name = this["first_name"]+' '+this["last_name"];
                                }
                                if (this["su_grade"] == null) {
                                    if (this["hte_grade"] == null) {
                                        assess = '       <a onclick="javascript:;" class="btn btn-primary btn-icon btn disabled" title="Grade Assessment"><i class="fa fa-pencil-alt text-white"></i></a>';
                                    } else {
                                        assess = '       <a onclick="ojtplacementAssessment(this)" class="btn btn-primary btn-icon btn" title="Grade Assessment"><i class="fa fa-pencil-alt text-white"></i></a>';
                                    }
                                }
                                table.append(
                                    '<tr id="'+this["ai_id"]+'">' +
                                    '   <td id="ojtplacement-rowstudno">'+this["stud_no"]+'</td>' +
                                    '   <td id="ojtplacement-rowname">'+name+'</td>' +
                                    '   <td>'+this["hte_name"]+'</td>' +
                                    '   <td>'+this["ai_stat"]+'</td>' +
                                    '   <td>' +
                                    '       <a onclick="ojtplacementView(this)" class="btn btn-success btn-icon btn" title="View Details"><i class="fa fa-eye text-white"></i></a>' +
                                           assess +
                                    '       <i id="ojtplacement-rowloading" class="fas fa-spinner fa-pulse m-l-10" hidden></i>' +
                                    '   </td>' +
                                    '</tr>');
                            });
                        }
                        else { first.show(); }*/
                        loading.attr('hidden','hidden');
                    },
                    error: function(error) {console.log("error in: "+ error)}
                });
            }
        }

        function recordListView(view) {
            //console.log($(view).closest('tr').attr('id'));
            var data = {
                _token: $("meta[name='csrf-token']").attr('content'),
                intern: $(view).closest('tr').attr('id')
            };
            console.log(data);
            var loading = $(view).siblings("i");
            loading.removeAttr('hidden','hidden');//console.log(data);
            $.ajax({
                type: "POST",
                url: "<?php echo e(route('intern.referralcode')); ?>",
                data: data,
                success: function(response) {
                    console.log(response);
                    loading.attr('hidden','hidden');
                    if (response.return) {
                        swal({
                            title: '',
                            html:
                                '<label>Referral Code:</label><br>' +
                                '<span class="f-s-20"><b>'+response.code+'</b></span>',
                            customClass: "width-sm",
                            confirmButtonClass: "btn btn-primary btn-sm",
                        });
                    } else {
                        swal({
                            title: '',
                            html: '<label>'+response.message+'</label>',
                            type: 'info',
                            customClass: 'width-md',
                            confirmButtonClass: 'btn btn-primary btn-sm'
                        });
                    }
                },
                error: function(data) {
                    console.log(data);
                    loading.attr('hidden','hidden');
                }
            });
        }

        function recordListPreAssess(assess) {
            swal({
                title: '<span class="f-s-18">Pre-Assessment</span>',
                html:
                    '<div class="row form-group m-b-10">' +
                    '   <label class="col-form-label col-md-4 text-md-right">GPA</label>' +
                    '   <div class="col-md-5">' +
                    '       <label class="col-form-label col-md-4 text-md-right"></label>' +
                    '   </div>' +
                    '</div>' +
                    '<div class="row form-group">' +
                    '   <label class="col-form-label col-md-4 text-md-right">Status</label>' +
                    '   <div class="col-md-5">' +
                    '       <select id="swal-recordstatus" class="form-control">' +
                    '           <option>--Select Status--</option>' +
                    '           <option value="Approved">Approved</option>' +
                    '           <option value="Not Qualified">Not Qualified</option>' +
                    '       </select>' +
                    '   </div>' +
                    '</div>',
                customClass: 'width-md height-210',
                confirmButtonClass: 'btn btn-sm',
                confirmButtonText: 'Submit',
                preConfirm: (submit) => {
                    var gpa = $("input[id='swal-recordgpa']").val();
                    var status = $("select[id='swal-recordstatus']").val();
                    if (gpa != "" && status != "--Select Status--") {
                        if (gpa < 1 || gpa >= 6) {
                            swal.showValidationMessage(`invalid GPA input!`);
                        } else {
                            swal.showLoading();
                            var data = {
                                _token: $("meta[name='csrf-token']").attr('content'),
                                intern: $(assess).closest("tr").attr("id"),
                                gpa: gpa,
                                status: status
                            };
                            console.log(data);
                            $.ajax({
                                type: "POST",
                                url: "<?php echo e(route("intern.preassess")); ?>",
                                data: data,
                                success: function(response) {
                                    console.log(response);
                                    swal({
                                        title: '',
                                        text: response.message,
                                        type: 'success',
                                        //confirmButtonText: 'OK',
                                    });
                                    loadRecordTable();
                                },
                                error: function(data) { console.log("error: "+data); loading.attr('hidden','hidden'); }
                            });
                        }
                    } else { swal.showValidationMessage(`Please provide the needed details!`); }
                }
            });
        }

        function recordListEditUpdate(edit) {
            var data = {_token: $("meta[name='csrf-token']").attr('content'), intern: $(edit).closest('tr').attr('id')};
            var loading = $(edit).siblings("i");
            loading.removeAttr('hidden','hidden');
            //console.log(data);
            $.ajax({
                type: "POST",
                url: "<?php echo e(route("intern.setupdate")); ?>",
                data: data,
                success: function(response) {
                    //console.log(response);
                    loading.attr('hidden','hidden');
                    var code = response.intern_code, middle_name = "", status = "", edit_stat = false, stat = "";
                    if (response.middle_name != null) {
                        middle_name = response.middle_name;
                    }
                    if (response.intern_stat == 'Approved') {
                        status =
                            '<div class="row form-group">' +
                            '   <label class="col-form-label col-md-4 f-s-16 text-md-right">Status</label>' +
                            '   <div class="col-md-7">' +
                            '       <select id="swal-recordstatus" class="form-control text-center">' +
                            '           <option>Enrolled</option>' +
                            '           <option selected>'+response.intern_stat+'</option>' +
                            '       </select>' +
                            '   </div>' +
                            '</div>';
                    } else {
                        status =
                            '<div class="row form-group" hidden>' +
                            '   <label class="col-form-label col-md-4 f-s-16 text-md-right">Status</label>' +
                            '   <div class="col-md-7">' +
                            '       <select id="swal-recordstatus" class="form-control text-center" disabled></select>' +
                            '   </div>' +
                            '</div>';
                    }
                    swal({
                        title: '<span class="f-s-18">Student Trainee</span>',
                        html:
                            '<div class="row form-group">' +
                            '   <label class="col-form-label col-md-4 f-s-16 text-md-right">Student Number</label>' +
                            '   <div class="col-md-7">' +
                            '       <input type="text" id="swal-recordstudno" placeholder="Student Number" value="'+response.stud_no+'" class="form-control" autofocus>' +
                            '   </div>' +
                            '</div>' +
                            '<div class="row form-group">' +
                            '   <label class="col-form-label col-md-4 f-s-16 text-md-right">First Name</label>' +
                            '   <div class="col-md-7">' +
                            '       <input type="text" id="swal-recordfname" placeholder="First Name" value="'+response.first_name+'" class="form-control">' +
                            '   </div>' +
                            '</div>' +
                            '<div class="row form-group">' +
                            '   <label class="col-form-label col-md-4 f-s-16 text-md-right">Middle Name</label>' +
                            '   <div class="col-md-7">' +
                            '       <input type="text" id="swal-recordmname" value="'+middle_name+'" placeholder="Middle Name" class="form-control">' +
                            '   </div>' +
                            '</div>' +
                            '<div class="row form-group">' +
                            '   <label class="col-form-label col-md-4 f-s-16 text-md-right">Last Name</label>' +
                            '   <div class="col-md-7">' +
                            '       <input type="text" id="swal-recordlname" value="'+response.last_name+'" placeholder="Last Name" class="form-control">' +
                            '   </div>' +
                            '</div>' +
                            '</div>' +
                            '<div class="row form-group">' +
                            '   <label class="col-form-label col-md-4 f-s-16 text-md-right">E-mail</label>' +
                            '   <div class="col-md-7">' +
                            '       <input type="email" id="swal-recordemail" value="'+response.contact_email+'" placeholder="Email Address" class="form-control">' +
                            '   </div>' +
                            '</div>' +
                            '<div class="row form-group">' +
                            '   <label class="col-form-label col-md-4 f-s-16 text-md-right">Program</label>' +
                            '   <div class="col-md-7">' +
                            '       <select id="swal-recordprogram" class="form-control">' +
                            '           <option>--Select Program--</option>' + getProgramsWithSelected(response.bc_id) +
                            '       </select>' +
                            '   </div>' +
                            '</div>' +
                            '<div class="row form-group">' +
                            '   <label class="col-form-label col-md-4 f-s-16 text-md-right">GPA</label>' +
                            '   <div class="col-md-7">' +
                            '       <select id="swal-recordgpa" class="form-control text-center">' +
                            '           <option></option>' + getGPAwithSelected(response.gpa) +
                            '       </select>' +
                            '   </div>' +
                            '</div>' + status,
                        showCancelButton: true,
                        confirmButtonClass: "btn btn-primary",
                        cancelButtonClass: "btn btn-secondary",
                        confirmButtonText: "Submit",
                        preConfirm: (update) => {
                            console.log(edit_stat);
                            if (response.intern_stat == 'Approved') {
                                if ($("select[id='swal-recordstatus']").val() != response.intern_stat) {
                                    edit_stat = true;
                                    stat = $("select[id='swal-recordstatus']").val();
                                }
                            }
                            data = {
                                _token: $("meta[name='csrf-token']").attr('content'),
                                code: response.intern_code,
                                studno: $("input[id='swal-recordstudno']").val(),
                                fname: $("input[id='swal-recordfname']").val(),
                                mname: $("input[id='swal-recordmname']").val(),
                                lname: $("input[id='swal-recordlname']").val(),
                                email: $("input[id='swal-recordemail']").val(),
                                program: $("select[id='swal-recordprogram']").val(),
                                gpa: $("select[id='swal-recordgpa']").val(),
                                editstat: edit_stat,
                                status: stat
                            };
                            console.log(data);
                            $.ajax({
                                type: "POST",
                                url: "<?php echo e(route('intern.update')); ?>",
                                data: data,
                                success: function(response) {
                                    console.log(response);
                                    swal({
                                        title: '',
                                        type: "success",
                                        text: response.message
                                    });
                                    loadRecordTable();
                                },
                                error: function(data) {console.log("error: "+ data); swal.showValidationMessage(`Something went wrong!`);}
                            });
                        }
                    });
                },
                error: function(data) {
                    console.log("error: "+data);
                    loading.attr('hidden','hidden');
                }
            });
        }

        function recordListAccountChanges() {
            alert("changes");
        }

        function ojtplacementView(intern) {
            var loading = $(intern).siblings("i");
            data = {_token: $("meta[name='csrf-token']").attr('content'), intern: $(intern).closest('tr').attr('id')};
            loading.removeAttr('hidden','hidden');
            console.log(data);
            $.ajax({
                type: "POST",
                url: "<?php echo e(route('intern.ojtdetails')); ?>",
                data: data,
                success: function(response) {
                    console.log(response);
                    loading.attr('hidden','hidden');
                    var name = '';
                    var program = response.course_abbrv + ' ' + response.year_section;

                    if (response.middle_name != null) { name = response.last_name + ', ' + response.first_name + ' ' + response.middle_name; }
                    else { name = response.last_name + ', ' + response.first_name; }

                    swal({
                        title: '',
                        html:
                            '<div class="row m-b-10">' +
                            '   <div class="width-100 m-l-25 m-r-5">' +
                            '       <img class="width-100 height-100">' +
                            '   </div>' +
                            '   <div class="col-md-"7>' +
                            '       <div class="row">' +
                            '           <label class="col-form-label">'+response.stud_no+'</lable>' +
                            '       </div>' +
                            '       <div class="row">' +
                            '           <label class="col-form-label f-s-15">'+name+'</lable>' +
                            '       </div>' +
                            '       <div class="row">' +
                            '           <label class="col-form-label f-s-15">'+program+'</label>' +
                            '       </div>' +
                            '       <div class="row">'  +
                            '           <div class="col-md-4">' +
                            '               <label class="label label-primary p-t-5">'+response.intern_stat+'</label>' +
                            '           </div>' +
                            '       </div>' +
                            '   </div>' +
                            '</div>' +
                            '<legend>' +
                            '   <span class="f-s-18">OJT Placement Details</span>' +
                            '</legend>' +
                            '<div class="row m-b-10">' +
                            '   <div class="col-md-3">' +
                            '       <div class="row m-l-10 pull-left">' +
                            '           <span class="f-s-14">Company Name</span>' +
                            '       </div>' +
                            '   </div>' +
                            '   <div class="col-md-8">' +
                            '       <div class="row pull-left">' +
                            '           <label class="col-form-label f-s-14">'+response.hte_name+'</label>' +
                            '       </div>' +
                            '   </div>' +
                            '</div>' +
                            '<div class="row m-b-10">' +
                            '   <div class="col-md-3">' +
                            '       <div class="row m-l-10 pull-left">' +
                            '           <span class="f-s-14">Address</span>' +
                            '       </div>' +
                            '   </div>' +
                            '   <div class="col-md-8">' +
                            '       <div class="row pull-left">' +
                            '           <label class="col-form-label f-s-14">'+response.address+'</label>' +
                            '       </div>' +
                            '   </div>' +
                            '</div>' +
                            '<div class="row m-b-10">' +
                            '   <div class="col-md-3">' +
                            '       <div class="row m-l-10 pull-left">' +
                            '           <span class="f-s-14">Contact Person</span>' +
                            '       </div>' +
                            '   </div>' +
                            '   <div class="col-md-8">' +
                            '       <div class="row pull-left">' +
                            '           <label class="col-form-label f-s-14">'+response.contact_person+'</label>' +
                            '       </div>' +
                            '   </div>' +
                            '</div>' +
                            '<div class="row m-b-10">' +
                            '   <div class="col-md-3">' +
                            '       <div class="row m-l-10 pull-left">' +
                            '           <span class="f-s-14">Designation</span>' +
                            '       </div>' +
                            '   </div>' +
                            '   <div class="col-md-8">' +
                            '       <div class="row pull-left">' +
                            '           <label class="col-form-label f-s-14">'+response.designation+'</label>' +
                            '       </div>' +
                            '   </div>' +
                            '</div>' +
                            '<div class="row m-b-10">' +
                            '   <div class="col-md-3">' +
                            '       <div class="row m-l-10 pull-left">' +
                            '           <span class="f-s-14">Contact Number</span>' +
                            '       </div>' +
                            '   </div>' +
                            '   <div class="col-md-8">' +
                            '       <div class="row pull-left">' +
                            '           <label class="col-form-label f-s-14">'+response.contact_no+'</label>' +
                            '       </div>' +
                            '   </div>' +
                            '</div>',
                        customClass: 'width-550',
                        showConfirmButton: false,
                        showCloseButton: true,
                    });
                },
                error: function(data) {console.log("error:"+data); loading.attr('hidden','hidden');}
            });
        }

        function ojtplacementAssessment(intern) {
            var details = [
                $(intern).closest("tr").find("td[id='ojtplacement-rowstudno']")[0].innerText,
                $(intern).closest("tr").find("td[id='ojtplacement-rowname']")[0].innerText,
                $("select[id='placement-program'] option[value='"+$("select[id='placement-program']").val()+"']").text()
            ];
            console.log($("select[id='placement-program'] option[value='"+$("select[id='placement-program']").val()+"']").text());
            swal({
                title: '',
                html:
                    '<div class="row m-l-30">' +
                    '   <label class="f-s-13">'+details[0]+'</label>' +
                    '</div>' +
                    '<div class="row m-l-30">' +
                    '   <label class="f-s-13">'+details[1]+'</label>' +
                    '</div>' +
                    '<div class="row m-l-30">' +
                    '   <label class="f-s-13">'+details[2]+'</label>' +
                    '</div>' +
                    '<legend>' +
                    '   <span class="f-s-18">Feedback Assessment</span>' +
                    '</legend>' +
                    '<div class="row form-group m-b-10">' +
                    '   <div class="col-md-2"></div>' +
                    '   <label class="col-form-label f-s-13 col-md-2">Grade</label>' +
                    '   <div class="col-md-3">' +
                    '       <input id="swal-placementgrade" type="number" class="form-control col-sm-12">' +
                    '   </div>' +
                    '</div>' +
                    '<div class="row form-group">' +
                    '   <div class="col-md-2"></div>' +
                    '   <label class="col-form-label f-s-13 col-md-2">Remarks</label>' +
                    '   <div class="col-md-5">' +
                    '       <textarea id="swal-placementremarks" rows="3" class="form-control"></textarea>' +
                    '   </div>' +
                    '</div>',
                customClass: "width-500",
                confirmButtonClass: "btn btn-primary btn-sm",
                confirmButtonText: "Submit",
                preConfirm: (validate) => {
                    if ($("input[id='swal-placementgrade']").val() == "") {
                        swal.showValidationMessage(`Please input the student trainee grade!`)
                    }
                    else {
                        var data = {
                            _token: $("meta[name='csrf-token']").attr('content'),
                            intern: $(intern).closest('tr').attr('id'),
                            grade: $("input[id='swal-placementgrade']").val(),
                            remarks: $("textarea[id='swal-placementremarks']").val()
                        };
                        console.log(data);
                        $.ajax({
                            type: 'POST',
                            url: '<?php echo e(route("intern.assessment")); ?>',
                            data: data,
                            success: function(response) {
                                console.log(response)
                                swal({
                                    title: '',
                                    type: 'success',
                                    text: response,
                                    customClass: 'width-sm'
                                })
                            },
                            error: function(data) {
                                console.log("error:"+data);
                                swal.showValidationMessage(`Something went wrong!`)
                            }
                        });
                    }
                },
            });
        }

        $("select[id='record-ay']").change(function() {
            loadRecordTable();
        });

        $("select[id='record-program']").change(function() {
            loadRecordTable();
        });

        $("select[id='placement-ay']").change(function() {
            loadPlacementTable();
        })

        $("select[id='placement-program']").change(function() {
            loadPlacementTable();
        })

        $("#btn-addrecord").on('click',function() {
            var ay = $("select[id='record-ay']").val();
            //console.log(ay);
            swal({
                title: '<span class="f-s-18">Student Trainee</span>',
                html:
                    '<div class="row form-group">' +
                    '   <label class="col-form-label col-md-4 f-s-16 text-md-right">Student Number</label>' +
                    '   <div class="col-md-7">' +
                    '       <input type="text" id="swal-recordstudno" placeholder="Student Number" class="form-control" autofocus>' +
                    '   </div>' +
                    '</div>' +
                    '<div class="row form-group">' +
                    '   <label class="col-form-label col-md-4 f-s-16 text-md-right">First Name</label>' +
                    '   <div class="col-md-7">' +
                    '       <input type="text" id="swal-recordfname" placeholder="First Name" class="form-control">' +
                    '   </div>' +
                    '</div>' +
                    '<div class="row form-group">' +
                    '   <label class="col-form-label col-md-4 f-s-16 text-md-right">Middle Name</label>' +
                    '   <div class="col-md-7">' +
                    '       <input type="text" id="swal-recordmname" placeholder="Middle Name" class="form-control">' +
                    '   </div>' +
                    '</div>' +
                    '<div class="row form-group">' +
                    '   <label class="col-form-label col-md-4 f-s-16 text-md-right">Last Name</label>' +
                    '   <div class="col-md-7">' +
                    '       <input type="text" id="swal-recordlname" placeholder="Last Name" class="form-control">' +
                    '   </div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="row form-group">' +
                    '   <label class="col-form-label col-md-4 f-s-16 text-md-right">E-mail</label>' +
                    '   <div class="col-md-7">' +
                    '       <input type="email" id="swal-recordemail" placeholder="Email Address" class="form-control">' +
                    '   </div>' +
                    '</div>',
                showCancelButton: true,
                confirmButtonClass: "btn btn-primary",
                cancelButtonClass: "btn btn-secondary",
                confirmButtonText: "Submit",

            }).then((submit) => {
                if (submit.value) {
                    var data = {
                        _token: $("meta[name='csrf-token']").attr('content'),
                        ay: ay,
                        studno: $("input[id='swal-recordstudno']").val(),
                        fname: $("input[id='swal-recordfname']").val(),
                        mname: $("input[id='swal-recordmname']").val(),
                        lname: $("input[id='swal-recordlname']").val(),
                        email: $("input[id='swal-recordemail']").val(),
                        program: $("select[id='record-program']").val()
                    };
                    //console.log(data);
                    $.ajax({
                        type: "POST",
                        url: "<?php echo e(route('intern.add')); ?>",
                        data: data,
                        success: function(data) {
                            swal({
                                title: '<span class="f-s-18">'+data+'</span>',
                                type: 'success'
                            });
                            loadRecordTable();
                        },
                        error: function(data) {console.log("error: "+data)}
                    });
                }
            });
        });

        $("#btn-import").on('click',function() {
            swal({
                title: '<span class="f-s-18">Student Trainee</span>',
                html:
                    '<div class="row form-group">' +
                    '   <div class="col-md-2"></div>' +
                    '   <label class="col-form-label col-md-8 f-s-14 m-b-5"><a href="javascript:;">Download excel format for import</a></label>' +
                    '</div>' +
                    '<div class="row form-group">' +
                    '   <label class="col-form-label col-md-3 f-s-16 text-md-right">Select File</label>' +
                    '   <div class="col-md-8">' +
                    '       <input type="file" id="swal-recordfile" name="swal-recordfile" class="form-control" autofocus required>' +
                    '   </div>' +
                    '</div>'/* +
                    '<div class="row">' +
                    '   <div class="col-md-3">' +
                    '       <button type="submit" class="btn btn-primary">Import</button>' +
                    '   </div>' +
                    '</div>'*/,
                /*showCloseButton: true,
                showConfirmButton: false,*/
                showCancelButton: true,
                confirmButtonClass: "btn btn-primary",
                cancelButtonClass: "btn btn-secondary",
                confirmButtonText: "Import",
                preConfirm: (result) => {
                    var file = $("input[id='swal-recordfile']").prop('files');
                    if (file.length == 0) {
                        swal.showValidationMessage(
                            `No excel file selected!`
                        )
                    }
                    else {
                        var formData = new FormData();
                        formData.append("_token", $("meta[name='csrf-token']").attr('content'));
                        formData.append('excel',file[0]);
                        formData.append("ay", $("select[id='record-ay']").val());
                        formData.append("program", $("select[id='record-program']").val());
                        /*{
                            _token: $("meta[name='csrf-token']").attr('content'),
                                ay: $("select[id='record-ay']").val(),
                            program: $("select[id='record-program']").val(),
                        }*/
                        console.log(file);
                        $.ajax({
                            type: "POST",
                            url: "<?php echo e(route("intern.import")); ?>",
                            processData: false,
                            contentType: false,
                            cache: false,
                            data: formData,
                            enctype: "multipart/form-data",
                            success: function(response) {
                                console.log(response);
                                if (response.result == "success") {
                                    swal({
                                        title: '<span class="f-s-18">'+response.message+'</span>',
                                        type: 'success'
                                    });
                                }
                                loadRecordTable();
                            },
                            error: function(data) {
                                console.log("error: "+data);
                            }
                        });
                    }
                }
            })
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.users', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>