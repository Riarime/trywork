<?php $__env->startSection('title', 'Complete Job Training List'); ?>

<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(asset('assets/backend/plugins/jquery-jvectormap/jquery-jvectormap.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/plugins/bootstrap-calendar/css/bootstrap_calendar.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/plugins/gritter/css/jquery.gritter.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/backend/plugins/nvd3/build/nv.d3.css')); ?>" rel="stylesheet" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.2/d3.min.js"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/nvd3/build/nv.d3.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/jquery-jvectormap/jquery-jvectormap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/jquery-jvectormap/jquery-jvectormap-world-merc-en.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/bootstrap-calendar/js/bootstrap_calendar.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/plugins/gritter/js/jquery.gritter.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/js/demo/dashboard-v2.min.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('document.ready'); ?>
    handleVisitorsAreaChart();
    handleVisitorsDonutChart();
    handleVisitorsVectorMap();
    handleScheduleCalendar();

    
    
    
    
    
    
    
    
    
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <!-- begin col-6 -->
        <div class="col-md-12">
            <div>
                <h4>All Job Training Listings</h4>
            </div>
            <div class="row">
                <?php $__currentLoopData = $ojts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ojt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-4">
                        <!-- begin card -->
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title"><?php echo e($ojt->ojtname); ?><br/><small><?php echo e($ojt->company_name); ?></small></h4>
                                <p class="card-text"><?php echo e($ojt->description); ?></p>
                                <button class="btn btn-primary">Details</button>
                                <p class="card-text" style="text-align: right"><small class="text-muted">Start Date: <?php echo e($ojt->date); ?></small></p>
                            </div>
                        </div>
                        <!-- end card -->
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master-backend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>