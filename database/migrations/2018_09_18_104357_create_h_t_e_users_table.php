<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHTEUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('r_hte_users', function (Blueprint $table) {
            $table->increments('hu_id');
            $table->unsignedInteger('hte_id');
            $table->foreign('hte_id','FK_hteusrs_hte_id_rfrnc')
                ->references('hte_id')->on('r_hte')
                ->onUpdate('cascade');
            $table->unsignedInteger('ur_id');
            $table->foreign('ur_id', 'FK_hteusrs_ur_id_rfrnc')
                ->references('ur_id')->on('user_roles')
                ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('r_hte_users');
    }
}
