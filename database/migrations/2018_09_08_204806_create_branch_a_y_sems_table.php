<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchAYSemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('r_branch_active_ay_sem', function (Blueprint $table) {
            $table->increments('aysem_id');
            $table->unsignedInteger('branch_id');
            $table->foreign('branch_id','FK_braysem_branch_id_rfrnc')
                ->references('branch_id')->on('r_su_branches')
                ->onUpdate('cascade');
            $table->unsignedInteger('ay_id');
            $table->foreign('ay_id','FK_braysem_ay_id_rfrnc')
                ->references('ay_id')->on('r_branch_acdmcyr')
                ->onUpdate('cascade');
            $table->unsignedInteger('sem_id');
            $table->foreign('sem_id','FK_braysem_sem_id_rfrnc')
                ->references('sem_id')->on('r_branch_sem')
                ->onUpdate('cascade');
            $table->enum('aysem_stat',['Active','Inactive'])->default('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('r_branch_active_ay_sem');
    }
}
