<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHTEsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('r_hte', function (Blueprint $table) {
            $table->increments('hte_id')->unsigned();
            $table->string('hte_code',25)->unique();
            $table->text('hte_name');
            $table->text('mission')->nullable();
            $table->text('vision')->nullable();
            $table->text('objectives')->nullable();
            $table->text('address')->nullable();
            $table->unsignedInteger('region_id')->nullable();
            $table->foreign('region_id','FK_hte_region_id_rfrnc')
                ->references('region_id')->on('r_regions')
                ->onUpdate('cascade');
            $table->enum('hte_stat',['Active','Inactive'])->default('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('r_hte');
    }
}
