<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnivUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('r_su_users', function (Blueprint $table) {
            $table->increments('uu_id');
            $table->unsignedInteger('branch_id')->nullable();
            $table->foreign('branch_id','FK_susr_branch_id_rfrnc')
                ->references('branch_id')->on('r_su_branches')
                ->onUpdate('cascade');
            $table->foreign('su_id');
            $table->foreign('su_id','FK_susr_su_id_rfrnc')
                ->references('su_id')->on('r_su')
                ->onUpdate('cascade');
            $table->unsignedInteger('ur_id');
            $table->foreign('ur_id','FK_susr_ur_id_rfrnc')
                ->references('ur_id')->on('user_roles')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('r_su_users');
    }
}
