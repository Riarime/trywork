<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnivUserEducBackgroundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('r_su_usr_educbg', function (Blueprint $table) {
            $table->unsignedInteger('uu_id');
            $table->foreign('uu_id','FK_susredcbg_uu_id_rfrnc')
                ->references('uu_id')->on('r_su_users')
                ->onUpdate('cascade');
            $table->enum('degree',["Bachelor\'s Degree","Master\'s Degree","Doctorate Degree"])->nullable();
            $table->text('school_graduated')->nullable();
            $table->text('year_graduated')->nullable();
            $table->text('received_awards')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('r_su_usr_educbg');
    }
}
