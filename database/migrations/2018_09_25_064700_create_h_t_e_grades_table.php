<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHTEGradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_hte_grade', function (Blueprint $table) {
            $table->unsignedInteger('grading_id');
            $table->foreign('grading_id','')
                ->references('grading_id')->on('t_intern_grading')
                ->onUpdate('cascade');
            $table->unsignedInteger('hte_id');
            $table->foreign('hte_id', 'FK_htegrd_hte_id_rfrnc')
                ->references('hte_id')->on('r_hte')
                ->onUpdate('cascade');
            $table->unsignedInteger('hu_id')->nullable();
            $table->foreign('hu_id', 'FK_htegrd_hu_id_rfrnc')
                ->references('hu_id')->on('r_hte_users')
                ->onUpdate('cascade');
            $table->double('intern_grade', 100,2);
            $table->text('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('h_t_e_grades');
    }
}
