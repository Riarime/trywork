<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('r_branch_courses', function (Blueprint $table) {
            $table->increments('bc_id');
            $table->unsignedInteger('branch_id');
            $table->foreign('branch_id', 'FK_brnchcrs_branch_id_rfrnc')
                ->references('branch_id')->on('r_su_branches')
                ->onUpdate('cascade');
            $table->unsignedInteger('uu_id');
            $table->foreign('uu_id','FK_brnchcrs_uu_id_rfrnc')
                ->references('uu_id')->on('r_su_users')
                ->onUpdate('cascade');
            $table->unsignedInteger('course_id');
            $table->foreign('course_id','FK_brnchcrs_course_id_rfrnc')
                ->references('course_id')->on('r_su_college_courses')
                ->onUpdate('cascade');
            $table->text('year_section');
            $table->enum('bc_stat',['Active','Inactive'])->default('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('r_branch_courses');
    }
}
