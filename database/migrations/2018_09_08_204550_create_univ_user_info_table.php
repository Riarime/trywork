<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnivUserInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('r_su_user_info', function (Blueprint $table) {
            $table->unsignedInteger('uu_id');
            $table->foreign('uu_id','FK_usrcllg_uu_id_rfrnc')
                ->references('uu_id')->on('r_su_users')
                ->onUpdate('cascade');
            $table->unsignedInteger('college_id')->nullable();
            $table->foreign('college_id','FK_usrcllg_college_id_rfrnc')
                ->references('college_id')->on('r_su_colleges')
                ->onUpdate('cascade');
            $table->unsignedInteger('rank_id')->nullable();
            $table->foreign('rank_id','FK_usrcllg_rank_id_rfrnc')
                ->references('rank_id')->on('r_su_academic_rank')
                ->onUpdate('cascade');
            $table->enum('emp_stat',['Contractual','Temporary','Permanent']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('r_su_user_info');
    }
}
