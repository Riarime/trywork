<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollegeCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('r_su_college_courses', function (Blueprint $table) {
            $table->increments('course_id');
            $table->unsignedInteger('college_id');
            $table->foreign('college_id','FK_sucrs_college_id_rfrnc')
                ->references('college_id')->on('r_su_colleges')
                ->onUpdate('cascade');
            $table->string('course_code',25)->unique();
            $table->text('course_abbrv');
            $table->text('course_name');
            $table->text('course_desc')->nullable();
            $table->enum('course_stat',['Active','Inactive'])->default('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('r_su_college_courses');
    }
}
