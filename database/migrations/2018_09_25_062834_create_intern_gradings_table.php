<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInternGradingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_intern_grading', function (Blueprint $table) {
            $table->increments('grading_id');
            $table->string('grading_code')->unique()->nullable();
            $table->unsignedInteger('intern_id');
            $table->foreign('intern_id','FK_intrngrdng_intern_id_rfrnc')
                ->references('intern_id')->on('r_su_intern')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_intern_grading');
    }
}
