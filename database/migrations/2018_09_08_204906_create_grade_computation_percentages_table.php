<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGradeComputationPercentagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('r_grade_percentage', function (Blueprint $table) {
            $table->increments('gp_id');
            $table->unsignedInteger('uu_id');
            $table->foreign('uu_id','FK_gp_uu_id_rfrnc')
                ->references('uu_id')->on('r_su_users')
                ->onUpdate('cascade');
            $table->double('college_percentage',2,0)->default(50);
            $table->double('hte_percentage',2,0)->default(50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('r_grade_percentage');
    }
}
