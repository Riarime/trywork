<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnivCollegesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('r_su_colleges', function (Blueprint $table) {
            $table->increments('college_id');
            $table->unsignedInteger('su_id');
            $table->foreign('su_id','FK_sucllgs_su_id_rfrnc')
                ->references('su_id')->on('r_su')
                ->onUpdate('cascade');
            $table->string('college_code')->unique();
            $table->text('college_abbrv');
            $table->text('college_name');
            $table->enum('college_stat',['Active','Inactive'])->default('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('r_su_colleges');
    }
}
