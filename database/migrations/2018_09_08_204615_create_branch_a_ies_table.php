<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchAIesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('r_branch_acdmcyr', function (Blueprint $table) {
            $table->increments('ay_id');
            $table->unsignedInteger('branch_id');
            $table->foreign('branch_id','FK_bracdyr_branch_id_rfrnc')
                ->references('branch_id')->on('r_su_branches')
                ->onUpdate('cascade');
            $table->string('ay_code',20)->unique();
            $table->string('academic_yr',25);
            $table->enum('ay_stat',['Active','Inactive'])->default('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('r_branch_acdmcyr');
    }
}
