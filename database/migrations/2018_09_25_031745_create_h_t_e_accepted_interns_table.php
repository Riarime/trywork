<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHTEAcceptedInternsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_hte_accepted_intern', function (Blueprint $table) {
            $table->increments('ai_id');
            $table->unsignedInteger('hte_id');
            $table->foreign('hte_id','FK_hteaccptdintrn_hte_id_rfrnc')
                ->references('hte_id')->on('r_hte')
                ->onUpdate('cascade');
            $table->unsignedInteger('intern_id');
            $table->foreign('intern_id','FK_hteaccptdintrn_intern_id_rfrnc')
                ->references('intern_id')->on('r_su_intern')
                ->onUpdate('cascade');
            $table->unsignedInteger('job_training_id')->nullable();
            $table->foreign('job_training_id','FK_hteaccptdintrn_job_training_id_rfrnc')
                ->references('job_training_id')->on('r_hte_job_trainings')
                ->onUpdate('cascade');
            $table->enum('su_grade',['Complete'])->nullable();
            $table->enum('hte_grade',['Complete'])->nullable();
            $table->enum('ai_stat',['On Going','Finished'])->default('On Going');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_hte_accepted_intern');
    }
}
