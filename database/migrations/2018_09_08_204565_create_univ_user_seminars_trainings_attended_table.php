<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnivUserSeminarsTrainingsAttendedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('r_seminar_trainings_attended', function (Blueprint $table) {
            $table->unsignedInteger('uu_id');
            $table->foreign('uu_id','FK_sta_uu_id_rfrnc')
                ->references('uu_id')->on('r_su_users')
                ->onUpdate('cascade');
            $table->text('title');
            $table->text('agency_sponsor');
            $table->enum('seminar_level',['International','Regional','Local']);
            $table->date('seminar_date');
            $table->string('seminar_venue');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('r_seminar_trainings_attended');
    }
}
