<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnivHTEsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_su_hte', function (Blueprint $table) {
            $table->increments('sh_id');
            $table->unsignedInteger('branch_id');
            $table->foreign('branch_id','suh_branch_id_rfrnc')
                ->references('branch_id')->on('r_su_branches')
                ->onUpdate('cascade');
            $table->unsignedInteger('college_id');
            $table->foreign('college_id','FK_suhte_college_id_rfrnc')
                ->references('college_id')->on('r_su_colleges')
                ->onUpdate('cascade');
            $table->unsignedInteger('hte_id');
            $table->foreign('hte_id','suh_hte_id_rfrnc')
                ->references('hte_id')->on('r_hte')
                ->onUpdate('cascade');
            $table->enum('affiliated_status',['Active','Inactive'])->default('Active');
            $table->text('moa_path')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_su_hte');
    }
}
