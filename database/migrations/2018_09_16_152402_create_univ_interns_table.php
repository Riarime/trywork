<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnivInternsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('r_su_intern', function (Blueprint $table) {
            $table->increments('intern_id');
            $table->unsignedInteger('branch_id');
            $table->foreign('branch_id','FK_intrn_branch_id_rfrnc')
                ->references('branch_id')->on('r_su_branches')
                ->onUpdate('cascade');
            $table->unsignedInteger('ay_id');
            $table->foreign('ay_id', 'FK_intrn_ay_id_rfrnc')
                ->references('ay_id')->on('r_branch_acdmcyr')
                ->onUpdate('cascade');
            $table->unsignedInteger('bc_id');
            $table->foreign('bc_id','FK_intrn_course_id_rfrnc')
                ->references('course_id')->on('r_su_college_courses')
                ->onUpdate('cascade');
            $table->unsignedInteger('info_id');
            $table->foreign('info_id','FK_intrn_info_id_rfrnc')
                ->references('info_id')->on('user_infos')
                ->onUpdate('cascade');
            $table->string('intern_code',20)->unique();
            $table->string('stud_no', 25);
            $table->double('GPA',5,3)->nullable();
            $table->string('contact_email');
            $table->text('intern_address')->nullable();
            $table->unsignedInteger('region_id')->nullable();
            $table->foreign('region_id','FK_intrn_region_id_rfrnc')
                ->references('region_id')->on('r_regions')
                ->onUpdate('cascade');
            $table->enum('intern_stat',['Enrolled','Approved','Not Qualified','Internship','Completed'])->default('Enrolled');
            $table->unsignedInteger('su_usr_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('r_su_intern');
    }
}
