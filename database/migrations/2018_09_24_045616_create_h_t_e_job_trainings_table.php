<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHTEJobTrainingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('r_hte_job_trainings', function (Blueprint $table) {
            $table->increments('job_training_id');
            $table->unsignedInteger('hte_id');
            $table->foreign('hte_id','hteojt_hte_id_rfrnc')
                ->references('hte_id')->on('r_hte')
                ->onUpdate('cascade');
            $table->string('training_code', 25);
            $table->text('job_training_name');
            $table->text('description');
            $table->date('start');
            $table->enum('job_training_stat', ['Active','Inactive'])->default('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('r_hte_job_trainings');
    }
}
