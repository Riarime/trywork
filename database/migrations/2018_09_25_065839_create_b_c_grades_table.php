<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBCGradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_bc_grades', function (Blueprint $table) {
            $table->unsignedInteger('grading_id');
            $table->foreign('grading_id','FK_bcgrades_grading_id_rfrnc')
                ->references('grading_id')->on('t_intern_grading')
                ->onUpdate('cascade');
            $table->unsignedInteger('branch_id');
            $table->foreign('branch_id','FK_bcgrades_branch_id_rfrnc')
                ->references('branch_id')->on('r_su_branches')
                ->onUpdate('cascade');
            $table->unsignedInteger('uu_id')->nullable();
            $table->foreign('uu_id','FK_bcgrades_uu_id_rfrnc')
                ->references('uu_id')->on('r_su_users')
                ->onUpdate('cascade');
            $table->double('intern_grade',100,2);
            $table->text('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_bc_grades');
    }
}
