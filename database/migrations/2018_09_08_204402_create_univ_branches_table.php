<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnivBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('r_su_branches', function (Blueprint $table) {
            $table->increments('branch_id');
            $table->unsignedInteger('su_id');
            $table->foreign('su_id','FK_subrnch_su_id_rfrnc')
                ->references('su_id')->on('r_su')
                ->onUpdate('cascade');
            $table->string('branch_code',50)->unique();
            $table->text('branch_abbrv');
            $table->text('branch_name');
            $table->text('branch_address');
            $table->text('branch_add_zipcode');
            $table->enum('branch_stat',['Active','Inactive'])->default('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('r_su_branches');
    }
}
