<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInternSkillSetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('r_intern_skill_sets', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('intern_id');
            $table->foreign('intern_id','FK_intrnskllsst')
                ->references('intern_id')->on('r_su_intern')
                ->onUpdate('cascade');
            $table->unsignedInteger('skill_id');
            $table->foreign('skill_id','FK_skllskllsst')
                ->references('skill_id')->on('r_intern_skills')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('intern_skill_sets');
    }
}
