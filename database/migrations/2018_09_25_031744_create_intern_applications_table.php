<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInternApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_intern_application', function (Blueprint $table) {
            $table->increments('iapp_id');
            $table->unsignedInteger('intern_id');
            $table->foreign('intern_id','FK_iapp-intern_id_rfrnc')
                ->references('intern_id')->on('r_su_intern')
                ->onUpdate('cascade');
            $table->unsignedInteger('job_training_id');
            $table->foreign('job_training_id','')
                ->references('job_training_id')->on('r_hte_job_trainings')
                ->onUpdate('cascade');
            $table->unsignedInteger('iappstat_id');
            $table->foreign('iappstat_id','FK_iapp_iappstat_id_rfrnc')
                ->references('iappstat_id')->on('r_intern_application_status')
                ->onUpdate('cascade');
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_intern_application');
    }
}
