<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferralCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('r_intern_referral', function (Blueprint $table) {
            $table->unsignedInteger('intern_id');
            $table->foreign('intern_id','FK_intrnrfrrl_intern_id_rfrnc')
                ->references('intern_id')->on('r_su_intern')
                ->onUpdate('cascade');
            $table->string('referral_code',100)->unique();
            $table->enum('Status',['Used'])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('r_intern_referral');
    }
}
