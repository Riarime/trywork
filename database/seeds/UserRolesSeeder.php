<?php

use Illuminate\Database\Seeder;

class UserRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $UserRole_inputs = [
            ['user_id' => 1, 'role_id' => 4, 'info_id' => 1],
            ['user_id' => 2, 'role_id' => 3, 'info_id' => 2],
            ['user_id' => 3, 'role_id' => 2, 'info_id' => 3]
        ];

        foreach($UserRole_inputs as $input) {
            $UserRole = new \App\UserRole();
            $UserRole->user_id = $input['user_id'];
            $UserRole->role_id = $input['role_id'];
            $UserRole->info_id = $input['info_id'];
            $UserRole->save();
        }
    }
}
