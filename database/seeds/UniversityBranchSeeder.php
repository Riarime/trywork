<?php

use Illuminate\Database\Seeder;
use App\StateUniversity;
use App\UnivBranches;

class UniversityBranchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $branch_inputs = [
            ['su_id' => 1,'branch_abbrv' => 'BSUM1','branch_name' => 'Batangas State University Pablo Borbon Main Campus I','branch_address' => 'Gov. Pablo Borbon, Main Campus I,Rizal Ave., Batangas City','branch_zipcode' => '1234']
        ];

        foreach($branch_inputs as $input) {
            $su = (StateUniversity::where('su_id','=',$input['su_id'])->get()->pluck('su_abbrv'));
            $branch = new \App\UnivBranches();
            $branch->su_id = $input['su_id'];
            $branch->branch_code = $su[0].'-CB'.(UnivBranches::where('su_id','=',$input['su_id'])->get()->count() + 1);
            $branch->branch_abbrv = $input['branch_abbrv'];
            $branch->branch_name = $input['branch_name'];
            $branch->branch_address = $input['branch_address'];
            $branch->branch_add_zipcode = $input['branch_zipcode'];
            $branch->save();
        }
    }
}
