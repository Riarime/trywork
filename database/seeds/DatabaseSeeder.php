<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesSeeder::class);
        $this->call(RegionSeeder::class);
        $this->call(AcademicRankSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(UserInfoSeeder::class);
        $this->call(UserRolesSeeder::class);
        $this->call(StateUniversitySeeder::class);
        $this->call(UniversityBranchSeeder::class);
        $this->call(UniversityCollegesSeeder::class);
        $this->call(CollegeCoursesSeeder::class);
        $this->call(UniversityUsersSeeder::class);
        $this->call(BranchCourseSeeder::class);
        $this->call(BranchAYSeeder::class);
        $this->call(BranchSemSeeder::class);
        $this->call(BranchAYSemSeeder::class);
        $this->call(GPASeeder::class);
        $this->call(HTESeeder::class);
        $this->call(UnivHTESeeder::class);
        $this->call(UnivInternSeeder::class);
        $this->call(InternAppStatusSeeder::class);
        $this->call(HTEAcceptedInternSeeder::class);
        //$this->call(InternAccountSeeder::class);
        $this->call(InternSkillsSeeder::class);
        //$this->call(HTEAccountsSeeder::class);
    }
}
