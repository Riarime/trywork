<?php

use Illuminate\Database\Seeder;
use App\UnivUserAcademicRank;
use Carbon\Carbon;

class AcademicRankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $rank_inputs = [
            ['name' => 'Instructor I]'],
            ['name' => 'Instructor II'],
            ['name' => 'Instructor III'],
            ['name' => 'Assisstant Professor I'],
            ['name' => 'Assisstant Professor II'],
            ['name' => 'Assisstant Professor III'],
            ['name' => 'Assisstant Professor IV'],
            ['name' => 'Associate Professor I'],
            ['name' => 'Associate Professor II'],
            ['name' => 'Associate Professor III'],
            ['name' => 'Associate Professor IV'],
            ['name' => 'Associate Professor V'],
            ['name' => 'Professor I'],
            ['name' => 'Professor II'],
            ['name' => 'Professor III'],
            ['name' => 'Professor IV'],
            ['name' => 'Professor V'],
            ['name' => 'Professor VI'],
            ['name' => 'Professor VII'],
            ['name' => 'College/University Professor']
        ];

        foreach($rank_inputs as $input) {
            $rank = new \App\UnivUserAcademicRank();
            $rank->code = 'TW'.(Carbon::now()->format('y-')).'AR'.(UnivUserAcademicRank::get()->count() + 1);
            $rank->rank_name = $input['name'];
            $rank->save();
        }
    }
}
