<?php

use Illuminate\Database\Seeder;

class UniversityUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_input = [
            ['branch_id' => 1,'ur_id' => 1,'college_id' => 1,'academic_rank' => 20,'emp_stat' => 'Permanent','degree' => 'Master\'s Degree','school_graduated' => null,'year_graduated' => null,'received_awards' => null],
            ['branch_id' => 1,'ur_id' => 2,'college_id' => null,'academic_rank' => 4,'emp_stat' => 'Permanent','degree' => 'Master\'s Degree','school_graduated' => null,'year_graduated' => null,'received_awards' => null],
            ['branch_id' => 1,'ur_id' => 3,'college_id' => null,'academic_rank' => null,'emp_stat' => 'Permanent','degree' => 'Master\'s Degree','school_graduated' => null,'year_graduated' => null,'received_awards' => null],
        ];

        foreach($user_input as $input) {
            $branch = $input['branch_id'];

            $user = new \App\UnivUsers();
            $user->branch_id = $branch;
            $user->ur_id = $input['ur_id'];
            $user->save();
            $id = $user->id;

            $user_info = new \App\UnivUserInfo();
            $user_info->uu_id = $id;
            $user_info->college_id = $input['college_id'];
            $user_info->rank_id = $input['academic_rank'];
            $user_info->emp_stat = $input['emp_stat'];
            $user_info->save();

            $user_educ_bg = new \App\UnivUserEducBackground();
            $user_educ_bg->uu_id = $id;
            $user_educ_bg->degree = $input['degree'];
            $user_educ_bg->school_graduated = 'null';     //$input['school_graduated'];
            $user_educ_bg->year_graduated = 'null';   //$input['year_graduated'];
            $user_educ_bg->received_awards = 'null';  //$input['received_awards'];
            $user_educ_bg->save();

            $grade_computation = new \App\GradeComputationPercentage();
            $grade_computation->uu_id = $id;
            $grade_computation->save();
        }
    }
}
