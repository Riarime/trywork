<?php

use Illuminate\Database\Seeder;
use App\StateUniversity;

class UniversityCollegesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $college_inputs = [
            ['su_id' => 1, 'college_abbrv' => 'CECS','college_name' => 'College of Engineering and Computing Sciences'],
            ['su_id' => 1, 'college_abbrv' => 'CBA','college_name' => 'College of Business Administration'],
            ['su_id' => 1, 'college_abbrv' => 'COED','college_name' => 'College of Education'],
            ['su_id' => 1, 'college_abbrv' => 'CAS','college_name' => 'College of Arts and Sciences'],
            ['su_id' => 1, 'college_abbrv' => 'CABEIHM','college_name' => 'College of Accountancy and Business Economics and Internatonal Hospitality Management']
        ];

        foreach($college_inputs as $input) {
            $code = StateUniversity::where('su_id','=',1)->get()->pluck('su_abbrv');
            $college = new \App\UnivColleges();
            $college->su_id = $input['su_id'];
            $college->college_code = 'TWC-'.($code[0]).'-'.$input['college_abbrv'];
            $college->college_abbrv = $input['college_abbrv'];
            $college->college_name = $input['college_name'];
            $college->save();
        }
    }
}
