<?php

use Carbon\Carbon;
use App\User;
use Illuminate\Database\Seeder;

class HTEAccountsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hte_accounts = [
            ['email' => 'hte_absolut@gmail.com', 'pass' => 'password']
        ];

        $hte_inputs = [
            ['title' => null, 'first_name' => 'John Patrick', 'middle_name' => 'B.', 'last_name' => 'Loyola', 'gender' => 'M', 'bdate' => '1995-04-01', 'tel_no' => null, 'cel_no' => null],    // HTE Representative
        ];

        $UserRole_inputs = [
            ['user_id' => 5, 'role_id' => 6, 'info_id' => 16]
        ];


        foreach($hte_accounts as $hte_account) {
            $code = 'TWU'.Carbon::now()->format('y-md-').(User::get()->count() + 1);
            $user = new \App\User();
            $user->usr_code = $code;
            $user->email = $hte_account['email'];
            $user->password = bcrypt($hte_account['pass']);
            $user->save();
        }

        foreach($hte_inputs as $input) {
            $info = new \App\UserInfo();
            $info->title = $input['title'];
            $info->first_name = $input['first_name'];
            $info->middle_name = $input['middle_name'];
            $info->last_name = $input['last_name'];
            $info->gender = $input['gender'];
            $info->bdate = $input['bdate'];
            $info->tel_no = $input['tel_no'];
            $info->cel_no = $input['cel_no'];
            $info->save();
        }

        foreach($UserRole_inputs as $input) {
            $UserRole = new \App\UserRole();
            $UserRole->user_id = $input['user_id'];
            $UserRole->role_id = $input['role_id'];
            $UserRole->info_id = $input['info_id'];
            $UserRole->save();
        }
    }
}
