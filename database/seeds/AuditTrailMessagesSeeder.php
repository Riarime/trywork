<?php

use Illuminate\Database\Seeder;
use App\AuditTrailMessage;

class AuditTrailMessagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $message = new AuditTrailMessage();
        $message->details = 'User %USER% logged in to the system at around %DATE%';
        $message->save();
    }
}
