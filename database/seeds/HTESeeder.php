<?php

use Illuminate\Database\Seeder;
use App\HTE;
use Carbon\Carbon;

class HTESeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hte_input = [
            ['name' => 'Absolut Distillers, Inc.', 'address' => 'Brgy. Malaruhatan, Lian, Batangas', 'contact_person' => 'Manolita C. Bayran', 'designation' => 'HR Manager', 'contact_no' => '09283924785'],
            ['name' => 'Adventus IT Service Inc.', 'address' => 'Ayala Avenue', 'contact_person' => 'Karen Mae J. Canopin', 'designation' => 'HR and Sales Assistant', 'contact_no' => '09352699225'],
            ['name' => 'Batangas I Electric Cooperative Inc.', 'address' => 'Km. 116 National Highway Calaca, Batangas', 'contact_person' => 'Marivic S. Balbacal', 'designation' => 'ISD Manager', 'contact_no' => '(101) 424-0100'],
            ['name' => 'Bausas Surveying Office', 'address' => '', 'contact_person' => 'Maria Flor A. Barinki', 'designation' => 'Office Staff', 'contact_no' => '09175302495'],
            ['name' => 'Blue Collar Manpower Services Inc.', 'address' => '2816 Borneo St., Brgy. San Isidro','contact_person' => 'Rolando Raz Nicodemuz', 'designation' => 'HR Manager', 'contact_no' => '09218222703'],
            ['name' => 'Bureau of Fire Protection Nasugbu Branch', 'address' => 'Municipal Hall, Escalera St.', 'contact_person' => 'SFO2 Zemon B. Bacit', 'designation' => 'OIC/MFM', 'contact_no' => '09351692690'],
            ['name' => 'Bureau of Fire Protection Lian Batangas', 'address' => 'J.P. Rizal St., Brgy. 1', 'contact_person' => 'SF01 Gregorio N. Chavez', 'designation' => 'OIC ,Municipal Fire Marshal', 'contact_no' => '09156021982'],
            ['name' => 'Calatagan Municipal Police Station', 'address' => '', 'contact_person' => 'Radam R. Ramos', 'designation' => 'Police Inspector', 'contact_no' => '09272979771'],
            ['name' => 'Canyon Cove', 'address' => '', 'contact_person' => 'Mhel Atienza', 'designation' => 'Marketing Supervisor', 'contact_no' => '09059733890'],
            ['name' => 'Caritas Banco ng Masa', 'address' => 'Km 1 National Highway', 'contact_person' => 'Lillosa G. Madrigal', 'designation' => 'Cashier', 'contact_no' => '09988564663'],
            ['name' => 'Central Azucarera Don Pedro Inc.', 'address' => 'Bgry. Lumbangan', 'contact_person' => 'Carlota V. Inumerable', 'designation' => 'Department Head', 'contact_no' => '09361977470'],
            ['name' => 'Chateau Royale Sports and Country Club', 'address' => 'Km. 72 Batulao', 'contact_person' => 'Frauline M. Padua', 'designation' => 'HR Officer', 'contact_no' => '09064012854'],
            ['name' => 'Club Punta Fuego', 'address' => 'Brgy. Balaytigue', 'contact_person' => 'Charlene Realon', 'designation' => 'HR Supervisor', 'contact_no' => '09173684930'],
            ['name' => 'Convergys Philippines, Inc.', 'address' => 'Convergys 4f Glorieta 5, Ayala', 'contact_person' => 'Myrna Garcia', 'designation' => 'Souring Dept.', 'contact_no' => '09178704337'],
            ['name' => 'FourPoint Zero, Inc.', 'address' => '814 West Tektite Tower, Ortigas Center', 'contact_person' => 'Marlon Randall Umali', 'designation' => 'CMO', 'contact_no' => '09062428531'],
            ['name' => 'Good Heart Marketing Company Inc.', 'address' => 'Caybunga', 'contact_person' => 'Juvy Ann J. Soriano', 'designation' => 'HR-Supervisor', 'contact_no' => '09971988331'],
            ['name' => 'Land Transportation Office Balayan Extension', 'address' => '', 'contact_person' => 'Sherwin J. Lontoc', 'designation' => 'Asst. Chief', 'contact_no' => '09275498332'],
            ['name' => 'Landbank of the Philippines', 'address' => 'J.P. Laurel St., 4231', 'contact_person' => 'Alicia E. Climaco', 'designation' => 'Acting BSO', 'contact_no' => '09209243170'],
            ['name' => 'Lian Municipal Police Station', 'address' => 'Malaruhatan', 'contact_person' => 'SPO4 Rafael M. Panganiban', 'designation' => 'MESPO', 'contact_no' => '09163157619'],
            ['name' => 'Lian Water District', 'address' => 'Brgy. 4', 'contact_person' => 'Sherwin Napoleon M. Jonson', 'designation' => 'General Manager', 'contact_no' => '09175906463'],
            ['name' => 'Malarayat Rural Bank, Inc.', 'address' => 'Burgos St.', 'contact_person' => 'Analissa P. Calingasan', 'designation' => 'Branch Manager', 'contact_no' => '09175920033'],
            ['name' => 'Medical Center Western Batangas', 'address' => 'Brgy. Lanatan', 'contact_person' => 'Aileen M. Villanueva', 'designation' => 'Payroll Officer', 'contact_no' => '09771543326'],
            ['name' => 'Municipality of Calatagan', 'address' => '', 'contact_person' => 'Glenn Z. Aytona', 'designation' => 'Executive Assistnat', 'contact_no' => '09178695431'],
            ['name' => 'Municipality of Lian', 'address' => 'J. P. Rizal St. Brgy. 1', 'contact_person' => 'Regiela M. Abreu', 'designation' => 'HRMO III', 'contact_no' => '09178463293'],
            ['name' => 'Municipality of Magallanes', 'address' => '', 'contact_person' => 'Vilma O. Solayao', 'designation' => 'HRMO IV', 'contact_no' => '09357555163'],
            ['name' => 'Municipality of Nasugbu', 'address' => 'Brgy. 2, Poblacion, Nasugbu, Batangas', 'contact_person' => 'Mr. Virgilio Vargas', 'designation' => 'HRMO IV', 'contact_no' => '09175052139'],
            ['name' => 'Municipality of Tuy', 'address' => 'Brgy. Luna,Poblacion', 'contact_person' => 'Emanuel A. Afable', 'designation' => 'HRMO', 'contact_no' => ''],
            ['name' => 'Nasugbu Water District', 'address' => 'J. P. Laurel St. Brgy. 12', 'contact_person' => 'Sean Romulo Zabala', 'designation' => 'Admin Finance', 'contact_no' => '(043) 216-2819'],
            ['name' => 'Orange Apps Inc.', 'address' => '#267 Ermin Garcia Unit 101,Minessota Mansion, Quezon City', 'contact_person' => 'Inno Angelo Ferrer', 'designation' => 'Office Manager', 'contact_no' => '09174355207'],
            ['name' => 'PhilAm Life', 'address' => 'J.P. Laurel St., Brgy.2', 'contact_person' => 'Glenna Borge', 'designation' => 'Associate Agency Manager', 'contact_no' => '09171187745'],
            ['name' => 'Philippine National Bank', 'address' => '', 'contact_person' => 'Amorlita Luya', 'designation' => 'Branch Manager', 'contact_no' => '(043) - 4160070'],
            ['name' => 'Philippine Red Cross Batangas Chapter', 'address' => 'Conception St., Brgy. 4', 'contact_person' => 'Angelica R. Rinoz', 'designation' => 'SIC-FG/ Cahier', 'contact_no' => '09255500558'],
            ['name' => 'PLDT', 'address' => 'Brgy. Bucana', 'contact_person' => 'Ronaldo J. Enriquez', 'designation' => 'STN Supervisor', 'contact_no' => '(02) 732-6704'],
            ['name' => 'QMS', 'address' => 'Cavite Economic Zone', 'contact_person' => 'Jennifer V. Fabic', 'designation' => 'DC', 'contact_no' => '09985629401'],
            ['name' => 'Quad X', 'address' => 'Chino Roces Ext.', 'contact_person' => 'Christine Crisostomo', 'designation' => 'Department Head', 'contact_no' => '(02) 877-8243'],
            ['name' => 'Seraph Security Agency Company, Inc.', 'address' => 'Brgy. Laginghanda', 'contact_person' => 'Atty. Magtanggol B. Gatdula', 'designation' => 'President/CEO', 'contact_no' => '(02) 3557644'],
            ['name' => 'St. Peter Life Plan Inc.', 'address' => 'J. P. Laurel St. Brgy. Lumbangan', 'contact_person' => 'Alejo Magsino', 'designation' => 'Branch Manager', 'contact_no' => '09178961569'],
            ['name' => 'Stilts Calatagan Bech Resort', 'address' => 'Brgy. Sta Ana', 'contact_person' => 'Arman D. Herrera', 'designation' => 'Accounting Supervisor', 'contact_no' => '09176174046'],
            ['name' => 'Teleperformance Philippines', 'address' => 'SM MOA Complex', 'contact_person' => 'Norman Lucero', 'designation' => 'Desktop Engineer', 'contact_no' => '09208079888'],
            ['name' => 'Teletech', 'address' => 'Robinsons Place', 'contact_person' => 'Allberto Pesigan Jr.', 'designation' => 'SITE IT', 'contact_no' => '09753879007'],
            ['name' => 'United Labor Service Cooperative', 'address' => 'Brgy. Lumbangan', 'contact_person' => 'Alice R. Cedo', 'designation' => 'Proj. Mgt and Dev Supervisor', 'contact_no' => '09153853269'],
            ['name' => 'Universal Robina Corporation Inc.', 'address' => '', 'contact_person' => 'Michelle M. Tapire', 'designation' => 'HR Manager', 'contact_no' => '09988403845'],
            ['name' => 'Upteam Corporation Limited', 'address' => 'Legaspi Village', 'contact_person' => 'Raphael Francis L. Quisumbing ', 'designation' => 'Chief Technology Officer', 'contact_no' => '09209717073'],
            ['name' => 'Western Balayan Telephone System Inc.', 'address' => '197 Paz St.',  'contact_person' => 'Ma Florizel B. Pantoja', 'designation' => 'Admin Manager', 'contact_no' => '(043) 211-4202'],
            ['name' => 'AMPLEON Phillipines, Inc.', 'address' => 'Binary St., Light Industry and Science Park, Cabuyao, Laguna',  'contact_person' => 'Ms. Jocelyn Cadiente', 'designation' => 'HR Senior Specialist', 'contact_no' => '9985624937'],
            ['name' => 'Convergys', 'address' => '8th flr., SM Megamall Bulding, C. Ortigas Center, Mandaluyong City', 'contact_person' => 'Mr. Mike Adrian Batuigas', 'designation' => 'OJT Coordinator', 'contact_no' => '9239679734'],
            ['name' => 'Digital Service Cambridge Limited ROHQ', 'address' => '2nd Flr., New Solid Building, Sen. Gil Puyat, Makati City', 'contact_person' => 'Ms. Camille Nicole C. Manalo', 'designation' => 'HR Assistant', 'contact_no' => '(02)865-7000 loc. 3206'],
            ['name' => 'Grafitix Digital Co.', 'address' => '2F. Unit H and I, Ridge Star Plaza, Rotonda, Aguinaldo Highway, Tagaytay City', 'contact_person' => 'Ms. Precious Maraphel L. Golez', 'designation' => 'HR Generalist', 'contact_no' => '9171239432'],
            ['name' => 'Immanuel Diagnostic and Drug Testing Lab.', 'address' => 'J.P. Laurel St., Brgy. 11, Nasugbu, Batangas', 'contact_person' => 'Ms. Joan V. Tolentino', 'designation' => 'Clinic Manager', 'contact_no' => '9054354571'],
            ['name' => 'Mount Carmel Rural Bank, Inc. ', 'address' => 'J.P.Rizal St., Lian, Batangas', 'contact_person' => 'Ms. Shiela Maura M. Lagus', 'designation' => 'Branch Manager', 'contact_no' => '9176837653'],
            ['name' => 'Municipality of Nasugbu', 'address' => 'Escalera St., Brgy. Poblacion, Brgy. 2, Nasugbu, Bats.', 'contact_person' => 'Mr. Virgilio O. Vargas', 'designation' => 'MGDH', 'contact_no' => '9175052139'],
            ['name' => 'Batangas Cable Landing Station - PLDT', 'address' => 'Apacible Bvld., Brgy. Bucana, Nasugbu, Batangas', 'contact_person' => 'Mr. Ronaldo J. Enriquez', 'designation' => 'STN Supervisor', 'contact_no' => '(02)7326704'],
            ['name' => 'Batangas I Electric Cooperative Inc.', 'address' => 'Km. 116 National Highway, Calaca, Batangas', 'contact_person' => 'Ms. Marivic S. Balbacal', 'designation' => 'ISD Manager', 'contact_no' => '(043)4240100 local 101'],
            ['name' => 'Direct Agent 5 Incorporated', 'address' => '2nd Flr., Expocraft Building, Metropolitan Ave., Makati City', 'contact_person' => 'Ms. Cimverly Ann', 'designation' => 'HR Supervisor', 'contact_no' => '9173253127'],
            ['name' => 'Hye Sung Industries Philippines, Inc.', 'address' => 'Lot 29, Block 6, Phase II, Peaza-Cez, Rosario, Cavite', 'contact_person' => 'Ms. Miriam F. Caulingan', 'designation' => 'HR Head', 'contact_no' => '(046) 4371501'],
            ['name' => 'KEPCO Ilijan Corporation', 'address' => 'Ilijan Site, Batangas City', 'contact_person' => 'Ms. Shiela Marie Ebora', 'designation' => 'HR Recruitment Staff', 'contact_no' => '(043)4256500 local 121'],
            ['name' => 'Philippine Overseas Employment Administration', 'address' => 'Ortigas Ave., Mandaluyong City.', 'contact_person' => 'Ms. Marilyn M. Adalia', 'designation' => 'Chief Accountant', 'contact_no' => '(02) 7221170'],
            ['name' => 'SEIDOPRO Global Inc. ', 'address' => '20th Flr., Net Quad Bldg., Bonifacio Global City, Taguig', 'contact_person' => 'Mr. Manuel Hipolito Jr.', 'designation' => 'MIS Supervisor', 'contact_no' => '09173159172
'],
            ['name' => 'Sky Cable Corporation', 'address' => 'GF, ELJ Communications, Quezon City', 'contact_person' => 'Ms. Erika S. Dela Cruz', 'designation' => 'HR Talent Manager', 'contact_no' => '(02) 6369292 local 9182'],
            ['name' => 'Commission on Audit Nasugbu', 'address' => 'JP Laurel St. Nasugbu,Batangas', 'contact_person' => 'Irenea Caringal', 'designation' => 'Audit Team Leader', 'contact_no' => '043-7402694'],
            ['name' => 'Flour Daniel Inc', 'address' => 'Polaris Building Spectrum Midway Alabang', 'contact_person' => 'Maricor Paola Hufano', 'designation' => 'HR Specialist', 'contact_no' => '02-8504451'],
            ['name' => 'Eight Aces Tours and Transport  Services', 'address' => 'JP Laurel St. Nasugbu,Batangas', 'contact_person' => 'Rovian Mahinay', 'designation' => 'Operations Manager', 'contact_no' => '9177059840'],
            ['name' => 'Stream Based Client Solutions', 'address' => 'F.castro St. Brgy.11,Nasugbu,Batangas', 'contact_person' => 'Eloisa Bea Lopez', 'designation' => '', 'contact_no' => ''],
            ['name' => 'Telus International Philippines Inc', 'address' => 'ADB Avenue,Ortigas Center,Pasig City,Philippines', 'contact_person' => 'Ron Ian Salazar', 'designation' => 'Controllership Supervisor', 'contact_no' => '9177172849'],
            ['name' => 'SKY AVIATION TRAINING CENTER INC', 'address' => 'PAIR-PAGGS NAIA AVE.PASAY CITY', 'contact_person' => 'AILEEN SAPINA', 'designation' => 'TRAINING HEAD AND ADMIN', 'contact_no' => '09553629711'],
            ['name' => 'TOP SERVE SERVICE SOLUTION', 'address' => 'KAMAGONG COR PAUL SAN ANTONIO VIL MKTI,MNLA', 'contact_person' => 'ANGELICA PENANO', 'designation' => 'PRACTICUM COORDINATOR', 'contact_no' => '09553629711'],
            ['name' => 'PTC-MILCOM AVIATION CENTER', 'address' => 'BROADBAND BLG.3,MIA RD,BRGY.TAMBO.PARANAQUE', 'contact_person' => 'DENNIS DEL MAR', 'designation' => 'LEAD INSTRUCTOR', 'contact_no' => '09178370784'],
            ['name' => 'OCEANSTAR TRAVEL AND TOURS', 'address' => '724 QUIRINO AVE.TAMBO,PARANAQUE', 'contact_person' => 'DENNIS DEL MAR', 'designation' => 'MANAGER', 'contact_no' => '9088642651'],
            ['name' => 'JERON TRAVEL TOURS', 'address' => 'ROXAS BLVD.COR.AIRPORT RD.BACLARAN,PARANAQUE', 'contact_person' => 'GILLIAN AGUILAR', 'designation' => 'HR ASISSTANT', 'contact_no' => '02-8541813'],
            ['name' => 'AMS TRAVEL AND TOURS', 'address' => 'SAN VICENTE,SAN PEDRO LAGUNA', 'contact_person' => 'JAMES ALMEDA', 'designation' => 'SALES REPRESENTATIVE', 'contact_no' => '9178553864'],
            ['name' => 'MOIRAH\'S TRAVEL LIGHT TOURS AND TRAVEL', 'address' => 'BRGY.LIANG,MALOLOS BULACAN', 'contact_person' => 'ANALYN UYCHOCO', 'designation' => 'MANAGER', 'contact_no' => '9088642651'],
            ['name' => 'Bureau of Fisheries and Aquatic Resources', 'address' => 'Los Baños, Laguna', 'contact_person' => 'Ms. Fritz Alvarez', 'designation' => 'Admin Assistant', 'contact_no' => '09068439669'],
            ['name' => 'Mnaila Broadcasting Company', 'address' => 'Pasay City', 'contact_person' => 'Mr. Jeff Senolos', 'designation' => 'HR Staff', 'contact_no' => '09052865936'],
            ['name' => 'Nasugbu West Central School', 'address' => 'Concepcion St. Barangay 4, Nasugbu, Batangas ', 'contact_person' => 'Mrs. Consuelo Sajona', 'designation' => ' Guidance Counselor', 'contact_no' => '09439313491'],
            ['name' => 'Nasugbu East Central School', 'address' => 'Brgy. Lumbangan Nasugbu, Batangas', 'contact_person' => 'Mr. Efren C. Andino', 'designation' => 'Principal II', 'contact_no' => '09159826149'],
            ['name' => 'Lian Central School', 'address' => 'Lian, Batangas', 'contact_person' => 'Ms. Sherlina P. Birot', 'designation' => ' Guidance Counselor', 'contact_no' => '09162131104'],
            ['name' => 'R.B. Cordero Academy - Nasugbu', 'address' => 'Concepcion St. Brgy 8 Nasugbu Batangas', 'contact_person' => 'Ms. Delia Mendoza', 'designation' => 'School Principal', 'contact_no' => '09356592391'],
            ['name' => 'Our Lady of Peace Academy - Tuy Batangas', 'address' => 'Tuy, Batangas', 'contact_person' => 'Ms. Eusebia P. Dimayuga', 'designation' => 'Principal', 'contact_no' => '09056691844'],
            ['name' => 'Lucsuhin National High School - Calatagan', 'address' => 'Lucsuhin Calatagn Batangas', 'contact_person' => 'Ms. Hermehia C. Hernandez', 'designation' => 'Head Teacher ', 'contact_no' => '09289748202'],
            ['name' => 'Gregorio Paradero Elementary School', 'address' => 'Tuy, Batangas', 'contact_person' => 'Ms. Aida S. Cudiamat', 'designation' => 'Principal', 'contact_no' => '09778830797'],
            ['name' => 'Nicolites Montessori School', 'address' => 'San Roque Street Brgy. 4 Nasugbu, Batangas', 'contact_person' => 'Mr. Alfredo D. Dela China', 'designation' => 'Guidance Counselor', 'contact_no' => '0916 495 4819'],
            ['name' => 'Adelaido A. Bayot Memorial School Inc.', 'address' => 'JP. Laurel St. Nasugbu, Batangas', 'contact_person' => 'Ms. Gian Carla M. Bayot', 'designation' => 'Principal', 'contact_no' => '09309876302'],
            ['name' => 'Saint  Anne Academy', 'address' => 'Binubusan Lian Batangas', 'contact_person' => 'Mr. Alexis R. Tumbaga', 'designation' => 'Guidance Counselor', 'contact_no' => '09958605999'],
            ['name' => 'Yazaki Torres Manufacturing Inc', 'address' => 'Makiling Calamba, Laguna', 'contact_person' => 'Mr. Felcaster Torres', 'designation' => 'VP for HRMO', 'contact_no' => '(049) 502 - 1530'],
            ['name' => 'Lian Institute', 'address' => 'Lian, Batangas', 'contact_person' => 'Mr. Redentor A. dela Alas', 'designation' => 'Principal', 'contact_no' => '09361273125'],
            ['name' => 'Balayan National High School', 'address' => '103 Paz St. Balayan,  Batangas', 'contact_person' => 'Ms. Veronica Caviteño', 'designation' => 'Cooperating Teacher', 'contact_no' => '0975-9624755'],
            ['name' => 'Banilad National High School', 'address' => 'Banilad Nasugbu, Batangas', 'contact_person' => 'Ms. Rochelle Delas Alas', 'designation' => 'Cooperating Teacher', 'contact_no' => 'null'],
            ['name' => 'Calatagan National HS', 'address' => 'Poblacion I Calatagan, Bats.', 'contact_person' => 'Ms. Vanessa A. Bautista', 'designation' => 'School Principal', 'contact_no' => 'null'],
            ['name' => 'Lucsuhin National High School', 'address' => 'Lucsuhin Calatagan, Batangas', 'contact_person' => 'Ms. Rowena Tagle', 'designation' => 'Cooperating Teacher', 'contact_no' => 'null'],
            ['name' => 'Catandaan National High Sch.', 'address' => 'Catandaan Nasugbu, Batangas', 'contact_person' => 'Mr. Pedro Nepomuceno Jr.', 'designation' => 'Cooperating Teacher', 'contact_no' => '0917-8925424'],
            ['name' => 'Dr. Crisogono B. Ermita Sr. Memorial National High School ', 'address' => 'Pantalan Nasugbu, Batangas', 'contact_person' => 'Mrs. Pearl Jhoan D. Bajar', 'designation' => 'Cooperating Teacher', 'contact_no' => '09958422014 09091405347'],
            ['name' => 'Jose Lopez Manzano National High School', 'address' => 'Mataywanak Tuy, Batangas', 'contact_person' => 'Ms. Mylene E. Sauña', 'designation' => 'Cooperating Teacher', 'contact_no' => '0917-3006601'],
            ['name' => 'Lian National High School', 'address' => 'Malaruhatan Lian, Batangas', 'contact_person' => 'Mrs. Francisca A. Hernandez', 'designation' => 'Cooperating Teacher', 'contact_no' => '0935-0653408'],
            ['name' => 'Lumbangan National H.S.', 'address' => 'Lumbangan Nasugbu, Bats.', 'contact_person' => 'Ms. Julieta G. Talisic', 'designation' => 'Cooperating Teacher', 'contact_no' => '0955 1773999'],
            ['name' => 'Malapad na Bato NationalH.S.', 'address' => 'Malapad na Bato Nas., Bats.', 'contact_person' => 'Ms. Lucila I. Sandoval', 'designation' => 'Cooperating Teacher', 'contact_no' => '0955 3429614'],
            ['name' => 'Sen. Gil Puyat National H.S.', 'address' => 'Balaytigue Nasugbu, Batangas', 'contact_person' => 'Ms. Julie Basbas', 'designation' => 'Cooperating Teacher', 'contact_no' => '0926 8936957'],
            ['name' => 'Academia de San Francisco Xavier', 'address' => 'Nasugbu, Batangas', 'contact_person' => 'Ms. Jamel le V. Rufo', 'designation' => 'Cooperating Teacher', 'contact_no' => 'null'],
            ['name' => 'Nicolites Montessori School', 'address' => 'Nasugbu, Batangas', 'contact_person' => 'Ms. Lydie M. Tolentino', 'designation' => 'Cooperating Teacher', 'contact_no' => '0917-7552101'],
            ['name' => 'BatStateU ARASOF-Nasugbu CTE Laboratory School', 'address' => 'Bucana Nasugbu, Batangas', 'contact_person' => 'Asst.Prof.Edwina D. Rodriguez', 'designation' => 'Cooperating Teacher', 'contact_no' => '0916-6762990'],
            ['name' => 'RCTI Training & Assessment Corp.', 'address' => '1020 Roxas Blvd., Ermita, Manila', 'contact_person' => 'Ms. Carol Purificacion', 'designation' => 'Owner/Manager', 'contact_no' => '09279617762'],
            ['name' => 'Herirage Hotel', 'address' => 'Roxas Blvd. Manila', 'contact_person' => 'Ms. Inah Polintan', 'designation' => 'HR Officer', 'contact_no' => '09753786954'],
            ['name' => 'Best Western Hotel La Corona Manila ', 'address' => '1166 M.H. Del Pilar cor. Arquiza St., Ermita, Manila', 'contact_person' => 'Mr. Ramon Mangulat', 'designation' => 'Exec. Housekeeper', 'contact_no' => '09772645076'],
            ['name' => 'Crosswinds Resorts Suites', 'address' => 'Tagaytay City', 'contact_person' => 'Ms. Rebecca Coquia', 'designation' => 'HR Manager', 'contact_no' => '09175937276'],
            ['name' => 'Splendido, Ladera', 'address' => 'Brgy. Dayap, Lemery, Batangas', 'contact_person' => 'Ms. Chey Mendiola', 'designation' => 'F & B Manager', 'contact_no' => '09178727595'],
            ['name' => 'Buena Asuncion Restaurant', 'address' => 'Nasugbu, Batangas', 'contact_person' => 'Mrs. Ana Barcelon', 'designation' => 'Owner/Manager', 'contact_no' => 'null'],
            ['name' => 'Lago De Oro Resort', 'address' => 'Calatagan, Batangas', 'contact_person' => 'Mr. Raymundo Plata', 'designation' => 'Training Coordinator', 'contact_no' => '09175042685'],
            ['name' => 'UNILAB F & B Service', 'address' => 'Manadauyong City Phils.', 'contact_person' => 'Mr. Lawrence Ong', 'designation' => 'F&B Manager', 'contact_no' => '09178155664'],
            ['name' => 'Peninsula de Punta Fuego', 'address' => 'Brgy. Balaytigue, Nasugbu, Batangas', 'contact_person' => 'Maria Lynne T. Castillo', 'designation' => 'HR Manager', 'contact_no' => 'null'],
            ['name' => 'Stilts Beach Resort', 'address' => 'Sta. Ana, Calatagan, Batangas', 'contact_person' => 'Mr. Arman Herrera', 'designation' => 'Accounting Supervisor', 'contact_no' => '09176174046'],
            ['name' => 'Crimson Hotel', 'address' => 'Entrata, Urban, Filinvest City, Alabang', 'contact_person' => 'Shamle Villanueva', 'designation' => 'HR Manager', 'contact_no' => '09985900437'],
            /*['name' => '', 'address' => '', 'contact_person' => '', 'designation' => '', 'contact_no' => ''],*/

        ];

        foreach ($hte_input as $input) {
            $code = 'HTE'.(Carbon::now()->format('y-m')).(HTE::get()->count() + 1); //'HTE18-091';

            $hte = new \App\HTE();
            $hte->hte_code = $code;
            $hte->hte_name = array_pull($input,'name');
            $hte->mission = null;
            $hte->vision = null;
            $hte->objectives = null;
            $hte->address = array_pull($input,'address');
            $hte->save();

            $contact = new \App\HTEContact();
            $contact->hte_id = $hte->id;
            $contact->contact_person = array_pull($input,'contact_person');
            $contact->designation = array_pull($input,'designation');
            $contact->contact_no = array_pull($input,'contact_no');
            $contact->save();
        }


//        $hte = new \App\HostTrainingEstablishmentInfo();
//        $hte->company_name = 'IBC Company';
//        $hte->mission = 'Here\'s a sample mission for the company';
//        $hte->vision = 'Here\'s a sample vision for the company';
//        $hte->objectives = 'Here\'s a sample objectives for the company';
//        $hte->address = '#51 Lot 7 BFD Compound, Diliman';
//        $hte->address2 = 'Quezon City, Metro Manila, 1124';
//        $hte->save();
//
//        $ojt = new \App\JobTraining();
//        $ojt->code = 'OJT041895';
//        $ojt->name = 'Job Training for IT Interns';
//        $ojt->description = 'Our company is open for Job Trainings. Our targets for our Internship Program must know how to program in Java, also in PHP.';
//        $ojt->start = date('Y-m-d', strtotime('September 7, 2018'));
//        $ojt->save();
    }
}
