<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\HTE;
use App\JobTraining;

class JobTrainingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $hte = [
            ['name' => 'Philippine OPPO Mobile Technology, Inc.', 'address' => 'Baguio, Benguet', 'region' => 2],
            ['name' => 'Teleperformance', 'address' => 'Baguio, Cordillera region', 'region' => 2],
            ['name' => 'Beijing Bangnishou Internet Education Technology Limited – Phils', 'address' => 'Baguio City', 'region' => 2],
            ['name' => 'Gerry’s Grill and Restaurants', 'address' => 'Legarda, Baguio City', 'region' => 2],
            ['name' => 'Cabalen Restaurant', 'address' => 'Baguio, Cordillera Administrative Region', 'region' => 2],
            ['name' => 'AMA Computer College', 'address' => '#6 Benitez Court, Magsaysay Ave. 
Baguio, Benguet', 'region' => 2],
            ['name' => 'Aboitiz Power Generation', 'address' => '214 Ambuclao Road, Obulan, Beckel, La Trinidad, Benguet', 'region' => 2],
            ['name' => 'Small Business Guarantee and Finance Corporation', 'address' => 'Baguio, Cordillera Administrative Region', 'region' => 2],
            ['name' => 'Utalk Philippines', 'address' => 'Baguio, Cordillera Administrative Region', 'region' => 2],
            ['name' => 'International Organization for Migration (IOM)', 'address' => 'Baguio, Cordillera Administrative Region', 'region' => 2],
            ['name' => 'Fuego de Amor Resort and Hotel, Inc.', 'address' => 'Ilog Malino, Bolinao, Ilocos Region', 'region' => 3],
            ['name' => 'LCC Liberty Commecial Center, Inc.', 'address' => 'Ilocos Region, pangasinan.', 'region' => 3],
            ['name' => 'Infinit – O', 'address' => 'Legazpi, Ilocos', 'region' => 3],
            ['name' => 'Agent Support Technology, Inc.', 'address' => 'Batac, Ilocos Norte', 'region' => 3],
            ['name' => 'TaskUs – Ilocos', 'address' => 'Ilocos Sur', 'region' => 3],
            /*['name' => '', 'address' => '', 'region' => ],*/
        ];

        $job_trainings = [/*
            ['hte' => 3, 'job_training_name' => 'Intern as Online Marketing Manager (Head Office)','description' => 'Focused on marketing electronic environment. This position  constantly track competitors and stay abreast of sales comparison data. Good Communication Skills, Marketing Strategy'],
            ['hte' => 3, 'job_training_name' => 'Interns as Call Centre','description' => 'Handling inbound customer service support calls from customers. Cross sell value added services or products that are offered by the company. Good English Vocabulary, resourceful, good communication skills.'],
            ['hte' => 4, 'job_training_name' => 'Intern as PHP Application Developer','description' => 'Willing to be an intern as web developer. PHP, HTML, CSS3, JavaScript, JQuery, Ajax, MySQL, PHP, MVC Framework'],
            ['hte' => 5, 'job_training_name' => 'Intern as Service Crew','description' => 'Takes order and serves food and beverages according to the prescribed standards of service. Good communication skills, Good Interpersonal Skills, expert in Food Industry'],
            ['hte' => 6, 'job_training_name' => 'Intern as Assistant Store Manager','description' => 'Intern must taken the course: BS in hotel and restaurant managemen. Knowledgeable in Profit and Loss Reports, Food and Labor Costs, and People Management.'],
            ['hte' => 7, 'job_training_name' => 'Intern as Asst. School Nurse','description' => 'Assists the doctor during consultation, annual physical exam of students, etc. Takes vital signs of students, plans and provides teaching to patients related to their understanding of the illness and treatment. Prepares verbal and written reports. Must take bachelor’s degree in Nursing. Must take bachelor’s degree in Nursing.'],
            ['hte' => 8, 'job_training_name' => 'Intern as Human Resource and Quality Specialist','description' => 'Must be taking Bachelor’s Degree in Human Resource Management. Planning, Organizing corporate events, grievance issues resolution, KPI development, Budgeting.'],
            ['hte' => 9, 'job_training_name' => 'Intern as Administrative Assistant (Releasing)','description' => 'must be taking Bachelor’s Degree in Business Administration Course. Good communication skills, computer literate, Strong Attention to detail.'],
            ['hte' => 10, 'job_training_name' => 'Intern as Senior Recruiter','description' => 'must be taking Bachelor’s Degree in Business Administration Course. Good Communication Skills, human resource principles, decision making.'],
            ['hte' => 11, 'job_training_name' => 'Intern as Social Mobilizer','description' => 'must be taking the Bachelor course in Psychology. teamwork, managing knowledge, accountability, communication.'],
            ['hte' => 12, 'job_training_name' => 'intern as Admin/HR Clerk','description' => 'Must be taking the course Bachelor’s Degree in Human Resource Management. Computer Literate, Highly Organized, Efficient, Pro-active.'],
            ['hte' => 122, 'job_training_name' => 'Intern as Recruitment Manager','description' => 'Must be taking the course Bachelor’s Degree in Human Resource Management. Good Communication, and Interpersonal Skills, and strong relationship'],
            ['hte' => 123, 'job_training_name' => 'Intern as Data Specialist','description' => 'must be taking the course Bachelor’s Degree in Information Technology. English Excellent, Reading and comprehensive skills.'],
            ['hte' => 124, 'job_training_name' => 'Intern as Technical Support Agents','description' => 'must be taking the Bachelor’s Degree course in Marketing Management. Customer Service, Assists customers, with knowledge in.'],
            ['hte' => 125, 'job_training_name' => 'Intern as Online Food Delivery Support','description' => 'must be taking the Bachelor’s Degree course in Marketing Management. good writing skills, content writing, email/chat expert'],*/
            /*['hte' => 3, 'job_training_name' => 'Electrical Engineering Internship','description' => 'Experience with: Wiring, Soldering, Reading schematics, Using Digital Multi-meter (DMM), Oscilloscopes, Function generators, Bench top power supplies, Engineering courses including digital and analog circuit design, Programming in embedded C is a plus'],
            ['hte' => 3, 'job_training_name' => 'Energy Risk Management Intern','description' => 'Qualified applicants must be currently enrolled in a Business, Accounting, Finance, Engineering or other related curriculum'],
            ['hte' => 3, 'job_training_name' => 'Intern – Data-Driven Power System Analysis','description' => 'Required Education, Experience, and SkillsMust be enrolled as a full-time student in a degree granting program, or graduated in the past 12 months from an accredited institution.'],
            ['hte' => 4, 'job_training_name' => 'Regional Economics (Business Surveys) Intern','description' => 'With an application, candidates are required to submit a cover letter, resume / curriculum vitae, and a writing sample. Documents should be uploaded as attachments to your electronic submission. Formatted attachments can include Microsoft Office products, PDFs, JPGs or HTML documents; size for each is limited to 1.5Mb.'],
            ['hte' => 4, 'job_training_name' => 'Research Assistant Intern - Summer','description' => 'currently seeking Research Assistant Interns to join our Professional Services team during our 2019 Summer Internship and Fellowship Program.'],
            ['hte' => 5, 'job_training_name' => 'LIAISON OFFICER','description' => 'Maintain thorough knowledge of the business, as well as an understanding of how that impacts the other entities of contact.Monitor, coordinate, and communicate strategic objectives of the business. Collaborate and communicate successfully with other entities outside of the business'],
            ['hte' => 5, 'job_training_name' => 'Receptionist','description' => 'Female must be at least 5\'2 inches in height. Male must be at least 5\'6 inches in height. Tourism, HRM and other Hospitality Management graduate is an advantage. With pleasing personality. Proficient in English and Filipino. Has the ability to multi-task. Must be a people-person'],
            ['hte' => 5, 'job_training_name' => 'Management Trainee','description' => 'Candidate must possess at least Bachelor\'s/College Degree in Food & Beverage Services Management, Hospitality/Tourism/Hotel Management or equivalent. Excellent verbal and written communication skills. Goal oriented and has passion to learn more.'],
            ['hte' => 6, 'job_training_name' => 'Recreation Technician Interns','description' => 'Applicants must have a minimum of or in progress of an undergraduate degree in Recreation Resource Management or other related discipline appropriate to the position. A combination of relevant education and appropriate experience is also acceptable. Resumes must contain enough information to show that the applicant meets the experience as defined in the position description. We are looking for determined, strong, and developing natural resource managers who are looking for experience working with land management agencies.'],
            ['hte' => 8, 'job_training_name' => 'Police-Police Intern','description' => 'Must be free from conviction of a Felony. A Class "A" Misdemeanor conviction will be cause for immediate dismissal from the process, unless otherwise directed by the Chief of Police.'],
            ['hte' => 8, 'job_training_name' => 'POLICE OFFICER INTERN','description' => 'The Police Officer (Intern) position responds to a variety of civil matters as well as enforces applicable local, state, and federal laws. Duties include, writing reports; performing preliminary investigations; collecting evidence; responding to various alarms; detecting and arresting wrongdoers; and community policing activities.'],
            ['hte' => 8, 'job_training_name' => 'Police Personnel Intern II','description' => 'This is an internship for the spring 2019 semester and is for academic credit for a current college (junior or senior) pursuing a degree in a field relevant to the internship position.'],
            ['hte' => 8, 'job_training_name' => 'POLICE INTERN','description' => 'Interns will report directly to the supervisor within the assigned area.   Interns will be under the management of the Internship Coordinators within the Detroit Police Department Human Resources Bureau.'],*/
            ['hte' => 9, 'job_training_name' => 'Public Attendant Internship','description' => 'At least 18 and above, preferrably highschool graduate/college undergraduate/vocational graduate/college graduate, male and female.'],
            ['hte' => 9, 'job_training_name' => 'Banquet waiters Internship','description' => 'At least 18 and above, preferrably highschool graduate/college undergraduate/vocational graduate/college graduate, male and female.'],
            ['hte' => 10, 'job_training_name' => '','description' => ''],
        ];

        /*foreach($hte as $input_hte) {
            $code = 'HTE'.(Carbon::now()->format('y-m')).(HTE::get()->count() + 1); //'HTE18-091';

            $hte = new HTE();
            $hte->hte_code = $code;
            $hte->hte_name = $input_hte['name'];
            $hte->mission = null;
            $hte->vision = null;
            $hte->objectives = null;
            $hte->address = $input_hte['address'];
            $hte->region_id = $input_hte['region'];
            $hte->save();
            $hte_id = $hte->id;
        }*/

        foreach($job_trainings as $training) {
            $training_code = 'JT'.(Carbon::now()->format('y-m')).(JobTraining::get()->count() + 1);
            $job_training = new JobTraining();
            $job_training->hte_id = $training["hte"];
            $job_training->training_code = $training_code;
            $job_training->job_training_name = $training["job_training_name"];
            $job_training->description = $training["description"];
            $job_training->start = Carbon::now();
            $job_training->save();
        }
    }
}
