<?php

use Illuminate\Database\Seeder;

class InternSkillsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $skill_inputs = [
            ['name' => 'C# Programming'],
            ['name' => 'Java Programming'],
            ['name' => 'Good Speaker'],
            ['name' => 'Video Editing'],
            ['name' => 'Music Production'],
            ['name' => 'Great Advertiser'],
            ['name' => 'PHP Programming']
        ];

        $skillsets_inputs = [
            ['info_id' => 1, 'skill_id' => 2],
            ['info_id' => 1, 'skill_id' => 7]
        ];

        foreach($skill_inputs as $input) {
            $skill = new \App\InternSkill();
            $skill->name = $input['name'];
            $skill->save();
        }

        foreach($skillsets_inputs as $input) {
            $skill = new \App\InternSkillSet();
            $skill->intern_id = $input['info_id'];
            $skill->skill_id = $input['skill_id'];
            $skill->save();
        }
    }
}
