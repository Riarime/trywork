<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\role;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_input = [
            ['name' => 'System Administrator'],
            ['name' => 'OJT Director'],
            ['name' => 'OJT Head'],
            ['name' => 'OJT Coordinator'],
            ['name' => 'Student Trainee'],
            ['name' => 'HTE Representative'],
            ['name' => 'HTE Supervisor']
        ];

        foreach($role_input as $input) {
            $code = 'TWR'.(Carbon::now()->format('y-md-')).(role::get()->count() + 1);
            $role = new role();
            $role->role_code = $code;
            $role->name = $input['name'];
            $role->save();
        }
    }
}
