<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Region;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $region_inputs = [
            ['code' => 'NCR','name' => 'National Capital Region'],
            ['code' => 'CAR','name' => 'Cordillera Administrative Region'],
            ['code' => 'R1','name' => 'Ilocos'],
            ['code' => 'R2','name' => 'Cagayan Valley'],
            ['code' => 'R3','name' => 'Central Luzon'],
            ['code' => 'R4-A','name' => 'CALABARZON'],
            ['code' => 'R4-B','name' => 'MIMAROPA'],
            ['code' => 'R5','name' => 'Bicol'],
            ['code' => 'R7','name' => 'Central Visayas'],
            ['code' => 'R8','name' => 'Eastern Visayas'],
            ['code' => 'R9','name' => 'Zamboanga Peninsula'],
            ['code' => 'R10','name' => 'Northern Mindanao'],
            ['code' => 'R11','name' => 'Davao Region'],
            ['code' => 'R12','name' => 'SOCCSKSARGEN'],
            ['code' => 'R13','name' => 'Caraga Administrative Region']
        ];

        foreach($region_inputs as $input) {
            $region = new \App\Region();
            $region->code = $input['code'];
            $region->name = $input['name'];
            $region->save();
        }
    }
}
