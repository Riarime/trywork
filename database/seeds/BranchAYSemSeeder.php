<?php

use Illuminate\Database\Seeder;

class BranchAYSemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $active_inputs = [
            ['branch_id' => 1, 'ay_id' => 1, 'sem_id' => 1, 'status' => 'Inactive'],
            ['branch_id' => 1, 'ay_id' => 1, 'sem_id' => 2, 'status' => 'Active']
        ];

        foreach($active_inputs as $input) {
            $active = new \App\BranchAYSem();
            $active->branch_id = $input['branch_id'];
            $active->ay_id = $input['ay_id'];
            $active->sem_id = $input['sem_id'];
            $active->aysem_stat = $input['status'];
            $active->save();
        }
    }
}
