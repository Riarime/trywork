<?php

use Illuminate\Database\Seeder;

class GPASeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gpa_inputs = [1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0, 5.5];

        foreach($gpa_inputs as $input) {
            $gpa = new \App\GPA();
            $gpa->gpa = $input;
            $gpa->save();
        }
    }
}
