<?php

use Illuminate\Database\Seeder;
use App\InternApplicationStatus;

class InternAppStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $status_inputs = [
            ['name' => 'Pending'],
            ['name' => 'Not Suitable'],
            ['name' => 'For Inteview'],
            ['name' => 'Passed']
        ];

        foreach($status_inputs as $input) {
            $status = new \App\InternApplicationStatus();
            $status->iappstat_code = 'TWIAS-'.(InternApplicationStatus::get()->count() + 1);
            $status->name = $input['name'];
            $status->save();
        }
    }
}
