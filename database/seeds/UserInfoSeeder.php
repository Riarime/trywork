<?php

use Illuminate\Database\Seeder;

class UserInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $info_inputs = [
            ['title' => null, 'first_name' => 'Alfredo', 'middle_name' => 'V.', 'last_name' => 'Atienza, Jr.', 'gender' => 'M', 'bdate' => '1995-01-01', 'tel_no' => null, 'cel_no' => null],    // OJT Coordinator
            ['title' => 'Asst. Prof.', 'first_name' => 'Mayette', 'middle_name' => 'A.', 'last_name' => 'Cananea', 'gender' => 'F', 'bdate' => '1995-02-02', 'tel_no' => null, 'cel_no' => null],    //  OJT Head
            ['title' => 'Engr.', 'first_name' => 'Adrian Ferdinand', 'middle_name' => 'M.', 'last_name' => 'Melo', 'gender' => 'M', 'bdate' => '1995-03-03', 'tel_no' => null, 'cel_no' => null],    //  OJT Director
        ];

        foreach($info_inputs as $input) {
            $info = new \App\UserInfo();
            $info->title = $input['title'];
            $info->first_name = $input['first_name'];
            $info->middle_name = $input['middle_name'];
            $info->last_name = $input['last_name'];
            $info->gender = $input['gender'];
            $info->bdate = $input['bdate'];
            $info->tel_no = $input['tel_no'];
            $info->cel_no = $input['cel_no'];
            $info->save();
        }
    }
}
