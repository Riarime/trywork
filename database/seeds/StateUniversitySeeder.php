<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\StateUniversity;

class StateUniversitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $su_inputs = [
            ['abbrv' => 'BATSTATE-U', 'name' => 'Batangas State University'],
            ['abbrv' => 'EARIST', 'name' => 'Eulogio "Amang" Rodriguez Institute of Science and Technology'],
            ['abbrv' => 'MPC', 'name' => 'Marikina Polytechnic College'],
            ['abbrv' => 'PNU', 'name' => 'Philippine Normal University'],
            ['abbrv' => 'PSCA', 'name' => 'Philippine State College of Aeronautics'],
            ['abbrv' => 'PUP', 'name' => 'Polytechnic University of the Philippines'],
            ['abbrv' => 'RTU', 'name' => 'Rizal Technological University'],
            ['abbrv' => 'TUP', 'name' => 'Technological University of the Philippines'],
            ['abbrv' => 'UP', 'name' => 'University of the Philippines System']
        ];

        foreach($su_inputs as $input) {
            $code = 'TWSU'.(Carbon::now()->format('y-m')).(StateUniversity::get()->count() + 1);
            $su = new \App\StateUniversity();
            $su->su_code = $code;
            $su->su_abbrv = $input['abbrv'];
            $su->su_name = $input['name'];
            $su->save();
        }
    }
}
