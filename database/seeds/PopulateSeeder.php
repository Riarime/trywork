<?php

use Illuminate\Database\Seeder;

class PopulateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $su_branches = [
//                ['su_id' => 6,'branch_abbrv' => 'PUPQC','branch_name' => 'Polytechnic University of the Philippines Quezon City','branch_address' => 'Don Fabian Ext. Brgy. Commonwealth, Quezon City','branch_zipcode' => '1101'],
        ];
        $su_colleges = [
//            ['su_id' => 6, 'college_abbrv' => 'CCIS','college_name' => 'College of Computer and Information Sciences'],
        ];
        $su_college_programs = [
            /*['college_id' => 6, 'program_abbrv' => 'BSIT', 'program_name' => 'Bachelor of Science in Information Technology'],
            ['college_id' => 6, 'program_abbrv' => 'BSCS', 'program_name' => 'Bachelor of Science in Computer Science'],*/
        ];
        $users = [
            /*['email' => 'emmanuel_deguzman@gmail.com', 'pass' => 'password', 'title' => 'Ph.D', 'first_name' => 'Emmanuel', 'middle_name' => 'Castro', 'last_name' => 'De Guzman', 'gender' => 'M', 'bdate' => '1965-04-27', 'tel_no' => null, 'cel_no' => null, 'role_id' => 2, 'su_id' => 6, 'branch_id' => null, 'college_id' => null, 'academic_rank' => null, 'emp_stat' => 'Permanent','degree' => 'Master\'s Degree','school_graduated' => null,'year_graduated' => null,'received_awards' => null],
            ['email' => 'firmo_esguerra@gmail.com', 'pass' => 'password', 'title' => null, 'first_name' => 'Firmo', 'middle_name' => 'A.', 'last_name' => 'Esguerra', 'gender' => 'M', 'bdate' => '1995-01-01', 'tel_no' => null, 'cel_no' => null, 'role_id' => 3, 'su_id' => 6, 'branch_id' => 2, 'college_id' => null, 'academic_rank' => null, 'emp_stat' => 'Permanent','degree' => 'Master\'s Degree','school_graduated' => null,'year_graduated' => null,'received_awards' => null],
            ['email' => 'demelyn_monzon@gmail.com', 'pass' => 'password', 'title' => null, 'first_name' => 'Demelyn', 'middle_name' => 'Espejo', 'last_name' => 'Monzon', 'gender' => 'F', 'bdate' => '1995-06-04', 'tel_no' => null, 'cel_no' => null, 'role_id' => 4, 'su_id' => 6, 'branch_id' => 2, 'college_id' => 6, 'academic_rank' => null, 'emp_stat' => 'Permanent','degree' => 'Master\'s Degree','school_graduated' => null,'year_graduated' => null,'received_awards' => null],*/
        ];

        for ($i = 0; $i < count($su_branches); $i++) {
            $su = (\App\StateUniversity::where('su_id','=',$su_branches[$i]['su_id'])->get()->pluck('su_abbrv'));
            $branch = new \App\UnivBranches();
            $branch->su_id = $su_branches[$i]['su_id'];
            $branch->branch_code = $su[0].'-CB'.(\App\UnivBranches::where('su_id','=',$su_branches[$i]['su_id'])->get()->count() + 1);
            $branch->branch_abbrv = $su_branches[$i]['branch_abbrv'];
            $branch->branch_name = $su_branches[$i]['branch_name'];
            $branch->branch_address = $su_branches[$i]['branch_address'];
            $branch->branch_add_zipcode = $su_branches[$i]['branch_zipcode'];
            $branch->save();
        }

        for ($i = 0; $i < count($su_colleges); $i++) {
            $code = \App\StateUniversity::where('su_id','=',1)->get()->pluck('su_abbrv');
            $college = new \App\UnivColleges();
            $college->su_id = $su_colleges[$i]['su_id'];
            $college->college_code = 'TWC-'.($code[0]).'-'.$su_colleges[$i]['college_abbrv'];
            $college->college_abbrv = $su_colleges[$i]['college_abbrv'];
            $college->college_name = $su_colleges[$i]['college_name'];
            $college->save();
        }

        for ($i = 0; $i < count($su_college_programs); $i++) {
            $code = DB::table('r_su as Univ')     //'BSU-CECS1'
            ->select('Univ.su_abbrv as SU')
                ->join('r_su_colleges as College','Univ.su_id','=','College.su_id')
                ->where('College.college_id','=',$su_college_programs[$i]['college_id'])
                ->get();
            $college = \App\UnivColleges::where('college_id','=',$su_college_programs[$i]['college_id'])->pluck('college_abbrv');
            $program = new \App\CollegeCourses();
            $program->college_id = $su_college_programs[$i]['college_id'];
            $program->course_code = $code[0]->SU.'-'.$college[0].(\App\CollegeCourses::where('college_id','=',$su_college_programs[$i]['college_id'])->get()->count() + 1);
            $program->course_abbrv = $su_college_programs[$i]['program_abbrv'];
            $program->course_name = $su_college_programs[$i]['program_name'];
            $program->save();
        }

        for ($i = 0; $i < count($users); $i++) {
            // Declare needed variables
            $user_code = 'TWU'.\Carbon\Carbon::now()->format('y-md-').(\App\User::get()->count() + 1);

            // Insert in table users
            $user = new \App\User();
            $user->usr_code = $user_code;
            $user->email = $users[$i]['email'];
            $user->password = bcrypt($users[$i]['pass']);
            $user->save();
            $user_id = $user->id;

            // Insert in table user_infos
            $info = new \App\UserInfo();
            $info->title = $users[$i]['title'];
            $info->first_name = $users[$i]['first_name'];
            $info->middle_name = $users[$i]['middle_name'];
            $info->last_name = $users[$i]['last_name'];
            $info->gender = $users[$i]['gender'];
            $info->bdate = $users[$i]['bdate'];
            $info->tel_no = $users[$i]['tel_no'];
            $info->cel_no = $users[$i]['cel_no'];
            $info->save();
            $info_id = $info->id;

            // Insert in table user_roles
            $UserRole = new \App\UserRole();
            $UserRole->user_id = $user_id;
            $UserRole->info_id = $info_id;
            $UserRole->role_id = $users[$i]['role_id'];
            $UserRole->save();
            $ur_id = $UserRole->id;

            // Insert in table r_su_users
            $UnivUser = new \App\UnivUsers();
            $UnivUser->branch_id = $users[$i]['branch_id'];
            $UnivUser->ur_id = $ur_id;
            $UnivUser->su_id = $users[$i]['su_id'];
            $UnivUser->save();
            $uu_id = $UnivUser->id;

            // Insert in table r_su_user_info
            $UnivUserInfo = new \App\UnivUserInfo();
            $UnivUserInfo->uu_id = $uu_id;
            $UnivUserInfo->college_id = $users[$i]['college_id'];
            $UnivUserInfo->rank_id = $users[$i]['academic_rank'];
            $UnivUserInfo->emp_stat = $users[$i]['emp_stat'];
            $UnivUserInfo->save();

            // Insert in table r_su_usr_educbg
            $UnivUserEducBG = new \App\UnivUserEducBackground();
            $UnivUserEducBG->uu_id = $uu_id;
            $UnivUserEducBG->degree = $users[$i]['degree'];
            $UnivUserEducBG->school_graduated = 'null';     //$input['school_graduated'];
            $UnivUserEducBG->year_graduated = 'null';   //$input['year_graduated'];
            $UnivUserEducBG->received_awards = 'null';  //$input['received_awards'];
            $UnivUserEducBG->save();

            // Insert in table
            $grade_computation = new \App\GradeComputationPercentage();
            $grade_computation->uu_id = $uu_id;
            $grade_computation->save();
        }
    }
}
