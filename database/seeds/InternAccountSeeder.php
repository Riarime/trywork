<?php

use Carbon\Carbon;
use App\User;
use App\UserRole;
use Illuminate\Database\Seeder;

class InternAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $intern_accounts_inputs = [
            ['email' => 'charles.angsioco06@gmail.com', 'password' => bcrypt('password'), 'info' => 1]
        ];

        foreach($intern_accounts_inputs as $input) {
            $code = 'TWU'.(Carbon::now()->format('y-md-')).(User::get()->count() + 1);
            $intern_account = new User();
            $intern_account->usr_code = $code;
            $intern_account->email = $input['email'];
            $intern_account->password = $input['password'];
            $intern_account->save();
            $id = $intern_account->id;

            $role = new UserRole();
            $role->user_id = $id;
            $role->role_id = 5;
            $role->info_id = $input['info'];
            $role->save();
        }
        /*$intern_accounts = [
            ['email' => 'charles.angsioco06@gmail.com', 'pass' => 'password']
        ];

        $UserRole_inputs = [
            ['user_id' => 4, 'role_id' => 5, 'info_id' => 4]
        ];

        foreach($intern_accounts as $intern_account) {
            $code = 'TWU'.Carbon::now()->format('y-md-').(User::get()->count() + 1);
            $user = new \App\User();
            $user->usr_code = $code;
            $user->email = $intern_account['email'];
            $user->password = bcrypt($intern_account['pass']);
            $user->save();
        }

        foreach($UserRole_inputs as $input) {
            $UserRole = new \App\UserRole();
            $UserRole->user_id = $input['user_id'];
            $UserRole->role_id = $input['role_id'];
            $UserRole->info_id = $input['info_id'];
            $UserRole->save();
        }*/
    }
}
