<?php

use Illuminate\Database\Seeder;
use App\UnivBranches;
use App\BranchSem;
use Carbon\Carbon;

class BranchSemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sem_inputs = [
            ['branch_id' => 1, 'semester' => '1st Semester'],
            ['branch_id' => 1, 'semester' => '2nd Semester'],
            ['branch_id' => 1, 'semester' => 'Summer Semester']
        ];

        foreach($sem_inputs as $input) {
            $branch = (UnivBranches::where('branch_id','=',$input['branch_id'])->pluck('branch_abbrv'));
            $sem = new \App\BranchSem();
            $sem->branch_id = $input['branch_id'];
            $sem->sem_code = $branch[0].'-SEM'.(Carbon::now()->format('y')).(BranchSem::get()->count() + 1);     //'BSUM1-SEM181';
            $sem->semester = $input['semester'];
            $sem->save();
        }
    }
}
