<?php

use Illuminate\Database\Seeder;
use App\UnivBranches;
use Carbon\Carbon;
use App\BranchAY;

class BranchAYSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $branch_ay_inputs = [
            ['branch_id' => 1, 'academic_yr' => '2015-2016'],
            ['branch_id' => 1, 'academic_yr' => '2017-2018'],
            ['branch_id' => 1, 'academic_yr' => '2018-2019'],
            ['branch_id' => 1, 'academic_yr' => '2020-2021'],
            ['branch_id' => 1, 'academic_yr' => '2021-2022']
        ];

        foreach($branch_ay_inputs as $input) {
            $branch = UnivBranches::where('branch_id','=',$input['branch_id'])->pluck('branch_abbrv');
            $ay = new \App\BranchAY();
            $ay->branch_id = $input['branch_id'];
            $ay->ay_code = $branch[0].'-'.'AY'.(Carbon::now()->format('y')).(BranchAY::count() + 1);   //'BSUM1-AY181';
            $ay->academic_yr = $input['academic_yr'];
            $ay->save();
        }
    }
}
