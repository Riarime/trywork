<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\User;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_inputs = [
            ['email' => 'alfredo_atienza_jr@gmail.com', 'pass' => 'password'],
            ['email' => 'mayette_canenea@gmail.com', 'pass' => 'password'],
            ['email' => 'adrian_melo@gmail.com', 'pass' => 'password']
        ];

        foreach($user_inputs as $input) {
            $code = 'TWU'.Carbon::now()->format('y-md-').(User::get()->count() + 1);
            $user = new \App\User();
            $user->usr_code = $code;
            $user->email = $input['email'];
            $user->password = bcrypt($input['pass']);
            $user->save();

        }



        // $user = new \App\User();
        // $user->email = 'joshuamiguelmagtibay17@gmail.com';
        // $user->password = bcrypt('password');
        // $user->save();

        // $role = new \App\role();
        // $role->code = 'intern';
        // $role->name = 'Intern';
        // $role->save();

        // $userrole = new \App\UserRole();
        // $userrole->user_id = 1;
        // $userrole->role_id = 1;
        // $userrole->save();

        // $suc = new \App\StateUniversityCollege();
        // $suc->name = 'Polytechnic University of the Philippines Quezon City Branch';
        // $suc->address = 'Don Fabian St., Brgy. Commonwealth, Quezon City';
        // $suc->contact_number = '+632 951 0423';
        // $suc->save();

        // $program = new \App\CourseProgram();
        // $program->code = 'BSIT';
        // $program->name = 'BS Information Technology';
        // $program->save();

        // $program = new \App\CourseProgram();
        // $program->code = 'BBTE';
        // $program->name = 'Bachelor in Business Teacher Education';
        // $program->save();

        // $program = new \App\CourseProgram();
        // $program->code = 'BSBA';
        // $program->name = 'BS Business Administration';
        // $program->save();

        // $major = new \App\CourseMajor();
        // $major->code = 'ITE';
        // $major->name = 'Information Technology Education';
        // $major->save();

        // $major = new \App\CourseMajor();
        // $major->code = 'MM';
        // $major->name = 'Marketing Management';
        // $major->save();

        // $succoursemajor = new \App\SUCProgramMajor();
        // $succoursemajor->state_university_college_id = \App\StateUniversityCollege::where('name', 'Polytechnic University of the Philippines Quezon City Branch')->first()->id;
        // $succoursemajor->course_program_id = \App\CourseProgram::where('code', 'BSIT')->first()->id;
        // $succoursemajor->course_major_id = null;
        // $succoursemajor->save();

        // $succoursemajor = new \App\SUCProgramMajor();
        // $succoursemajor->state_university_college_id = \App\StateUniversityCollege::where('name', 'Polytechnic University of the Philippines Quezon City Branch')->first()->id;
        // $succoursemajor->course_program_id = \App\CourseProgram::where('code', 'BBTE')->first()->id;
        // $succoursemajor->course_major_id = \App\CourseMajor::where('code', 'ITE')->first()->id;
        // $succoursemajor->save();

        // $succoursemajor = new \App\SUCProgramMajor();
        // $succoursemajor->state_university_college_id = \App\StateUniversityCollege::where('name', 'Polytechnic University of the Philippines Quezon City Branch')->first()->id;
        // $succoursemajor->course_program_id = \App\CourseProgram::where('code', 'BSBA')->first()->id;
        // $succoursemajor->course_major_id = \App\CourseMajor::where('code', 'MM')->first()->id;
        // $succoursemajor->save();

//         $skill = new \App\StudentSkill();
//         $skill->name = 'C# Programming';
//         $skill->save();
//
//         $skill = new \App\StudentSkill();
//         $skill->name = 'Java Programming';
//         $skill->save();
//
//         $skill = new \App\StudentSkill();
//         $skill->name = 'Good Speaker';
//         $skill->save();
//
//         $skill = new \App\StudentSkill();
//         $skill->name = 'Video Editing';
//         $skill->save();
//
//         $skill = new \App\StudentSkill();
//         $skill->name = 'Music Production';
//         $skill->save();
//
//         $skill = new \App\StudentSkill();
//         $skill->name = 'Great Advertiser';
//         $skill->save();
//
//         $skill = new \App\StudentSkill();
//         $skill->name = 'PHP Programming';
//         $skill->save();
    }
}
