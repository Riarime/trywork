<?php

use Illuminate\Database\Seeder;
use App\UnivColleges;
use App\CollegeCourses;
use Illuminate\Support\Facades\DB;

class CollegeCoursesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $program_inputs = [
            ['college_id' => 1, 'program_abbrv' => 'BSIT', 'program_name' => 'Bachelor of Science in Information Technology'],
            ['college_id' => 1, 'program_abbrv' => 'BSCS', 'program_name' => 'Bachelor of Science in Computer Sciences'],
            ['college_id' => 1, 'program_abbrv' => 'BSCOE', 'program_name' => 'Bachelor of Science in Computer Engineering'],
            ['college_id' => 2, 'program_abbrv' => 'BSBA-MM', 'program_name' => 'Bachelor of Science in Business Administration Major in Marketing Management'],
            ['college_id' => 2, 'program_abbrv' => 'BSBA-HRDM', 'program_name' => 'Bachelor of Science in Business Administration Major in Human Resource Development Management'],
            ['college_id' => 2, 'program_abbrv' => 'BSEntrep', 'program_name' => 'Bachelor of Science in Entrepreneurship'],
            ['college_id' => 3, 'program_abbrv' => 'BBTE', 'program_name' => 'Bachelor in Business Teacher Education'],
            ['college_id' => 3, 'program_abbrv' => 'BSED-ENG', 'program_name' => 'Bachelor of Secondary Education Major in English'],
            ['college_id' => 4, 'program_abbrv' => 'BSF', 'program_name' => 'Bachelor of Science in Fisheries'],
            ['college_id' => 5, 'program_abbrv' => 'BSTM', 'program_name' => 'Bachelor of Science in Tourism Management'],
            ['college_id' => 5, 'program_abbrv' => 'BSHRM', 'program_name' => 'Bachelor of Science in Hotel and Restaurant Management'],
            ['college_id' => 5, 'program_abbrv' => 'BSA', 'program_name' => 'Bachelor of Science in Accountancy'],
            ['college_id' => 4, 'program_abbrv' => 'BAC', 'program_name' => 'Bachelor of Arts in Communication'],
            ['college_id' => 4, 'program_abbrv' => 'BSP', 'program_name' => 'Bachelor of Science in Psychology'],
        ];

        foreach($program_inputs as $input) {
            $code = DB::table('r_su as Univ')     //'BSU-CECS1'
                ->select('Univ.su_abbrv as SU')
                ->join('r_su_colleges as College','Univ.su_id','=','College.su_id')
                ->where('College.college_id','=',$input['college_id'])
                ->get();
            $college = UnivColleges::where('college_id','=',$input['college_id'])->pluck('college_abbrv');
            $program = new \App\CollegeCourses();
            $program->college_id = $input['college_id'];
            $program->course_code = $code[0]->SU.'-'.$college[0].(CollegeCourses::where('college_id','=',$input['college_id'])->get()->count() + 1);
            $program->course_abbrv = $input['program_abbrv'];
            $program->course_name = $input['program_name'];
            $program->save();
        }
    }
}
