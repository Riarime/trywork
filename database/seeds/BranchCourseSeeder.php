<?php

use Illuminate\Database\Seeder;

class BranchCourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $branch_course_inputs = [
            ['branch' => 1,'user' => 1, 'course' => 1],
            ['branch' => 1,'user' => 1, 'course' => 2],
            ['branch' => 1,'user' => 1, 'course' => 3],
            ['branch' => 1,'user' => 1, 'course' => 9],
            ['branch' => 1,'user' => 1, 'course' => 8],
            ['branch' => 1,'user' => 1, 'course' => 10],
            ['branch' => 1,'user' => 1, 'course' => 11],
            ['branch' => 1,'user' => 1, 'course' => 12],
            ['branch' => 1,'user' => 1, 'course' => 13],
            ['branch' => 1,'user' => 1, 'course' => 14],
        ];

        foreach ($branch_course_inputs as $input) {
            $branch_course = new \App\BranchCourse();
            $branch_course->branch_id = $input['branch'];
            $branch_course->course_id = $input['course'];
            $branch_course->year_section = '4-1';
            $branch_course->uu_id = $input['user'];
            $branch_course->save();
        }
    }
}
