<?php

use Illuminate\Database\Seeder;

class UnivHTESeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $univ_hte_inputs = [
            ['branch' => 1, 'college' => 1, 'hte' => 1],
            ['branch' => 1, 'college' => 1, 'hte' => 2],
            ['branch' => 1, 'college' => 1, 'hte' => 3],
            ['branch' => 1, 'college' => 1, 'hte' => 4],
            ['branch' => 1, 'college' => 1, 'hte' => 5],
            ['branch' => 1, 'college' => 1, 'hte' => 6],
            ['branch' => 1, 'college' => 1, 'hte' => 7],
            ['branch' => 1, 'college' => 1, 'hte' => 8],
            ['branch' => 1, 'college' => 1, 'hte' => 9],
            ['branch' => 1, 'college' => 1, 'hte' => 10],
            ['branch' => 1, 'college' => 1, 'hte' => 11],
            ['branch' => 1, 'college' => 1, 'hte' => 12],
            ['branch' => 1, 'college' => 1, 'hte' => 13],
            ['branch' => 1, 'college' => 1, 'hte' => 14],
            ['branch' => 1, 'college' => 1, 'hte' => 15],
            ['branch' => 1, 'college' => 1, 'hte' => 16],
            ['branch' => 1, 'college' => 1, 'hte' => 17],
            ['branch' => 1, 'college' => 1, 'hte' => 18],
            ['branch' => 1, 'college' => 1, 'hte' => 19],
            ['branch' => 1, 'college' => 1, 'hte' => 20],
            ['branch' => 1, 'college' => 1, 'hte' => 21],
            ['branch' => 1, 'college' => 1, 'hte' => 22],
            ['branch' => 1, 'college' => 1, 'hte' => 23],
            ['branch' => 1, 'college' => 1, 'hte' => 24],
            ['branch' => 1, 'college' => 1, 'hte' => 25],
            ['branch' => 1, 'college' => 1, 'hte' => 26],
            ['branch' => 1, 'college' => 1, 'hte' => 27],
            ['branch' => 1, 'college' => 1, 'hte' => 28],
            ['branch' => 1, 'college' => 1, 'hte' => 29],
            ['branch' => 1, 'college' => 1, 'hte' => 30],
            ['branch' => 1, 'college' => 1, 'hte' => 31],
            ['branch' => 1, 'college' => 1, 'hte' => 32],
            ['branch' => 1, 'college' => 1, 'hte' => 33],
            ['branch' => 1, 'college' => 1, 'hte' => 34],
            ['branch' => 1, 'college' => 1, 'hte' => 35],
            ['branch' => 1, 'college' => 1, 'hte' => 36],
            ['branch' => 1, 'college' => 1, 'hte' => 37],
            ['branch' => 1, 'college' => 1, 'hte' => 38],
            ['branch' => 1, 'college' => 1, 'hte' => 39],
            ['branch' => 1, 'college' => 1, 'hte' => 40],
            ['branch' => 1, 'college' => 1, 'hte' => 41],
            ['branch' => 1, 'college' => 1, 'hte' => 42],
            ['branch' => 1, 'college' => 1, 'hte' => 43],
            ['branch' => 1, 'college' => 1, 'hte' => 44]
        ];

        foreach($univ_hte_inputs as $input) {
            $univ_hte = new \App\UnivHTE();
            $univ_hte->branch_id = $input['branch'];
            $univ_hte->hte_id = $input['hte'];
            $univ_hte->college_id = $input['college'];
            $univ_hte->save();
        }
    }
}
