-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 21, 2018 at 03:29 AM
-- Server version: 10.2.12-MariaDB
-- PHP Version: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id7505384_trywork_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `role_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role_stat` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `role_code`, `name`, `created_at`, `updated_at`, `role_stat`) VALUES
(1, 'TWR18-1015-1', 'System Administrator', '2018-10-15 19:29:08', '2018-10-15 19:29:08', 'Active'),
(2, 'TWR18-1015-2', 'OJT Director', '2018-10-15 19:29:08', '2018-10-15 19:29:08', 'Active'),
(3, 'TWR18-1015-3', 'OJT Head', '2018-10-15 19:29:08', '2018-10-15 19:29:08', 'Active'),
(4, 'TWR18-1015-4', 'OJT Coordinator', '2018-10-15 19:29:08', '2018-10-15 19:29:08', 'Active'),
(5, 'TWR18-1015-5', 'Student Trainee', '2018-10-15 19:29:08', '2018-10-15 19:29:08', 'Active'),
(6, 'TWR18-1015-6', 'HTE Representative', '2018-10-15 19:29:08', '2018-10-15 19:29:08', 'Active'),
(7, 'TWR18-1015-7', 'HTE Supervisor', '2018-10-15 19:29:08', '2018-10-15 19:29:08', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `r_branch_acdmcyr`
--

CREATE TABLE `r_branch_acdmcyr` (
  `ay_id` int(10) UNSIGNED NOT NULL,
  `branch_id` int(10) UNSIGNED NOT NULL,
  `ay_code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `academic_yr` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ay_stat` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `r_branch_acdmcyr`
--

INSERT INTO `r_branch_acdmcyr` (`ay_id`, `branch_id`, `ay_code`, `academic_yr`, `ay_stat`, `created_at`, `updated_at`) VALUES
(1, 1, 'BSUM1-AY181', '2015-2016', 'Active', '2018-10-15 19:29:17', '2018-10-15 19:29:17'),
(2, 1, 'BSUM1-AY182', '2017-2018', 'Active', '2018-10-15 19:29:17', '2018-10-15 19:29:17'),
(3, 1, 'BSUM1-AY183', '2018-2019', 'Active', '2018-10-15 19:29:17', '2018-10-15 19:29:17'),
(4, 1, 'BSUM1-AY184', '2020-2021', 'Active', '2018-10-15 19:29:17', '2018-10-15 19:29:17'),
(5, 1, 'BSUM1-AY185', '2021-2022', 'Active', '2018-10-15 19:29:17', '2018-10-15 19:29:17');

-- --------------------------------------------------------

--
-- Table structure for table `r_branch_active_ay_sem`
--

CREATE TABLE `r_branch_active_ay_sem` (
  `aysem_id` int(10) UNSIGNED NOT NULL,
  `branch_id` int(10) UNSIGNED NOT NULL,
  `ay_id` int(10) UNSIGNED NOT NULL,
  `sem_id` int(10) UNSIGNED NOT NULL,
  `aysem_stat` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `r_branch_active_ay_sem`
--

INSERT INTO `r_branch_active_ay_sem` (`aysem_id`, `branch_id`, `ay_id`, `sem_id`, `aysem_stat`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 'Inactive', '2018-10-15 19:29:18', '2018-10-15 19:29:18'),
(2, 1, 1, 2, 'Active', '2018-10-15 19:29:18', '2018-10-15 19:29:18');

-- --------------------------------------------------------

--
-- Table structure for table `r_branch_courses`
--

CREATE TABLE `r_branch_courses` (
  `bc_id` int(10) UNSIGNED NOT NULL,
  `branch_id` int(10) UNSIGNED NOT NULL,
  `uu_id` int(10) UNSIGNED NOT NULL,
  `course_id` int(10) UNSIGNED NOT NULL,
  `year_section` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `bc_stat` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `r_branch_courses`
--

INSERT INTO `r_branch_courses` (`bc_id`, `branch_id`, `uu_id`, `course_id`, `year_section`, `bc_stat`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '4-1', 'Active', '2018-10-15 19:29:16', '2018-10-15 19:29:16'),
(2, 1, 1, 2, '4-1', 'Active', '2018-10-15 19:29:16', '2018-10-15 19:29:16'),
(3, 1, 1, 3, '4-1', 'Active', '2018-10-15 19:29:16', '2018-10-15 19:29:16');

-- --------------------------------------------------------

--
-- Table structure for table `r_branch_sem`
--

CREATE TABLE `r_branch_sem` (
  `sem_id` int(10) UNSIGNED NOT NULL,
  `branch_id` int(10) UNSIGNED NOT NULL,
  `sem_code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `semester` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sem_stat` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `r_branch_sem`
--

INSERT INTO `r_branch_sem` (`sem_id`, `branch_id`, `sem_code`, `semester`, `sem_stat`, `created_at`, `updated_at`) VALUES
(1, 1, 'BSUM1-SEM181', '1st Semester', 'Active', '2018-10-15 19:29:17', '2018-10-15 19:29:17'),
(2, 1, 'BSUM1-SEM182', '2nd Semester', 'Active', '2018-10-15 19:29:17', '2018-10-15 19:29:17'),
(3, 1, 'BSUM1-SEM183', 'Summer Semester', 'Active', '2018-10-15 19:29:18', '2018-10-15 19:29:18');

-- --------------------------------------------------------

--
-- Table structure for table `r_grade_percentage`
--

CREATE TABLE `r_grade_percentage` (
  `gp_id` int(10) UNSIGNED NOT NULL,
  `uu_id` int(10) UNSIGNED NOT NULL,
  `college_percentage` double NOT NULL DEFAULT 50,
  `hte_percentage` double NOT NULL DEFAULT 50,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `r_grade_percentage`
--

INSERT INTO `r_grade_percentage` (`gp_id`, `uu_id`, `college_percentage`, `hte_percentage`, `created_at`, `updated_at`) VALUES
(1, 1, 50, 50, '2018-10-15 19:29:15', '2018-10-15 19:29:15'),
(2, 2, 50, 50, '2018-10-15 19:29:16', '2018-10-15 19:29:16'),
(3, 3, 50, 50, '2018-10-15 19:29:16', '2018-10-15 19:29:16');

-- --------------------------------------------------------

--
-- Table structure for table `r_hte`
--

CREATE TABLE `r_hte` (
  `hte_id` int(10) UNSIGNED NOT NULL,
  `hte_code` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hte_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `mission` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vision` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `objectives` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region_id` int(10) UNSIGNED DEFAULT NULL,
  `hte_stat` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `r_hte`
--

INSERT INTO `r_hte` (`hte_id`, `hte_code`, `hte_name`, `mission`, `vision`, `objectives`, `address`, `region_id`, `hte_stat`, `created_at`, `updated_at`) VALUES
(1, 'HTE18-101', 'Absolut Distillers, Inc.', NULL, NULL, NULL, 'Brgy. Malaruhatan, Lian, Batangas', 6, 'Active', '2018-10-15 19:29:18', '2018-10-15 19:29:18'),
(2, 'HTE18-102', 'Adventus IT Service Inc.', NULL, NULL, NULL, 'Ayala Avenue', 1, 'Active', '2018-10-15 19:29:18', '2018-10-15 19:29:18'),
(3, 'HTE18-103', 'Batangas I Electric Cooperative Inc.', NULL, NULL, NULL, 'Km. 116 National Highway Calaca, Batangas', 6, 'Active', '2018-10-15 19:29:19', '2018-10-15 19:29:19'),
(4, 'HTE18-104', 'Bausas Surveying Office', NULL, NULL, NULL, '', 6, 'Active', '2018-10-15 19:29:19', '2018-10-15 19:29:19'),
(5, 'HTE18-105', 'Blue Collar Manpower Services Inc.', NULL, NULL, NULL, '2816 Borneo St., Brgy. San Isidro', 6, 'Active', '2018-10-15 19:29:19', '2018-10-15 19:29:19'),
(6, 'HTE18-106', 'Bureau of Fire Protection Nasugbu Branch', NULL, NULL, NULL, 'Municipal Hall, Escalera St.', 6, 'Active', '2018-10-15 19:29:19', '2018-10-15 19:29:19'),
(7, 'HTE18-107', 'Bureau of Fire Protection Lian Batangas', NULL, NULL, NULL, 'J.P. Rizal St., Brgy. 1', 6, 'Active', '2018-10-15 19:29:19', '2018-10-15 19:29:19'),
(8, 'HTE18-108', 'Calatagan Municipal Police Station', NULL, NULL, NULL, '', 6, 'Active', '2018-10-15 19:29:20', '2018-10-15 19:29:20'),
(9, 'HTE18-109', 'Canyon Cove', NULL, NULL, NULL, '', 6, 'Active', '2018-10-15 19:29:20', '2018-10-15 19:29:20'),
(10, 'HTE18-1010', 'Caritas Banco ng Masa', NULL, NULL, NULL, 'Km 1 National Highway', 6, 'Active', '2018-10-15 19:29:20', '2018-10-15 19:29:20'),
(11, 'HTE18-1011', 'Central Azucarera Don Pedro Inc.', NULL, NULL, NULL, 'Bgry. Lumbangan', 6, 'Active', '2018-10-15 19:29:20', '2018-10-15 19:29:20'),
(12, 'HTE18-1012', 'Chateau Royale Sports and Country Club', NULL, NULL, NULL, 'Km. 72 Batulao', 6, 'Active', '2018-10-15 19:29:20', '2018-10-15 19:29:20'),
(13, 'HTE18-1013', 'Club Punta Fuego', NULL, NULL, NULL, 'Brgy. Balaytigue', 6, 'Active', '2018-10-15 19:29:20', '2018-10-15 19:29:20'),
(14, 'HTE18-1014', 'Convergys Philippines, Inc.', NULL, NULL, NULL, 'Convergys 4f Glorieta 5, Ayala', 6, 'Active', '2018-10-15 19:29:20', '2018-10-15 19:29:20'),
(15, 'HTE18-1015', 'FourPoint Zero, Inc.', NULL, NULL, NULL, '814 West Tektite Tower, Ortigas Center', 6, 'Active', '2018-10-15 19:29:20', '2018-10-15 19:29:20'),
(16, 'HTE18-1016', 'Good Heart Marketing Company Inc.', NULL, NULL, NULL, 'Caybunga', 6, 'Active', '2018-10-15 19:29:21', '2018-10-15 19:29:21'),
(17, 'HTE18-1017', 'Land Transportation Office Balayan Extension', NULL, NULL, NULL, '', 6, 'Active', '2018-10-15 19:29:21', '2018-10-15 19:29:21'),
(18, 'HTE18-1018', 'Landbank of the Philippines', NULL, NULL, NULL, 'J.P. Laurel St., 4231', 6, 'Active', '2018-10-15 19:29:21', '2018-10-15 19:29:21'),
(19, 'HTE18-1019', 'Lian Municipal Police Station', NULL, NULL, NULL, 'Malaruhatan', 6, 'Active', '2018-10-15 19:29:21', '2018-10-15 19:29:21'),
(20, 'HTE18-1020', 'Lian Water District', NULL, NULL, NULL, 'Brgy. 4', 6, 'Active', '2018-10-15 19:29:22', '2018-10-15 19:29:22'),
(21, 'HTE18-1021', 'Malarayat Rural Bank, Inc.', NULL, NULL, NULL, 'Burgos St.', 6, 'Active', '2018-10-15 19:29:22', '2018-10-15 19:29:22'),
(22, 'HTE18-1022', 'Medical Center Western Batangas', NULL, NULL, NULL, 'Brgy. Lanatan', 6, 'Active', '2018-10-15 19:29:22', '2018-10-15 19:29:22'),
(23, 'HTE18-1023', 'Municipality of Calatagan', NULL, NULL, NULL, '', 6, 'Active', '2018-10-15 19:29:22', '2018-10-15 19:29:22'),
(24, 'HTE18-1024', 'Municipality of Lian', NULL, NULL, NULL, 'J. P. Rizal St. Brgy. 1', 6, 'Active', '2018-10-15 19:29:22', '2018-10-15 19:29:22'),
(25, 'HTE18-1025', 'Municipality of Magallanes', NULL, NULL, NULL, '', 6, 'Active', '2018-10-15 19:29:22', '2018-10-15 19:29:22'),
(26, 'HTE18-1026', 'Municipality of Nasugbu', NULL, NULL, NULL, '', 6, 'Active', '2018-10-15 19:29:22', '2018-10-15 19:29:22'),
(27, 'HTE18-1027', 'Municipality of Tuy', NULL, NULL, NULL, 'Brgy. Luna,Poblacion', 6, 'Active', '2018-10-15 19:29:22', '2018-10-15 19:29:22'),
(28, 'HTE18-1028', 'Nasugbu Water District', NULL, NULL, NULL, 'J. P. Laurel St. Brgy. 12', 6, 'Active', '2018-10-15 19:29:22', '2018-10-15 19:29:22'),
(29, 'HTE18-1029', 'Orange Apps Inc.', NULL, NULL, NULL, 'Unit 101 Minnesota Mansion', 6, 'Active', '2018-10-15 19:29:22', '2018-10-15 19:29:22'),
(30, 'HTE18-1030', 'PhilAm Life', NULL, NULL, NULL, 'J.P. Laurel St., Brgy.2', 6, 'Active', '2018-10-15 19:29:23', '2018-10-15 19:29:23'),
(31, 'HTE18-1031', 'Philippine National Bank', NULL, NULL, NULL, '', 6, 'Active', '2018-10-15 19:29:23', '2018-10-15 19:29:23'),
(32, 'HTE18-1032', 'Philippine Red Cross Batangas Chapter', NULL, NULL, NULL, 'Conception St., Brgy. 4', 6, 'Active', '2018-10-15 19:29:23', '2018-10-15 19:29:23'),
(33, 'HTE18-1033', 'PLDT', NULL, NULL, NULL, 'Brgy. Bucana', 6, 'Active', '2018-10-15 19:29:23', '2018-10-15 19:29:23'),
(34, 'HTE18-1034', 'QMS', NULL, NULL, NULL, 'Cavite Economic Zone', 6, 'Active', '2018-10-15 19:29:23', '2018-10-15 19:29:23'),
(35, 'HTE18-1035', 'Quad X', NULL, NULL, NULL, 'Chino Roces Ext.', 6, 'Active', '2018-10-15 19:29:24', '2018-10-15 19:29:24'),
(36, 'HTE18-1036', 'Seraph Security Agency Company, Inc.', NULL, NULL, NULL, 'Brgy. Laginghanda', 6, 'Active', '2018-10-15 19:29:24', '2018-10-15 19:29:24'),
(37, 'HTE18-1037', 'St. Peter Life Plan Inc.', NULL, NULL, NULL, 'J. P. Laurel St. Brgy. Lumbangan', 6, 'Active', '2018-10-15 19:29:24', '2018-10-15 19:29:24'),
(38, 'HTE18-1038', 'Stilts Calatagan Beach Resort', NULL, NULL, NULL, 'Brgy. Sta Ana', 6, 'Active', '2018-10-15 19:29:24', '2018-10-15 19:29:24'),
(39, 'HTE18-1039', 'Teleperformance Philippines', NULL, NULL, NULL, 'SM MOA Complex', 6, 'Active', '2018-10-15 19:29:24', '2018-10-15 19:29:24'),
(40, 'HTE18-1040', 'Teletech', NULL, NULL, NULL, 'Robinsons Place', 6, 'Active', '2018-10-15 19:29:24', '2018-10-15 19:29:24'),
(41, 'HTE18-1041', 'United Labor Service Cooperative', NULL, NULL, NULL, 'Brgy. Lumbangan', 6, 'Active', '2018-10-15 19:29:25', '2018-10-15 19:29:25'),
(42, 'HTE18-1042', 'Universal Robina Corporation Inc.', NULL, NULL, NULL, '', 6, 'Active', '2018-10-15 19:29:25', '2018-10-15 19:29:25'),
(43, 'HTE18-1043', 'Upteam Corporation Limited', NULL, NULL, NULL, 'Legaspi Village', 6, 'Active', '2018-10-15 19:29:25', '2018-10-15 19:29:25'),
(44, 'HTE18-1044', 'Western Balayan Telephone System Inc.', NULL, NULL, NULL, '197 Paz St.', 6, 'Active', '2018-10-15 19:29:25', '2018-10-15 19:29:25'),
(45, 'HTE18-1045', 'AMPLEON Phillipines, Inc.', NULL, NULL, NULL, 'Binary St., Light Industry and Science Park, Cabuyao, Laguna', 6, 'Active', '2018-10-15 19:29:25', '2018-10-15 19:29:25');

-- --------------------------------------------------------

--
-- Table structure for table `r_hte_contact`
--

CREATE TABLE `r_hte_contact` (
  `hte_id` int(10) UNSIGNED NOT NULL,
  `contact_person` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_no` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `r_hte_contact`
--

INSERT INTO `r_hte_contact` (`hte_id`, `contact_person`, `designation`, `contact_no`) VALUES
(1, 'Manolita C. Bayran', 'HR Manager', '09283924785'),
(2, 'Karen Mae J. Canopin', 'HR and Sales Assistant', '09352699225'),
(3, 'Marivic S. Balbacal', 'ISD Manager', '(101) 424-0100'),
(4, 'Maria Flor A. Barinki', 'Office Staff', '09175302495'),
(5, 'Rolando Raz Nicodemuz', 'HR Manager', '09218222703'),
(6, 'SFO2 Zemon B. Bacit', 'OIC/MFM', '09351692690'),
(7, 'SF01 Gregorio N. Chavez', 'OIC ,Municipal Fire Marshal', '09156021982'),
(8, 'Radam R. Ramos', 'Police Inspector', '09272979771'),
(9, 'Mhel Atienza', 'Marketing Supervisor', '09059733890'),
(10, 'Lillosa G. Madrigal', 'Cashier', '09988564663'),
(11, 'Carlota V. Inumerable', 'Department Head', '09361977470'),
(12, 'Frauline M. Padua', 'HR Officer', '09064012854'),
(13, 'Charlene Realon', 'HR Supervisor', '09173684930'),
(14, 'Myrna Garcia', 'Souring Dept.', '09178704337'),
(15, 'Marlon Randall Umali', 'CMO', '09062428531'),
(16, 'Juvy Ann J. Soriano', 'HR-Supervisor', '09971988331'),
(17, 'Sherwin J. Lontoc', 'Asst. Chief', '09275498332'),
(18, 'Alicia E. Climaco', 'Acting BSO', '09209243170'),
(19, 'SPO4 Rafael M. Panganiban', 'MESPO', '09163157619'),
(20, 'Sherwin Napoleon M. Jonson', 'General Manager', '09175906463'),
(21, 'Analissa P. Calingasan', 'Branch Manager', '09175920033'),
(22, 'Aileen M. Villanueva', 'Payroll Officer', '09771543326'),
(23, 'Glenn Z. Aytona', 'Executive Assistnat', '09178695431'),
(24, 'Regiela M. Abreu', 'HRMO III', '09178463293'),
(25, 'Vilma O. Solayao', 'HRMO IV', '09357555163'),
(26, 'Virgilio Vargas', 'HRMO IV', '09175052139'),
(27, 'Emanuel A. Afable', 'HRMO', ''),
(28, 'Sean Romulo Zabala', 'Admin Finance', '(043) 216-2819'),
(29, 'Inno Angelo Ferrer', 'Office Manager', '09174355207'),
(30, 'Glenna Borge', 'Associate Agency Manager', '09171187745'),
(31, 'Amorlita Luya', 'Branch Manager', '(043) - 4160070'),
(32, 'Angelica R. Rinoz', 'SIC-FG/ Cahier', '09255500558'),
(33, 'Ronaldo J. Enriquez', 'STN Supervisor', '(02) 732-6704'),
(34, 'Jennifer V. Fabic', 'DC', '09985629401'),
(35, 'Christine Crisostomo', 'Department Head', '(02) 877-8243'),
(36, 'Atty. Magtanggol B. Gatdula', 'President/CEO', '(02) 3557644'),
(37, 'Alejo Magsino', 'Branch Manager', '09178961569'),
(38, 'Arman D. Herrera', 'Accounting Supervisor', '09176174046'),
(39, 'Norman Lucero', 'Desktop Engineer', '09208079888'),
(40, 'Allberto Pesigan Jr.', 'SITE IT', '09753879007'),
(41, 'Alice R. Cedo', 'Proj. Mgt and Dev Supervisor', '09153853269'),
(42, 'Michelle M. Tapire', 'HR Manager', '09988403845'),
(43, 'Raphael Francis L. Quisumbing ', 'Chief Technology Officer', '09209717073'),
(44, 'Ma Florizel B. Pantoja', 'Admin Manager', '(043) 211-4202'),
(45, 'Ms. Jocelyn Cadiente', 'HR Senior Specialist', '9985624937');

-- --------------------------------------------------------

--
-- Table structure for table `r_hte_job_trainings`
--

CREATE TABLE `r_hte_job_trainings` (
  `job_training_id` int(10) UNSIGNED NOT NULL,
  `hte_id` int(10) UNSIGNED NOT NULL,
  `training_code` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_training_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `start` date NOT NULL,
  `job_training_stat` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `r_hte_job_trainings`
--

INSERT INTO `r_hte_job_trainings` (`job_training_id`, `hte_id`, `training_code`, `job_training_name`, `description`, `start`, `job_training_stat`, `created_at`, `updated_at`) VALUES
(1, 1, 'OJT1234', 'Training for OJT Interns with Information Technology Course', 'This internship is dedicated to the ones who have skills in Java and PHP Programming.', '2018-10-17', 'Active', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `r_hte_users`
--

CREATE TABLE `r_hte_users` (
  `hu_id` int(10) UNSIGNED NOT NULL,
  `hte_id` int(10) UNSIGNED NOT NULL,
  `ur_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `r_intern_application_status`
--

CREATE TABLE `r_intern_application_status` (
  `iappstat_id` int(10) UNSIGNED NOT NULL,
  `iappstat_code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `r_intern_application_status`
--

INSERT INTO `r_intern_application_status` (`iappstat_id`, `iappstat_code`, `name`, `created_at`, `updated_at`) VALUES
(1, 'TWIAS-1', 'Pending', '2018-10-15 19:30:17', '2018-10-15 19:30:17'),
(2, 'TWIAS-2', 'Not Suitable', '2018-10-15 19:30:17', '2018-10-15 19:30:17'),
(3, 'TWIAS-3', 'For Inteview', '2018-10-15 19:30:17', '2018-10-15 19:30:17'),
(4, 'TWIAS-4', 'Passed', '2018-10-15 19:30:17', '2018-10-15 19:30:17');

-- --------------------------------------------------------

--
-- Table structure for table `r_intern_referral`
--

CREATE TABLE `r_intern_referral` (
  `intern_id` int(10) UNSIGNED NOT NULL,
  `referral_code` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Status` enum('Used') COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `r_intern_referral`
--

INSERT INTO `r_intern_referral` (`intern_id`, `referral_code`, `Status`) VALUES
(1, '181510-1929280', NULL),
(2, '181510-1929291', NULL),
(3, '181510-1929302', NULL),
(4, '181510-1929313', NULL),
(5, '181510-1929324', NULL),
(6, '181510-1929325', NULL),
(7, '181510-1929336', NULL),
(8, '181510-1929347', NULL),
(9, '181510-1929348', NULL),
(10, '181510-1929359', NULL),
(11, '181510-19293610', NULL),
(12, '181510-19293611', NULL),
(13, '181510-19293712', NULL),
(14, '181510-19293813', NULL),
(15, '181510-19293814', NULL),
(16, '181510-19293915', NULL),
(17, '181510-19294016', NULL),
(18, '181510-19294117', NULL),
(19, '181510-19294218', NULL),
(20, '181510-19294219', NULL),
(21, '181510-19294320', NULL),
(22, '181510-19294421', NULL),
(23, '181510-19294522', NULL),
(24, '181510-19294623', NULL),
(25, '181510-19294624', NULL),
(26, '181510-19294725', NULL),
(27, '181510-19294826', NULL),
(28, '181510-19294927', NULL),
(29, '181510-19295028', NULL),
(30, '181510-19295129', NULL),
(31, '181510-19295130', NULL),
(32, '181510-19295231', NULL),
(33, '181510-19295332', NULL),
(34, '181510-19295333', NULL),
(35, '181510-19295434', NULL),
(36, '181510-19295535', NULL),
(37, '181510-19295636', NULL),
(38, '181510-19295737', NULL),
(39, '181510-19295738', NULL),
(40, '181510-19295839', NULL),
(41, '181510-19295940', NULL),
(42, '181510-19295941', NULL),
(43, '181510-19300042', NULL),
(44, '181510-19300143', NULL),
(45, '181510-19300244', NULL),
(46, '181510-19300245', NULL),
(47, '181510-19300346', NULL),
(48, '181510-19300347', NULL),
(49, '181510-19300448', NULL),
(50, '181510-19300549', NULL),
(51, '181510-19300550', NULL),
(52, '181510-19300651', NULL),
(53, '181510-19300752', NULL),
(54, '181510-19300753', NULL),
(55, '181510-19300854', NULL),
(56, '181510-19300955', NULL),
(57, '181510-19300956', NULL),
(58, '181510-19301057', NULL),
(59, '181510-19301158', NULL),
(60, '181510-19301259', NULL),
(61, '181510-19301360', NULL),
(62, '181510-19301361', NULL),
(63, '181510-19301462', NULL),
(64, '181510-19301563', NULL),
(65, '181510-19301664', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `r_intern_skills`
--

CREATE TABLE `r_intern_skills` (
  `skill_id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `r_intern_skills`
--

INSERT INTO `r_intern_skills` (`skill_id`, `name`) VALUES
(1, 'C# Programming'),
(2, 'Java Programming'),
(3, 'Good Speaker'),
(4, 'Video Editing'),
(5, 'Music Production'),
(6, 'Great Advertiser'),
(7, 'PHP Programming');

-- --------------------------------------------------------

--
-- Table structure for table `r_intern_skill_sets`
--

CREATE TABLE `r_intern_skill_sets` (
  `id` int(10) UNSIGNED NOT NULL,
  `intern_id` int(10) UNSIGNED NOT NULL,
  `skill_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `r_intern_skill_sets`
--

INSERT INTO `r_intern_skill_sets` (`id`, `intern_id`, `skill_id`) VALUES
(1, 1, 2),
(2, 1, 7);

-- --------------------------------------------------------

--
-- Table structure for table `r_regions`
--

CREATE TABLE `r_regions` (
  `region_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `r_regions`
--

INSERT INTO `r_regions` (`region_id`, `code`, `name`, `created_at`, `updated_at`) VALUES
(1, 'NCR', 'National Capital Region', '2018-10-15 19:29:08', '2018-10-15 19:29:08'),
(2, 'CAR', 'Cordillera Administrative Region', '2018-10-15 19:29:08', '2018-10-15 19:29:08'),
(3, 'R1', 'Ilocos', '2018-10-15 19:29:09', '2018-10-15 19:29:09'),
(4, 'R2', 'Cagayan Valley', '2018-10-15 19:29:09', '2018-10-15 19:29:09'),
(5, 'R3', 'Central Luzon', '2018-10-15 19:29:09', '2018-10-15 19:29:09'),
(6, 'R4-A', 'CALABARZON', '2018-10-15 19:29:09', '2018-10-15 19:29:09'),
(7, 'R4-B', 'MIMAROPA', '2018-10-15 19:29:09', '2018-10-15 19:29:09'),
(8, 'R5', 'Bicol', '2018-10-15 19:29:09', '2018-10-15 19:29:09'),
(9, 'R7', 'Central Visayas', '2018-10-15 19:29:09', '2018-10-15 19:29:09'),
(10, 'R8', 'Eastern Visayas', '2018-10-15 19:29:09', '2018-10-15 19:29:09'),
(11, 'R9', 'Zamboanga Peninsula', '2018-10-15 19:29:09', '2018-10-15 19:29:09'),
(12, 'R10', 'Northern Mindanao', '2018-10-15 19:29:09', '2018-10-15 19:29:09'),
(13, 'R11', 'Davao Region', '2018-10-15 19:29:09', '2018-10-15 19:29:09'),
(14, 'R12', 'SOCCSKSARGEN', '2018-10-15 19:29:09', '2018-10-15 19:29:09'),
(15, 'R13', 'Caraga Administrative Region', '2018-10-15 19:29:09', '2018-10-15 19:29:09');

-- --------------------------------------------------------

--
-- Table structure for table `r_seminar_trainings_attended`
--

CREATE TABLE `r_seminar_trainings_attended` (
  `uu_id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `agency_sponsor` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `seminar_level` enum('International','Regional','Local') COLLATE utf8mb4_unicode_ci NOT NULL,
  `seminar_date` date NOT NULL,
  `seminar_venue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `r_su`
--

CREATE TABLE `r_su` (
  `su_id` int(10) UNSIGNED NOT NULL,
  `su_code` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `su_abbrv` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `su_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `su_stat` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `r_su`
--

INSERT INTO `r_su` (`su_id`, `su_code`, `su_abbrv`, `su_name`, `su_stat`, `created_at`, `updated_at`) VALUES
(1, 'TWSU18-101', 'BATSTATE-U', 'Batangas State University', 'Active', '2018-10-15 19:29:12', '2018-10-15 19:29:12'),
(2, 'TWSU18-102', 'EARIST', 'Eulogio \"Amang\" Rodriguez Institute of Science and Technology', 'Active', '2018-10-15 19:29:13', '2018-10-15 19:29:13'),
(3, 'TWSU18-103', 'MPC', 'Marikina Polytechnic College', 'Active', '2018-10-15 19:29:13', '2018-10-15 19:29:13'),
(4, 'TWSU18-104', 'PNU', 'Philippine Normal University', 'Active', '2018-10-15 19:29:13', '2018-10-15 19:29:13'),
(5, 'TWSU18-105', 'PSCA', 'Philippine State College of Aeronautics', 'Active', '2018-10-15 19:29:13', '2018-10-15 19:29:13'),
(6, 'TWSU18-106', 'PUP', 'Polytechnic University of the Philippines', 'Active', '2018-10-15 19:29:13', '2018-10-15 19:29:13'),
(7, 'TWSU18-107', 'RTU', 'Rizal Technological University', 'Active', '2018-10-15 19:29:13', '2018-10-15 19:29:13'),
(8, 'TWSU18-108', 'TUP', 'Technological University of the Philippines', 'Active', '2018-10-15 19:29:13', '2018-10-15 19:29:13'),
(9, 'TWSU18-109', 'UP', 'University of the Philippines System', 'Active', '2018-10-15 19:29:13', '2018-10-15 19:29:13');

-- --------------------------------------------------------

--
-- Table structure for table `r_su_academic_rank`
--

CREATE TABLE `r_su_academic_rank` (
  `rank_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rank_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `rank_stat` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `r_su_academic_rank`
--

INSERT INTO `r_su_academic_rank` (`rank_id`, `code`, `rank_name`, `rank_stat`, `created_at`, `updated_at`) VALUES
(1, 'TW18-AR1', 'Instructor I]', 'Active', '2018-10-15 19:29:09', '2018-10-15 19:29:09'),
(2, 'TW18-AR2', 'Instructor II', 'Active', '2018-10-15 19:29:09', '2018-10-15 19:29:09'),
(3, 'TW18-AR3', 'Instructor III', 'Active', '2018-10-15 19:29:09', '2018-10-15 19:29:09'),
(4, 'TW18-AR4', 'Assisstant Professor I', 'Active', '2018-10-15 19:29:10', '2018-10-15 19:29:10'),
(5, 'TW18-AR5', 'Assisstant Professor II', 'Active', '2018-10-15 19:29:10', '2018-10-15 19:29:10'),
(6, 'TW18-AR6', 'Assisstant Professor III', 'Active', '2018-10-15 19:29:10', '2018-10-15 19:29:10'),
(7, 'TW18-AR7', 'Assisstant Professor IV', 'Active', '2018-10-15 19:29:10', '2018-10-15 19:29:10'),
(8, 'TW18-AR8', 'Associate Professor I', 'Active', '2018-10-15 19:29:10', '2018-10-15 19:29:10'),
(9, 'TW18-AR9', 'Associate Professor II', 'Active', '2018-10-15 19:29:10', '2018-10-15 19:29:10'),
(10, 'TW18-AR10', 'Associate Professor III', 'Active', '2018-10-15 19:29:10', '2018-10-15 19:29:10'),
(11, 'TW18-AR11', 'Associate Professor IV', 'Active', '2018-10-15 19:29:10', '2018-10-15 19:29:10'),
(12, 'TW18-AR12', 'Associate Professor V', 'Active', '2018-10-15 19:29:10', '2018-10-15 19:29:10'),
(13, 'TW18-AR13', 'Professor I', 'Active', '2018-10-15 19:29:10', '2018-10-15 19:29:10'),
(14, 'TW18-AR14', 'Professor II', 'Active', '2018-10-15 19:29:10', '2018-10-15 19:29:10'),
(15, 'TW18-AR15', 'Professor III', 'Active', '2018-10-15 19:29:10', '2018-10-15 19:29:10'),
(16, 'TW18-AR16', 'Professor IV', 'Active', '2018-10-15 19:29:10', '2018-10-15 19:29:10'),
(17, 'TW18-AR17', 'Professor V', 'Active', '2018-10-15 19:29:10', '2018-10-15 19:29:10'),
(18, 'TW18-AR18', 'Professor VI', 'Active', '2018-10-15 19:29:10', '2018-10-15 19:29:10'),
(19, 'TW18-AR19', 'Professor VII', 'Active', '2018-10-15 19:29:10', '2018-10-15 19:29:10'),
(20, 'TW18-AR20', 'College/University Professor', 'Active', '2018-10-15 19:29:10', '2018-10-15 19:29:10');

-- --------------------------------------------------------

--
-- Table structure for table `r_su_branches`
--

CREATE TABLE `r_su_branches` (
  `branch_id` int(10) UNSIGNED NOT NULL,
  `su_id` int(10) UNSIGNED NOT NULL,
  `branch_code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_abbrv` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_add_zipcode` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_stat` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `r_su_branches`
--

INSERT INTO `r_su_branches` (`branch_id`, `su_id`, `branch_code`, `branch_abbrv`, `branch_name`, `branch_address`, `branch_add_zipcode`, `branch_stat`, `created_at`, `updated_at`) VALUES
(1, 1, 'BATSTATE-U-CB1', 'BSUM1', 'Batangas State University Pablo Borbon Main Campus I', 'Gov. Pablo Borbon, Main Campus I,Rizal Ave., Batangas City', '1234', 'Active', '2018-10-15 19:29:13', '2018-10-15 19:29:13');

-- --------------------------------------------------------

--
-- Table structure for table `r_su_colleges`
--

CREATE TABLE `r_su_colleges` (
  `college_id` int(10) UNSIGNED NOT NULL,
  `su_id` int(10) UNSIGNED NOT NULL,
  `college_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `college_abbrv` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `college_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `college_stat` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `r_su_colleges`
--

INSERT INTO `r_su_colleges` (`college_id`, `su_id`, `college_code`, `college_abbrv`, `college_name`, `college_stat`, `created_at`, `updated_at`) VALUES
(1, 1, 'TWC-BATSTATE-U-CECS', 'CECS', 'College of Engineering and Computing Sciences', 'Active', '2018-10-15 19:29:13', '2018-10-15 19:29:13'),
(2, 1, 'TWC-BATSTATE-U-CBA', 'CBA', 'College of Business Administration', 'Active', '2018-10-15 19:29:13', '2018-10-15 19:29:13'),
(3, 1, 'TWC-BATSTATE-U-COED', 'COED', 'College of Education', 'Active', '2018-10-15 19:29:13', '2018-10-15 19:29:13'),
(4, 1, 'TWC-BATSTATE-U-CAS', 'CAS', 'College of Arts and Sciences', 'Active', '2018-10-15 19:29:14', '2018-10-15 19:29:14'),
(5, 1, 'TWC-BATSTATE-U-CABEIHM', 'CABEIHM', 'College of Accountancy and Business Economics and Internatonal Hospitality Management', 'Active', '2018-10-15 19:29:14', '2018-10-15 19:29:14');

-- --------------------------------------------------------

--
-- Table structure for table `r_su_college_courses`
--

CREATE TABLE `r_su_college_courses` (
  `course_id` int(10) UNSIGNED NOT NULL,
  `college_id` int(10) UNSIGNED NOT NULL,
  `course_code` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_abbrv` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_desc` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `course_stat` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `r_su_college_courses`
--

INSERT INTO `r_su_college_courses` (`course_id`, `college_id`, `course_code`, `course_abbrv`, `course_name`, `course_desc`, `course_stat`, `created_at`, `updated_at`) VALUES
(1, 1, 'BATSTATE-U-CECS1', 'BSIT', 'Bachelor of Science in Information Technology', NULL, 'Active', '2018-10-15 19:29:14', '2018-10-15 19:29:14'),
(2, 1, 'BATSTATE-U-CECS2', 'BSCS', 'Bachelor of Science in Computer Sciences', NULL, 'Active', '2018-10-15 19:29:14', '2018-10-15 19:29:14'),
(3, 1, 'BATSTATE-U-CECS3', 'BSCOE', 'Bachelor of Science in Computer Engineering', NULL, 'Active', '2018-10-15 19:29:14', '2018-10-15 19:29:14'),
(4, 2, 'BATSTATE-U-CBA1', 'BSBA-MM', 'Bachelor of Science in Business Administration Major in Marketing Management', NULL, 'Active', '2018-10-15 19:29:14', '2018-10-15 19:29:14'),
(5, 2, 'BATSTATE-U-CBA2', 'BSBA-HRDM', 'Bachelor of Science in Business Administration Major in Human Resource Development Management', NULL, 'Active', '2018-10-15 19:29:14', '2018-10-15 19:29:14'),
(6, 2, 'BATSTATE-U-CBA3', 'BSEntrep', 'Bachelor of Science in Entrepreneurship', NULL, 'Active', '2018-10-15 19:29:14', '2018-10-15 19:29:14'),
(7, 3, 'BATSTATE-U-COED1', 'BBTE', 'Bachelor in Business Teacher Education', NULL, 'Active', '2018-10-15 19:29:14', '2018-10-15 19:29:14'),
(8, 3, 'BATSTATE-U-COED2', 'BSED-ENG', 'Bachelor of Secondary Education Major in English', NULL, 'Active', '2018-10-15 19:29:14', '2018-10-15 19:29:14'),
(9, 4, 'BATSTATE-U-CAS1', 'BSF', 'Bachelor of Science in Fisheries', NULL, 'Active', '2018-10-15 19:29:15', '2018-10-15 19:29:15'),
(10, 5, 'BATSTATE-U-CABEIHM1', 'BSTM', 'Bachelor of Science in Tourism Management', NULL, 'Active', '2018-10-15 19:29:15', '2018-10-15 19:29:15'),
(11, 5, 'BATSTATE-U-CABEIHM2', 'BSHRM', 'Bachelor of Science in Hotel and Restaurant Management', NULL, 'Active', '2018-10-15 19:29:15', '2018-10-15 19:29:15');

-- --------------------------------------------------------

--
-- Table structure for table `r_su_intern`
--

CREATE TABLE `r_su_intern` (
  `intern_id` int(10) UNSIGNED NOT NULL,
  `branch_id` int(10) UNSIGNED NOT NULL,
  `ay_id` int(10) UNSIGNED NOT NULL,
  `bc_id` int(10) UNSIGNED NOT NULL,
  `info_id` int(10) UNSIGNED NOT NULL,
  `intern_code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stud_no` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `intern_address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region_id` int(10) UNSIGNED DEFAULT NULL,
  `intern_stat` enum('Enrolled','Approved','Not Qualified','Internship','Completed') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Enrolled',
  `su_usr_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `r_su_intern`
--

INSERT INTO `r_su_intern` (`intern_id`, `branch_id`, `ay_id`, `bc_id`, `info_id`, `intern_code`, `stud_no`, `contact_email`, `intern_address`, `region_id`, `intern_stat`, `su_usr_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 4, 'TW-I1-0', '2015-00001', 'charles.angsioco06@gmail.com', 'ACM Woodstock Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:28', '2018-10-15 19:29:28'),
(2, 1, 1, 1, 5, 'TW-I1-1', '2015-00002', 'johnhenry_angsioco@yahoo.com', 'Miralles Subd. I Talangan Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:29', '2018-10-15 19:29:29'),
(3, 1, 1, 1, 6, 'TW-I1-2', '2015-00003', 'reyann.angioco@gmail.com', 'Brgy. Bucana, Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:30', '2018-10-15 19:29:30'),
(4, 1, 1, 1, 7, 'TW-I1-3', '2015-00004', 'rome.anthony00@gmail.com', 'Lumbangan, Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:31', '2018-10-15 19:29:31'),
(5, 1, 1, 1, 8, 'TW-I1-4', '2015-00005', 'dennerson1993@gmail.com', 'Brgy. Luntal, Tuy, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:32', '2018-10-15 19:29:32'),
(6, 1, 1, 1, 9, 'TW-I1-5', '2015-00006', 'bacquianjezer@gmail.com', 'Brgy. Malaruhatan, Lian, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:32', '2018-10-15 19:29:32'),
(7, 1, 1, 1, 10, 'TW-I1-6', '2015-00007', 'barrionjohnlawrrence@gmail.com', 'Brgy. Dayap, Nasgubu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:33', '2018-10-15 19:29:33'),
(8, 1, 1, 1, 11, 'TW-I1-7', '2015-00008', 'jmbayubay8888@gmail.com', 'M. Apacible St. Brgy. 3 Lian, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:34', '2018-10-15 19:29:34'),
(9, 1, 1, 1, 12, 'TW-I1-8', '2015-00009', 'zongdex2993@gmail.com', 'Brgy. Kapito Lian, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:34', '2018-10-15 19:29:34'),
(10, 1, 1, 1, 13, 'TW-I1-9', '2015-00010', 'caisip.alexis@gmail.com', 'Catandaan, Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:35', '2018-10-15 19:29:35'),
(11, 1, 1, 1, 14, 'TW-I1-10', '2015-00011', '9murcielago.kj@gmail.com', 'Brgy. Cogonan, Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:36', '2018-10-15 19:29:36'),
(12, 1, 1, 1, 15, 'TW-I1-11', '2015-00012', 'joshuacaylecantojos@gmail.com', 'Brgy. 9 Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:36', '2018-10-15 19:29:36'),
(13, 1, 1, 1, 16, 'TW-I1-12', '2015-00013', 'manucerrado@gmail.com', 'Rizal St. Tuy, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:37', '2018-10-15 19:29:37'),
(14, 1, 1, 1, 17, 'TW-I1-13', '2015-00014', 'nilbertchavez0716@gmail.com', 'Balanoy Prenza Lian, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:38', '2018-10-15 19:29:38'),
(15, 1, 1, 1, 18, 'TW-I1-14', '2015-00015', 'christiancruzado49@gmail.com', 'Brgy. Puting Kahoy, Lian, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:38', '2018-10-15 19:29:38'),
(16, 1, 1, 1, 19, 'TW-I1-15', '2015-00016', 'orieldecastro16@gmail.com', 'Brgy. Lumbangan, Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:39', '2018-10-15 19:29:39'),
(17, 1, 1, 1, 20, 'TW-I1-16', '2015-00017', 'roicedelasalas@yahoo.com', 'Brgy. 7, Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:40', '2018-10-15 19:29:40'),
(18, 1, 1, 1, 21, 'TW-I1-17', '2015-00018', 'Kel03deleola@yahoo.com', 'Brgy. Magahis 2, Tuy, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:41', '2018-10-15 19:29:41'),
(19, 1, 1, 1, 22, 'TW-I1-18', '2015-00019', 'earlrandel@gmail.com', 'Brgy. Banilad, Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:42', '2018-10-15 19:29:42'),
(20, 1, 1, 1, 23, 'TW-I1-19', '2015-00020', 'markjosephdolormente@gmail.com', 'Brgy. Cumba, Lian, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:42', '2018-10-15 19:29:42'),
(21, 1, 1, 1, 24, 'TW-I1-20', '2015-00021', 'wadglenn6@gmail.com', 'Brgy. 8 Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:43', '2018-10-15 19:29:43'),
(22, 1, 1, 1, 25, 'TW-I1-21', '2015-00022', 'eroadan@gmail.com', 'Brgy. Preza, Lian, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:44', '2018-10-15 19:29:44'),
(23, 1, 1, 1, 26, 'TW-I1-22', '2015-00023', 'ianespiritu@gmail.com', 'Mulingbayan Sudb. Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:45', '2018-10-15 19:29:45'),
(24, 1, 1, 1, 27, 'TW-I1-23', '2015-00024', 'jhunjhunfeliciano29@gmail.com', 'Sitio Faye, Brgy. Lumaniag, Lian, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:45', '2018-10-15 19:29:45'),
(25, 1, 1, 1, 28, 'TW-I1-24', '2015-00025', 'Miku109117@gmail.com', 'Brgy. 7, Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:46', '2018-10-15 19:29:46'),
(26, 1, 1, 1, 29, 'TW-I1-25', '2015-00026', 'rr101716@gmail.com', 'Munting-Tubig Balayan, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:47', '2018-10-15 19:29:47'),
(27, 1, 1, 1, 30, 'TW-I1-26', '2015-00027', 'Jisuuun57@gmail.com', 'Malaruhatan Lian, batangas', 6, 'Completed', NULL, '2018-10-15 19:29:48', '2018-10-15 19:29:48'),
(28, 1, 1, 1, 31, 'TW-I1-27', '2015-00028', 'jemck1096@gmail.com', 'Brgy. 1 Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:49', '2018-10-15 19:29:49'),
(29, 1, 1, 1, 32, 'TW-I1-28', '2015-00029', 'limetaargie@gmail.com', 'Brgy. Calayo, Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:49', '2018-10-15 19:29:49'),
(30, 1, 1, 1, 33, 'TW-I1-29', '2015-00030', 'llnesmarkgil05@gmai.com', 'Brgy. Banilad, Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:50', '2018-10-15 19:29:50'),
(31, 1, 1, 1, 34, 'TW-I1-30', '2015-00031', 'harveyloreno7@gmail.com', 'Lumaniag, Lian, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:51', '2018-10-15 19:29:51'),
(32, 1, 1, 1, 35, 'TW-I1-31', '2015-00032', 'juliusmagaling@gmail.com', '112 F. Castro St., Brgy. 11, Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:52', '2018-10-15 19:29:52'),
(33, 1, 1, 1, 36, 'TW-I1-32', '2015-00033', 'byronjesterm@gmail.com', 'Brgy. Tala, Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:53', '2018-10-15 19:29:53'),
(34, 1, 1, 1, 37, 'TW-I1-33', '2015-00034', 'kimmrnn@gmail.com', 'Brgy. Tumalim, Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:53', '2018-10-15 19:29:53'),
(35, 1, 1, 1, 38, 'TW-I1-34', '2015-00035', 'joserizalmendoza@gmail.com', 'Lumaniag, Lian, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:54', '2018-10-15 19:29:54'),
(36, 1, 1, 1, 39, 'TW-I1-35', '2015-00036', 'jerremymorilla@gmail.com', 'Tala, Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:55', '2018-10-15 19:29:55'),
(37, 1, 1, 1, 40, 'TW-I1-36', '2015-00037', 'nimuel24@gmail.com', 'Brgy. Baldeo Bungahan, Lian, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:56', '2018-10-15 19:29:56'),
(38, 1, 1, 1, 41, 'TW-I1-37', '2015-00038', 'kuyanimzy@gmail.com', 'Tuy, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:56', '2018-10-15 19:29:56'),
(39, 1, 1, 1, 42, 'TW-I1-38', '2015-00039', 'jeffersonnucom@yahoo.com', 'Banilad, Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:57', '2018-10-15 19:29:57'),
(40, 1, 1, 1, 43, 'TW-I1-39', '2015-00040', 'philkevin24@gmail.com', 'Brgy. 3 Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:58', '2018-10-15 19:29:58'),
(41, 1, 1, 1, 44, 'TW-I1-40', '2015-00041', 'palencialawrence8@gmail.com', 'Brgy. Luyahan Lian, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:58', '2018-10-15 19:29:58'),
(42, 1, 1, 1, 45, 'TW-I1-41', '2015-00042', 'kaizerpanganibannds@gmail.com', 'Tali Beach Balayatigue, Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:29:59', '2018-10-15 19:29:59'),
(43, 1, 1, 1, 46, 'TW-I1-42', '2015-00043', 'panganibanpogi123@gmail.com', 'Brgy. 2 Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:30:00', '2018-10-15 19:30:00'),
(44, 1, 1, 1, 47, 'TW-I1-43', '2015-00044', 'pantoja.redelallen@gmail.com', 'Balibago, Lian, Batangas', 6, 'Completed', NULL, '2018-10-15 19:30:01', '2018-10-15 19:30:01'),
(45, 1, 1, 1, 48, 'TW-I1-44', '2015-00045', 'a.percano13@gmail.com', 'Roxaco Landing Subd. Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:30:01', '2018-10-15 19:30:01'),
(46, 1, 1, 1, 49, 'TW-I1-45', '2015-00046', 'louelplaton@gmail.com', 'Bilaran, Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:30:02', '2018-10-15 19:30:02'),
(47, 1, 1, 1, 50, 'TW-I1-46', '2015-00047', 'relacio.mj@gmail.com', 'Langgangan Balayan, Batangas', 6, 'Completed', NULL, '2018-10-15 19:30:03', '2018-10-15 19:30:03'),
(48, 1, 1, 1, 51, 'TW-I1-47', '2015-00048', 'respicioll.ar@gmail.com', 'Banilad, Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:30:03', '2018-10-15 19:30:03'),
(49, 1, 1, 1, 52, 'TW-I1-48', '2015-00049', 'jhnplrynld@gmail.com', 'Brgy. 3 Balayan, Batangas', 6, 'Completed', NULL, '2018-10-15 19:30:04', '2018-10-15 19:30:04'),
(50, 1, 1, 1, 53, 'TW-I1-49', '2015-00050', 'thanrol21@gmail.com', 'Brgy. 1 Brias St. Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:30:05', '2018-10-15 19:30:05'),
(51, 1, 1, 1, 54, 'TW-I1-50', '2015-00051', 'rol.jayjade@gmail.com', 'Brgy. Lumbangan, Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:30:05', '2018-10-15 19:30:05'),
(52, 1, 1, 1, 55, 'TW-I1-51', '2015-00052', 'neilneilranz@gmail.com', '288 Lumbangan Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:30:06', '2018-10-15 19:30:06'),
(53, 1, 1, 1, 56, 'TW-I1-52', '2015-00053', 'pappertruffy@gmail.com', 'Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:30:07', '2018-10-15 19:30:07'),
(54, 1, 1, 1, 57, 'TW-I1-53', '2015-00054', 'ruizjohnrick@gmail.com', 'Brgy. Bucana, Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:30:07', '2018-10-15 19:30:07'),
(55, 1, 1, 1, 58, 'TW-I1-54', '2015-00055', 'jonarruiz@gmail.com', 'Role Subd. Brgy. Lumbangan, Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:30:08', '2018-10-15 19:30:08'),
(56, 1, 1, 1, 59, 'TW-I1-55', '2015-00056', 'sarmientop1114@gmail.com', 'Brgy. 5 Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:30:09', '2018-10-15 19:30:09'),
(57, 1, 1, 1, 60, 'TW-I1-56', '2015-00057', '0411cedricksimuangco@gmail.com', '107 M. Apacible St., Brgy. 2, Calatagan, Batangas', 6, 'Completed', NULL, '2018-10-15 19:30:09', '2018-10-15 19:30:09'),
(58, 1, 1, 1, 61, 'TW-I1-57', '2015-00058', 'cristiansinag@yahoo.com', 'Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:30:10', '2018-10-15 19:30:10'),
(59, 1, 1, 1, 62, 'TW-I1-58', '2015-00059', 'soriorodelito@gmail.com', 'Brgy. Wawa Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:30:11', '2018-10-15 19:30:11'),
(60, 1, 1, 1, 63, 'TW-I1-59', '2015-00060', 'admiralpeps@gmail.com', 'Brgy. 1 Brias St. Nasugbu Batangas', 6, 'Completed', NULL, '2018-10-15 19:30:12', '2018-10-15 19:30:12'),
(61, 1, 1, 1, 64, 'TW-I1-60', '2015-00061', 'alxx.jai.gmail.com', 'Lumbangan, Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:30:12', '2018-10-15 19:30:12'),
(62, 1, 1, 1, 65, 'TW-I1-61', '2015-00062', 'rommel.villarojo18@gmail.com', 'Brgy. Bucana, Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:30:13', '2018-10-15 19:30:13'),
(63, 1, 1, 1, 66, 'TW-I1-62', '2015-00063', 'christianvillaviray@gmail.com', 'Brgy. Bucana, Nasugbu, Batangas', 6, 'Completed', NULL, '2018-10-15 19:30:14', '2018-10-15 19:30:14'),
(64, 1, 1, 1, 67, 'TW-I1-63', '2015-00064', 'dexter.villaviray00@gmail.com', 'Bungahan Lian, Batangas', 6, 'Completed', NULL, '2018-10-15 19:30:15', '2018-10-15 19:30:15'),
(65, 1, 1, 1, 68, 'TW-I1-64', '2015-00065', 'juliusvitales@gmai.com', 'Brgy. Malibu, Tuy, Batangas', 6, 'Completed', NULL, '2018-10-15 19:30:16', '2018-10-15 19:30:16');

-- --------------------------------------------------------

--
-- Table structure for table `r_su_users`
--

CREATE TABLE `r_su_users` (
  `uu_id` int(10) UNSIGNED NOT NULL,
  `branch_id` int(10) UNSIGNED NOT NULL,
  `ur_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `r_su_users`
--

INSERT INTO `r_su_users` (`uu_id`, `branch_id`, `ur_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `r_su_usr_educbg`
--

CREATE TABLE `r_su_usr_educbg` (
  `uu_id` int(10) UNSIGNED NOT NULL,
  `degree` enum('Bachelor''s Degree','Master''s Degree','Doctorate Degree') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_graduated` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year_graduated` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `received_awards` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `r_su_usr_educbg`
--

INSERT INTO `r_su_usr_educbg` (`uu_id`, `degree`, `school_graduated`, `year_graduated`, `received_awards`) VALUES
(1, 'Master\'s Degree', 'null', 'null', 'null'),
(2, 'Master\'s Degree', 'null', 'null', 'null'),
(3, 'Master\'s Degree', 'null', 'null', 'null');

-- --------------------------------------------------------

--
-- Table structure for table `t_hte_accepted_intern`
--

CREATE TABLE `t_hte_accepted_intern` (
  `ai_id` int(10) UNSIGNED NOT NULL,
  `hte_id` int(10) UNSIGNED NOT NULL,
  `intern_id` int(10) UNSIGNED NOT NULL,
  `job_training_id` int(10) UNSIGNED DEFAULT NULL,
  `su_grade` enum('Complete') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hte_grade` enum('Complete') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ai_stat` enum('On Going','Finished') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'On Going',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `t_hte_accepted_intern`
--

INSERT INTO `t_hte_accepted_intern` (`ai_id`, `hte_id`, `intern_id`, `job_training_id`, `su_grade`, `hte_grade`, `ai_stat`, `created_at`, `updated_at`) VALUES
(1, 29, 1, NULL, NULL, 'Complete', 'On Going', '2018-10-15 19:30:17', '2018-10-15 19:30:17'),
(2, 13, 2, NULL, NULL, 'Complete', 'On Going', '2018-10-15 19:30:17', '2018-10-15 19:30:17'),
(3, 29, 3, NULL, NULL, 'Complete', 'On Going', '2018-10-15 19:30:18', '2018-10-15 19:30:18'),
(4, 29, 4, NULL, NULL, 'Complete', 'On Going', '2018-10-15 19:30:18', '2018-10-15 19:30:18'),
(5, 29, 5, NULL, NULL, 'Complete', 'On Going', '2018-10-15 19:30:18', '2018-10-15 19:30:18'),
(6, 29, 6, NULL, NULL, 'Complete', 'On Going', '2018-10-15 19:30:18', '2018-10-15 19:30:18'),
(7, 29, 7, NULL, NULL, 'Complete', 'On Going', '2018-10-15 19:30:18', '2018-10-15 19:30:18'),
(8, 29, 8, NULL, NULL, 'Complete', 'On Going', '2018-10-15 19:30:19', '2018-10-15 19:30:19'),
(9, 29, 9, NULL, NULL, 'Complete', 'On Going', '2018-10-15 19:30:19', '2018-10-15 19:30:19'),
(10, 29, 10, NULL, NULL, 'Complete', 'On Going', '2018-10-15 19:30:19', '2018-10-15 19:30:19'),
(11, 29, 11, NULL, NULL, 'Complete', 'On Going', '2018-10-15 19:30:19', '2018-10-15 19:30:19'),
(12, 29, 12, NULL, NULL, 'Complete', 'On Going', '2018-10-15 19:30:19', '2018-10-15 19:30:19');

-- --------------------------------------------------------

--
-- Table structure for table `t_hte_grade`
--

CREATE TABLE `t_hte_grade` (
  `grading_id` int(10) UNSIGNED NOT NULL,
  `hte_id` int(10) UNSIGNED NOT NULL,
  `hu_id` int(10) UNSIGNED DEFAULT NULL,
  `intern_grade` double(100,2) NOT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `t_hte_grade`
--

INSERT INTO `t_hte_grade` (`grading_id`, `hte_id`, `hu_id`, `intern_grade`, `remarks`, `created_at`, `updated_at`) VALUES
(1, 29, NULL, 85.00, NULL, '2018-10-15 19:30:17', '2018-10-15 19:30:17'),
(2, 13, NULL, 85.00, NULL, '2018-10-15 19:30:18', '2018-10-15 19:30:18'),
(3, 29, NULL, 85.00, NULL, '2018-10-15 19:30:18', '2018-10-15 19:30:18'),
(4, 29, NULL, 85.00, NULL, '2018-10-15 19:30:18', '2018-10-15 19:30:18'),
(5, 29, NULL, 85.00, NULL, '2018-10-15 19:30:18', '2018-10-15 19:30:18'),
(6, 29, NULL, 85.00, NULL, '2018-10-15 19:30:18', '2018-10-15 19:30:18'),
(7, 29, NULL, 85.00, NULL, '2018-10-15 19:30:19', '2018-10-15 19:30:19'),
(8, 29, NULL, 85.00, NULL, '2018-10-15 19:30:19', '2018-10-15 19:30:19'),
(9, 29, NULL, 85.00, NULL, '2018-10-15 19:30:19', '2018-10-15 19:30:19'),
(10, 29, NULL, 85.00, NULL, '2018-10-15 19:30:19', '2018-10-15 19:30:19'),
(11, 29, NULL, 85.00, NULL, '2018-10-15 19:30:19', '2018-10-15 19:30:19'),
(12, 29, NULL, 85.00, NULL, '2018-10-15 19:30:19', '2018-10-15 19:30:19');

-- --------------------------------------------------------

--
-- Table structure for table `t_intern_application`
--

CREATE TABLE `t_intern_application` (
  `iapp_id` int(10) UNSIGNED NOT NULL,
  `intern_id` int(10) UNSIGNED NOT NULL,
  `job_training_id` int(10) UNSIGNED NOT NULL,
  `iappstat_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `t_intern_grading`
--

CREATE TABLE `t_intern_grading` (
  `grading_id` int(10) UNSIGNED NOT NULL,
  `grading_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `intern_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `t_intern_grading`
--

INSERT INTO `t_intern_grading` (`grading_id`, `grading_code`, `intern_id`) VALUES
(1, NULL, 1),
(2, NULL, 2),
(3, NULL, 3),
(4, NULL, 4),
(5, NULL, 5),
(6, NULL, 6),
(7, NULL, 7),
(8, NULL, 8),
(9, NULL, 9),
(10, NULL, 10),
(11, NULL, 11),
(12, NULL, 12);

-- --------------------------------------------------------

--
-- Table structure for table `t_su_hte`
--

CREATE TABLE `t_su_hte` (
  `sh_id` int(10) UNSIGNED NOT NULL,
  `branch_id` int(10) UNSIGNED NOT NULL,
  `college_id` int(10) UNSIGNED NOT NULL,
  `hte_id` int(10) UNSIGNED NOT NULL,
  `affiliated_status` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `moa_path` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `t_su_hte`
--

INSERT INTO `t_su_hte` (`sh_id`, `branch_id`, `college_id`, `hte_id`, `affiliated_status`, `moa_path`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 'Active', NULL, '2018-10-15 19:29:25', '2018-10-15 19:29:25'),
(2, 1, 1, 2, 'Active', NULL, '2018-10-15 19:29:25', '2018-10-15 19:29:25'),
(3, 1, 1, 3, 'Active', NULL, '2018-10-15 19:29:26', '2018-10-15 19:29:26'),
(4, 1, 1, 4, 'Active', NULL, '2018-10-15 19:29:26', '2018-10-15 19:29:26'),
(5, 1, 1, 5, 'Active', NULL, '2018-10-15 19:29:26', '2018-10-15 19:29:26'),
(6, 1, 1, 6, 'Active', NULL, '2018-10-15 19:29:26', '2018-10-15 19:29:26'),
(7, 1, 1, 7, 'Active', NULL, '2018-10-15 19:29:26', '2018-10-15 19:29:26'),
(8, 1, 1, 8, 'Active', NULL, '2018-10-15 19:29:26', '2018-10-15 19:29:26'),
(9, 1, 1, 9, 'Active', NULL, '2018-10-15 19:29:26', '2018-10-15 19:29:26'),
(10, 1, 1, 10, 'Active', NULL, '2018-10-15 19:29:26', '2018-10-15 19:29:26'),
(11, 1, 1, 11, 'Active', NULL, '2018-10-15 19:29:26', '2018-10-15 19:29:26'),
(12, 1, 1, 12, 'Active', NULL, '2018-10-15 19:29:26', '2018-10-15 19:29:26'),
(13, 1, 1, 13, 'Active', NULL, '2018-10-15 19:29:26', '2018-10-15 19:29:26'),
(14, 1, 1, 14, 'Active', NULL, '2018-10-15 19:29:26', '2018-10-15 19:29:26'),
(15, 1, 1, 15, 'Active', NULL, '2018-10-15 19:29:26', '2018-10-15 19:29:26'),
(16, 1, 1, 16, 'Active', NULL, '2018-10-15 19:29:26', '2018-10-15 19:29:26'),
(17, 1, 1, 17, 'Active', NULL, '2018-10-15 19:29:26', '2018-10-15 19:29:26'),
(18, 1, 1, 18, 'Active', NULL, '2018-10-15 19:29:26', '2018-10-15 19:29:26'),
(19, 1, 1, 19, 'Active', NULL, '2018-10-15 19:29:26', '2018-10-15 19:29:26'),
(20, 1, 1, 20, 'Active', NULL, '2018-10-15 19:29:27', '2018-10-15 19:29:27'),
(21, 1, 1, 21, 'Active', NULL, '2018-10-15 19:29:27', '2018-10-15 19:29:27'),
(22, 1, 1, 22, 'Active', NULL, '2018-10-15 19:29:27', '2018-10-15 19:29:27'),
(23, 1, 1, 23, 'Active', NULL, '2018-10-15 19:29:27', '2018-10-15 19:29:27'),
(24, 1, 1, 24, 'Active', NULL, '2018-10-15 19:29:27', '2018-10-15 19:29:27'),
(25, 1, 1, 25, 'Active', NULL, '2018-10-15 19:29:27', '2018-10-15 19:29:27'),
(26, 1, 1, 26, 'Active', NULL, '2018-10-15 19:29:27', '2018-10-15 19:29:27'),
(27, 1, 1, 27, 'Active', NULL, '2018-10-15 19:29:27', '2018-10-15 19:29:27'),
(28, 1, 1, 28, 'Active', NULL, '2018-10-15 19:29:27', '2018-10-15 19:29:27'),
(29, 1, 1, 29, 'Active', NULL, '2018-10-15 19:29:27', '2018-10-15 19:29:27'),
(30, 1, 1, 30, 'Active', NULL, '2018-10-15 19:29:27', '2018-10-15 19:29:27'),
(31, 1, 1, 31, 'Active', NULL, '2018-10-15 19:29:27', '2018-10-15 19:29:27'),
(32, 1, 1, 32, 'Active', NULL, '2018-10-15 19:29:27', '2018-10-15 19:29:27'),
(33, 1, 1, 33, 'Active', NULL, '2018-10-15 19:29:27', '2018-10-15 19:29:27'),
(34, 1, 1, 34, 'Active', NULL, '2018-10-15 19:29:27', '2018-10-15 19:29:27'),
(35, 1, 1, 35, 'Active', NULL, '2018-10-15 19:29:27', '2018-10-15 19:29:27'),
(36, 1, 1, 36, 'Active', NULL, '2018-10-15 19:29:27', '2018-10-15 19:29:27'),
(37, 1, 1, 37, 'Active', NULL, '2018-10-15 19:29:27', '2018-10-15 19:29:27'),
(38, 1, 1, 38, 'Active', NULL, '2018-10-15 19:29:27', '2018-10-15 19:29:27'),
(39, 1, 1, 39, 'Active', NULL, '2018-10-15 19:29:27', '2018-10-15 19:29:27'),
(40, 1, 1, 40, 'Active', NULL, '2018-10-15 19:29:28', '2018-10-15 19:29:28'),
(41, 1, 1, 41, 'Active', NULL, '2018-10-15 19:29:28', '2018-10-15 19:29:28'),
(42, 1, 1, 42, 'Active', NULL, '2018-10-15 19:29:28', '2018-10-15 19:29:28'),
(43, 1, 1, 43, 'Active', NULL, '2018-10-15 19:29:28', '2018-10-15 19:29:28'),
(44, 1, 1, 44, 'Active', NULL, '2018-10-15 19:29:28', '2018-10-15 19:29:28');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `usr_code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `usr_stat` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `usr_code`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `usr_stat`) VALUES
(1, 'TWU18-1015-1', 'alfredo_atienza_jr@gmail.com', '$2y$10$GYEoR9vRc9vhov.VDfvtLekiDmRewUwoCJ26TGAXIoBm4UhsXZ6eW', NULL, '2018-10-15 19:29:11', '2018-10-15 19:29:11', 'Active'),
(2, 'TWU18-1015-2', 'mayette_canenea@gmail.com', '$2y$10$KP82NYUVKfCsgkKV0DBOIO2qSJE66Oa09miArfzVeqX0QCbzXbAa2', NULL, '2018-10-15 19:29:12', '2018-10-15 19:29:12', 'Active'),
(3, 'TWU18-1015-3', 'adrian_melo@gmail.com', '$2y$10$5aio35DmU2FgprcAa4/zUemK/RIoWC9osH4AWhqiE6dV4R7EJxNGq', NULL, '2018-10-15 19:29:12', '2018-10-15 19:29:12', 'Active'),
(4, 'TWU18-1015-4', 'charles.angsioco06@gmail.com', '$2y$10$HqKRDSXKvpInoNZ9xMwxZu75D2gH51y7liLV6VBfr7UDFfmPbBEca', '21cTjHG7EDGNSGSzmcYYgN0wmMjKuXSgpyGZFVxn47KjxQT5UAdohspzDVkW', '2018-10-15 19:29:29', '2018-10-15 19:29:29', 'Active'),
(5, 'TWU18-1015-5', 'johnhenry_angsioco@yahoo.com', '$2y$10$5hVF8hmk0CGcq1SrAUMzsenHm6Hw9HCFyACL2n8Y2WmaiSBrl.zHO', NULL, '2018-10-15 19:29:30', '2018-10-15 19:29:30', 'Active'),
(6, 'TWU18-1015-6', 'reyann.angioco@gmail.com', '$2y$10$Ns/ybLz7HwgxxtoREjwK1OJHh1XBI.lYS5VLWoZDRqceWrbl94w7m', NULL, '2018-10-15 19:29:30', '2018-10-15 19:29:30', 'Active'),
(7, 'TWU18-1015-7', 'rome.anthony00@gmail.com', '$2y$10$It.RKbYHnGBB/yC1f7FccOG1GORs5c41LG.r0uWZ.x0qNfyaG5hli', NULL, '2018-10-15 19:29:31', '2018-10-15 19:29:31', 'Active'),
(8, 'TWU18-1015-8', 'dennerson1993@gmail.com', '$2y$10$LABlz0plhknAdBQtS5JZmeq0p6sMe.ehxLc5MyRxpg9TlihJ76AXq', NULL, '2018-10-15 19:29:32', '2018-10-15 19:29:32', 'Active'),
(9, 'TWU18-1015-9', 'bacquianjezer@gmail.com', '$2y$10$q1xV3hTyIuxm9JyZDAbNu.8K4FyPTaiYqTrOxhB8SK3dgUZJgfgcu', NULL, '2018-10-15 19:29:33', '2018-10-15 19:29:33', 'Active'),
(10, 'TWU18-1015-10', 'barrionjohnlawrrence@gmail.com', '$2y$10$sNuwwRQK6pl1Ph0kMAcAV.CG06NvO14wg4wnk8feoy5ULBi2Tt1kq', NULL, '2018-10-15 19:29:33', '2018-10-15 19:29:33', 'Active'),
(11, 'TWU18-1015-11', 'jmbayubay8888@gmail.com', '$2y$10$RbadYl9c4pUbQGUjzSnYHedr53o5sLWqeaT4YpxT403TVWnyuuk.W', NULL, '2018-10-15 19:29:34', '2018-10-15 19:29:34', 'Active'),
(12, 'TWU18-1015-12', 'zongdex2993@gmail.com', '$2y$10$ztCOizCF977f8dzv1/8Sq.AFmk6gAVlo1TkB7YFGhjpBTKsYLy3Fu', NULL, '2018-10-15 19:29:35', '2018-10-15 19:29:35', 'Active'),
(13, 'TWU18-1015-13', 'caisip.alexis@gmail.com', '$2y$10$DJzGIL9iezWNhHvGoT.zKOPVSyh3qCh7llSy.mdYejZytP50of6aq', NULL, '2018-10-15 19:29:35', '2018-10-15 19:29:35', 'Active'),
(14, 'TWU18-1015-14', '9murcielago.kj@gmail.com', '$2y$10$71n7KpgOFFYbeoL7cpo6guSa8eQtbMzo4k0j3mGqwo/zpy/MNe0Zi', NULL, '2018-10-15 19:29:36', '2018-10-15 19:29:36', 'Active'),
(15, 'TWU18-1015-15', 'joshuacaylecantojos@gmail.com', '$2y$10$Ek2trKo./eCRuOU3NfD4lOydoE3jmMJTDVMzthUEiCBSZhhIOh6gW', NULL, '2018-10-15 19:29:37', '2018-10-15 19:29:37', 'Active'),
(16, 'TWU18-1015-16', 'manucerrado@gmail.com', '$2y$10$wgyQqG3g5KLxNEOqEYVToes5d/uFb8loavS84tqvvpUQHxn9Ql2Dy', NULL, '2018-10-15 19:29:37', '2018-10-15 19:29:37', 'Active'),
(17, 'TWU18-1015-17', 'nilbertchavez0716@gmail.com', '$2y$10$5CW73l9mtlOP/6UNhL.w7.d9XbZYyXvyJxOS2dFfT6GbHqpZbbxYu', NULL, '2018-10-15 19:29:38', '2018-10-15 19:29:38', 'Active'),
(18, 'TWU18-1015-18', 'christiancruzado49@gmail.com', '$2y$10$H5q4Dp1okN.pA39.irMwFuq8ZUe3nbUorZ6mTVuHBgl2xyDg7pbFC', NULL, '2018-10-15 19:29:39', '2018-10-15 19:29:39', 'Active'),
(19, 'TWU18-1015-19', 'orieldecastro16@gmail.com', '$2y$10$KSKJ4xlDgoWB991Swso5/eu2T3kHBFoXq/E5ARfejBIhLLoRM4Vya', NULL, '2018-10-15 19:29:40', '2018-10-15 19:29:40', 'Active'),
(20, 'TWU18-1015-20', 'roicedelasalas@yahoo.com', '$2y$10$fWGpzHSNhY9LdKqLS0VAx.0Qj0/4a6W9l56itVUys09Jw6HVqVdJK', NULL, '2018-10-15 19:29:41', '2018-10-15 19:29:41', 'Active'),
(21, 'TWU18-1015-21', 'Kel03deleola@yahoo.com', '$2y$10$FzF2lEYJJN6ntdeXm3AwN.ayvmNksKHyVgZTtHO0MOlJrDMnwRh.K', NULL, '2018-10-15 19:29:42', '2018-10-15 19:29:42', 'Active'),
(22, 'TWU18-1015-22', 'earlrandel@gmail.com', '$2y$10$Zn1YiPJ4iEr58fZtx1lc/.xfxi0LR83q5U9nN01eZ7vbDE.4X3ZvW', NULL, '2018-10-15 19:29:42', '2018-10-15 19:29:42', 'Active'),
(23, 'TWU18-1015-23', 'markjosephdolormente@gmail.com', '$2y$10$CcPQgNt4IBCicnuPksizAezsXR0yeCl0z.aVJEIpYz9OahUHQhNjW', NULL, '2018-10-15 19:29:43', '2018-10-15 19:29:43', 'Active'),
(24, 'TWU18-1015-24', 'wadglenn6@gmail.com', '$2y$10$WShMQEy9h1ZWp2rbFtMHCOCDj3EZ/SRik8R9FtHyTj776zQinlGDa', NULL, '2018-10-15 19:29:44', '2018-10-15 19:29:44', 'Active'),
(25, 'TWU18-1015-25', 'eroadan@gmail.com', '$2y$10$kNWsonmAeB4b0rhOYqDpwubSl8yAajw1F9XZ7EAOkt6p9hHgX95B.', NULL, '2018-10-15 19:29:44', '2018-10-15 19:29:44', 'Active'),
(26, 'TWU18-1015-26', 'ianespiritu@gmail.com', '$2y$10$ugco5nGfJYgcVo9XSSWvUewrbAq8uHPBkBREt1pux5hCGU6c5yam2', NULL, '2018-10-15 19:29:45', '2018-10-15 19:29:45', 'Active'),
(27, 'TWU18-1015-27', 'jhunjhunfeliciano29@gmail.com', '$2y$10$cxLgKE4cMnqisGtNjcJTA.mqNl1tgXHTPysCuB1PcPbcN182rHO7e', NULL, '2018-10-15 19:29:46', '2018-10-15 19:29:46', 'Active'),
(28, 'TWU18-1015-28', 'Miku109117@gmail.com', '$2y$10$BRFRaWD8pzJMDEZ7crLOUeyLgFdrSz/O3kOVi3I779mzASYDkgu/y', NULL, '2018-10-15 19:29:47', '2018-10-15 19:29:47', 'Active'),
(29, 'TWU18-1015-29', 'rr101716@gmail.com', '$2y$10$JWbO8gII/BJz/seEwYVIp.oyqxWm1o31yTuM8G9aWPvQN0bo59itW', NULL, '2018-10-15 19:29:48', '2018-10-15 19:29:48', 'Active'),
(30, 'TWU18-1015-30', 'Jisuuun57@gmail.com', '$2y$10$uXSNusTSPybyS3sOGQlbfeJiVWmImuvl1W9HyF.qDjeRDJIJdwUKm', NULL, '2018-10-15 19:29:48', '2018-10-15 19:29:48', 'Active'),
(31, 'TWU18-1015-31', 'jemck1096@gmail.com', '$2y$10$vB9nsXiuwYAUND3a4IIcl.OMvZKtKpyn5BwZqt7ExRsPj9cCaM2nC', NULL, '2018-10-15 19:29:49', '2018-10-15 19:29:49', 'Active'),
(32, 'TWU18-1015-32', 'limetaargie@gmail.com', '$2y$10$lbgs1ked3HTpkdo3XWT.LOWh.F8OPcHhdyOlRfSMGSo/KFlcdTEgy', NULL, '2018-10-15 19:29:50', '2018-10-15 19:29:50', 'Active'),
(33, 'TWU18-1015-33', 'llnesmarkgil05@gmai.com', '$2y$10$Gef3nX1VsM5tKaOl0WXZduXTedv7iJYthYdD2rcPmPqpXlMSlCQPO', NULL, '2018-10-15 19:29:51', '2018-10-15 19:29:51', 'Active'),
(34, 'TWU18-1015-34', 'harveyloreno7@gmail.com', '$2y$10$JEL.cRw8jOfKpFfUH47wXuLZW/XNcv.DsvIJhXpGNthChMjCNblrq', NULL, '2018-10-15 19:29:52', '2018-10-15 19:29:52', 'Active'),
(35, 'TWU18-1015-35', 'juliusmagaling@gmail.com', '$2y$10$Uin1xk6NT1MkxFq/QjmGguSM/ry8Ozuw7jR.zEEE.YWPpnROeuVSm', NULL, '2018-10-15 19:29:52', '2018-10-15 19:29:52', 'Active'),
(36, 'TWU18-1015-36', 'byronjesterm@gmail.com', '$2y$10$J1OXLDcVb6LhEJSar6hg9.DF79fknI/5zIm2MgZSmDxuITL6lZ8Ri', NULL, '2018-10-15 19:29:53', '2018-10-15 19:29:53', 'Active'),
(37, 'TWU18-1015-37', 'kimmrnn@gmail.com', '$2y$10$WI1pzqWKAju0tZ89K4.NGev4/PW0GWF3oxObYq4LJX2CHhaK1GQ0S', NULL, '2018-10-15 19:29:54', '2018-10-15 19:29:54', 'Active'),
(38, 'TWU18-1015-38', 'joserizalmendoza@gmail.com', '$2y$10$giMifDVF5CAM95sRUnWSfedc/k.KXgCVdjU/z7xG4ZHqKbBCeLZte', NULL, '2018-10-15 19:29:54', '2018-10-15 19:29:54', 'Active'),
(39, 'TWU18-1015-39', 'jerremymorilla@gmail.com', '$2y$10$h8QR3kcObZKI0cjU1Sc4SuFQLpPrutHxPC7LABBL8iBTvathJMOvu', NULL, '2018-10-15 19:29:55', '2018-10-15 19:29:55', 'Active'),
(40, 'TWU18-1015-40', 'nimuel24@gmail.com', '$2y$10$PAP7tytmKUglshRhmapfru9XPZaI1gTUdfTbWMPDE4kDyjAZZXjIK', NULL, '2018-10-15 19:29:56', '2018-10-15 19:29:56', 'Active'),
(41, 'TWU18-1015-41', 'kuyanimzy@gmail.com', '$2y$10$O55YFkAX8sEtgzCMqeloku3KLSqN8qfw72p70lRpRDpk7mfqV4Eca', NULL, '2018-10-15 19:29:57', '2018-10-15 19:29:57', 'Active'),
(42, 'TWU18-1015-42', 'jeffersonnucom@yahoo.com', '$2y$10$HjgrPjg1Wp9q1EtcfmE03.jgv1K6x3eYlpyAkapUG79D1oTgMtl9W', NULL, '2018-10-15 19:29:57', '2018-10-15 19:29:57', 'Active'),
(43, 'TWU18-1015-43', 'philkevin24@gmail.com', '$2y$10$acEhJGSKZYlbU37NePu/qu.y43K8MKD.MzZhUw3LLryDvU6uKpfrC', NULL, '2018-10-15 19:29:58', '2018-10-15 19:29:58', 'Active'),
(44, 'TWU18-1015-44', 'palencialawrence8@gmail.com', '$2y$10$UO4DFCQR5QMhVAd0z2Cgy.AB4CQR3DOFbJejYg0beTyoeWJ94J7BG', NULL, '2018-10-15 19:29:59', '2018-10-15 19:29:59', 'Active'),
(45, 'TWU18-1015-45', 'kaizerpanganibannds@gmail.com', '$2y$10$fp8IAjWylA/WdmVz.T8Q.umlKKUNVmAMA5k5s3b4aTxEaKBOCq5Na', NULL, '2018-10-15 19:30:00', '2018-10-15 19:30:00', 'Active'),
(46, 'TWU18-1015-46', 'panganibanpogi123@gmail.com', '$2y$10$ybkjLXKyWwtn6dN82TS8IecivlK80OR4Gj9TZlTCtq4hFalSqXt7q', NULL, '2018-10-15 19:30:00', '2018-10-15 19:30:00', 'Active'),
(47, 'TWU18-1015-47', 'pantoja.redelallen@gmail.com', '$2y$10$.si4vepNBVr2Z72GnURU0.bKS3fdZKhIV1yIkp2kewl3wscWpm6zO', NULL, '2018-10-15 19:30:01', '2018-10-15 19:30:01', 'Active'),
(48, 'TWU18-1015-48', 'a.percano13@gmail.com', '$2y$10$e.Rj6lncheSOUMX.g7cCIeI0JbAsOJBSyDpYtq9ehB3XKRCrauj2q', NULL, '2018-10-15 19:30:02', '2018-10-15 19:30:02', 'Active'),
(49, 'TWU18-1015-49', 'louelplaton@gmail.com', '$2y$10$b88U00vDH0O.U24vDhFL9eYBdXi8IAxFrarYzbRb.Ja1aLVpi/Jju', NULL, '2018-10-15 19:30:02', '2018-10-15 19:30:02', 'Active'),
(50, 'TWU18-1015-50', 'relacio.mj@gmail.com', '$2y$10$Ck2p6UrDNrTmsfihZyI.mO4UYWize./AGLjI9tEr2d6/XQvOc4QB2', NULL, '2018-10-15 19:30:03', '2018-10-15 19:30:03', 'Active'),
(51, 'TWU18-1015-51', 'respicioll.ar@gmail.com', '$2y$10$g3K2tpSqqkd2pC9IE2pnMuRuPAGycJk77fpg2LWSCtwHdV4wMilZq', NULL, '2018-10-15 19:30:04', '2018-10-15 19:30:04', 'Active'),
(52, 'TWU18-1015-52', 'jhnplrynld@gmail.com', '$2y$10$3LpgCINKq6uI48tnkRFYcuCcK9IYSpd7D0nwD8MSpwKBhvVQKgOV.', NULL, '2018-10-15 19:30:04', '2018-10-15 19:30:04', 'Active'),
(53, 'TWU18-1015-53', 'thanrol21@gmail.com', '$2y$10$RATvyQiJs3WgdiFBkXOt7ORnRu5siD8r5dVU2dJ0gXuzaHcbPlz0W', NULL, '2018-10-15 19:30:05', '2018-10-15 19:30:05', 'Active'),
(54, 'TWU18-1015-54', 'rol.jayjade@gmail.com', '$2y$10$1GkIOlU0IP0aifzsuZxux.h7uwhpAxr1P/5r8dQ8WNhiiz7aI7d8S', NULL, '2018-10-15 19:30:06', '2018-10-15 19:30:06', 'Active'),
(55, 'TWU18-1015-55', 'neilneilranz@gmail.com', '$2y$10$1eSCKjAv8DF1/9xMSCAiresVrAeGsuN37mzanO2eU6mCsyDEy1hNK', NULL, '2018-10-15 19:30:06', '2018-10-15 19:30:06', 'Active'),
(56, 'TWU18-1015-56', 'pappertruffy@gmail.com', '$2y$10$rge6R7zDTG3Iacux4NLCpu8kdkMqGppmg/IJrlVF9f/WfQEcJRHye', NULL, '2018-10-15 19:30:07', '2018-10-15 19:30:07', 'Active'),
(57, 'TWU18-1015-57', 'ruizjohnrick@gmail.com', '$2y$10$OqgbCsDIYBaY3rUMY3CsnuYycYQSs1IlClZZ3Nk3aLyduBo/Chlzm', NULL, '2018-10-15 19:30:08', '2018-10-15 19:30:08', 'Active'),
(58, 'TWU18-1015-58', 'jonarruiz@gmail.com', '$2y$10$o1tn8qNHI1B6znIgSiqm7.KSYqd2yZs6xOiSNTpopJTnSOKfY4y0.', NULL, '2018-10-15 19:30:08', '2018-10-15 19:30:08', 'Active'),
(59, 'TWU18-1015-59', 'sarmientop1114@gmail.com', '$2y$10$heYY.X7NHfVwDClr3Z84yeGfx6ihUkFhqubkVI5wTwvfFgMRtqfKa', NULL, '2018-10-15 19:30:09', '2018-10-15 19:30:09', 'Active'),
(60, 'TWU18-1015-60', '0411cedricksimuangco@gmail.com', '$2y$10$SXTl355IBbsYIZJbEFcU5ujxukpZiOfduprcS0t9rrKeSZWD1Ob3m', NULL, '2018-10-15 19:30:10', '2018-10-15 19:30:10', 'Active'),
(61, 'TWU18-1015-61', 'cristiansinag@yahoo.com', '$2y$10$6MS4jKI5QCazZRkaHiYniusSJ.F0joSpuf5RJ5pSeaYRNFL45D4eu', NULL, '2018-10-15 19:30:11', '2018-10-15 19:30:11', 'Active'),
(62, 'TWU18-1015-62', 'soriorodelito@gmail.com', '$2y$10$HeBeoiFpAvUPoFe5zjPqgOuEZbkSX2dURrN.rsWCwvswkJX2xq.sm', NULL, '2018-10-15 19:30:12', '2018-10-15 19:30:12', 'Active'),
(63, 'TWU18-1015-63', 'admiralpeps@gmail.com', '$2y$10$4OQpaeX5Sp.LHL/2qStVue19xicI2EIR2EFhKpss36FWBPuNd12L6', NULL, '2018-10-15 19:30:12', '2018-10-15 19:30:12', 'Active'),
(64, 'TWU18-1015-64', 'alxx.jai.gmail.com', '$2y$10$CNWPOzxiDt3bJR99aqEeQu7Ki17WbiRF/xfTaq2XDCHJktzJVxo7u', NULL, '2018-10-15 19:30:13', '2018-10-15 19:30:13', 'Active'),
(65, 'TWU18-1015-65', 'rommel.villarojo18@gmail.com', '$2y$10$Sg5HpzHVoGilYIboFBFTnOAFwydUh.mIv8PhmLAnpERrw/XmPz.Fi', NULL, '2018-10-15 19:30:14', '2018-10-15 19:30:14', 'Active'),
(66, 'TWU18-1015-66', 'christianvillaviray@gmail.com', '$2y$10$.rZMvQuTEcnJS3vPA6aGYOrqDSiNdEww8d/TdJa4mlOIEJX/FAhGi', NULL, '2018-10-15 19:30:15', '2018-10-15 19:30:15', 'Active'),
(67, 'TWU18-1015-67', 'dexter.villaviray00@gmail.com', '$2y$10$WUlxmAYh.fcp3mcvXJnweeT0I0huUXqcAWcByJYAPJxG86G3yVUwe', NULL, '2018-10-15 19:30:15', '2018-10-15 19:30:15', 'Active'),
(68, 'TWU18-1015-68', 'juliusvitales@gmai.com', '$2y$10$iFximayOHwoH9.MImYoWy.8JMXdDwR/l3PTbbYnNOzPeNO.Z0uO2u', NULL, '2018-10-15 19:30:16', '2018-10-15 19:30:16', 'Active'),
(69, 'TWR-214113', 'hte_absolut@gmail.com', '$2y$10$KP82NYUVKfCsgkKV0DBOIO2qSJE66Oa09miArfzVeqX...', NULL, NULL, NULL, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `user_infos`
--

CREATE TABLE `user_infos` (
  `info_id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `middle_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` enum('M','F') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bdate` date DEFAULT NULL,
  `tel_no` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cel_no` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `user_infos`
--

INSERT INTO `user_infos` (`info_id`, `title`, `first_name`, `middle_name`, `last_name`, `gender`, `bdate`, `tel_no`, `cel_no`) VALUES
(1, NULL, 'Alfredo', 'V.', 'Atienza, Jr.', 'M', '1995-01-01', NULL, NULL),
(2, 'Asst. Prof.', 'Mayette', 'A.', 'Cananea', 'F', '1995-02-02', NULL, NULL),
(3, 'Engr.', 'Adrian Ferdinand', 'M.', 'Melo', 'M', '1995-03-03', NULL, NULL),
(4, NULL, 'CHARLES FRANCIS', 'B', 'ANGSIOCO', 'M', '1997-01-25', 'null', '09353107528'),
(5, NULL, 'JOHN HENRY', 'F', 'ANGSIOCO', 'M', '1997-02-26', 'null', '09266603222'),
(6, NULL, 'REYANN', 'B', 'ANGSIOCO', 'M', '1997-03-27', 'null', '09358686704'),
(7, NULL, 'ROME ANTHONY', 'V', 'ANSUS', 'M', '1997-04-28', 'null', '09357363292'),
(8, NULL, 'DENNERSON', 'C', 'AUSTRIA', 'M', '1997-05-29', 'null', '09351731827'),
(9, NULL, 'JEZER DAVE', 'V', 'BACQUIAN', 'M', '1997-06-30', 'null', '09359775420'),
(10, NULL, 'JOHN LAWRENCE', 'C', 'BARRION', 'M', '1997-07-01', 'null', '09097870732'),
(11, NULL, 'JUAN MIGUEL', 'C', 'BAYUBAY', 'M', '1997-08-02', 'null', '09357155504'),
(12, NULL, 'CARLO', 'M', 'BOTONES', 'M', '1997-09-03', 'null', '09269738895'),
(13, NULL, 'ALEXIS', 'Null', 'CAISIP', 'M', '1997-10-04', 'null', '09754100431'),
(14, NULL, 'KIM JOSHUA', 'M', 'CAISIP', 'M', '1997-11-05', 'null', '09365142009'),
(15, NULL, 'JOSHUA CAYLE', 'P', 'CANTONJOS', 'M', '1997-12-06', 'null', '09269471194'),
(16, NULL, 'MANUEL JR.', 'V', 'CERRADO', 'M', '1997-01-07', 'null', '09365345606'),
(17, NULL, 'NILBERT', 'V', 'CHAVEZ', 'M', '1997-02-08', 'null', '09972819936'),
(18, NULL, 'CHRISTIAN', 'P', 'CRUZADO', 'M', '1997-03-09', 'null', '09975080903'),
(19, NULL, 'ORIEL HARRY', 'O', 'DE CASTRO', 'M', '1998-04-08', 'null', '09153851757'),
(20, NULL, 'ROICE JOHN', 'A', 'DE LAS ALAS', 'M', '1997-05-17', 'null', '09351546266'),
(21, NULL, 'MICHAEL', 'M', 'DE LEOLA', 'M', '1997-10-27', 'null', '09359422183'),
(22, NULL, 'EARL RANDEL', 'E', 'DE LEOLA', 'M', '2001-08-07', 'null', '09165227218'),
(23, NULL, 'MARK JOSEPH', 'A', 'DOLORMENTE', 'M', '2001-08-14', 'null', '09059439927'),
(24, NULL, 'GLENN', 'D', 'DELOS SANTOS', 'M', '2001-09-21', 'null', '09556044918'),
(25, NULL, 'DAN CARLO', 'M', 'EROA', 'M', '2002-12-07', 'null', '09268060946'),
(26, NULL, 'IAN', 'B', 'ESPIRITU', 'M', '2000-08-04', 'null', '09350653305'),
(27, NULL, 'NOLASCO', 'C', 'FELICIANO', 'M', '2001-10-26', 'null', '09350653305'),
(28, NULL, 'CLARO JAMES', 'R', 'FRANCIA', 'M', '2001-08-07', 'null', '09267001135'),
(29, NULL, 'RONILO', 'F', 'FUERTE', 'M', '1999-01-10', 'null', '09057288079'),
(30, NULL, 'JAYSON', 'I', 'GALACGAC', 'M', '2004-03-09', 'null', '09355676764'),
(31, NULL, ' JEFFERSON MCKINLEY', 'C', 'GONZALES ', 'M', '1998-08-07', 'null', '09068372960'),
(32, NULL, 'ARGIE BOY', 'A', 'LIMETA', 'M', '2001-06-29', 'null', ' 09127352861 4'),
(33, NULL, 'MARK GIL', 'B', 'LLANES', 'M', '2002-06-07', 'null', '09355676764'),
(34, NULL, 'HARVEY', 'A', 'LORENO', 'M', '1995-08-02', 'null', ' 09265285993'),
(35, NULL, 'JULIUS', 'T', 'MAGALING', 'M', '1994-04-18', 'null', ' 09955869422'),
(36, NULL, 'BYRON JESTER', 'M', 'MANALO', 'M', '2000-07-07', 'null', '09354315556'),
(37, NULL, 'KIM JOSHUA', 'P', 'MARANAN', 'M', '1998-08-06', 'null', '09360965829'),
(38, NULL, 'JOSE RIZAL', 'O', 'MENDOZA', 'M', '2001-08-07', 'null', '09268514935'),
(39, NULL, 'JEREMY', 'C', 'MOLINA', 'M', '2001-08-07', 'null', '09179216451'),
(40, NULL, 'NIMUEL EIMAN', 'M', 'NEBREJA', 'M', '2001-08-07', 'null', '09364031353'),
(41, NULL, 'JOSEPH', 'C', 'NIMO', 'M', '2001-08-07', 'null', '09269276478'),
(42, NULL, 'JEFFERSON', 'B', 'NUCOM', 'M', '2001-08-07', 'null', '09556487036'),
(43, NULL, 'PHIL KEEVIN', 'A', 'PACIA', 'M', '2001-08-07', 'null', '09351313834'),
(44, NULL, 'LAWRENCE', 'M', 'PALENCIA', 'M', '2001-08-07', 'null', '09364031353'),
(45, NULL, 'ROLANDO JR.', 'G', 'PANGANIBAN', 'M', '2001-08-07', 'null', '09061321795'),
(46, NULL, 'SEDRICK', 'M', 'PANGANIBAN', 'M', '2001-08-07', 'null', '09550552736'),
(47, NULL, 'REDELL ALLEN ', 'A', 'PANTOJA', 'M', '2001-08-07', 'null', '09352021054'),
(48, NULL, 'ALJOHN RIEL', 'U', 'PERCANO', 'M', '2001-08-07', 'null', '09175964040'),
(49, NULL, 'LOUEL', 'Null', 'PLATON', 'M', '2001-08-07', 'null', '09268096959'),
(50, NULL, 'MICHAEL JOHN', 'D', 'RELACIO', 'M', '2001-08-07', 'null', '09176586191'),
(51, NULL, 'ADRIAN', 'Null', 'RESPICIO', 'M', '2001-08-07', 'null', '09753802042'),
(52, NULL, 'JOHN PAUL', 'E', 'REYNALDO', 'M', '2001-08-07', 'null', '09366222756'),
(53, NULL, 'JONATHAN', 'Null', 'ROL', 'M', '2001-08-07', 'null', '09563398592'),
(54, NULL, 'REYJADE', 'A', 'ROL', 'M', '2001-08-07', 'null', '09357155086'),
(55, NULL, 'NEIL RANZ', 'D', 'ROQUE', 'M', '2001-08-07', 'null', '09952686623'),
(56, NULL, 'RALPH WALDO', 'A', 'RUFFY', 'M', '2001-08-07', 'null', '09175360908'),
(57, NULL, 'JOHN RICK', 'M', 'RUIZ', 'M', '2001-08-07', 'null', '09056200658'),
(58, NULL, 'JONAR', 'M', 'RUIZ', 'M', '2001-08-07', 'null', '09755597077'),
(59, NULL, 'PATRICK', 'M', 'SARMIENTO', 'M', '2001-08-07', 'null', '09367825465'),
(60, NULL, 'CEDRICK', 'D', 'SIMUANGCO', 'M', '2001-08-07', 'null', '09169755737'),
(61, NULL, 'CRISTIAN', 'D', 'SINAG', 'M', '2001-08-07', 'null', '09755990854'),
(62, NULL, 'RODELITO', 'Null', 'SORIO', 'M', '2001-08-07', 'null', '09556046596'),
(63, NULL, 'JOSHUA REY', 'V', 'URENA', 'M', '1994-08-16', 'null', '09268238459'),
(64, NULL, 'ALEX JAIRO', 'L', 'VILLANUEVA', 'M', '1993-12-11', 'null', '09568101509'),
(65, NULL, 'ROMMEL', 'B', 'VILLAROJO', 'M', '2001-08-07', 'null', '09758311187'),
(66, NULL, 'CHRISTIAN', 'G', 'VILLAVIRAY', 'M', '1997-07-09', 'null', '09553669860'),
(67, NULL, 'DEXTER', 'M', 'VILLAVIRAY', 'M', '1996-08-22', 'null', '09161873626'),
(68, NULL, 'MARK JULIUS', 'D', 'VITALES', 'M', '1995-03-06', 'null', '09974313978');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `ur_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `info_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`ur_id`, `user_id`, `role_id`, `info_id`) VALUES
(1, 1, 4, 1),
(2, 2, 3, 2),
(3, 3, 2, 3),
(4, 4, 5, 4),
(5, 5, 5, 5),
(6, 6, 5, 6),
(7, 7, 5, 7),
(8, 8, 5, 8),
(9, 9, 5, 9),
(10, 10, 5, 10),
(11, 11, 5, 11),
(12, 12, 5, 12),
(13, 13, 5, 13),
(14, 14, 5, 14),
(15, 15, 5, 15),
(16, 16, 5, 16),
(17, 17, 5, 17),
(18, 18, 5, 18),
(19, 19, 5, 19),
(20, 20, 5, 20),
(21, 21, 5, 21),
(22, 22, 5, 22),
(23, 23, 5, 23),
(24, 24, 5, 24),
(25, 25, 5, 25),
(26, 26, 5, 26),
(27, 27, 5, 27),
(28, 28, 5, 28),
(29, 29, 5, 29),
(30, 30, 5, 30),
(31, 31, 5, 31),
(32, 32, 5, 32),
(33, 33, 5, 33),
(34, 34, 5, 34),
(35, 35, 5, 35),
(36, 36, 5, 36),
(37, 37, 5, 37),
(38, 38, 5, 38),
(39, 39, 5, 39),
(40, 40, 5, 40),
(41, 41, 5, 41),
(42, 42, 5, 42),
(43, 43, 5, 43),
(44, 44, 5, 44),
(45, 45, 5, 45),
(46, 46, 5, 46),
(47, 47, 5, 47),
(48, 48, 5, 48),
(49, 49, 5, 49),
(50, 50, 5, 50),
(51, 51, 5, 51),
(52, 52, 5, 52),
(53, 53, 5, 53),
(54, 54, 5, 54),
(55, 55, 5, 55),
(56, 56, 5, 56),
(57, 57, 5, 57),
(58, 58, 5, 58),
(59, 59, 5, 59),
(60, 60, 5, 60),
(61, 61, 5, 61),
(62, 62, 5, 62),
(63, 63, 5, 63),
(64, 64, 5, 64),
(65, 65, 5, 65),
(66, 66, 5, 66),
(67, 67, 5, 67),
(68, 68, 5, 68),
(69, 69, 6, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`) USING BTREE,
  ADD UNIQUE KEY `roles_role_code_unique` (`role_code`) USING BTREE;

--
-- Indexes for table `r_branch_acdmcyr`
--
ALTER TABLE `r_branch_acdmcyr`
  ADD PRIMARY KEY (`ay_id`) USING BTREE,
  ADD UNIQUE KEY `r_branch_acdmcyr_ay_code_unique` (`ay_code`) USING BTREE,
  ADD KEY `FK_bracdyr_branch_id_rfrnc` (`branch_id`) USING BTREE;

--
-- Indexes for table `r_branch_active_ay_sem`
--
ALTER TABLE `r_branch_active_ay_sem`
  ADD PRIMARY KEY (`aysem_id`) USING BTREE,
  ADD KEY `FK_braysem_branch_id_rfrnc` (`branch_id`) USING BTREE,
  ADD KEY `FK_braysem_ay_id_rfrnc` (`ay_id`) USING BTREE,
  ADD KEY `FK_braysem_sem_id_rfrnc` (`sem_id`) USING BTREE;

--
-- Indexes for table `r_branch_courses`
--
ALTER TABLE `r_branch_courses`
  ADD PRIMARY KEY (`bc_id`) USING BTREE,
  ADD KEY `FK_brnchcrs_branch_id_rfrnc` (`branch_id`) USING BTREE,
  ADD KEY `FK_brnchcrs_uu_id_rfrnc` (`uu_id`) USING BTREE,
  ADD KEY `FK_brnchcrs_course_id_rfrnc` (`course_id`) USING BTREE;

--
-- Indexes for table `r_branch_sem`
--
ALTER TABLE `r_branch_sem`
  ADD PRIMARY KEY (`sem_id`) USING BTREE,
  ADD UNIQUE KEY `r_branch_sem_sem_code_unique` (`sem_code`) USING BTREE,
  ADD KEY `FK_brsem_branch_id_rfrnc` (`branch_id`) USING BTREE;

--
-- Indexes for table `r_grade_percentage`
--
ALTER TABLE `r_grade_percentage`
  ADD PRIMARY KEY (`gp_id`) USING BTREE,
  ADD KEY `FK_gp_uu_id_rfrnc` (`uu_id`) USING BTREE;

--
-- Indexes for table `r_hte`
--
ALTER TABLE `r_hte`
  ADD PRIMARY KEY (`hte_id`) USING BTREE,
  ADD UNIQUE KEY `r_hte_hte_code_unique` (`hte_code`) USING BTREE,
  ADD KEY `FK_hte_region_id_rfrnc` (`region_id`) USING BTREE;

--
-- Indexes for table `r_hte_contact`
--
ALTER TABLE `r_hte_contact`
  ADD KEY `FK_htecontact_hte_id_rfrnc` (`hte_id`) USING BTREE;

--
-- Indexes for table `r_hte_job_trainings`
--
ALTER TABLE `r_hte_job_trainings`
  ADD PRIMARY KEY (`job_training_id`) USING BTREE,
  ADD KEY `hteojt_hte_id_rfrnc` (`hte_id`) USING BTREE;

--
-- Indexes for table `r_hte_users`
--
ALTER TABLE `r_hte_users`
  ADD PRIMARY KEY (`hu_id`) USING BTREE,
  ADD KEY `FK_hteusrs_hte_id_rfrnc` (`hte_id`) USING BTREE,
  ADD KEY `FK_hteusrs_ur_id_rfrnc` (`ur_id`) USING BTREE;

--
-- Indexes for table `r_intern_application_status`
--
ALTER TABLE `r_intern_application_status`
  ADD PRIMARY KEY (`iappstat_id`) USING BTREE,
  ADD UNIQUE KEY `r_intern_application_status_iappstat_code_unique` (`iappstat_code`) USING BTREE;

--
-- Indexes for table `r_intern_referral`
--
ALTER TABLE `r_intern_referral`
  ADD UNIQUE KEY `r_intern_referral_referral_code_unique` (`referral_code`) USING BTREE,
  ADD KEY `FK_intrnrfrrl_intern_id_rfrnc` (`intern_id`) USING BTREE;

--
-- Indexes for table `r_intern_skills`
--
ALTER TABLE `r_intern_skills`
  ADD PRIMARY KEY (`skill_id`) USING BTREE;

--
-- Indexes for table `r_intern_skill_sets`
--
ALTER TABLE `r_intern_skill_sets`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `FK_intrnskllsst` (`intern_id`) USING BTREE,
  ADD KEY `FK_skllskllsst` (`skill_id`) USING BTREE;

--
-- Indexes for table `r_regions`
--
ALTER TABLE `r_regions`
  ADD PRIMARY KEY (`region_id`) USING BTREE,
  ADD UNIQUE KEY `r_regions_code_unique` (`code`) USING BTREE;

--
-- Indexes for table `r_seminar_trainings_attended`
--
ALTER TABLE `r_seminar_trainings_attended`
  ADD KEY `FK_sta_uu_id_rfrnc` (`uu_id`) USING BTREE;

--
-- Indexes for table `r_su`
--
ALTER TABLE `r_su`
  ADD PRIMARY KEY (`su_id`) USING BTREE,
  ADD UNIQUE KEY `r_su_su_code_unique` (`su_code`) USING BTREE;

--
-- Indexes for table `r_su_academic_rank`
--
ALTER TABLE `r_su_academic_rank`
  ADD PRIMARY KEY (`rank_id`) USING BTREE;

--
-- Indexes for table `r_su_branches`
--
ALTER TABLE `r_su_branches`
  ADD PRIMARY KEY (`branch_id`) USING BTREE,
  ADD UNIQUE KEY `r_su_branches_branch_code_unique` (`branch_code`) USING BTREE,
  ADD KEY `FK_subrnch_su_id_rfrnc` (`su_id`) USING BTREE;

--
-- Indexes for table `r_su_colleges`
--
ALTER TABLE `r_su_colleges`
  ADD PRIMARY KEY (`college_id`) USING BTREE,
  ADD UNIQUE KEY `r_su_colleges_college_code_unique` (`college_code`) USING BTREE,
  ADD KEY `FK_sucllgs_su_id_rfrnc` (`su_id`) USING BTREE;

--
-- Indexes for table `r_su_college_courses`
--
ALTER TABLE `r_su_college_courses`
  ADD PRIMARY KEY (`course_id`) USING BTREE,
  ADD UNIQUE KEY `r_su_college_courses_course_code_unique` (`course_code`) USING BTREE,
  ADD KEY `FK_sucrs_college_id_rfrnc` (`college_id`) USING BTREE;

--
-- Indexes for table `r_su_intern`
--
ALTER TABLE `r_su_intern`
  ADD PRIMARY KEY (`intern_id`) USING BTREE,
  ADD UNIQUE KEY `r_su_intern_intern_code_unique` (`intern_code`) USING BTREE,
  ADD KEY `FK_intrn_branch_id_rfrnc` (`branch_id`) USING BTREE,
  ADD KEY `FK_intrn_ay_id_rfrnc` (`ay_id`) USING BTREE,
  ADD KEY `FK_intrn_course_id_rfrnc` (`bc_id`) USING BTREE,
  ADD KEY `FK_intrn_info_id_rfrnc` (`info_id`) USING BTREE,
  ADD KEY `FK_intrn_region_id_rfrnc` (`region_id`) USING BTREE;

--
-- Indexes for table `r_su_users`
--
ALTER TABLE `r_su_users`
  ADD PRIMARY KEY (`uu_id`) USING BTREE,
  ADD KEY `FK_susr_branch_id_rfrnc` (`branch_id`) USING BTREE,
  ADD KEY `FK_susr_ur_id_rfrnc` (`ur_id`) USING BTREE;

--
-- Indexes for table `r_su_usr_educbg`
--
ALTER TABLE `r_su_usr_educbg`
  ADD KEY `FK_susredcbg_uu_id_rfrnc` (`uu_id`) USING BTREE;

--
-- Indexes for table `t_hte_accepted_intern`
--
ALTER TABLE `t_hte_accepted_intern`
  ADD PRIMARY KEY (`ai_id`) USING BTREE,
  ADD KEY `FK_hteaccptdintrn_hte_id_rfrnc` (`hte_id`) USING BTREE,
  ADD KEY `FK_hteaccptdintrn_intern_id_rfrnc` (`intern_id`) USING BTREE,
  ADD KEY `FK_hteaccptdintrn_job_training_id_rfrnc` (`job_training_id`) USING BTREE;

--
-- Indexes for table `t_hte_grade`
--
ALTER TABLE `t_hte_grade`
  ADD KEY `t_hte_grade_grading_id_foreign` (`grading_id`) USING BTREE,
  ADD KEY `FK_htegrd_hte_id_rfrnc` (`hte_id`) USING BTREE,
  ADD KEY `FK_htegrd_hu_id_rfrnc` (`hu_id`) USING BTREE;

--
-- Indexes for table `t_intern_application`
--
ALTER TABLE `t_intern_application`
  ADD PRIMARY KEY (`iapp_id`) USING BTREE,
  ADD KEY `FK_iapp-intern_id_rfrnc` (`intern_id`) USING BTREE,
  ADD KEY `t_intern_application_job_training_id_foreign` (`job_training_id`) USING BTREE,
  ADD KEY `FK_iapp_iappstat_id_rfrnc` (`iappstat_id`) USING BTREE;

--
-- Indexes for table `t_intern_grading`
--
ALTER TABLE `t_intern_grading`
  ADD PRIMARY KEY (`grading_id`) USING BTREE,
  ADD UNIQUE KEY `t_intern_grading_grading_code_unique` (`grading_code`) USING BTREE,
  ADD KEY `FK_intrngrdng_intern_id_rfrnc` (`intern_id`) USING BTREE;

--
-- Indexes for table `t_su_hte`
--
ALTER TABLE `t_su_hte`
  ADD PRIMARY KEY (`sh_id`) USING BTREE,
  ADD KEY `suh_branch_id_rfrnc` (`branch_id`) USING BTREE,
  ADD KEY `FK_suhte_college_id_rfrnc` (`college_id`) USING BTREE,
  ADD KEY `suh_hte_id_rfrnc` (`hte_id`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `users_usr_code_unique` (`usr_code`) USING BTREE,
  ADD UNIQUE KEY `users_email_unique` (`email`) USING BTREE;

--
-- Indexes for table `user_infos`
--
ALTER TABLE `user_infos`
  ADD PRIMARY KEY (`info_id`) USING BTREE;

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`ur_id`) USING BTREE,
  ADD KEY `user_roles_user_id_foreign` (`user_id`) USING BTREE,
  ADD KEY `user_roles_role_id_foreign` (`role_id`) USING BTREE,
  ADD KEY `user_roles_info_id_foreign` (`info_id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `r_branch_acdmcyr`
--
ALTER TABLE `r_branch_acdmcyr`
  MODIFY `ay_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `r_branch_active_ay_sem`
--
ALTER TABLE `r_branch_active_ay_sem`
  MODIFY `aysem_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `r_branch_courses`
--
ALTER TABLE `r_branch_courses`
  MODIFY `bc_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `r_branch_sem`
--
ALTER TABLE `r_branch_sem`
  MODIFY `sem_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `r_grade_percentage`
--
ALTER TABLE `r_grade_percentage`
  MODIFY `gp_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `r_hte`
--
ALTER TABLE `r_hte`
  MODIFY `hte_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `r_hte_job_trainings`
--
ALTER TABLE `r_hte_job_trainings`
  MODIFY `job_training_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `r_hte_users`
--
ALTER TABLE `r_hte_users`
  MODIFY `hu_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `r_intern_application_status`
--
ALTER TABLE `r_intern_application_status`
  MODIFY `iappstat_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `r_intern_skills`
--
ALTER TABLE `r_intern_skills`
  MODIFY `skill_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `r_intern_skill_sets`
--
ALTER TABLE `r_intern_skill_sets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `r_regions`
--
ALTER TABLE `r_regions`
  MODIFY `region_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `r_su`
--
ALTER TABLE `r_su`
  MODIFY `su_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `r_su_academic_rank`
--
ALTER TABLE `r_su_academic_rank`
  MODIFY `rank_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `r_su_branches`
--
ALTER TABLE `r_su_branches`
  MODIFY `branch_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `r_su_colleges`
--
ALTER TABLE `r_su_colleges`
  MODIFY `college_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `r_su_college_courses`
--
ALTER TABLE `r_su_college_courses`
  MODIFY `course_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `r_su_intern`
--
ALTER TABLE `r_su_intern`
  MODIFY `intern_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `r_su_users`
--
ALTER TABLE `r_su_users`
  MODIFY `uu_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `t_hte_accepted_intern`
--
ALTER TABLE `t_hte_accepted_intern`
  MODIFY `ai_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `t_intern_application`
--
ALTER TABLE `t_intern_application`
  MODIFY `iapp_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `t_intern_grading`
--
ALTER TABLE `t_intern_grading`
  MODIFY `grading_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `t_su_hte`
--
ALTER TABLE `t_su_hte`
  MODIFY `sh_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `user_infos`
--
ALTER TABLE `user_infos`
  MODIFY `info_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `ur_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `r_branch_acdmcyr`
--
ALTER TABLE `r_branch_acdmcyr`
  ADD CONSTRAINT `FK_bracdyr_branch_id_rfrnc` FOREIGN KEY (`branch_id`) REFERENCES `r_su_branches` (`branch_id`) ON UPDATE CASCADE;

--
-- Constraints for table `r_branch_active_ay_sem`
--
ALTER TABLE `r_branch_active_ay_sem`
  ADD CONSTRAINT `FK_braysem_ay_id_rfrnc` FOREIGN KEY (`ay_id`) REFERENCES `r_branch_acdmcyr` (`ay_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_braysem_branch_id_rfrnc` FOREIGN KEY (`branch_id`) REFERENCES `r_su_branches` (`branch_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_braysem_sem_id_rfrnc` FOREIGN KEY (`sem_id`) REFERENCES `r_branch_sem` (`sem_id`) ON UPDATE CASCADE;

--
-- Constraints for table `r_branch_courses`
--
ALTER TABLE `r_branch_courses`
  ADD CONSTRAINT `FK_brnchcrs_branch_id_rfrnc` FOREIGN KEY (`branch_id`) REFERENCES `r_su_branches` (`branch_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_brnchcrs_course_id_rfrnc` FOREIGN KEY (`course_id`) REFERENCES `r_su_college_courses` (`course_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_brnchcrs_uu_id_rfrnc` FOREIGN KEY (`uu_id`) REFERENCES `r_su_users` (`uu_id`) ON UPDATE CASCADE;

--
-- Constraints for table `r_branch_sem`
--
ALTER TABLE `r_branch_sem`
  ADD CONSTRAINT `FK_brsem_branch_id_rfrnc` FOREIGN KEY (`branch_id`) REFERENCES `r_su_branches` (`branch_id`) ON UPDATE CASCADE;

--
-- Constraints for table `r_grade_percentage`
--
ALTER TABLE `r_grade_percentage`
  ADD CONSTRAINT `FK_gp_uu_id_rfrnc` FOREIGN KEY (`uu_id`) REFERENCES `r_su_users` (`uu_id`) ON UPDATE CASCADE;

--
-- Constraints for table `r_hte`
--
ALTER TABLE `r_hte`
  ADD CONSTRAINT `FK_hte_region_id_rfrnc` FOREIGN KEY (`region_id`) REFERENCES `r_regions` (`region_id`) ON UPDATE CASCADE;

--
-- Constraints for table `r_hte_contact`
--
ALTER TABLE `r_hte_contact`
  ADD CONSTRAINT `FK_htecontact_hte_id_rfrnc` FOREIGN KEY (`hte_id`) REFERENCES `r_hte` (`hte_id`) ON UPDATE CASCADE;

--
-- Constraints for table `r_hte_job_trainings`
--
ALTER TABLE `r_hte_job_trainings`
  ADD CONSTRAINT `hteojt_hte_id_rfrnc` FOREIGN KEY (`hte_id`) REFERENCES `r_hte` (`hte_id`) ON UPDATE CASCADE;

--
-- Constraints for table `r_hte_users`
--
ALTER TABLE `r_hte_users`
  ADD CONSTRAINT `FK_hteusrs_hte_id_rfrnc` FOREIGN KEY (`hte_id`) REFERENCES `r_hte` (`hte_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_hteusrs_ur_id_rfrnc` FOREIGN KEY (`ur_id`) REFERENCES `user_roles` (`ur_id`) ON UPDATE CASCADE;

--
-- Constraints for table `r_intern_referral`
--
ALTER TABLE `r_intern_referral`
  ADD CONSTRAINT `FK_intrnrfrrl_intern_id_rfrnc` FOREIGN KEY (`intern_id`) REFERENCES `r_su_intern` (`intern_id`) ON UPDATE CASCADE;

--
-- Constraints for table `r_intern_skill_sets`
--
ALTER TABLE `r_intern_skill_sets`
  ADD CONSTRAINT `FK_intrnskllsst` FOREIGN KEY (`intern_id`) REFERENCES `r_su_intern` (`intern_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_skllskllsst` FOREIGN KEY (`skill_id`) REFERENCES `r_intern_skills` (`skill_id`) ON UPDATE CASCADE;

--
-- Constraints for table `r_seminar_trainings_attended`
--
ALTER TABLE `r_seminar_trainings_attended`
  ADD CONSTRAINT `FK_sta_uu_id_rfrnc` FOREIGN KEY (`uu_id`) REFERENCES `r_su_users` (`uu_id`) ON UPDATE CASCADE;

--
-- Constraints for table `r_su_branches`
--
ALTER TABLE `r_su_branches`
  ADD CONSTRAINT `FK_subrnch_su_id_rfrnc` FOREIGN KEY (`su_id`) REFERENCES `r_su` (`su_id`) ON UPDATE CASCADE;

--
-- Constraints for table `r_su_colleges`
--
ALTER TABLE `r_su_colleges`
  ADD CONSTRAINT `FK_sucllgs_su_id_rfrnc` FOREIGN KEY (`su_id`) REFERENCES `r_su` (`su_id`) ON UPDATE CASCADE;

--
-- Constraints for table `r_su_college_courses`
--
ALTER TABLE `r_su_college_courses`
  ADD CONSTRAINT `FK_sucrs_college_id_rfrnc` FOREIGN KEY (`college_id`) REFERENCES `r_su_colleges` (`college_id`) ON UPDATE CASCADE;

--
-- Constraints for table `r_su_intern`
--
ALTER TABLE `r_su_intern`
  ADD CONSTRAINT `FK_intrn_ay_id_rfrnc` FOREIGN KEY (`ay_id`) REFERENCES `r_branch_acdmcyr` (`ay_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_intrn_branch_id_rfrnc` FOREIGN KEY (`branch_id`) REFERENCES `r_su_branches` (`branch_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_intrn_course_id_rfrnc` FOREIGN KEY (`bc_id`) REFERENCES `r_su_college_courses` (`course_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_intrn_info_id_rfrnc` FOREIGN KEY (`info_id`) REFERENCES `user_infos` (`info_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_intrn_region_id_rfrnc` FOREIGN KEY (`region_id`) REFERENCES `r_regions` (`region_id`) ON UPDATE CASCADE;

--
-- Constraints for table `r_su_users`
--
ALTER TABLE `r_su_users`
  ADD CONSTRAINT `FK_susr_branch_id_rfrnc` FOREIGN KEY (`branch_id`) REFERENCES `r_su_branches` (`branch_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_susr_ur_id_rfrnc` FOREIGN KEY (`ur_id`) REFERENCES `user_roles` (`ur_id`) ON UPDATE CASCADE;

--
-- Constraints for table `r_su_usr_educbg`
--
ALTER TABLE `r_su_usr_educbg`
  ADD CONSTRAINT `FK_susredcbg_uu_id_rfrnc` FOREIGN KEY (`uu_id`) REFERENCES `r_su_users` (`uu_id`) ON UPDATE CASCADE;

--
-- Constraints for table `t_hte_accepted_intern`
--
ALTER TABLE `t_hte_accepted_intern`
  ADD CONSTRAINT `FK_hteaccptdintrn_hte_id_rfrnc` FOREIGN KEY (`hte_id`) REFERENCES `r_hte` (`hte_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_hteaccptdintrn_intern_id_rfrnc` FOREIGN KEY (`intern_id`) REFERENCES `r_su_intern` (`intern_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_hteaccptdintrn_job_training_id_rfrnc` FOREIGN KEY (`job_training_id`) REFERENCES `r_hte_job_trainings` (`job_training_id`) ON UPDATE CASCADE;

--
-- Constraints for table `t_hte_grade`
--
ALTER TABLE `t_hte_grade`
  ADD CONSTRAINT `FK_htegrd_hte_id_rfrnc` FOREIGN KEY (`hte_id`) REFERENCES `r_hte` (`hte_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_htegrd_hu_id_rfrnc` FOREIGN KEY (`hu_id`) REFERENCES `r_hte_users` (`hu_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `t_hte_grade_grading_id_foreign` FOREIGN KEY (`grading_id`) REFERENCES `t_intern_grading` (`grading_id`) ON UPDATE CASCADE;

--
-- Constraints for table `t_intern_application`
--
ALTER TABLE `t_intern_application`
  ADD CONSTRAINT `FK_iapp-intern_id_rfrnc` FOREIGN KEY (`intern_id`) REFERENCES `r_su_intern` (`intern_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_iapp_iappstat_id_rfrnc` FOREIGN KEY (`iappstat_id`) REFERENCES `r_intern_application_status` (`iappstat_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `t_intern_application_job_training_id_foreign` FOREIGN KEY (`job_training_id`) REFERENCES `r_hte_job_trainings` (`job_training_id`) ON UPDATE CASCADE;

--
-- Constraints for table `t_intern_grading`
--
ALTER TABLE `t_intern_grading`
  ADD CONSTRAINT `FK_intrngrdng_intern_id_rfrnc` FOREIGN KEY (`intern_id`) REFERENCES `r_su_intern` (`intern_id`) ON UPDATE CASCADE;

--
-- Constraints for table `t_su_hte`
--
ALTER TABLE `t_su_hte`
  ADD CONSTRAINT `FK_suhte_college_id_rfrnc` FOREIGN KEY (`college_id`) REFERENCES `r_su_colleges` (`college_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `suh_branch_id_rfrnc` FOREIGN KEY (`branch_id`) REFERENCES `r_su_branches` (`branch_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `suh_hte_id_rfrnc` FOREIGN KEY (`hte_id`) REFERENCES `r_hte` (`hte_id`) ON UPDATE CASCADE;

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_info_id_foreign` FOREIGN KEY (`info_id`) REFERENCES `user_infos` (`info_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
